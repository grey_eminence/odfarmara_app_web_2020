const fastifyEnv = require('fastify-env')
const app = require('fastify')({
  logger: true
})
const path = require('path')
const fs = require('fs')

const { Op } = require("sequelize");
const sharp = require("sharp");

// db 
const fsequelize = require('./src/plugins/cockroachdb')
const sequelizeConfig = {
  instance: 'db', // name of instance will be mapped to fastify
  // autoConnect: true, // auto authentication and test connection on first run
  dialect: 'postgres',
  host: 'localhost',
  port: '26257',
  username: 'root',
  password: '',
  database: 'odfarmara_imported',
  timezone: 'Europe/Bratislava',
  dialectOptions: {
    timezone: "local",
    // ssl: {
    //   rejectUnauthorized: false,
    //   // For secure connection:
    //   /*ca: fs.readFileSync('certs/ca.crt').toString()*/
    // },
  },
  // timout for long running queries
  pool: {
    max: 10,
    min: 0,
    idle: 100000
  },
  logging: false,
}
app.register(fsequelize, sequelizeConfig).ready()

app.register(require('./src/models/farmer_image'))
app.register(require('./src/models/farmer_rating'))
app.register(require('./src/models/user'))

async function resizeImage(file) {
  // try {
  //   await sharp('public/assets_web/upload/' + file)
  //     .resize({
  //       height: 300,
  //     })
  //     .toFile('public/assets_web/thumb_small/' + file);
  // } catch (error) {
  //   console.log(error);
  // }

  try {
    await sharp('public/assets_web/upload/' + file)
      .resize({
        height: 600,
      })
      .toFile('public/assets_web/thumb_medium/' + file);
  } catch (error) {
    console.log(error);
  }
}


async function start() {
    let farmer_images = await app.db.models.farmer_image.findAll();

    for(x in farmer_images){

        let user_id = farmer_images[x].user_id;
        let filename = farmer_images[x].company_logo.replace(/^.*[\\\/]/, '');

        await resizeImage(filename);

        console.log(filename);
        console.log(user_id);

        // let user = await app.db.models.user.update({ profile_img_filename: filename }, {
        //     where: {
        //       id: user_id
        //     }
        // });

    }
}

const schema = {
    type: 'object',
    required: [ 'PORT' ],
    properties: {
        PORT: {
            type: 'string',
            default: 3000
        },
        ROOT: {
            type: 'string',
            default: '/'
        },
        ASSETS: {
            type: 'string',
            default: '/public'
        }
    }
}


const options = {
    confKey: 'config',
    schema: schema,
    dotenv: true
}

app.register(fastifyEnv, options)
.ready((err) => {
    start();
})



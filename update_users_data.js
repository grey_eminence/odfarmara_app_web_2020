const fastifyEnv = require('fastify-env')
const app = require('fastify')({
  logger: true
})
const path = require('path')
const fs = require('fs')
const { v4: uuidv4 } = require('uuid')
const slugify = require('slugify')
const { Op, QueryTypes, Sequelize } = require("sequelize");
const mysql = require('mysql2');
const argon2 = require('argon2')
const v = require('voca')

// db 
const fsequelize = require('./src/plugins/cockroachdb')
const sequelizeConfig = {
  instance: 'db', // name of instance will be mapped to fastify
  // autoConnect: true, // auto authentication and test connection on first run
  dialect: 'postgres',
  host: 'localhost',
  port: '26257',
  username: 'root',
  password: '',
  database: 'odfarmara_imported',
  timezone: 'Europe/Bratislava',
  dialectOptions: {
    timezone: "local",
    // ssl: {
    //   rejectUnauthorized: false,
    //   // For secure connection:
    //   /*ca: fs.readFileSync('certs/ca.crt').toString()*/
    // },
  },
  // timout for long running queries
  pool: {
    max: 10,
    min: 0,
    idle: 100000
  },
  logging: false,
}

app.register(fsequelize, sequelizeConfig).ready()

app.register(require('./src/models/farmer_rating'))
app.register(require('./src/models/region'))
app.register(require('./src/models/favorite'))
app.register(require('./src/models/category'))
app.register(require('./src/models/user'))
app.register(require('./src/models/image'))
app.register(require('./src/models/tag'))
app.register(require('./src/models/product'))
app.register(require('./src/models/product_view'))
app.register(require('./src/models/article'))
app.register(require('./src/models/farmer_image'))
app.register(require('./src/models/newsletter'))
app.register(require('./src/models/opening_hours'))
app.register(require('./src/models/demand'))
app.register(require('./src/models/inbox'))
app.register(require('./src/models/recipe'))
app.register(require('./src/models/allergen'))
app.register(require('./src/models/nutrition_value'))
app.register(require('./src/models/cities'))
app.register(require('./src/models/seasonal_offer'))
app.register(require('./src/models/flags'))

async function start() {

  //const odfarmara_old = new Sequelize('mysql://root:@localhost:3306/odfarmara_old')
  //const [results, metadata] = await Sequelize.query("SELECT * FROM users");

  // create the connection to database
  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    database: 'odfarmara_old'
  });

  connection.query(
    'SELECT * FROM users',
    async function (err, results, fields) {
      //console.log(err); // results contains rows returned by server
      //console.log(results); // results contains rows returned by server

      for (var x = 0; x < results.length; x++) {

        let item = results[x];

        let id = item.id;
        let name = item.name;
        let username = item.username;
        let email = item.email;
        let company_name = item.company_name;
        let active = item.active;
        let created = item.created;
        let modified = item.modified;
        let description = item.description;
        let short_description = item.short_description;
        let long_description = item.long_description;
        if (active) {
          active = "1";
        }
        else {
          active = "0";
        }
        let gps = item.GPS;
        let region_id = item.region_id;

        const password_hash = await argon2.hash('mtA4iuLExlSQqRWFdLHfQXLqSXlgnR', { type: argon2.argon2id });

        let privilegs = '';
        if (item.user_group_id == 2) {
          privilegs = 'customer';
        }
        if (item.user_group_id == 3) {
          privilegs = 'farmer';
        }

        let street = item.street;
        let houseno = item.houseno;
        let zip = item.code;
        let phone = item.phone;
        let city = item.city;

        let names = "";
        let initials = "";
        if (name.length > 0) {
          names = v(name).latinise().upperCase().words()
        }
        else {
          if (company_name.length > 0) {
            names = v(company_name).latinise().upperCase().words()
          }
        }

        if (names.length > 0) {
          for (let x = 0; x < names.length; x++) {
            if (x > 1) {
              break;
            }
            let char = names[x].charAt(0);
            initials += char;
          }
        }

        if (initials.length == 0) {
          initials = v(email).latinise().upperCase().charAt(0).value();
        }

        // todo opening hours

        let data = {
          id: id,
          name: name,
          username: username,
          company_name: company_name,
          street: street,
          houseno: houseno,
          zip: zip,
          code: zip,
          phone: phone,
          city: city,
          region_id: region_id,
          password_new: password_hash,
          email: email,
          contact_email: email,
          active: active,
          privilegs: privilegs,
          gps: gps,
          initials: initials,
          created: created,
          modiefied: modified,
          description: description,
          long_description: long_description,
          short_description: short_description
        }

        let newUser = await app.db.models.user.create(data);

        connection.query(
          'SELECT * FROM farmar_categories WHERE farmar_id = ' + id,
          async function (err, results, fields) {
  
            if(results && results.length){
              let categories = [];
              for (var x = 0; x < results.length; x++) {
                let item = results[x];
                let farmar_id = item.farmar_id;
                let category_id = item.category_id;
                categories.push(category_id);
              }

              data = {
                categories: categories
              }

              let updated = await app.db.models.user.update(data, {
                where: {
                  id: id
                }
              });
            }
          }
        );

        connection.query(
          'SELECT * FROM upload_company_logo WHERE user_id = ' + id,
          async function (err, results, fields) {
  
            for (var x = 0; x < results.length; x++) {
  
              let item = results[x];
              let id = item.user_id;
              let company_logo = item.company_logo;
              let filename = company_logo.replace(/^.*[\\\/]/, '');
              let profile_image = filename;
  
              let data = {
                profile_image: profile_image,
              }
  
              let updated = await app.db.models.user.update(data, {
                where: {
                  id: id
                }
              });
  
            }
          }
        );

        console.log(id);

      }

    }

  );



  //console.log('results', results);

  // let items = await app.db.models.product.findAll();

  // for(x in items){

  //     let id = items[x].old_id;
  //     let uuid = uuidv4();
  //     let title = items[x].title;
  //     let slug = slugify(title, {
  //       lower: true,      // convert to lower case, defaults to `false`
  //       locale: 'sk'       // language code of the locale to use
  //     });

  //     let user = await app.db.models.product.update({ 
  //       uuid: uuid,
  //       slug: slug,
  //     }, {
  //         where: {
  //           old_id: id
  //         }
  //     });

  // }
}

app.ready((err) => {
  start();
})



module.exports = {
  apps : [
  {
    name   : "cockroach_db",
    script : "cockroach start --insecure --store=node1 --listen-addr=localhost:26257 --http-addr=localhost:8080 --join=localhost:26257",
  },
  {
    name   : "odfarmara_dev",
    script : "./server.js",
	instances : "max",
    exec_mode : "cluster"
  }
  ]
}

const fastifyEnv = require('fastify-env')
const app = require('fastify')({
  logger: true
})
const path = require('path')
const fs = require('fs')
const { v4: uuidv4 } = require('uuid')
const slugify = require('slugify')
const { Op, QueryTypes, Sequelize } = require("sequelize");
const mysql = require('mysql2');
const argon2 = require('argon2')
const v = require('voca')

// db 
const fsequelize = require('./src/plugins/cockroachdb')
const sequelizeConfig = {
  instance: 'db', // name of instance will be mapped to fastify
  // autoConnect: true, // auto authentication and test connection on first run
  dialect: 'postgres',
  host: 'localhost',
  port: '26257',
  username: 'root',
  password: '',
  database: 'odfarmara_imported',
  timezone: 'Europe/Bratislava',
  dialectOptions: {
    timezone: "local",
    // ssl: {
    //   rejectUnauthorized: false,
    //   // For secure connection:
    //   /*ca: fs.readFileSync('certs/ca.crt').toString()*/
    // },
  },
  // timout for long running queries
  pool: {
    max: 10,
    min: 0,
    idle: 100000
  },
  logging: false,
}

app.register(fsequelize, sequelizeConfig).ready()

app.register(require('./src/models/farmer_rating'))
app.register(require('./src/models/region'))
app.register(require('./src/models/favorite'))
app.register(require('./src/models/category'))
app.register(require('./src/models/user'))

// // app.register(require('./src/models/tag'))
// app.register(require('./src/models/image'))
// app.register(require('./src/models/product'))
// app.register(require('./src/models/article'))
// app.register(require('./src/models/farmer_image'))

// // app.register(require('./src/models/newsletter'))
// // app.register(require('./src/models/page'))
// app.register(require('./src/models/opening_hours'))

async function start() {

        // let name = 'Marián Michalovič';
        // let username = 'marian.michalovic@greyeminence.sk';
        // let email = 'marian.michalovic@greyeminence.sk';
        // let active = "1";
        // let admin = "1";
        // let created = new Date();

        // const password_hash = await argon2.hash('AZcIZJdFdcZjOsvG46jI_', { type: argon2.argon2id });

        // let data = {
        //   name: name,
        //   username: username,
        //   password_new: password_hash,
        //   email: email,
        //   contact_email: email,
        //   active: active,
        //   privilegs: 'customer',
        //   initials: 'MM',
        //   created: created,
        //   admin: admin
        // }

        // let newUser = await app.db.models.user.update(data, {
        //   where: {
        //     id: 3903
        //   }
        // });


        const email = 'info@odfarmara.sk';
        const password = "HQvhsYN4wDycziRmULvFuwEzfdsGuFMOZaQ4d_";

        const hash = await argon2.hash(password, {type: argon2.argon2id});
        const uuid = uuidv4();

        const name = "Radoslav Sládeček";

        let initials = "RS";

        let data = {
          username: email,
          email: email,
          active: "1",
          password_new: hash,
          uuid: uuid,
          privilegs: 'customer',
          initials: initials
        }

        // try {
        //   let newUser = await app.db.models.user.create(data);
        // }
        // catch (e) {
        //   console.log(e);
        // }

        //app.db.query("SELECT setval('users_auto_inc', 8276, true)");
        // app.db.query("SELECT setval('offers_auto_inc', 835, true)");
        // app.db.query("SELECT setval('offer_uploaded_images_auto_inc', 1450, true)");
};

app.ready((err) => {
  start();
})

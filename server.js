// Require the framework and instantiate it
const URI = require('urijs')
const fastifyEnv = require('fastify-env')
const app = require('fastify')({
  // logger: true,
  logger: false,
  maxParamLength: 256
})
const path = require('path')
const fs = require('fs')
const argon2 = require('argon2')
const dayjs = require('dayjs')
const moment = require('moment')
const { v4: uuidv4 } = require('uuid')
const v = require('voca')
const passwordGenerator = require('generate-password')
const solr = require('solr-client')

app.register(require('fastify-multipart'),{
  limits: {
    // fieldNameSize: 100, // Max field name size in bytes
    // fieldSize: 100,     // Max field value size in bytes
    // fields: 10,         // Max number of non-file fields
    fileSize: 5242880,  // For multipart forms, the max file size in bytes
    // fileSize: 1000,  // For multipart forms, the max file size in bytes
    //files: 8,           // Max number of file fields
    // headerPairs: 2000   // Max number of header key=>value pairs
  }
})

// const fileUpload = require('fastify-file-upload')
// app.register(fileUpload,{
//   limits: { fileSize: 50 * 1024 * 1024 },
//   preserveExtension: true,
//   useTempFiles: true,
//   // createParentPath: true
// })

// const Ajv = require('ajv')
// const ajv = new Ajv({
//   removeAdditional: true,
//   useDefaults: true,
//   coerceTypes: true,
//   allErrors: true,
//   nullable: true,
//   $data: true
// })

// app.setValidatorCompiler(({ schema, method, url, httpPart }) => {
//   return ajv.compile(schema)
// })

// const validation = require("./src/validation")
// // ajv validation response
// app.setErrorHandler(function (error, request, reply) {
//   let errors = validation.validationErrorsFormat(error)
//   reply.status(400).send(errors)
// })

app.register(require('fastify-static'), {
  root: path.join(__dirname, 'public'),
  prefix: '/assets/', // optional: default '/'
})

// admin static
app.register(require('fastify-static'), {
  root: path.join(__dirname, 'public/admin'),
  prefix: '/administracia/',
  decorateReply: false // the reply decorator has been added by the first plugin registration
})

const fastifyPassport = require('fastify-passport');
const fastifySecureSession = require('fastify-secure-session');
const passportCustom = require('passport-custom');
const CustomStrategy = passportCustom.Strategy;
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const FacebookStrategy = require('passport-facebook');
const { Op } = require("sequelize");

app.register(require('fastify-cors'), { 
  origin: ['http://localhost:8081', 'http://localhost:4000'],
  // origin: true,
  credentials: true
})

const schema = {
  type: 'object',
  required: [ 'PORT', 'HOST' ],
  properties: {
    HOST: {
      type: 'string',
      default: '127.0.0.1'
    },
    PORT: {
      type: 'string',
      default: 3000
    },
    ROOT: {
      type: 'string',
      default: '/'
    },
    ASSETS: {
      type: 'string',
      default: '/public'
    },
    GOOGLE_CLIENT_ID: {
      type: 'string',
      default: ''
    },
    GOOGLE_CLIENT_SECRET: {
      type: 'string',
      default: ''
    }
  }
}

const options = {
  confKey: 'config', // optional, default: 'config'
  schema: schema,
  dotenv: true
}

app.register(fastifyEnv, options)
  .ready((err) => {
    if (err) console.error(err)

    console.log(app.config); // or fastify[options.confKey] 

    try {
      app.listen(app.config.PORT, app.config.HOST) 
    } catch (err) {
      app.log.error(err)
      process.exit(1) 
    }

  })


// db 
const fsequelize = require('./src/plugins/cockroachdb')
const { default: fastify } = require('fastify')
const sequelizeConfig = {
  instance: 'db', // name of instance will be mapped to fastify
  // autoConnect: true, // auto authentication and test connection on first run
  dialect: 'postgres',
  host: 'localhost',
  port: '26257',
  username: 'root',
  password: '',
  database: 'odfarmara_imported',
  timezone: 'Europe/Bratislava',
  dialectOptions: {
    timezone: "local",
    // ssl: {
    //   rejectUnauthorized: false,
    //   // For secure connection:
    //   /*ca: fs.readFileSync('certs/ca.crt').toString()*/
    // },
  },
  // timout for long running queries
  pool: {
    max: 10,
    min: 0,
    idle: 100000
  },
  // logging: (...msg) => console.log(msg),
  //logging: console.log,
  logging: false,
}

app.register(require('fastify-mailer'), {
  defaults: { from: 'odfarmara.sk <info@odfarmara.sk>' },
  transport: {
    // host: 'smtp.mailgun.org',
    // port: 465,
    // secure: true, // use TLS
    // auth: {
    //   user: 'marian@greyeminence.sk',
    //   pass: '9d833461d7938b390d4560c4352fd1a7-28d78af2-e75009ca'
    // }
    host: 'dex204.fastinternet.cz',
    port: 465,
    secure: true, // use TLS
    auth: {
      user: 'info@odfarmara.sk',
      pass: 'IPogfsrv214K5Kh'
    }
  }
})


app.register(fsequelize, sequelizeConfig)
app.register(require('fastify-formbody'))

app.register(fastifySecureSession, { 
  key: fs.readFileSync(path.join(__dirname, 'secret-key')),
  cookieName: 'of',
  cookie: {
    path: '/'
    // options for setCookie, see https://github.com/fastify/fastify-cookie
  }
})

app.register(fastifyPassport.secureSession());
app.register(fastifyPassport.initialize());

fastifyPassport.registerUserSerializer(async (user, request) => Promise.resolve(user.id))
fastifyPassport.registerUserDeserializer(async function(id, request) {

  const user = await app.db.models.user.findOne({
    include: [ 
      { model: app.db.models.favorite, as: "favorites"},
      // { model: app.db.models.favorite, as: "favorites", where: { id_offer: {[Op.not]: null}}},
    ],
    // raw: true,
    where: { id: id }
  })

  // console.log('date', new Date());
  // console.log('registerUserDeserializer', user);

  // console.log('registerUserDeserializer');

  return user;
});

fastifyPassport.use('form', new CustomStrategy(
  async function(req, done) {

    const email = req.body.email;
    const password = req.body.password;

    const user = await app.db.models.user.findOne({
      raw: true,
      where: { email: email }
    })

    // console.log('user', user);

    if(user){
      const hash = user.password_new;
      let compare_result = false;
      if(hash){
        compare_result = await argon2.verify(hash, password)
      }

      if(compare_result && user.active){
        done(null, user);
        req.flash('info', 'success login');
      }
      else{
        req.flash('error', 'invalid_login');
        done(null, false, {message: 'invalid_login'});
      }
    }
    else{
      req.flash('error', 'invalid_login');
        done(null, false, {message: 'invalid_login'});
    }
    
  }
));

app.decorate('fp', fastifyPassport);

app.post('/prihlasenie', { preValidation: fastifyPassport.authenticate('form', { 
  successRedirect: '/', 
  failureRedirect: '/prihlasenie', 
  failureMessage: false,
  failureFlash: false,
  authInfo: false }) }, async (request, reply, err, user, info, status) => {
  if (err !== null) {
    console.warn(err)
  } else if (user) {
    console.log('authUser', user)
  }
})

fastifyPassport.use('facebook', new FacebookStrategy({
  clientID: '719227186078931',
  clientSecret: '2ca36f239db8e14304f33969baf7d38a',
  callbackURL: 'http://localhost:4000/auth/facebook',
  profileFields: ['id', 'emails', 'name', 'displayName'],
  passReqToCallback: true,
}, async function (request, accessToken, refreshToken, profile, cb){

    const actionType = request.session.get('actionType');
    const registrationType = request.session.get('registrationType');

    let id = profile.id;
    let email = profile.emails[0].value;
    let displayName = profile.displayName;
    let familyName = profile.name.familyName;
    let givenName = profile.name.givenName;
    // let photo = profile.photos[0].value;

    // console.log('email', email);

    console.log('fastifyPassport.use');

    let existUser = await app.db.models.user.findOne({
      where:{ 
        email: email
      }
    });

    if(existUser){
      cb(undefined, existUser);
    }
    else{

      if(actionType == 'login'){
        request.session.set('federatedProfile', profile);
        cb(undefined, 'login_user_not_exist');
      }

      if(actionType == 'registration'){
        
        let type = "buyer";
        if(registrationType == "seller"){
          type = "seller";
        }

        //generate password
        const password = passwordGenerator.generate({
          length: 20,
          numbers: true,
          symbols: true,
        });
        const hash = await argon2.hash(password, {type: argon2.argon2id});
        const uuid = uuidv4();

        let data = {
          username: email,
          email: email,
          active: '1',
          password_new: hash,
          uuid: uuid,
          privilegs: 'customer',
          profile_type: 'free',
          name: displayName,
          created: new Date().toISOString(),
        }
  
        if(type == "seller"){
          data.privilegs = 'farmer';
          data.company_name = displayName;
        }

        // calculate initials
        let initials = "";
        let names = v(displayName).latinise().upperCase().words();
        if(names.length > 0){
          for(let x=0; x < names.length; x++){
            if(x > 1){
              break;
            }
            let char = names[x].charAt(0);
            initials += char;
          }
        }
        if(initials == ""){
          initials = v(email).latinise().upperCase().charAt(0).value();
        }
        data.initials = initials;

        const newUser = await app.db.models.user.create(data);

        // create solr index

        let m_created_date = moment(newUser.created);
        let created_date = m_created_date.toISOString();
        
        if(type == "seller"){
          const client = solr.createClient({
            path: '/solr/odfarmara_farmers'
          });

          let doc = {
              id : newUser.id,
              ge_id: {"set": newUser.id},
              ge_name : {"set": displayName},
              ge_company_name : {"set": displayName},
              ge_email : {"set": email},
              ge_active: {"set": true},
              ge_created_date: {"set": created_date},
          }

          // add and commit to solr
          const obj = await client.add(doc);
          const commit = await client.commit();
        }

        if(type == "buyer"){
          const client = solr.createClient({
            path: '/solr/odfarmara_users'
          });

          let doc = {
              id : newUser.id,
              ge_id: {"set": newUser.id},
              ge_name: {"set": displayName},
              ge_email : {"set": email},
              ge_active: {"set": true},
              ge_created_date: {"set": created_date},
          }

          // add and commit to solr
          const obj = await client.add(doc);
          const commit = await client.commit();
        }

        // callback
        cb(undefined, newUser);

      }
      
    }

  }
));


fastifyPassport.use('google', new GoogleStrategy({
  // local
  clientID: '328632108968-vgmlq9kbfi26j66mhoug8l8eiflhslth.apps.googleusercontent.com',
  clientSecret: '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',
  callbackURL: 'http://localhost:4000/auth/google',

  // dev
  // clientID: '299954856184-3kn5h6ofd2tmgajtmlmn9v9l9spljjnd.apps.googleusercontent.com',
  // clientSecret:  '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',
  // callbackURL:  'http://81.31.38.19:4000/auth/google',
  passReqToCallback: true,
  }, async function (request, accessToken, refreshToken, profile, cb){

    // console.log('google profile', profile);
    // console.log('request', request);

    const actionType = request.session.get('actionType');
    const registrationType = request.session.get('registrationType');

    let id = profile.id;
    let email = profile.emails[0].value;
    let displayName = profile.displayName;
    let familyName = profile.name.familyName;
    let givenName = profile.name.givenName;
    let photo = profile.photos[0].value;

    // console.log('email', email);

    console.log('fastifyPassport.use');

    let existUser = await app.db.models.user.findOne({
      where:{ 
        email: email
      }
    });

    // console.log('existUser', existUser);

    if(existUser){
      cb(undefined, existUser);
    }
    else{

      console.log('actionType', actionType);

      if(actionType == 'login'){
        request.session.set('federatedProfile', profile);
        cb(undefined, 'login_user_not_exist');
      }

      if(actionType == 'registration'){
        
        let type = "buyer";
        if(registrationType == "seller"){
          type = "seller";
        }

        //generate password
        const password = passwordGenerator.generate({
          length: 20,
          numbers: true,
          symbols: true,
        });
        const hash = await argon2.hash(password, {type: argon2.argon2id});
        const uuid = uuidv4();

        let data = {
          username: email,
          email: email,
          active: '1',
          password_new: hash,
          uuid: uuid,
          privilegs: 'customer',
          profile_type: 'free',
          name: displayName,
          created: new Date().toISOString(),
        }
  
        if(type == "seller"){
          data.privilegs = 'farmer';
          data.company_name = displayName;
        }

        // calculate initials
        let initials = "";
        let names = v(displayName).latinise().upperCase().words();
        if(names.length > 0){
          for(let x=0; x < names.length; x++){
            if(x > 1){
              break;
            }
            let char = names[x].charAt(0);
            initials += char;
          }
        }
        if(initials == ""){
          initials = v(email).latinise().upperCase().charAt(0).value();
        }
        data.initials = initials;
        data.created = new Date().toISOString();

        const newUser = await app.db.models.user.create(data);

        // create solr index

        let m_created_date = moment(newUser.created);
        let created_date = m_created_date.toISOString();
        
        if(type == "seller"){
          const client = solr.createClient({
            path: '/solr/odfarmara_farmers'
          });

          let doc = {
              id : newUser.id,
              ge_id: {"set": newUser.id},
              ge_name : {"set": displayName},
              ge_company_name : {"set": displayName},
              ge_email : {"set": email},
              ge_active: {"set": true},
              ge_created_date: {"set": created_date},
          }

          // add and commit to solr
          const obj = await client.add(doc);
          const commit = await client.commit();
        }

        if(type == "buyer"){
          const client = solr.createClient({
            path: '/solr/odfarmara_users'
          });

          let doc = {
              id : newUser.id,
              ge_id: {"set": newUser.id},
              ge_name: {"set": displayName},
              ge_email : {"set": email},
              ge_active: {"set": true},
              ge_created_date: {"set": created_date},
          }

          // add and commit to solr
          const obj = await client.add(doc);
          const commit = await client.commit();
        }

        // callback
        cb(undefined, newUser);

      }
      
    }
    
  }
));

app.get('/auth/facebook', { 
  preValidation: fastifyPassport.authenticate('facebook', {scope:['email'], assignProperty: 'result'})}, 
  async (req, res) => {

    if(req.result === "login_user_not_exist"){
      res.redirect('/prihlasenie/profil');
    }

    if(typeof req.result === 'object'){
      await req.login(req.result.dataValues);
      res.redirect('/');
    }

  }
)

app.get('/auth/google', { 
  preValidation: fastifyPassport.authenticate('google', {scope:['profile', 'email'], assignProperty: 'result'} )}, 
  async (req, res) => {

    if(req.result === "login_user_not_exist"){
      res.redirect('/prihlasenie/profil');
    }

    if(typeof req.result === 'object'){
      await req.login(req.result.dataValues);
      res.redirect('/');
    }
    
  }
)

// app.get('/auth/apple', { 
//   preValidation: fastifyPassport.authenticate('apple', {scope:['profile', 'email']})}, 
//   async (req, res) => {
//     res.redirect('/')
//   }
// )

// template engine
app.register(require('point-of-view'), {
  engine: {
    nunjucks: require('nunjucks')
  },
  root: path.join(__dirname, 'src/views'),
  viewExt: 'njk',
  options: {
    onConfigure: (env) => {
      env.addFilter('price', function(value) {
        return new Intl.NumberFormat('sk-SK', { style: 'currency', currency: 'EUR' }).format(parseFloat(value))
      });
      env.addFilter('rating', function(value) {
        if(value){
          let afterDecimal = parseFloat(value) % 1;
          let decimal = value - afterDecimal;
          afterDecimal = Math.round(afterDecimal * 2) / 2;
          return decimal + afterDecimal;
        }
        else{
          return 0;
        }
      });
      env.addFilter('format_date', function(value) {
        if(value){
          return dayjs(value).format('DD.MM.YYYY');
        }
        else{
          return '-';
        }
      });
      env.addFilter('format_date_time', function(value) {
        if(value){
          return dayjs(value).format('DD.MM.YYYY HH:mm:ss');
        }
        else{
          return '-';
        }
      });
      env.addFilter('before_humanize', function(value) {
        if(value){
          const now = moment();
          moment.locale('sk');
          const created = moment(value);
          return moment.duration(created.diff(now)).humanize(true);
        }
        return '';
      });
      env.addGlobal('thisYear', function(){
        return new Date().getFullYear();
      });
      env.addGlobal('rating', function(value){
        let afterDecimal = parseFloat(value) % 1;
        let decimal = value - afterDecimal;
        afterDecimal = Math.round(afterDecimal * 2) / 2;
        return decimal + afterDecimal;
      });
      env.addGlobal('title', function(title){
        title = title.replace(" ", "_");
        return title;
      });
      env.addGlobal('favoriteProduct', function(favoriteProducts, productId){

        let favorite = favoriteProducts.includes(parseInt(productId));
        // console.log('favoriteProducts', favoriteProducts);
        // console.log('productId', productId);
        // console.log('favorite', favorite);
        return favorite;
        
      });
      env.addGlobal('favoriteFarmer', function(favoriteFarmers, farmerId){

        let favorite = favoriteFarmers.includes(parseInt(farmerId));
        // console.log('favoriteProducts', favoriteFarmers);
        // console.log('productId', farmerId);
        // console.log('favorite', favorite);
        return favorite;
        
      });
      env.addGlobal('expanded', function(id, url){
        
        let uri = URI(url);
        let query = uri.search(true);

        let e = query.e;

        if(Array.isArray(e)){
          return e.includes(String(id));
        }
        else{
          if(e == id){
            return true;
          }
          else{
            return false;
          }
        }
        
      });
      env.addGlobal('currentCategory', function(id, url){
        
        let uri = URI(url);
        let query = uri.search(true);

        let c = query.c;

        if(Array.isArray(c)){
          return c.includes(String(id));
        }
        else{
          if(c == id){
            return true;
          }
          else{
            return false;
          }
        }
        
      });
      env.addGlobal('currentMainCategory', function(id, url){

        if(!url){
          return false;
        }

        let uri = URI(url);
        let query = uri.search(true);

        let mc = query.mc;

        if(Array.isArray(mc)){
          return mc.includes(String(id));
        }
        else{
          if(mc == id){
            return true;
          }
          else{
            return false;
          }
        }
        
      });
      env.addGlobal('categoryUrl', function(mc_id, id, url){

        let uri = new URI(url);
        let query = uri.search(true);

        let c = query.c;
        let mc = query.mc;

        if(Array.isArray(c)){
          if(!c.includes(String(id))){
            c.push(id);
          }
        }
        else{
          if(c && c != id){
            c = [c]
            c.push(id);
          }
          else{
            c = id;
          }
        }

        if(Array.isArray(mc)){
          const index = mc.indexOf(String(mc_id));
          if (index > -1) {
            mc.splice(index, 1);
            uri.setSearch("mc", mc);
          }
        }
        else{
          if(mc && mc == mc_id){
            mc = null;
            uri.removeSearch('mc');
          }
        }

        uri.setSearch("c", c);
        uri.setSearch("p", 1);
        
        return uri;
        
      });
      env.addGlobal('mainCategoryUrl', function(id, url, categories){

        let uri = new URI(url);
        let query = uri.search(true); 
        let c = query.c;
        let mc = query.mc;
        
        if(c){
          let catToRemove = [];
          for(let x = 0; x < categories.length; x++){
            let cat_id = categories[x].id;
            let actualCat = false;
  
            if(id == cat_id){
              actualCat = true;
            }
    
            let subcategories = categories[x].subcategory;
            if(subcategories.length){
              for(let y = 0; y < subcategories.length; y++){
  
                let id = String(subcategories[y].id);
  
                if(actualCat){
                  catToRemove.push(id);
                }
  
              }
            }
          }

          if(!Array.isArray(c)){
            c = [c];
          }
          
          c = c.filter(n => !catToRemove.includes(n));
          uri.setSearch('c', c);
        }

        if(Array.isArray(mc)){
          if(!mc.includes(String(id))){
            mc.push(id);
          }
        }
        else{
          if(mc && mc != id){
            mc = [mc];
            mc.push(id);
          }
          else{
            mc = id;
          }
        }
        
        uri.setSearch("mc", mc);
        
        return uri;
        
      });
      env.addGlobal('mainCategoryUrl2', function(id, url){

        let uri = new URI(url);
        uri.setSearch("mc", id);
        
        return uri;
        
      });
      env.addGlobal('removeMainCategoryUrl', function(id, url){
        
        let uri = new URI(url);
        let query = uri.search(true);
        let mc = query.mc;

        if(Array.isArray(mc)){
          const index = mc.indexOf(String(id));
          if (index > -1) {
            mc.splice(index, 1);
            uri.setSearch("mc", mc);
          }
        }
        else{
          if(mc && mc == id){
            mc = null;
            uri.removeSearch('mc');
          }
        }
        
        uri.setSearch("p", 1);
        
        return uri;
        
      });
      env.addGlobal('removeCategoryUrl', function(id, url){
        
        let uri = new URI(url);
        let query = uri.search(true);
        let c = query.c;

        if(Array.isArray(c)){
          const index = c.indexOf(String(id));
          if (index > -1) {
            c.splice(index, 1);
            uri.setSearch("c", c);
          }
        }
        else{
          if(c && c == id){
            c = null
            uri.removeSearch('c');
          }
        }
        
        uri.setSearch("p", 1);
        
        return uri;
        
      });
      env.addGlobal('regionUrl', function(id, url){
        
        let uri = new URI(url);
        let query = uri.search(true);

        let r = query.r;

        if(Array.isArray(r)){
          if(!r.includes(String(id))){
            r.push(id);
          }
        }
        else{
          if(r && r != id){
            r = [r]
            r.push(id);
          }
          else{
            r = id;
          }
        }

        uri.setSearch("r", r);
        uri.setSearch("p", 1);
        
        return uri;
        
      });
      env.addGlobal('removeRegionUrl', function(id, url){
        
        let uri = new URI(url);
        let query = uri.search(true);
        let r = query.r;

        if(Array.isArray(r)){
          const index = r.indexOf(String(id));
          if (index > -1) {
            r.splice(index, 1);
            uri.setSearch("r", r);
          }
        }
        else{
          if(r && r == id){
            r = null
            uri.removeSearch('r');
          }
        }
        
        uri.setSearch("p", 1);

        console.log('r', r);
        
        return uri;
        
      });
      env.addGlobal('currentRegion', function(id, url){
        
        let uri = URI(url);
        let query = uri.search(true);

        let r = query.r;

        if(Array.isArray(r)){
          return r.includes(String(id));
        }
        else{
          if(r == id){
            return true;
          }
          else{
            return false;
          }
        }
        
      });
      env.addGlobal('paginationUrl', function(page, url){
        
        let uri = new URI(url);
        uri.setSearch("p", page);
        return uri;
        
      });
      env.addGlobal('isArray', function(array){
        return Array.isArray(array);
      });

      env.addGlobal('inputValidationClasses', function(validationObject, input){

        console.log('validation', validationObject);
        console.log('input', input);
        
        if(validationObject && validationObject.hasOwnProperty(input)){
          return "text-red-of placeholder:text-red-of";
        }
        else{
          return "";
        }
        
      });
      env.addGlobal('isArray', function(array){
        return Array.isArray(array);
      });
      env.addGlobal('old_date', function(date){
        
        let after = dayjs().isAfter(dayjs(date), 'day');
        // console.log('date', date);
        // console.log('old_date', after);
        return after;
        
      });
      
    }
  }
})

// shared data available in all views
app.addHook('preHandler', async function (request, reply) {

    const categories = await app.db.models.category.findAll({
      where: { parent_id: {[Op.is]: null}, type: 'product', active: true },
      include: [ 
        { model: app.db.models.category, as: "subcategory"},
      ],
    })

    reply.locals = {
      categories: categories,
      root: app.config.ROOT,
      user: request.user,
    }

})



// 404
app.decorate('notFound', (request, reply) => {
  reply.view('/pages/404', { 
    title: 'Odfarmara.sk'
  })
})
app.setNotFoundHandler(app.notFound)

// models
app.register(require('./src/models/farmer_rating'))
app.register(require('./src/models/region'))
app.register(require('./src/models/favorite'))
app.register(require('./src/models/category'))
app.register(require('./src/models/user'))
app.register(require('./src/models/image'))
app.register(require('./src/models/tag'))
app.register(require('./src/models/product'))
app.register(require('./src/models/product_view'))
app.register(require('./src/models/article'))
app.register(require('./src/models/farmer_image'))
app.register(require('./src/models/newsletter'))
// app.register(require('./src/models/page'))
app.register(require('./src/models/opening_hours'))
app.register(require('./src/models/demand'))
app.register(require('./src/models/inbox'))
app.register(require('./src/models/recipe'))
app.register(require('./src/models/allergen'))
app.register(require('./src/models/nutrition_value'))
app.register(require('./src/models/cities'))
app.register(require('./src/models/seasonal_offer'))
app.register(require('./src/models/flags'))

app.register(require('./src/utils/season_offers'))

// routes
// index
app.register(require('./src/routes/index'))
// search
app.register(require('./src/routes/search'))
// form
app.register(require('./src/routes/form'))
// action
app.register(require('./src/routes/action'))
// category
app.register(require('./src/routes/category'))
// produkt
app.register(require('./src/routes/product'))
// produkty
app.register(require('./src/routes/products'))
// farmari
app.register(require('./src/routes/farmers'))
// farm
app.register(require('./src/routes/farm'))
// recept
app.register(require('./src/routes/recipe'))
// recepty
app.register(require('./src/routes/recipes'))
// page tag
app.register(require('./src/routes/tag'))
// ako-nakupovat
app.register(require('./src/routes/shopping'))
// ako-predavat
app.register(require('./src/routes/selling'))
// o-projekte
app.register(require('./src/routes/project'))
// faq
app.register(require('./src/routes/faq'))
// blog post
app.register(require('./src/routes/blog_post'))
// blog
app.register(require('./src/routes/blog'))
// kontakt
app.register(require('./src/routes/contact'))
// podporte projekt
app.register(require('./src/routes/support'))
// ochrana osobnych udajov
app.register(require('./src/routes/privacy'))
// cookies
app.register(require('./src/routes/cookies'))
// registracia
app.register(require('./src/routes/registration'))
// login
app.register(require('./src/routes/login'))
// maintenance
app.register(require('./src/routes/maintenance'))
// page fragment
app.register(require('./src/routes/fragment'))
// terms of use
app.register(require('./src/routes/terms_of_use'))

// profile
app.register(require('./src/routes/profile/index'))
app.register(require('./src/routes/profile/favorites'))
app.register(require('./src/routes/profile/messages'))
app.register(require('./src/routes/profile/profile'))
app.register(require('./src/routes/profile/settings'))
app.register(require('./src/routes/profile/subscription'))
app.register(require('./src/routes/profile/contacts/my-contacts'))
app.register(require('./src/routes/profile/contacts/following-users'))
app.register(require('./src/routes/profile/contacts/requesting-users'))
app.register(require('./src/routes/profile/photos_upload'))
app.register(require('./src/routes/profile/my_offers/index'))
app.register(require('./src/routes/profile/my_offers/create'))
app.register(require('./src/routes/profile/my_offers/edit'))

// admin
app.register(require('./src/routes/admin/index'))
app.register(require('./src/routes/admin/maintenance'))
app.register(require('./src/routes/admin/products'))
app.register(require('./src/routes/admin/farmers'))
app.register(require('./src/routes/admin/tags'))
app.register(require('./src/routes/admin/users'))
app.register(require('./src/routes/admin/categories'))
app.register(require('./src/routes/admin/slug'))
app.register(require('./src/routes/admin/allergens'))
app.register(require('./src/routes/admin/nutritions'))
app.register(require('./src/routes/admin/recipes'))
app.register(require('./src/routes/admin/blog'))
app.register(require('./src/routes/admin/seasonal_offers'))
app.register(require('./src/routes/admin/flags'))
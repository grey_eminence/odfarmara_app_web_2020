const fp = require('fastify-plugin')
const moment = require('moment')
const { Op } = require("sequelize")
const _ = require('lodash')

async function init(fastify, options, done) {

  fastify.decorate('getSeasonOffersProducts', async function(){

    let current_date_formated = moment().format('YYYY-MM-DD'); 
    // get sessional offer
    let sessional_offers = await fastify.db.models.seasonal_offer.findAll({
      where: {
        active: '1',
        display_from: {
          [Op.lte] : current_date_formated
        },
        display_to: {
          [Op.gte] : current_date_formated
        },
      }
    });

    let sessional_offers_products = [];
    let sessional_offers_categories = [];
    let sessional_offers_tags = [];
    let tagQuery = [];
    if(sessional_offers){
      
      for(let x = 0; x < sessional_offers.length; x++){
        let item = sessional_offers[x];

        // categories
        if(item.categories && item.categories.length){
          for(let y = 0; y < item.categories.length; y++){
            sessional_offers_categories.push(item.categories[y]);
          }
        }

        // tags
        if(item.tags && item.tags.length){
          for(let y = 0; y < item.tags.length; y++){
            let id = item.tags[y].id;
            sessional_offers_tags.push(id);
            tagQuery.push({tags: {
              [Op.contains]: [{'id': id}]
            }});
          }
        }
        
      }

    }

    // get products
    console.log('sessional_offers_categories', sessional_offers_categories);
    console.log('sessional_offers_tags', sessional_offers_tags);
    console.log('tagQuery', tagQuery);

    let products_categories = await fastify.db.models.product.findAll({
      limit: 10,
      include: [ 
        { model: fastify.db.models.image, as: "image" },
        { model: fastify.db.models.user, as: "farmer", required: true },
      ],
      where: {
        active: '1',
        [Op.or]: [
        { start_date: {
          [Op.lte] : current_date_formated
          },
          end_date: {
            [Op.gte] : current_date_formated
          },
         },
        { unlimited: true }
        ], 
        subcategory_id: {[Op.in]: sessional_offers_categories },
      },
      order: [
        ['created', 'DESC'], ['image', 'img_id', 'ASC']
      ]
    });
    
    let products_tags = await fastify.db.models.product.findAll({
      limit: 10,
      include: [ 
        { model: fastify.db.models.image, as: "image" },
        { model: fastify.db.models.user, as: "farmer", required: true },
      ],
      where: {
        active: '1',
        [Op.or]: [
        { start_date: {
          [Op.lte] : current_date_formated
          },
          end_date: {
            [Op.gte] : current_date_formated
          },
         },
        { unlimited: true }
        ],
        [Op.or]: tagQuery
      },
      order: [
        ['created', 'DESC'], ['image', 'img_id', 'ASC']
      ]
    });

    let allowed_product_count = 10;
    let allowed_categories_product_count = 5;
    let allowed_tags_product_count = 5;
    if(products_categories.length){
      allowed_product_count = 9;
    }

    if(allowed_product_count === 9){
      if(products_categories.length > 5 && products_tags.length > 4){
        allowed_categories_product_count = 5;
        allowed_tags_product_count = 4;
      }

      if(products_categories.length > 5 && products_tags.length <= 4){
        allowed_categories_product_count = allowed_product_count - products_tags.length;
        allowed_tags_product_count = products_tags.length;
      }

      if(products_categories.length <= 5 && products_tags.length > 4){
        allowed_categories_product_count = products_categories.length;
        allowed_tags_product_count = allowed_product_count - products_categories.length;
      }
    }

    if(allowed_product_count === 10){
      if(products_categories.length > 5 && products_tags.length > 5){
        allowed_categories_product_count = 5;
        allowed_tags_product_count = 5;
      }

      if(products_categories.length > 5 && products_tags.length <= 5){
        allowed_categories_product_count = allowed_product_count - products_tags.length;
        allowed_tags_product_count = products_tags.length;
      }

      if(products_categories.length <= 5 && products_tags.length > 5){
        allowed_categories_product_count = products_categories.length;
        allowed_tags_product_count = allowed_product_count - products_categories.length;
      }
    }

    for(let i = 0; i < products_categories.length; i++){
      let item = products_categories[i];

      let finded = _.find(sessional_offers_products, function(o) { return o.id == item.id });
      if(!finded){
        sessional_offers_products.push(item);
      }
      
      if(i == allowed_categories_product_count - 1){
        break;
      }
    }

    for(let i = 0; i < products_tags.length; i++){
      let item = products_tags[i];

      let finded = _.find(sessional_offers_products, function(o) { return o.id == item.id });
      if(!finded){
        sessional_offers_products.push(item);
      }
      
      if(i == allowed_tags_product_count - 1){
        break;
      }
    }

    // console.log('sessional_offers_products_categories', products_categories);
    // console.log('sessional_offers_products_tags', products_tags);

    console.log('products_categories', products_categories.length);
    console.log('products_tags', products_tags.length);
    console.log('sessional_offers_products', sessional_offers_products.length);

    return sessional_offers_products;

  })

  fastify.decorate('getSeasonOffersCategories', async function(){

    let current_date_formated = moment().format('YYYY-MM-DD'); 
    // get sessional offer
    let sessional_offers = await fastify.db.models.seasonal_offer.findAll({
      where: {
        active: '1',
        display_from: {
          [Op.lte] : current_date_formated
        },
        display_to: {
          [Op.gte] : current_date_formated
        },
      }
    });

    let sessional_offers_categories = [];
    let sessional_offers_categories_db = [];
    if(sessional_offers){
      
      for(let x = 0; x < sessional_offers.length; x++){
        let item = sessional_offers[x];
        if(item.categories && item.categories.length){
          for(let y = 0; y < item.categories.length; y++){
            sessional_offers_categories.push(item.categories[y]);
          }
        }
      }

      // unique
      sessional_offers_categories = _.uniq(sessional_offers_categories);
    }

    if(sessional_offers_categories.length){
      sessional_offers_categories_db = await fastify.db.models.category.findAll({
        where: {
          id: {[Op.in]: sessional_offers_categories }
        }
      })
    }

    return sessional_offers_categories_db;

  })

  done()
}

module.exports = fp(init)
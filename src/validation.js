const luhn = require("luhn")
const passwordValidator = require('password-validator');

function validateCsvItems(items) {
    let errors = []

    items.forEach(function(item){

      

    })
    
    return errors
}

function validateCreditcard(ccn){
    ccn = ccn.toString().replace(/ /g,'')
    if(ccn.length !== 14){
      return false
    }
    ccn = parseInt(ccn)
    return luhn.validate(ccn)
}

function validatePassword(password){

    let schema = new passwordValidator();
 
     schema
    .is().min(8)                // Minimum length 8
    .is().max(100)              // Maximum length 100
    .has().uppercase()          // Must have uppercase letters
    .has().lowercase()          // Must have lowercase letters
    .has().digits(1)            // Must have at least 1 digits
    .has().symbols(1)           // Must have at least 1 special symbols
    .has().not().spaces()            

    return schema.validate(password)
}

const validateCreditcardSchema = [
    function validate(data) {
        console.log('validateCreditcardSchema', data)
        return luhn.validate(data)
    },
    function errorMessage(schema, parentSchema, data) {
      return '${data} is not a valid creditcard.'
    },
  ];

function validationErrorsFormat(error) {

  if (error.validation) {
 
    let errors = {}
   
    if(Array.isArray(error.validation)){
   
      error.validation.forEach( function(item) {
  
        console.log('item', item)

        // if(Array.isArray(item.params.errors) && item.params.errors.length > 0){

        //   item.params.errors.forEach( function(subitem) {
            
        //     if(subitem.keyword === 'required'){
        //       let property = subitem.params.missingProperty
        //       errors[property] = item.message
        //     }
        //     else{
        //       let property = subitem.instancePath.replace('/', '')
        //       errors[property] = item.message
        //     }
        //   })
        // }

        let property = item.params.missingProperty

        if(item.keyword === 'required'){
          errors[property] = item.message
        }
        else{
          let property = subitem.dataPath.replace('/', '')
          errors[property] = item.message
        }
            
      })
  
    }
   
    return {'validation_errors': errors}
  }
 
}

exports.validateCreditcardSchema = validateCreditcardSchema
exports.validateCreditcard = validateCreditcard
exports.validatePassword = validatePassword
exports.validateCsvItems = validateCsvItems
exports.validationErrorsFormat = validationErrorsFormat
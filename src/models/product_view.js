const { DataTypes, Sequelize } = require('sequelize')

async function model (fastify, options) {

    const product = fastify.db.define('product_view', {
        // Model attributes are defined here
        rowid: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        product_id: {
            type: DataTypes.INTEGER
        },
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at'  
    })

  }

  module.exports = model
const { DataTypes, Sequelize } = require('sequelize')

async function model (fastify, options) {

    const category = fastify.db.define('category', {
        // Model attributes are defined here
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        slug: {
            type: DataTypes.STRING,
            // allowNull: false
        },
        parent_id: {
            type: DataTypes.INTEGER
            // allowNull defaults to true
        },
        icon: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        type: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        uuid: {
            type: DataTypes.UUID,
        },
        active: {
            type: DataTypes.BOOLEAN
        }
    }, {
        // createdAt: 'created_at',
        // updatedAt: 'updated_at'
        timestamps: false,
        // If don't want createdAt
        createdAt: false,
        // If don't want updatedAt
        updatedAt: false,  
    })

    category.hasMany(fastify.db.models.category, {
        foreignKey: {
            name: 'parent_id',
            type: DataTypes.INTEGER
        },
        targetKey: 'id',
        as: 'subcategory'
    })

    category.belongsTo(fastify.db.models.category, {
        foreignKey: {
            name: 'parent_id',
            type: DataTypes.INTEGER
        },
        targetKey: 'id',
        as: 'parentCategory'
    })
    
    //category.sync({ force: true })
  }

  module.exports = model
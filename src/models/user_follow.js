const { DataTypes, Sequelize } = require('sequelize')
const argon2 = require('argon2')
const { v4: uuidv4 } = require('uuid')

async function model (fastify, options) {

    const user = fastify.db.define('user', {
        // Model attributes are defined here
        first_name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        surname: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        username: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        password: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        email: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        privilegs: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        last_login: {
            type: DataTypes.DATE
            // allowNull defaults to true
        },
        ip: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        uuid: {
            type: DataTypes.UUID,
        },
        active: {
            type: DataTypes.BOOLEAN
        }
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    })

    // const voucher = fastify.db.define('voucher', {
    //     // Model attributes are defined here
    //     voucher_code: {
    //         type: DataTypes.STRING(16),
    //         allowNull: false
    //     },
    //     voucher_type: {
    //         type: DataTypes.STRING(1)
    //         // allowNull defaults to true
    //     },
    //     voucher_uuid: {
    //         type: DataTypes.UUID,
    //         defaultValue: Sequelize.UUIDV4
    //     },
    //     voucher_created_by: {
    //         type: DataTypes.STRING
    //         // allowNull defaults to true
    //     },
    //     voucher_create_date: {
    //         type: DataTypes.DATE
    //         // allowNull defaults to true
    //     },
    //     voucher_expiry: {
    //         type: DataTypes.DATEONLY
    //         // allowNull defaults to true
    //     },
    //     voucher_commit: {
    //         type: DataTypes.DATE
    //         // allowNull defaults to true
    //     },
    //     voucher_commit_by: {
    //         type: DataTypes.STRING
    //         // allowNull defaults to true
    //     },
    //     voucher_ordered_by: {
    //         type: DataTypes.STRING
    //         // allowNull defaults to true
    //     },
    //     voucher_hash: {
    //         type: DataTypes.STRING
    //         // allowNull defaults to true
    //     },
    //     voucher_ccno_safe: {
    //         type: DataTypes.STRING
    //         // allowNull defaults to true
    //     },
    //     voucher_ccno_last_digits: {
    //         type: DataTypes.STRING
    //         // allowNull defaults to true
    //     },
    //     voucher_cc_name: {
    //         type: DataTypes.STRING
    //         // allowNull defaults to true
    //     },
    //     voucher_cc_surname: {
    //         type: DataTypes.STRING
    //         // allowNull defaults to true
    //     },
    //     voucher_printed_by: {
    //         type: DataTypes.STRING
    //         // allowNull defaults to true
    //     },
    //     voucher_sent: {
    //         type: DataTypes.DATE
    //         // allowNull defaults to true
    //     },
    //     voucher_country: {
    //         type: DataTypes.STRING(2)
    //         // allowNull defaults to true
    //     },
    //     voucher_note: {
    //         type: DataTypes.TEXT
    //         // allowNull defaults to true
    //     },
    // }, {
    //     createdAt: 'created_at',
    //     updatedAt: 'updated_at'
    // })

    // voucher.belongsTo(user, {
    //     foreignKey: {
    //         name: 'voucher_created_by',
    //         type: DataTypes.UUID
    //     },
    //     targetKey: 'uuid',
    //     as: 'user_created_by'
    // })

    // voucher.belongsTo(user, {
    //     foreignKey: {
    //         name: 'voucher_commit_by',
    //         type: DataTypes.UUID
    //     },
    //     targetKey: 'uuid',
    //     as: 'user_activated_by'
    // })

    // voucher.belongsTo(user, {
    //     foreignKey: {
    //         name: 'voucher_printed_by',
    //         type: DataTypes.UUID
    //     },
    //     targetKey: 'uuid',
    //     as: 'user_shipped_by'
    // })

    // voucher.belongsTo(user, {
    //     foreignKey: {
    //         name: 'voucher_ordered_by',
    //         type: DataTypes.UUID
    //     },
    //     targetKey: 'uuid',
    //     as: 'user_ordered_by'
    // })


    // voucher.hasOne(user, {
    //     foreignKey: {
    //       name: 'voucher_created_by',
    //       type: DataTypes.UUID
    //     },
    //     as: 'user_created_by'
    // })

    //voucher.hasOne(user)

    
    // user.sync({ force: true })
    // const password = "123456"
    // const hash = await argon2.hash(password, {type: argon2.argon2id})
    // const uuid = uuidv4()
    // user.create({ first_name: "Marián", surname: "Michalovič", email: "marian.michalovic@greyeminence.sk", active: 1, password: hash, uuid: uuid })

  }

  module.exports = model
const { DataTypes, Sequelize } = require('sequelize')
const argon2 = require('argon2')
const { v4: uuidv4 } = require('uuid')

async function model (fastify, options) {

    const tag = fastify.db.define('tag', {
        // Model attributes are defined here
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        slug: {
            type: DataTypes.STRING,
            allowNull: false
        },
        description: {
            type: DataTypes.TEXT,
        },
        icon: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        uuid: {
            type: DataTypes.UUID,
        },
        system: {
            type: DataTypes.BOOLEAN
        },
        active: {
            type: DataTypes.BOOLEAN
        },
        user_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: fastify.db.models.user,
                key: 'id'
            }
        },
        landing_page: {
            type: DataTypes.BOOLEAN
        },
        display_from: {
            type: DataTypes.DATEONLY
        },
        display_to: {
            type: DataTypes.DATEONLY
        },
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    })

    tag.belongsTo(fastify.db.models.user, {
        foreignKey: 'user_id',
    });

    // fastify.db.models.user.hasMany(tag, {
    //     foreignKey: 'old_id',
    //     targetKey: 'user_id',
    // });
    
    // tag.sync({ force: true }) 
  }

  module.exports = model
const { DataTypes, Sequelize } = require('sequelize')
const argon2 = require('argon2')
const { v4: uuidv4 } = require('uuid')

async function model (fastify, options) {

    const newsletter = fastify.db.define('newsletter', {
        // Model attributes are defined here
        newsl_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false
        },
        // uuid: {
        //     type: DataTypes.UUID,
        // },
        active: {
            type: DataTypes.BOOLEAN
        }
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        tableName: 'newsletter'
    })
    
    // newsletter.sync({ force: true })
  }

  module.exports = model
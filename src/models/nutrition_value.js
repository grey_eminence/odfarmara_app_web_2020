const { DataTypes, Sequelize } = require('sequelize')

async function model (fastify, options) {

    const nutrition_value = fastify.db.define('nutrition_value', {
        // Model attributes are defined here
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        uuid: {
            type: DataTypes.UUID,
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        category_id: {
            type: DataTypes.INTEGER,
        },
        values: {
            type: DataTypes.JSONB,
        },
        created_by: {
            type: DataTypes.INTEGER,
        },
        active: {
            type: DataTypes.BOOLEAN
        }
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    })

    nutrition_value.belongsTo(fastify.db.models.category, {
        foreignKey: 'category_id',
    });

    //nutrition_value.sync({ force: true })
  }

  module.exports = model
const { DataTypes, Sequelize } = require('sequelize')
const { v4: uuidv4 } = require('uuid')

async function model (fastify, options) {

    const demand = fastify.db.define('demand', {
        // Model attributes are defined here
        id_demand: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        id_offer: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        price: {
            type: DataTypes.DECIMAL(10, 2)
        },
        price_for: {
            type: DataTypes.STRING
        },
        confirmed: {
            type: DataTypes.BOOLEAN,
        },
        quantity: {
            type: DataTypes.INTEGER
            // allowNull defaults to true
        },
        created_by: {
            type: DataTypes.INTEGER,
            allowNull: false
            // allowNull defaults to true
        },
        finished: {
            type: DataTypes.BOOLEAN,
        },
        farma_seen: {
            type: DataTypes.BOOLEAN,
        },
        user_seen: {
            type: DataTypes.BOOLEAN,
        }
    }, {
        createdAt: 'created',
        updatedAt: 'modified'
    })

    demand.belongsTo(fastify.db.models.product, {
        foreignKey: {
            name: 'id_offer'
        },
        targetKey: 'old_id',
        as: 'product'
    })

    // demand.sync({ force: true })
  }

  module.exports = model
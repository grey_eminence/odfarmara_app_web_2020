const { DataTypes, Sequelize } = require('sequelize')

async function model (fastify, options) {

    const article = fastify.db.define('article', {
        // Model attributes are defined here
        article_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        cat_id: {
            type: DataTypes.INTEGER,
        },
        title: {
            type: DataTypes.STRING(1024)
            // allowNull defaults to true
        },
        slug: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        seo_ext: {
            type: DataTypes.TEXT
            // allowNull defaults to true
        },
        seo_title: {
            type: DataTypes.TEXT
            // allowNull defaults to true
        },
        seo_description: {
            type: DataTypes.TEXT
            // allowNull defaults to true
        },
        seo_keywords: {
            type: DataTypes.TEXT
            // allowNull defaults to true
        },
        tags: {
            type: DataTypes.JSONB
            // allowNull defaults to true
        },
        perex: {
            type: DataTypes.TEXT
            // allowNull defaults to true
        },
        contents: {
            type: DataTypes.TEXT
            // allowNull defaults to true
        },
        blog_img: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        blog_img_alt: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        filename: {
            type: DataTypes.STRING(1024)
            // allowNull defaults to true
        },
        active: {
            type: DataTypes.BOOLEAN
        },
        counter_view: {
            type: DataTypes.INTEGER
        },
        date: {
            type: DataTypes.DATEONLY
        },
        created: {
            type: DataTypes.DATEONLY
        }
    }, {
        // createdAt: 'created_at',
        // updatedAt: 'updated_at'
        timestamps: false,
        // If don't want createdAt
        createdAt: false,
        // If don't want updatedAt
        updatedAt: false, 
    })

    article.belongsTo(fastify.db.models.category, {
        foreignKey: {
            name: 'cat_id'
        },
        targetKey: 'id',
        as: 'category'
    })

    //article.sync({ force: true })
  }

  module.exports = model
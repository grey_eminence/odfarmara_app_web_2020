const { DataTypes, Sequelize } = require('sequelize')
const argon2 = require('argon2')
const { v4: uuidv4 } = require('uuid')

async function model (fastify, options) {

    const inbox = fastify.db.define('inbox', {
        // Model attributes are defined here
        id_inbox: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        id_demand: {
            type: DataTypes.INTEGER,
            // allowNull: false
        },
        content: {
            type: DataTypes.TEXT          
            // allowNull defaults to true
        },
        farmar_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        user_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        created_by: {
            type: DataTypes.INTEGER,
        },
        status: {
            type: DataTypes.TINYINT,
        },
        seen: {
            type: DataTypes.BOOLEAN,
        },
        seen_farmar: {
            type: DataTypes.BOOLEAN,
        },
        hidden_to: {
            type: DataTypes.INTEGER,
        },
        flag: {
            type: DataTypes.SMALLINT,
        },
    }, {
        createdAt: 'created',
        updatedAt: 'modified',
        tableName: 'inboxes',
    })

    inbox.belongsTo(fastify.db.models.user, {
        foreignKey: {
            name: 'farmar_id'
        },
        targetKey: 'id',
        as: 'farmer'
    })

    inbox.belongsTo(fastify.db.models.user, {
        foreignKey: {
            name: 'user_id'
        },
        targetKey: 'id',
        as: 'user'
    })

    inbox.belongsTo(fastify.db.models.demand, {
        foreignKey: {
            name: 'id_demand'
        },
        targetKey: 'id_demand',
        as: 'demand'
    })

  }

  module.exports = model
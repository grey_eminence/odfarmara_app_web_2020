const { DataTypes, Sequelize } = require('sequelize')

async function model (fastify, options) {

    const allergen = fastify.db.define('allergen', {
        // Model attributes are defined here
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        description: {
            type: DataTypes.TEXT,
        },
        slug: {
            type: DataTypes.STRING,
            // allowNull: false
        },
        uuid: {
            type: DataTypes.UUID,
        },
        created_by: {
            type: DataTypes.INTEGER,
        },
        active: {
            type: DataTypes.BOOLEAN
        }
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    })

    // allergen.sync({ force: true })
  }

  module.exports = model
const { DataTypes, Sequelize } = require('sequelize')
const argon2 = require('argon2')
const { v4: uuidv4 } = require('uuid')

async function model (fastify, options) {

    const user = fastify.db.define('user', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        name: {
            type: DataTypes.STRING(255),
        },
        // Model attributes are defined here
        // first_name: {
        //     type: DataTypes.STRING,
        //     allowNull: false
        // },
        // surname: {
        //     type: DataTypes.STRING
        //     // allowNull defaults to true
        // },
        username: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        password: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        password_new: {
            type: DataTypes.STRING(1024)
            // allowNull defaults to true
        },
        email: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        privilegs: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        gps: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        company_name: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        // company:{
        //     type: DataTypes.JSONB
        // },
        user_group_id: {
            type: DataTypes.INTEGER
        },
        region_id: {
            type: DataTypes.INTEGER
        },
        account_type: {
            type: DataTypes.INTEGER
        },
        profile_type: {
            type: DataTypes.STRING(256)
        },
        profile_from: {
            type: DataTypes.DATE
        },
        profile_to: {
            type: DataTypes.DATE
        },
        created: {
            type: DataTypes.DATE
        },
        // last_login: {
        //     type: DataTypes.DATE
        //     // allowNull defaults to true
        // },
        // ip: {
        //     type: DataTypes.STRING
        //     // allowNull defaults to true
        // },
        // uuid: {
        //     type: DataTypes.UUID,
        // },
        active: {
            type: DataTypes.BOOLEAN
        },
        admin: {
            type: DataTypes.BOOLEAN
        },
        // verified: {
        //     type: DataTypes.DATE
        // },
        // verified_at: {
        //     type: DataTypes.BOOLEAN
        // },
        city: {
            type: DataTypes.STRING(1024)
        },
        street: {
            type: DataTypes.STRING(1024)
        },
        // address_country: {
        //     type: DataTypes.STRING(2)
        // },
        zip: {
            type: DataTypes.STRING(1024)
        },
        // address_name: {
        //     type: DataTypes.STRING(1024)
        // },
        phone: {
            type: DataTypes.STRING(1024)
        },
        contact_email: {
            type: DataTypes.STRING(1024)
        },
        // slug: {
        //     type: DataTypes.STRING(1024)
        // },
        reset_pass_token: {
            type: DataTypes.STRING(222)
        },
        reset_pass_time: {
            type: DataTypes.DATE,
        },
        // address_pass_at: {
        //     type: DataTypes.DATE
        // },
        // is_company: {
        //     type: DataTypes.BOOLEAN
        // },
        short_description: {
            type: DataTypes.TEXT
        },
        long_description: {
            type: DataTypes.TEXT
        },
        // rating_value: {
        //     type: DataTypes.FLOAT
        // },
        houseno: {
            type: DataTypes.STRING(255)
        },
        code: {
            type: DataTypes.STRING(255)
        },
        rating_avg: {
            type: DataTypes.FLOAT,
        },
        profile_img_filename: {
            type: DataTypes.STRING(1024),
        },
        website: {
            type: DataTypes.STRING(1024),
        },
        notification_email: {
            type: DataTypes.BOOLEAN
        },
        notification_only_week: {
            type: DataTypes.BOOLEAN
        },
        notification_new_products: {
            type: DataTypes.BOOLEAN
        },
        additional_information: {
            type: DataTypes.JSONB
        },
        opening_hours_new: {
            type: DataTypes.JSONB
        },
        categories: {
            type: DataTypes.JSONB
        },
        // subcategories: {
        //     type: DataTypes.JSONB
        // },
        slug: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        category_id: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        profile_image: {
            type: DataTypes.STRING(1024),
        },
        intro_image: {
            type: DataTypes.STRING(1024),
        },
        awards: {
            type: DataTypes.JSONB
        },
        initials: {
            type: DataTypes.STRING(2),
        },
        activate_token: {
            type: DataTypes.STRING(1024),
        },
        activate_token_expiration: {
            type: DataTypes.DATE,
        },
        tags: {
            type: DataTypes.JSONB,
        },
        invoice: {
            type: DataTypes.JSONB,
        },
        // contacts: {
        //     type: DataTypes.JSONB,
        // },
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    })

    user.belongsTo(fastify.db.models.category, {
        foreignKey: {
            name: 'category_id',
            type: DataTypes.INTEGER
        },
        targetKey: 'id',
        as: 'category'
    })

    user.hasMany(fastify.db.models.farmer_rating, {
        foreignKey: {
            name: 'farmer_id',
            type: DataTypes.INTEGER
        },
        targetKey: 'id',
        as: 'rating'
    })

    user.belongsTo(fastify.db.models.region, {
        foreignKey: {
            name: 'region_id',
            type: DataTypes.INTEGER
        },
        targetKey: 'region_id',
        as: 'region'
    })

    user.hasMany(fastify.db.models.favorite, {
        foreignKey: {
            name: 'user_id',
            type: DataTypes.INTEGER
        },
        targetKey: 'id',
        as: 'favorites'
    })

    // const password = "123456"
    // const hash = await argon2.hash(password, {type: argon2.argon2id})
    // const uuid = uuidv4()
    // user.update({  password_new: hash }, { where: 
    //     {
    //         id: 3903
    //     }
    // })

  }

  module.exports = model
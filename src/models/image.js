const { DataTypes, Sequelize } = require('sequelize')
const { v4: uuidv4 } = require('uuid')

async function model (fastify, options) {

    const image = fastify.db.define('image', {
        // Model attributes are defined here
        img_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        product_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        user_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        path: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        filename: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        alt: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        // uuid: {
        //     type: DataTypes.UUID,
        // },
        // active: {
        //     type: DataTypes.BOOLEAN
        // }
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    })

    // category.sync({ force: true })
  }

  module.exports = model
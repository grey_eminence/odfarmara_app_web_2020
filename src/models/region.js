const { DataTypes, Sequelize } = require('sequelize')

async function model (fastify, options) {

    const region = fastify.db.define('region', {
        // Model attributes are defined here
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        region_id: {
            type: DataTypes.INTEGER,
        },
        name: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        icon: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        active: {
            type: DataTypes.BOOLEAN
        }
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    })

    // region.sync({ force: true })
  }

  module.exports = model
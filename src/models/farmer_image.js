const { DataTypes, Sequelize } = require('sequelize')

async function model (fastify, options) {

    const farmer_image = fastify.db.define('farmer_image', {
        logo_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        company_logo: {
            type: DataTypes.STRING(1024)
            // allowNull defaults to true
        },
        user_id: {
            type: DataTypes.INTEGER
            // allowNull defaults to true
        },
    },
    {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        tableName: 'upload_company_logo'
    })
  }

  module.exports = model
const { DataTypes, Sequelize } = require('sequelize')

async function model (fastify, options) {

    const farmer_rating = fastify.db.define('farmer_rating', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        user_id: {
            type: DataTypes.INTEGER
            // allowNull defaults to true
        },
        farmer_id: {
            type: DataTypes.INTEGER
            // allowNull defaults to true
        },
        rating: {
            type: DataTypes.FLOAT
            // allowNull defaults to true
        },
    },
    {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        tableName: 'farmer_rating'
    })

    // farmer_rating.belongsTo(fastify.db.models.user, {
    //     foreignKey: {
    //         name: 'farmer_id'
    //     },
    //     targetKey: 'id',
    //     as: 'farmer'
    // })
  }

  module.exports = model
const { DataTypes, Sequelize } = require('sequelize')

async function model (fastify, options) {

    const product = fastify.db.define('product', {
        // Model attributes are defined here
        old_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        title: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        description: {
            type: DataTypes.TEXT,
        },
        short_description: {
            type: DataTypes.TEXT,
        },
        // seo_title: {
        //     type: DataTypes.STRING,
        // },
        // seo_description: {
        //     type: DataTypes.STRING,
        // },
        // seo_keywords: {
        //     type: DataTypes.TEXT,
        // },
        slug: {
            type: DataTypes.STRING(1024),
            allowNull: true
        },
        // images: {
        //     type: DataTypes.JSON,
        //     allowNull: true
        // },
        category_id: {
            type: DataTypes.INTEGER
        },
        subcategory_id: {
            type: DataTypes.INTEGER
        },
        user_id: {
            type: DataTypes.INTEGER
        },
        region_id: {
            type: DataTypes.INTEGER
        },
        price: {
            type: DataTypes.DECIMAL(10, 2)
        },
        price_for: {
            type: DataTypes.STRING
        },
        quantity: {
            type: DataTypes.INTEGER
        },
        min_quantity: {
            type: DataTypes.INTEGER
        },
        unit: {
            type: DataTypes.INTEGER
        },
        created: {
            type: DataTypes.DATE,
            allowNull: true
        },
        start_date: {
            type: DataTypes.DATEONLY,
            allowNull: true
        },
        end_date: {
            type: DataTypes.DATEONLY,
            allowNull: true
        },
        uuid: {
            type: DataTypes.UUID,
        },
        counter_click: {
            type: DataTypes.INTEGER
        },
        counter_view: {
            type: DataTypes.INTEGER
        },
        external: {
            type: DataTypes.BOOLEAN
        },
        external_url: {
            type: DataTypes.STRING
        },
        nutritions: {
            type: DataTypes.JSONB,
        },
        allergens: {
            type: DataTypes.JSONB,
        },
        tags: {
            type: DataTypes.JSONB,
        },
        flags: {
            type: DataTypes.JSONB,
        },
        unlimited: {
            type: DataTypes.BOOLEAN
        },
        active: {
            type: DataTypes.TINYINT 
        },
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at'  
    })

    // const product_tag = fastify.db.define('product_tag', {
    //     // Model attributes are defined here
    //     id: {
    //         type: DataTypes.INTEGER,
    //         primaryKey: true,
    //         autoIncrement: true
    //     },
    //     product_id: {
    //         type: DataTypes.INTEGER,
    //         allowNull: false,
    //         onDelete: 'CASCADE',
    //         references: {
    //             model: fastify.db.models.product,
    //             key: 'old_id',
    //         }
    //     },
    //     tag_id: {
    //         type: DataTypes.INTEGER,
    //         allowNull: false,
    //         references: {
    //             model: fastify.db.models.tag,
    //             key: 'id'
    //         }
    //     },
    // }, {
    //     createdAt: 'created_at',
    //     updatedAt: 'updated_at',
    // })

    // product_tag.belongsTo(fastify.db.models.product, {
    //     foreignKey: {
    //         name: 'product_id',
    //     },
    //     targetKey: "old_id",
    // });

    // product_tag.sync({ force: true });

    product.hasMany(fastify.db.models.image, {
        foreignKey: {
            name: 'product_id',
            type: DataTypes.INTEGER
        },
        targetKey: 'old_id',
        as: 'image'
    })

    // product.belongsToMany(fastify.db.models.tag, {
    //     foreignKey: 'product_id',
    //     through: product_tag,
    // });
    // fastify.db.models.tag.belongsToMany(product, {
    //     foreignKey: 'tag_id',
    //     through: product_tag,
    // });

    product.belongsTo(fastify.db.models.user, {
        foreignKey: {
            name: 'user_id'
        },
        targetKey: 'id',
        as: 'farmer'
    })

    product.belongsTo(fastify.db.models.category, {
        foreignKey: {
            name: 'category_id'
        },
        targetKey: 'id',
        as: 'category'
    })

    product.belongsTo(fastify.db.models.category, {
        foreignKey: {
            name: 'subcategory_id'
        },
        targetKey: 'id',
        as: 'subcategory'
    })

    // product.belongsTo(fastify.db.models.category, {
    //     foreignKey: {
    //         name: 'subcategory_id'
    //     },
    //     targetKey: 'id',
    //     as: 'subcategory_id'
    // })
    
    //product.sync({ force: true })
  }

  module.exports = model
const { DataTypes, Sequelize } = require('sequelize')

async function model (fastify, options) {

    const recipe = fastify.db.define('recipe', {
        // Model attributes are defined here
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        category_id: {
            type: DataTypes.INTEGER,
        },
        title: {
            type: DataTypes.STRING(1024)
            // allowNull defaults to true
        },
        slug: {
            type: DataTypes.STRING(1024)
            // allowNull defaults to true
        },
        tags: {
            type: DataTypes.JSONB
            // allowNull defaults to true
        },
        perex: {
            type: DataTypes.TEXT
            // allowNull defaults to true
        },
        contents: {
            type: DataTypes.TEXT
            // allowNull defaults to true
        },
        img: {
            type: DataTypes.STRING
            // allowNull defaults to true
        },
        time:{
            type: DataTypes.JSONB,
        },
        calories: {
            type: DataTypes.INTEGER
        },
        ingredients:{
            type: DataTypes.JSONB,
        },
        procedure:{
            type: DataTypes.JSONB,
        },
        active: {
            type: DataTypes.BOOLEAN
        },
        counter_view: {
            type: DataTypes.INTEGER
        },
        counter_click: {
            type: DataTypes.INTEGER
        },
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    })

    recipe.belongsTo(fastify.db.models.category, {
        foreignKey: {
            name: 'category_id'
        },
        targetKey: 'id',
        as: 'category'
    })

    // recipe.sync({ force: true })
  }

  module.exports = model
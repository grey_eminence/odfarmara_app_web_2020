const { DataTypes, Sequelize } = require('sequelize')
const argon2 = require('argon2')
const { v4: uuidv4 } = require('uuid')

async function model (fastify, options) {

    const favorite = fastify.db.define('favorite', {
        // Model attributes are defined here
        follow_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        user_id: {
            type: DataTypes.INTEGER,
        },
        farmar_id: {
            type: DataTypes.INTEGER,
        },
        id_offer: {
            type: DataTypes.INTEGER
        }
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
    })
    
    // favorite.sync({ force: true })
  }

  module.exports = model
const { DataTypes, Sequelize } = require('sequelize')

async function model (fastify, options) {

    const page = fastify.db.define('page', {
        // Model attributes are defined here
        slug: {
            type: DataTypes.STRING(1024),
        },
        content: {
            type: DataTypes.TEXT
        },
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    })
    
    // const t = await Sequelize.transaction();
    // page.sync({ force: true })
    // await t.commit();  
  }

  module.exports = model
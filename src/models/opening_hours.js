const { DataTypes, Sequelize } = require('sequelize')

async function model (fastify, options) {

    const opening_hours = fastify.db.define('opening_hours', {
        // Model attributes are defined here
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        farmer_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        // farmer_uuid: {
        //     type: DataTypes.UUID,
        // },
        monday_from: {
            type: DataTypes.STRING
        },
        monday_to: {
            type: DataTypes.STRING
        },
        tuesday_from: {
            type: DataTypes.STRING
        },
        tuesday_to: {
            type: DataTypes.STRING
        },
        wednesday_from: {
            type: DataTypes.STRING
        },
        wednesday_to: {
            type: DataTypes.STRING
        },
        thursday_from: {
            type: DataTypes.STRING
        },
        thursday_to: {
            type: DataTypes.STRING
        },
        friday_from: {
            type: DataTypes.STRING
        },
        friday_to: {
            type: DataTypes.STRING
        },
        saturday_from: {
            type: DataTypes.STRING
        },
        saturday_to: {
            type: DataTypes.STRING
        },
        sunday_from: {
            type: DataTypes.STRING
        },
        sunday_to: {
            type: DataTypes.STRING
        },
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    })
    
    // const t = await Sequelize.transaction();
    // opening_hours.sync({ force: true })
    // await t.commit();  
  }

  module.exports = model
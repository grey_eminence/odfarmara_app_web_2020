const { DataTypes, Sequelize } = require('sequelize')
const argon2 = require('argon2')
const { v4: uuidv4 } = require('uuid')

async function model (fastify, options) {

    const city = fastify.db.define('city', {
        // Model attributes are defined here
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
        },
        slug: {
            type: DataTypes.STRING,
        },
        zip: {
            type: DataTypes.STRING,
        },
        region: {
            type: DataTypes.TEXT,
        }
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    })

    // fastify.db.models.user.hasMany(tag, {
    //     foreignKey: 'old_id',
    //     targetKey: 'user_id',
    // });
    
    //city.sync({ force: true });
  }

  module.exports = model
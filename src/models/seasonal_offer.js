const { DataTypes, Sequelize } = require('sequelize')
const argon2 = require('argon2')
const { v4: uuidv4 } = require('uuid')

async function model (fastify, options) {

    const seasonalOffer = fastify.db.define('seasonal_offer', {
        // Model attributes are defined here
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        slug: {
            type: DataTypes.STRING,
            allowNull: false
        },
        description: {
            type: DataTypes.TEXT,
        },
        tags: {
            type: DataTypes.JSONB,
        },
        flags: {
            type: DataTypes.JSONB,
        },
        categories: {
            type: DataTypes.JSONB,
        },
        display_from: {
            type: DataTypes.DATEONLY
        },
        display_to: {
            type: DataTypes.DATEONLY
        },
        active: {
            type: DataTypes.BOOLEAN
        },
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    })

    // seasonalOffer.sync({ force: true })
  }

  module.exports = model
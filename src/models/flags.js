const { DataTypes, Sequelize } = require('sequelize')
const argon2 = require('argon2')
const { v4: uuidv4 } = require('uuid')

async function model (fastify, options) {

    const flag = fastify.db.define('flag', {
        // Model attributes are defined here
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        slug: {
            type: DataTypes.STRING,
            allowNull: false
        },
        description: {
            type: DataTypes.TEXT,
        },
        system: {
            type: DataTypes.BOOLEAN,
        },
        active: {
            type: DataTypes.BOOLEAN
        },
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    })

    // flag.sync({ force: true })
  }

  module.exports = model
const { Sequelize, Op } = require('sequelize')

async function routes (fastify, options) {
    fastify.get('/home/ponuka/:id/:slug', async (request, reply) => {

      const id = request.params.id;
      const user =  request.user;

      const product = await fastify.db.models.product.findOne({
        include: [ 
          { model: fastify.db.models.image, as: "image"},
          { 
            model: fastify.db.models.user, 
            as: "farmer"
          },
        ],
        where: {
          old_id: id
        },
      })

      if (!product) {
        reply.code(404).view('/pages/404');
      }
      else{

        const user_id = product.user_id;

        let farm_products = await fastify.db.models.product.findAll({
          limit: 2,
          include: [ 
            { model: fastify.db.models.image, as: "image"},
          ],
          where: {
            active: '1',
            user_id: user_id,
            [Op.not]: [
              { old_id: id }
            ]
          },
          order: [
            ['created', 'DESC'], ['image', 'img_id', 'ASC']
          ]
        })

        let new_products = await fastify.db.models.product.findAll({
          limit: 5,
          include: [ 
            // { model: fastify.db.models.image, as: "image", required: true },
            { model: fastify.db.models.image, as: "image" },
            { model: fastify.db.models.user, as: "farmer", required: true },
          ],
          where: { 
            active: '1',
          },
          order: [
            ['created', 'DESC'], ['image', 'img_id', 'ASC']
          ]
        });

        let vegetable_products = await fastify.db.models.product.findAll({
          limit: 2,
          include: [ 
            // { model: fastify.db.models.image, as: "image", required: true },
            { model: fastify.db.models.image, as: "image" },
            { model: fastify.db.models.user, as: "farmer", required: true },
            { 
              model: fastify.db.models.category, 
              required: true,
              as: "category" ,
              where: {
                id: 1
              }
            },
          ],
          where: { 
            active: '1',
          },
          order: [
            ['created', 'DESC'], ['image', 'img_id', 'ASC']
          ]
        });

        let fruit_products = await fastify.db.models.product.findAll({
          limit: 2,
          include: [ 
            // { model: fastify.db.models.image, as: "image", required: true },
            { model: fastify.db.models.image, as: "image" },
            { model: fastify.db.models.user, as: "farmer", required: true },
            { 
              model: fastify.db.models.category, 
              required: true,
              as: "category" ,
              where: {
                id: 2
              }
            },
          ],
          where: { 
            active: '1',
          },
          order: [
            ['created', 'DESC'], ['image', 'img_id', 'ASC']
          ]
        });

        let herbs_products = await fastify.db.models.product.findAll({
          limit: 2,
          include: [ 
            // { model: fastify.db.models.image, as: "image", required: true },
            { model: fastify.db.models.image, as: "image" },
            { model: fastify.db.models.user, as: "farmer", required: true },
            { 
              model: fastify.db.models.category, 
              required: true,
              as: "category" ,
              where: {
                id: 3
              }
            },
          ],
          where: { 
            active: '1',
          },
          order: [
            ['created', 'DESC'], ['image', 'img_id', 'ASC']
          ]
        });

        let meat_products = await fastify.db.models.product.findAll({
          limit: 2,
          include: [ 
            // { model: fastify.db.models.image, as: "image", required: true },
            { model: fastify.db.models.image, as: "image" },
            { model: fastify.db.models.user, as: "farmer", required: true },
            { 
              model: fastify.db.models.category, 
              required: true,
              as: "category" ,
              where: {
                id: 4
              }
            },
          ],
          where: { 
            active: '1',
          },
          order: [
            ['created', 'DESC'], ['image', 'img_id', 'ASC']
          ]
        });

        let bakery_products = await fastify.db.models.product.findAll({
          limit: 1,
          include: [ 
            // { model: fastify.db.models.image, as: "image", required: true },
            { model: fastify.db.models.image, as: "image" },
            { model: fastify.db.models.user, as: "farmer", required: true },
            { 
              model: fastify.db.models.category, 
              required: true,
              as: "category" ,
              where: {
                id: 5
              }
            },
          ],
          where: { 
            active: '1',
          },
          order: [
            ['created', 'DESC'], ['image', 'img_id', 'ASC']
          ]
        });

        // console.log('farm_products', farm_products);

        // create product view
        let created_product_view = await fastify.db.models.product_view.create({ 
          product_id: id,
        })

        let favorite = false;
        if(user){
          let favoriteItem = await fastify.db.models.favorite.findOne({
            where: {
              user_id: user.id,
              id_offer: id
            },
          })

          if(favoriteItem){
            favorite = true;
          }
        }

        console.log('created_product_view', created_product_view);

        let system_nutrition = null;
        if(product.category_id){
          system_nutrition = await fastify.db.models.nutrition_value.findOne({
            where: { category_id: product.category_id },
          });
        }

        let item_nutritions = {};
        let item_has_nutritions = false;
        if(product.nutritions && product.nutritions.type === 'custom'){
          item_has_nutritions = true;
          item_nutritions = product.nutritions.values;
        }

        if(product.nutritions && product.nutritions.type === 'system'){
          if(system_nutrition){
            item_has_nutritions = true;
            item_nutritions = system_nutrition;
          }
        }

        let item_has_allergens = false;
        if(product.allergens && product.allergens.length > 0){
          item_has_allergens = true;
        }

        console.log("product.tags", product.tags);

        let item_min_price = product.price;
        if(product.min_quantity){
          item_min_price = product.price * (product.min_quantity / product.unit);
        }

        reply.view('/pages/product', { 
          title: product.title + ' - Odfarmara.sk',
          description: product.description,
          keywords: 'odfarmara, ponuka, farma, farmár, domáce produkty',
          id: id,
          root: fastify.config.ROOT,
          farmer_id: user_id,
          user: user,
          item: product,
          item_min_price: item_min_price,
          item_nutritions: item_nutritions,
          item_has_nutritions: item_has_nutritions,
          item_has_allergens: item_has_allergens,
          favorite: favorite,
          new_products: new_products,
          vegetable_products: vegetable_products,
          fruit_products: fruit_products,
          herbs_products: herbs_products,
          meat_products: meat_products,
          bakery_products: bakery_products,
          farm_products: farm_products
        })
      }

    })
  }
  
  module.exports = routes
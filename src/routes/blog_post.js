const { Op } = require("sequelize");

async function routes (fastify, options) {
  
    fastify.get('/cms/blogdetails/:id/:slug', async (request, reply) => {

      const id = request.params.id; 

      const article = await fastify.db.models.article.findOne({
        include: [ 
          { model: fastify.db.models.category, as: "category" },
        ],
        where: { 
          article_id: id,
        },
      });

      if (!article) {
        reply.code(404).view('/pages/404');
      }
      else{
        const categories = await fastify.db.models.category.findAll({
          where: { parent_id: {[Op.is]: null}, type: "blog" },
        })

        let baseUrl = fastify.config.ROOT + "/blog";

        reply.view('/pages/blog_post', { 
          title: article.title + ' - Odfarmara.sk',
          article: article,
          categories: categories,
          baseUrl: baseUrl,
          current_url: request.url,
        })
      }
    })
  }
  
  module.exports = routes
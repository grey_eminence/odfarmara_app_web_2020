
const { v4: uuidv4 } = require('uuid')
const Joi = require('joi')
const myCustomJoi = Joi.extend(require('joi-phone-number'))
const { Op } = require("sequelize")

async function routes (fastify, options) {
  
    fastify.post('/form/newsletter', async (request, reply) => {
  
        const email = request.body.email;

        // validation schema
        const schema = Joi.object({
          email: Joi.string().email().required().messages({
            'string.empty': 'pole email je povinné pole',
            'string.email': 'pole email musí obsahovať platný email',
            'any.required': 'pole email je povinné pole'
          }),
        });

        // validation errors
        let validation_error = false;
        try {
            const validation = await schema.validateAsync({ email: email }, {abortEarly: false});
            console.log('validation', validation);
        }
        catch (err) {
            console.log('validation error', err);
            validation_error = err;
        }

        if(validation_error){

            let validation_error_sanitized = {};
            for(var key in validation_error.details){
                let message = validation_error.details[key].message;
                let path = validation_error.details[key].path[0];

                validation_error_sanitized[path] = message;
            }

            console.log('validation_error_sanitized', validation_error_sanitized);

            reply.send({ status: 'validationError' });
        }

        const exists = await fastify.db.models.newsletter.findOne({ 
          where: { email: email } 
        });

        console.log('exists', exists);

        if(!exists){
            await fastify.db.models.newsletter.create({ 
                email: email,
                // uuid: uuidv4(),
                active: '1'
            });

            reply.send({ status: 'ok' });
        }
        else{
            reply.send({ status: 'exists' });
        }
  
    })

    fastify.post('/form/farmer/contact', async (request, reply) => {
  
        const id = request.body.id;
        const name = request.body.name;
        const email = request.body.email;
        const phone = request.body.phone;
        const message = request.body.message;

        const user = request.user;

        const { mailer } = fastify;

        // validation schema
        const schema = myCustomJoi.object({
          id: myCustomJoi.number().greater(10).required().messages({
            'any.empty': 'parameter id je povinný',
            'any.required': 'parameter id je povinný'
          }),
          name: myCustomJoi.string().required().messages({
            'string.empty': 'pole meno je povinné pole',
            'string.required': 'pole meno je povinné pole'
          }),
          email: myCustomJoi.string().email().required().messages({
            'string.empty': 'pole email je povinné pole',
            'string.required': 'pole email je povinné pole',
            'string.email': 'pole email musí obsahovať platný email',
          }),
          phone: myCustomJoi.string().phoneNumber({ defaultCountry: 'SK', format: 'international', strict: true }).messages({
            'string.empty': 'pole telefón je povinné pole',
            'phoneNumber.invalid': 'pole telefón musí mať platný formát telefónneho čísla',
            'string.required': 'pole telefón je povinné pole'
          }),
          message: myCustomJoi.string().required().messages({
            'string.empty': 'pole komentár je povinné pole',
            'string.required': 'pole komentár je povinné pole'
          }),
        });

        let processedSuccess = false;

        // validation errors
        let validation_error = false;
        let validation_error_sanitized = {};
        try {
            const validation = await schema.validateAsync({ 
              id: id, 
              name: name, 
              email: email, 
              phone: phone, 
              message: message
            }, {abortEarly: false});
            console.log('validation', validation);
        }
        catch (err) {
            console.log('validation error', err);
            validation_error = err;
        }

        if(validation_error){
            
            for(var key in validation_error.details){
                let message = validation_error.details[key].message;
                let path = validation_error.details[key].path[0];

                validation_error_sanitized[path] = message;
            }

            console.log('validation_error_sanitized', validation_error_sanitized);

            // reply.send({ status: 'validationErrors' });

            // reply.view("/pages/password_reset", { validationErrors: validation_error_sanitized, email: email });
        }
        else{
          //get farmer email
          const farmer = await fastify.db.models.user.findOne({
            where: {
              id: id
            },
          })

          if(farmer){

            processedSuccess = true;

            const farmerEmail = farmer.email;
            console.log('farmerEmail', farmerEmail);

            let content = "<p>Meno: " + name + ", email: " + email + ", telefón: " + phone + "</p>";
            content += "<p>"+message+"</p>";

            let inboxData = {
              content: content,
              farmar_id: farmer.id,
              user_id: user.id,
              status: '0',
              seen: '0',
              seen_farmar: '0',
              hidden_to: '0',
              flag: '0',
              created_by: user.id,
            };
            
            const inbox = await fastify.db.models.inbox.create(inboxData);

            //send message to mail
            mailer.sendMail({
            // to: 'marian.michalovic@greyeminence.sk',
            to: farmerEmail,
            subject: 'Nová správa',
            text: 'hello world !'
            }, (errors, info) => {
            if (errors) {
                fastify.log.error(errors)

                reply.status(500)
                return {
                status: 'error',
                message: 'Something went wrong'
                }
            }

            // reply.send({ status: 'ok' })
            })

          }
        }

        //save form submission to db

        //save message to db

        let resultData = {
          id: id,
          name: name,
          email: email,
          phone: phone,
          message: message,
        };

        if(processedSuccess){
          resultData.success_message = "Ďakujeme za odoslanie správy!";
        }

        if(validation_error){
          resultData.validationErrors = validation_error_sanitized;
        }

        reply.view("/elements/content/form/farmer_contact", resultData);
  
    })

    fastify.post('/form/product/demand', async (request, reply) => {
  
        const id = request.body.id;
        const qty = request.body.qty;
        const message = request.body.message;

        const user = request.user;

        if(!user){
          reply.send({ status: 'userLogout' });
        }
        else{
          // validation schema
          const schema = Joi.object({
            id: Joi.number().greater(0).required().messages({
              'any.empty': 'parameter id je povinný',
              'any.required': 'parameter id je povinný'
            }),
            qty: Joi.number().greater(0).required().messages({
              'any.empty': 'pole počet je povinné pole',
              'any.required': 'pole počet je povinné pole'
            }),
          });

          // validation errors
          let validation_error = false;
          let validation_error_sanitized = false;
          try {
              const validation = await schema.validateAsync({ id: id, qty: qty }, {abortEarly: false});
              console.log('validation', validation);
          }
          catch (err) {
              console.log('validation error', err);
              validation_error = err;
          }

          if(validation_error){

              validation_error_sanitized = {};
              
              for(var key in validation_error.details){
                  let message = validation_error.details[key].message;
                  let path = validation_error.details[key].path[0];

                  validation_error_sanitized[path] = message;
              }

              console.log('validation_error_sanitized', validation_error_sanitized);

              reply.send({ status: 'validationErrors' });

              // reply.view("/pages/password_reset", { validationErrors: validation_error_sanitized, email: email });
          }
          else{

            const { mailer } = fastify;

            //get farmer email
            const product = await fastify.db.models.product.findOne({
                include: [ 
                  { model: fastify.db.models.image, as: "image"},
                  { 
                    model: fastify.db.models.user, 
                    as: "farmer"
                  },
                ],
                where: {
                  old_id: id
                },
            })

            let content = message;
            let farmar_id = null;
            if(product){
              farmar_id = product.farmer.id;
            }
            let user_id = user.id;
            let created = new Date().toISOString();

            let demandData = {
              confirmed: '0',
              id_offer: product.old_id,
              quantity: qty,
              created: created,
              created_by: user_id,
              finished: '0',
              farma_seen: '0',
              user_seen: '0',
              price: product.price,
              price_for: product.price_for,
            };

            const demand = await fastify.db.models.demand.create(demandData);

            let inboxData = {
              content: content,
              id_demand: demand.id_demand,
              farmar_id: farmar_id,
              user_id: user_id,
              status: '0',
              seen: '0',
              seen_farmar: '0',
              hidden_to: '0',
              flag: '0',
              created_by: user_id,
            };
            
            const inbox = await fastify.db.models.inbox.create(inboxData);

            const mailText = "";
            const mailHtml = await fastify.view("/email/new_request");

            if(product){
                //send message to mail
                mailer.sendMail({
                to: 'marian.michalovic@greyeminence.sk',
                subject: 'Nový dopyt',
                text: mailText,
                html: mailHtml,
                }, (errors, info) => {
                if (errors) {
                    fastify.log.error(errors)
            
                    reply.status(500)
                    return {
                    status: 'error',
                    message: 'Something went wrong'
                    }
                }
              })
            }

            reply.send({ status: 'ok' });
          }
        }
        
        //save form submission to db

        //save message to db

        // const exists = await fastify.db.models.newsletter.findOne({ where: { email: email } });
        // if(!exists){
        //     await fastify.db.models.newsletter.create({ 
        //         email: email,
        //         uuid: uuidv4(),
        //         active: '1'
        //     });

        //     reply.send({ status: 'ok' });
        // }
        // else{
        //     reply.send({ status: 'exists' });
        // }

        //reply.view("/elements/content/product/qty-selector", { success_message: "Ďakujeme za odoslanie!", id: id, item: product });

        
  
    })

    fastify.post('/form/product/demand/success/qty', async (request, reply) => {

        const id = request.body.id;
        const qty = request.body.qty;
        const message = request.body.message;

        const product = await fastify.db.models.product.findOne({
            include: [ 
              { model: fastify.db.models.image, as: "image"},
              { 
                model: fastify.db.models.user, 
                as: "farmer"
              },
            ],
            where: {
              old_id: id
            },
        })

        let item_min_price = product.price;
        if(product.min_quantity){
          item_min_price = product.price * (product.min_quantity / product.unit);
        }

        reply.view("/elements/content/product/qty-selector", { 
          success_message: "Ďakujeme za odoslanie dopytu!", 
          id: id, 
          qty: qty,
          message: message,
          item: product,
          item_min_price: item_min_price
        });
    })

    fastify.post('/form/product/demand/success/modal', async (request, reply) => {

        const id = parseInt(request.body.id);
        const qty = parseInt(request.body.qty);
        const message = request.body.message;

        const user = request.user;
        let userLogout = false; 

        if(!user){
          userLogout = true; 
        }

        // validation schema
        const schema = Joi.object({
          id: Joi.number().greater(0).required().messages({
            'any.empty': 'parameter id je povinný',
            'any.required': 'parameter id je povinný'
          }),
          qty: Joi.number().greater(0).required().messages({
            'any.empty': 'pole počet je povinné pole',
            'any.required': 'pole počet je povinné pole'
          }),
        });

        // validation errors
        let validation_error = false;
        let validation_error_sanitized = false;
        try {
            const validation = await schema.validateAsync({ id: id, qty: qty }, {abortEarly: false});
            console.log('validation', validation);
        }
        catch (err) {
            console.log('validation error', err);
            validation_error = err;
        }

        if(validation_error){

            validation_error_sanitized = {};
            
            for(var key in validation_error.details){
                let message = validation_error.details[key].message;
                let path = validation_error.details[key].path[0];

                validation_error_sanitized[path] = message;
            }

            console.log('validation_error_sanitized', validation_error_sanitized);

            // reply.view("/pages/password_reset", { validationErrors: validation_error_sanitized, email: email });
        }

        const product = await fastify.db.models.product.findOne({
            include: [ 
              { model: fastify.db.models.image, as: "image"},
              { 
                model: fastify.db.models.user, 
                as: "farmer"
              },
            ],
            where: {
              old_id: id
            },
        })

        let item_min_price = product.price;
        if(product.min_quantity){
          item_min_price = product.price * (product.min_quantity / product.unit);
        }

        reply.view("/elements/content/form/product_demand_modal", { 
          id: id, 
          qty: qty,
          message: message,
          item: product,
          item_min_price: item_min_price,
          validationErrors: validation_error_sanitized,
          userLogout: userLogout
        });
        
    })

    fastify.get('/form/product/favorite/:id', async (request, reply) => {

      const product_id = request.params.id;
      const user =  request.user;
      let favorite_value = false;

      if(user){

        const product = await fastify.db.models.product.findOne({
            where: {
              old_id: product_id
            },
        })

        const favorite = await fastify.db.models.favorite.findOne({
          where: {
            user_id: user.id,
            id_offer: product_id
          },
        })

        console.log('favorite', favorite);

        // exist
        if(favorite){
          await fastify.db.models.favorite.destroy({ 
            where: {
              user_id: user.id,
              id_offer: product_id
            },
          });
          
        }
        // not exist
        else{
          favorite_value = true;
          // delete
          await fastify.db.models.favorite.create({ 
            user_id: user.id, 
            id_offer: product_id,
            farmar_id: product.user_id
          });
        }

        reply.send({ status: 'ok' });
      }
      else{
        reply.send({ status: 'logout' });
      }

    })

    fastify.get('/form/farmer/favorite/:id', async (request, reply) => {

      const favorite_id = request.params.id;
      const user =  request.user;
      let favorite_value = false;

      if(user){

        const farmer = await fastify.db.models.user.findOne({
            where: {
              id: favorite_id
            },
        })

        const favorite = await fastify.db.models.favorite.findOne({
          where: {
            user_id: user.id,
            farmar_id: favorite_id,
            id_offer: {[Op.is]: null},
          },
        })

        console.log('favorite', favorite);

        // exist
        if(favorite){
          await fastify.db.models.favorite.destroy({ 
            where: {
              user_id: user.id,
              farmar_id: favorite_id,
              id_offer: {[Op.is]: null}
            },
          });
          
        }
        // not exist
        else{
          favorite_value = true;
          // delete
          await fastify.db.models.favorite.create({ 
            user_id: user.id, 
            farmar_id: favorite_id
          });
        }

        reply.send({ status: 'ok' });
      }
      else{
        reply.send({ status: 'logout' });
      }

    })
  }
  
  module.exports = routes

const { Op, QueryTypes, Sequelize } = require("sequelize");
var URI = require('urijs')

async function routes (fastify, options) {
    fastify.get('/home/profil/:id/:slug', async (request, reply) => {

      const id = parseInt(request.params.id);
      const user =  request.user;
      let farm;

      try {
        farm = await fastify.db.models.user.findOne({
          // include: [ 
          //   { model: fastify.db.models.image, as: "image"},
          //   { 
          //     model: fastify.db.models.user, 
          //     as: "farmer"
          //   },
          // ],
          where: {
            id: id,
            privilegs: 'farmer'
          },
        })
      } catch (error) {
        console.error(error);
      }

      if (!farm) {
        reply.code(404).view('/pages/404');
      }
      else{

        let products = await fastify.db.models.product.findAll({
          include: [ 
            { model: fastify.db.models.image, as: "image", required: true },
            { model: fastify.db.models.category, as: "subcategory" },
          ],
          where: { 
            active: '1',
            user_id: id
          },
          order: [
            ['created', 'DESC'], ['image', 'img_id', 'ASC']
          ]
        });

        const farmerCategories = await fastify.db.models.category.findAll({
          where: { parent_id: {[Op.is]: null}, type: "farmer" },
        })

        let opening_hours = {};
        if(farm.opening_hours_new){

          let day_1 = "";
          let day_2 = "";
          let day_3 = "";
          let day_4 = "";
          let day_5 = "";
          let day_6 = "";
          let day_7 = "";

          if(farm.opening_hours_new.oh_1_from_h != 0 || farm.opening_hours_new.oh_1_from_m != 0 || farm.opening_hours_new.oh_1_to_h != 0 || farm.opening_hours_new.oh_1_to_m != 0){
            let oh_1_from_h = farm.opening_hours_new.oh_1_from_h.toString().length > 1 ? farm.opening_hours_new.oh_1_from_h : "0" + farm.opening_hours_new.oh_1_from_h;
            let oh_1_from_m = farm.opening_hours_new.oh_1_from_m.toString().length > 1 ? farm.opening_hours_new.oh_1_from_m : "0" + farm.opening_hours_new.oh_1_from_m;
            let oh_1_to_h = farm.opening_hours_new.oh_1_to_h.toString().length > 1 ? farm.opening_hours_new.oh_1_to_h : "0" + farm.opening_hours_new.oh_1_to_h;
            let oh_1_to_m = farm.opening_hours_new.oh_1_to_m.toString().length > 1 ? farm.opening_hours_new.oh_1_to_m : "0" + farm.opening_hours_new.oh_1_to_m;
            day_1 = oh_1_from_h + ":" + oh_1_from_m + "-" + oh_1_to_h + ":" + oh_1_to_m;
          }
          else{
            day_1 = "zatvorené";
          }

          if(farm.opening_hours_new.oh_2_from_h != 0 || farm.opening_hours_new.oh_2_from_m != 0 || farm.opening_hours_new.oh_2_to_h != 0 || farm.opening_hours_new.oh_2_to_m != 0){
            let oh_2_from_h = farm.opening_hours_new.oh_2_from_h.toString().length > 1 ? farm.opening_hours_new.oh_2_from_h : "0" + farm.opening_hours_new.oh_2_from_h;
            let oh_2_from_m = farm.opening_hours_new.oh_2_from_m.toString().length > 1 ? farm.opening_hours_new.oh_2_from_m : "0" + farm.opening_hours_new.oh_2_from_m;
            let oh_2_to_h = farm.opening_hours_new.oh_2_to_h.toString().length > 1 ? farm.opening_hours_new.oh_2_to_h : "0" + farm.opening_hours_new.oh_2_to_h;
            let oh_2_to_m = farm.opening_hours_new.oh_2_to_m.toString().length > 1 ? farm.opening_hours_new.oh_2_to_m : "0" + farm.opening_hours_new.oh_2_to_m;
            day_2 = oh_2_from_h + ":" + oh_2_from_m + "-" + oh_2_to_h + ":" + oh_2_to_m;
          }
          else{
            day_2 = "zatvorené";
          }

          if(farm.opening_hours_new.oh_3_from_h != 0 || farm.opening_hours_new.oh_3_from_m != 0 || farm.opening_hours_new.oh_3_to_h != 0 || farm.opening_hours_new.oh_3_to_m != 0){
            let oh_3_from_h = farm.opening_hours_new.oh_3_from_h.toString().length > 1 ? farm.opening_hours_new.oh_3_from_h : "0" + farm.opening_hours_new.oh_3_from_h;
            let oh_3_from_m = farm.opening_hours_new.oh_3_from_m.toString().length > 1 ? farm.opening_hours_new.oh_3_from_m : "0" + farm.opening_hours_new.oh_3_from_m;
            let oh_3_to_h = farm.opening_hours_new.oh_3_to_h.toString().length > 1 ? farm.opening_hours_new.oh_3_to_h : "0" + farm.opening_hours_new.oh_3_to_h;
            let oh_3_to_m = farm.opening_hours_new.oh_3_to_m.toString().length > 1 ? farm.opening_hours_new.oh_3_to_m : "0" + farm.opening_hours_new.oh_3_to_m;
            day_3 = oh_3_from_h + ":" + oh_3_from_m + "-" + oh_3_to_h + ":" + oh_3_to_m;
          }
          else{
            day_3 = "zatvorené";
          }

          if(farm.opening_hours_new.oh_4_from_h != 0 || farm.opening_hours_new.oh_4_from_m != 0 || farm.opening_hours_new.oh_4_to_h != 0 || farm.opening_hours_new.oh_4_to_m != 0){
            let oh_4_from_h = farm.opening_hours_new.oh_4_from_h.toString().length > 1 ? farm.opening_hours_new.oh_4_from_h : "0" + farm.opening_hours_new.oh_4_from_h;
            let oh_4_from_m = farm.opening_hours_new.oh_4_from_m.toString().length > 1 ? farm.opening_hours_new.oh_4_from_m : "0" + farm.opening_hours_new.oh_4_from_m;
            let oh_4_to_h = farm.opening_hours_new.oh_4_to_h.toString().length > 1 ? farm.opening_hours_new.oh_4_to_h : "0" + farm.opening_hours_new.oh_4_to_h;
            let oh_4_to_m = farm.opening_hours_new.oh_4_to_m.toString().length > 1 ? farm.opening_hours_new.oh_4_to_m : "0" + farm.opening_hours_new.oh_4_to_m;
            day_4 = oh_4_from_h + ":" + oh_4_from_m + "-" + oh_4_to_h + ":" + oh_4_to_m;
          }
          else{
            day_4 = "zatvorené";
          }

          if(farm.opening_hours_new.oh_5_from_h != 0 || farm.opening_hours_new.oh_5_from_m != 0 || farm.opening_hours_new.oh_5_to_h != 0 || farm.opening_hours_new.oh_5_to_m != 0){
            let oh_5_from_h = farm.opening_hours_new.oh_5_from_h.toString().length > 1 ? farm.opening_hours_new.oh_5_from_h : "0" + farm.opening_hours_new.oh_5_from_h;
            let oh_5_from_m = farm.opening_hours_new.oh_5_from_m.toString().length > 1 ? farm.opening_hours_new.oh_5_from_m : "0" + farm.opening_hours_new.oh_5_from_m;
            let oh_5_to_h = farm.opening_hours_new.oh_5_to_h.toString().length > 1 ? farm.opening_hours_new.oh_5_to_h : "0" + farm.opening_hours_new.oh_5_to_h;
            let oh_5_to_m = farm.opening_hours_new.oh_5_to_m.toString().length > 1 ? farm.opening_hours_new.oh_5_to_m : "0" + farm.opening_hours_new.oh_5_to_m;
            day_5 = oh_5_from_h + ":" + oh_5_from_m + "-" + oh_5_to_h + ":" + oh_5_to_m;
          }
          else{
            day_5 = "zatvorené";
          }

          if(farm.opening_hours_new.oh_6_from_h != 0 || farm.opening_hours_new.oh_6_from_m != 0 || farm.opening_hours_new.oh_6_to_h != 0 || farm.opening_hours_new.oh_6_to_m != 0){
            let oh_6_from_h = farm.opening_hours_new.oh_6_from_h.toString().length > 1 ? farm.opening_hours_new.oh_6_from_h : "0" + farm.opening_hours_new.oh_6_from_h;
            let oh_6_from_m = farm.opening_hours_new.oh_6_from_m.toString().length > 1 ? farm.opening_hours_new.oh_6_from_m : "0" + farm.opening_hours_new.oh_6_from_m;
            let oh_6_to_h = farm.opening_hours_new.oh_6_to_h.toString().length > 1 ? farm.opening_hours_new.oh_6_to_h : "0" + farm.opening_hours_new.oh_6_to_h;
            let oh_6_to_m = farm.opening_hours_new.oh_6_to_m.toString().length > 1 ? farm.opening_hours_new.oh_6_to_m : "0" + farm.opening_hours_new.oh_6_to_m;
            day_6 = oh_6_from_h + ":" + oh_6_from_m + "-" + oh_6_to_h + ":" + oh_6_to_m;
          }
          else{
            day_6 = "zatvorené";
          }

          if(farm.opening_hours_new.oh_7_from_h != 0 || farm.opening_hours_new.oh_7_from_m != 0 || farm.opening_hours_new.oh_7_to_h != 0 || farm.opening_hours_new.oh_7_to_m != 0){
            let oh_7_from_h = farm.opening_hours_new.oh_7_from_h.toString().length > 1 ? farm.opening_hours_new.oh_7_from_h : "0" + farm.opening_hours_new.oh_7_from_h;
            let oh_7_from_m = farm.opening_hours_new.oh_7_from_m.toString().length > 1 ? farm.opening_hours_new.oh_7_from_m : "0" + farm.opening_hours_new.oh_7_from_m;
            let oh_7_to_h = farm.opening_hours_new.oh_7_to_h.toString().length > 1 ? farm.opening_hours_new.oh_7_to_h : "0" + farm.opening_hours_new.oh_7_to_h;
            let oh_7_to_m = farm.opening_hours_new.oh_7_to_m.toString().length > 1 ? farm.opening_hours_new.oh_7_to_m : "0" + farm.opening_hours_new.oh_7_to_m;
            day_7 = oh_7_from_h + ":" + oh_7_from_m + "-" + oh_7_to_h + ":" + oh_7_to_m;
          }
          else{
            day_7 = "zatvorené";
          }
          
          opening_hours.day_1 = day_1;
          opening_hours.day_2 = day_2;
          opening_hours.day_3 = day_3;
          opening_hours.day_4 = day_4;
          opening_hours.day_5 = day_5;
          opening_hours.day_6 = day_6;
          opening_hours.day_7 = day_7;
        }

        let gps = false;
        let gps_lat = "";
        let gps_lng = "";

        if(farm.gps && farm.gps.length){
          gps = true;
          let gps_arr = farm.gps.split(',');
          gps_lat = gps_arr[0];
          gps_lng = gps_arr[1];
        }

        let baseUrl = fastify.config.ROOT + "/farmari";

        let favorite = false;
        if(user){
          let favoriteItem = await fastify.db.models.favorite.findOne({
            where: {
              user_id: user.id,
              farmar_id: id,
              id_offer: {[Op.is]: null}
            },
          })

          // console.log('favoriteItem', favoriteItem);

          if(favoriteItem){
            favorite = true;
          }
        }

        const region = farm.region_id;
        let regionFarmQuery = {
          privilegs: 'farmer',
          active: 1
        };
        if(region){
          regionFarmQuery.region_id = region;
        }

        let regionFarm = await fastify.db.models.user.findAll({
          where: regionFarmQuery,
          order: [ [ Sequelize.fn('RANDOM') ] ],
          limit: 1 ,
        });

        if(regionFarm){
          regionFarm = regionFarm[0].dataValues;
        }

        // favorite farmers
        let favoriteFarmersIds = [];
        if(user && user.favorites && user.favorites.length){
          for(let a = 0; a < user.favorites.length; a++){
            if(!user.favorites[a].id_offer){
              favoriteFarmersIds.push(user.favorites[a].farmar_id);
            }
          }
        }

        let favoriteFarmers = await fastify.db.models.user.findAll({
          where: {
            id: {[Op.in]: favoriteFarmersIds }
          },
        });

        // categories from products
        let productCategoriesTemp = [];
        let productCategories = [];
        if(products && products.length){
          for(let a = 0; a < products.length; a++){
            let product = products[a];
            console.log('product.subcategory', product.subcategory);
            if(product.subcategory){
              let subcategory = product.subcategory;
              console.log('subcategory', subcategory);
              let subcategory_id = subcategory.id;
              let subcategory_name = subcategory.name;
              if(!productCategoriesTemp.includes(subcategory_id)){
                productCategoriesTemp.push(subcategory_id);
                productCategories.push({
                  id: subcategory_id,
                  name: subcategory_name
                });
              }
            }
          }
        }
        // categories from farmers
        if(farm.categories && farm.categories.length){

          // get farm categories
          let farmCategories = await fastify.db.models.category.findAll({
            where: {id: {[Op.in]: farm.categories}},
            raw: true
          });

          if(farmCategories){
            for(let a = 0; a < farmCategories.length; a++){
              let item = farmCategories[a];
              console.log("item", item);
              let id = item.id;
              let name = item.name;
              if(!productCategoriesTemp.includes(id)){
                productCategories.push({
                  id: id,
                  name: name
                });
              }
            }
          }

        }

        // user has already rated
        let user_rated = false;
        let user_rating = 0;
        if(user){
          let farmer_rating = await fastify.db.models.farmer_rating.findOne({
            where: {
              user_id: user.id,
              farmer_id: id
            },
            raw: true
          });

          if(farmer_rating){
            user_rated = true;
            user_rating = farmer_rating.rating;
          }
        }

        // console.log('user_rating', user_rating);

        // console.log('farm', farm);
        // console.log('productCategories', productCategories);

        let name = farm.company_name;
        if(!name){
          name = farm.name;
        }
        if(!name){
          name = 'Farmár ' + farm.id;
        }

        reply.view('/pages/farm', {
          title: name + ' - Odfarmara.sk',
          description: farm.short_description,
          keywords: 'odfarmara ponuka farma farmar domace produkty',
          root: fastify.config.ROOT,
          item: farm,
          id: id,
          products: products,
          favorite: favorite,
          favoriteFarmers: favoriteFarmers,
          opening_hours: opening_hours,
          baseUrl: baseUrl,
          current_url: fastify.config.ROOT + '/farmari',
          farmerCategories: farmerCategories,
          productCategories: productCategories,
          regionFarm: regionFarm,
          gps: gps,
          gps_lat: gps_lat,
          gps_lng: gps_lng,
          user_rated: user_rated,
          user_rating: user_rating,
        })
      }
    })

    fastify.post('/farma/hodnotit', async (request, reply) => {

      const id = parseInt(request.body.id);
      const rating_value = parseInt(request.body.rating);
      const user =  request.user;
      let value = 0;

      const rated = await fastify.db.models.farmer_rating.findOne({
        where: {
          user_id: user.id, 
          farmer_id: id,
        }
      });

      if(!rated && user){

        let data = { 
          user_id: user.id, 
          farmer_id: id,
          rating: rating_value
        };

        console.log('data', data); 

        // add item to rating
        let created_rating = await fastify.db.models.farmer_rating.create(data);

        // console.log("ideeeeeeeee");

        // update rating
        const result = await fastify.db.query("SELECT AVG(rating) as rating FROM farmer_rating WHERE farmer_rating.farmer_id = " + id, { type: QueryTypes.SELECT });
        const rating = result[0].rating;

        console.log("result", result);
        
        if(rating){
            value = rating;
            let afterDecimal = parseFloat(value) % 1;
            let decimal = value - afterDecimal;
            afterDecimal = Math.round(afterDecimal * 2) / 2;
            value = decimal + afterDecimal;
        }

        let updated_user = await fastify.db.models.user.update({ rating_avg: value }, {
            where: {
                id: id
            }
        });

      }

      // return average rating
      reply.send({ status: 'ok' })
      
    })
  
  }


  
  module.exports = routes
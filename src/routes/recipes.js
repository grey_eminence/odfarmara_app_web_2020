const { Op } = require("sequelize")
const solr = require('solr-client')
var URI = require('urijs')

async function routes (fastify, options) {
    fastify.get('/recepty', async (request, reply) => {

      const latestRecipes = await fastify.db.models.recipe.findAll({
        include: [ 
          { model: fastify.db.models.category, as: "category" },
        ],
        limit: 3,
        order: [
          ['created_at', 'DESC']
        ],
        where: { 
          active: '1',
        },
      });

      const popularRecipes = await fastify.db.models.recipe.findAll({
        include: [ 
          { model: fastify.db.models.category, as: "category" },
        ],
        limit: 3,
        order: [
          ['counter_view', 'DESC']
        ],
        where: { 
          active: '1',
        },
      });

      const categories = await fastify.db.models.category.findAll({
        where: { parent_id: {[Op.is]: null}, type: "recipe" },
      })

      let docs = [];
      let query_text = "*:*";
      let ipp = 9;
      let startItem = 0;

      let uri = new URI(request.url);
      let urlQuery = uri.search(true);
      let c = urlQuery.c;
      let mc = urlQuery.mc;
      let e = urlQuery.e;
      let q = urlQuery.q;
      let p = urlQuery.p;
      let r = urlQuery.r;
      let pages = false;

      // selected categories
      let baseUrl = fastify.config.ROOT + "/recepty";

      let sort = {ge_created_date : 'desc'};

      let searching = false;
      if(q || mc){
        searching = true;
  
        // default page
        if(!p){
          p = 1;
        }
        startItem = (ipp * (p-1));
  
        if(q){
          query_text = 'ge_name:"' + q + '", ' + 'ge_description:"' + q + '", ' + 'ge_ingredient:"' + q + '"';
          sort = {};
        }

        //Create a client
        const client = solr.createClient({
          path: '/solr/odfarmara_recipes'
        });

        const query = client
        .query()
        .q(query_text)
        .start(startItem)
        .sort(sort)
        .rows(ipp);

        if(mc){
          query.matchFilter('ge_category', mc );
        }

        query.matchFilter('ge_active', true);

        const data = await client.search(query);
        docs = data.response.docs;

        pages = Math.floor(data.response.numFound / ipp);
        let pages_modulo = data.response.numFound % ipp;
        if(pages_modulo){
          pages++;
        }
      }

      const recipes = await fastify.db.models.recipe.findAll({
        where: { 
          active: '1',
        },
      });

      let tags = [];
      if(recipes && recipes.length){
        for(let a = 0; a < recipes.length; a++){
          let item = recipes[a];
          let item_tags = item.tags;

          if(item_tags && item_tags.length){
            for(let b = 0; b < item_tags.length; b++){
              let tag_obj = {slug: item_tags[b].ge_slug, name: item_tags[b].ge_name};

              if (tags.filter(e => e.slug === item_tags[b].ge_slug).length === 0) {
                tags.push(tag_obj);
              }

              // if(!tags.includes(tag_obj)){
              //   tags.push(tag_obj);
              // }
            }
          }
        }
      }

      console.log('tags', tags);

      reply.view('/pages/recipes', { 
        title: 'Recepty - Odfarmara.sk',
        categories: categories,
        latestRecipes: latestRecipes,
        popularRecipes: popularRecipes,
        baseUrl: baseUrl,
        current_url: request.url,
        searching: searching,
        q: q,
        p: parseInt(p),
        c: c,
        mc: mc,
        e: e,
        r: r,
        docs: docs,
        tags: tags,
        pages: parseInt(pages),
      })
    })
  }
  
  module.exports = routes
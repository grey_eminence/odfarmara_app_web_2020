
const { Op } = require("sequelize");
var URI = require('urijs');

async function routes (fastify, options) {
  
  fastify.get('/tag/:slug', async (request, reply) => {

    const user =  request.user;
    const slug =  request.params.slug;

    // favorite products
    let favoriteProducts = [];
    if(user && user.favorites.length){
      for(let a = 0; a < user.favorites.length; a++){
        if(user.favorites[a].id_offer){
          favoriteProducts.push(user.favorites[a].id_offer);
        }
      }
    }

    const tag = await fastify.db.models.tag.findOne({
      where: { 
        active: '1',
        slug: slug,
      },
    });

    const recipes = await fastify.db.models.recipe.findAll({
      include: [ 
        { 
          model: fastify.db.models.category, 
          required: true,
          as: "category" ,
        },
      ],
      where: { 
        active: '1',
        tags: {
          [Op.contains]: [
            {'ge_slug': slug}
          ]
        },
      },
    });

    const products = await fastify.db.models.product.findAll({
      include: [ 
        { model: fastify.db.models.image, as: "image" },
        { model: fastify.db.models.user, as: "farmer" },
        { model: fastify.db.models.category, as: "category" },
      ],
      where: { 
        active: '1',
        tags: {
          [Op.contains]: [
            {'ge_slug': slug}
          ]
        },
      },
      order: [
        ['created', 'DESC'], ['image', 'img_id', 'ASC']
      ],
      limit: 4
    });

    const farmers = await fastify.db.models.user.findAll({
      where: { 
        active: '1',
        privilegs: "farmer",
        tags: {
          [Op.contains]: [
            {'ge_slug': slug}
          ]
        },
      },
    });

    const articles = await fastify.db.models.article.findAll({
      include: [ 
        { 
          model: fastify.db.models.category, 
          required: true,
          as: "category" ,
        },
      ],
      where: { 
        active: '1',
        tags: {
          [Op.contains]: [
            {'ge_slug': slug}
          ]
        },
      },
    });

    console.log('recipes', recipes);

    let docs = [];
    let query_text = "*:*";
    let ipp = 12;
    let startItem = 0;

    let uri = new URI(request.url);
    let urlQuery = uri.search(true);
    let c = urlQuery.c;
    let mc = urlQuery.mc;
    let e = urlQuery.e;
    let q = urlQuery.q;
    let p = urlQuery.p;
    let r = urlQuery.r;


    reply.view('/pages/tag', { 
      title: 'Odfarmara.sk',
      favoriteProducts: favoriteProducts,
      recipes: recipes,
      products: products,
      farmers: farmers,
      articles: articles,
      docs: docs,
      q: q,
      p: parseInt(p),
      c: c,
      mc: mc,
      e: e,
      r: r,
      // tags: tags,
      tag: tag
    })

  })
}

module.exports = routes
const solr = require('solr-client')
var URI = require('urijs')
const { Op, Sequelize } = require("sequelize")

async function routes (fastify, options) {

    fastify.get('/cms/farmarivovasomokoli', async (request, reply) => {
      reply.redirect('/farmari');
    })

    fastify.get('/farmari', async (request, reply) => {

      const user = request.user;

      let docs = [];
      let all_docs = [];
      let query_text = "*:*";
      let ipp = 6;
      let startItem = 0;

      let uri = new URI(request.url);
      let urlQuery = uri.search(true);
      let c = urlQuery.c;
      let mc = urlQuery.mc;
      let e = urlQuery.e;
      let q = urlQuery.q;
      let p = urlQuery.p;
      let r = urlQuery.r;

      // selected categories
      let selectedCategories = [];
      let selectedMainCategories = [];
      let categories = [];
      categories = await fastify.db.models.category.findAll({
        where: { parent_id: {[Op.is]: null} },
        include: [ 
          { model: fastify.db.models.category, as: "subcategory"},
        ],
      });

      let randomFarmQuery = {
        privilegs: 'farmer',
        active: 1
      };

      let randomFarm = await fastify.db.models.user.findAll({
        where: randomFarmQuery,
        order: [ [ Sequelize.fn('RANDOM') ] ],
        limit: 1 ,
      });

      if(randomFarm){
        randomFarm = randomFarm[0].dataValues;
      }
      
      if(c){
          
        for(let x = 0; x < categories.length; x++){
          let id = categories[x].id;
          let name = categories[x].name;
  
          if(Array.isArray(c)){
            if(c.includes(String(id))){
              selectedCategories.push({id: id, name: name, subcategory: false});
            }
          }
          else{
            if(c == String(id)){
              selectedCategories.push({id: id, name: name, subcategory: false});
            }
          }
  
          let subcategories = categories[x].subcategory;
          if(subcategories.length){
            for(let y = 0; y < subcategories.length; y++){

              let id = subcategories[y].id;
              let name = subcategories[y].name;
      
              if(Array.isArray(c)){
                if(c.includes(String(id))){
                  selectedCategories.push({id: id, name: name, subcategory: true});
                }
              }
              else{
                if(c == String(id)){
                  selectedCategories.push({id: id, name: name, subcategory: true});
                }
              }

            }
          }
        }
      }

      if(mc){

        for(let x = 0; x < categories.length; x++){
          let id = categories[x].id;
          let name = categories[x].name;
  
          if(Array.isArray(mc)){
            if(mc.includes(String(id))){
              selectedMainCategories.push({id: id, name: name, subcategory: false});
            }
          }
          else{
            if(mc == String(id)){
              selectedMainCategories.push({id: id, name: name, subcategory: false});
            }
          }
        }
        
      }

      // default page
      if(!p){
        p = 1;
      }
      startItem = (ipp * (p-1));

      let sort = {ge_created_date : 'desc'};
      
      if(q){
        query_text = 'ge_company_name_keyword:"' + q + '", ge_company_name:"' + q + '"';
        sort = {};
      }

      console.log('query_text', query_text);

      //Create a client
      const client = solr.createClient({
        path: '/solr/odfarmara_farmers'
      });


      const query = client
      .query()
      .q(query_text)
      .start(startItem)
      .sort(sort)
      .rows(ipp);

      // all items without pagination for map
      const query_all = client
      .query()
      .q(query_text)
      .start(0)
      .sort(sort)
      .rows(2147483647);

      // region filter
      if(r){
        query.matchFilter('ge_region', r );
        query_all.matchFilter('ge_region', r );
      }

      if(mc){
        query.matchFilter('ge_category', mc );
        query_all.matchFilter('ge_category', mc );
      }

      if(c){
        query.matchFilter('ge_subcategory', c );
        query_all.matchFilter('ge_subcategory', c );
      }

      // from - to date filter
      // query.matchFilter('ge_start_date', '[* TO NOW]');
      // query.matchFilter('ge_end_date', '[NOW TO *]');

      // active filter
      query.matchFilter('ge_active', true);
      query_all.matchFilter('ge_active', true );

      // regions
      let countryRegions = await fastify.db.models.region.findAll({
        where: { 
          active: '1',
        }
      });

      // selected regions
      let selectedRegions = [];
      if(r){
        for(let x = 0; x < countryRegions.length; x++){
          let id = countryRegions[x].region_id;
          let name = countryRegions[x].name;
          let icon = countryRegions[x].icon;
  
          if(Array.isArray(r)){
            if(r.includes(String(id))){
              selectedRegions.push({id: id, name: name, icon: icon});
            }
          }
          else{
            if(r == String(id)){
              selectedRegions.push({id: id, name: name, icon: icon});
            }
          }
        }
      }

      const data = await client.search(query);
      docs = data.response.docs;

      const data_all = await client.search(query_all);
      all_docs = data_all.response.docs;

      // set pages count
      let pages = Math.floor(data.response.numFound / ipp);
      let pages_modulo = data.response.numFound % ipp;
      if(pages_modulo){
        pages++;
      }

      // favorite farmers
      let favoriteFarmers = [];
      if(user && user.favorites.length){
        for(let a = 0; a < user.favorites.length; a++){
          if(!user.favorites[a].id_offer){
            favoriteFarmers.push(user.favorites[a].farmar_id);
          }
        }
      }

      let favoriteFarmersItems = await fastify.db.models.user.findAll({
        where: {
          id: {[Op.in]: favoriteFarmers }
        },
      });

      console.log('farms', docs);

      let baseUrl = fastify.config.ROOT + "/farmari";

      reply.view('/pages/farmers', { 
        title: 'Farmári - Odfarmara.sk',
        farms: docs,
        farms_all: all_docs,
        q: q,
        p: parseInt(p),
        c: c,
        mc: mc,
        e: e,
        r: r,
        pages: parseInt(pages),
        startItem: startItem,
        numFound: data.response.numFound,
        baseUrl: baseUrl,
        current_url: request.url,
        selectedCategories: selectedCategories,
        selectedMainCategories: selectedMainCategories,
        selectedRegions: selectedRegions,
        countryRegions: countryRegions,
        localCategories: categories,
        favoriteFarmers: favoriteFarmers,
        favoriteFarmersItems: favoriteFarmersItems,
        randomFarm: randomFarm
      })
    })
  }
  
  module.exports = routes
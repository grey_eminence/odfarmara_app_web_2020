
async function routes (fastify, options) {
    fastify.get('/kontakt', async (request, reply) => {
      reply.view('/pages/contact', { 
        title: 'Odfarmara.sk',
      })
    })
  }
  
  module.exports = routes
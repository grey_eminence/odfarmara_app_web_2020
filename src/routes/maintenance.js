
const solr = require('solr-client')
const argon2 = require('argon2')
const moment = require('moment')
const { Op } = require('sequelize')
const { v4: uuidv4 } = require('uuid')
const v = require('voca')
const xlsx = require('node-xlsx').default;
const _ = require('lodash');
const { nanoid } = require('nanoid')

async function routes (fastify, options) {
    fastify.get('/maintenance/slugify', async (request, reply) => {


      const slugify = require('slugify')

      const categories = await fastify.db.models.category.findAll({raw: true})

      categories.forEach(async function(category){

        const slug = slugify(category.name, {
          lower: true,      // convert to lower case, defaults to `false`
          locale: 'sk'       // language code of the locale to use
        })

        await fastify.db.models.category.update({ slug: slug }, {
          where: {
            id: category.id
          }
        })

      })

      reply.send({ status: 'ok' })

    })

    fastify.get('/maintenance/product-data', async (request, reply) => {


      const slugify = require('slugify')

      const products = await fastify.db.models.product.findAll({raw: true})

      products.forEach(async function(product){
        
        const product_id = product.id;
        const old_id = product.old_id;
        const user = await fastify.db.models.user.findOne({ where: { old_id: old_id } });
        if(user){
          const user_uuid = user.uuid;

          await fastify.db.models.product.update({ user_uuid: user_uuid }, {
            where: {
              id: product_id
            }
          })
        }

      })

      reply.send({ status: 'ok' })

    })

    fastify.get('/maintenance/user-data', async (request, reply) => {


      const users = await fastify.db.models.user.findAll({raw: true})

      users.forEach(async function(user){
        
        const user_id = user.id;
        const user_group_id = user.user_group_id;

        let privilegs = '';
        if(user_group_id == 2){
          privilegs = 'customer';
        }
        if(user_group_id == 3){
          privilegs = 'farmer';
        }

        if(user_group_id){

          await fastify.db.models.user.update({ privilegs: privilegs }, {
            where: {
              id: user_id
            }
          })
        }

      })

      reply.send({ status: 'ok' })

    })

    fastify.get('/maintenance/search/update-product-data', async (request, reply) => {

      const client = solr.createClient({
        path: '/solr/odfarmara_products'
      });

      client.autoCommit = true;
      
      // const items = await fastify.db.models.product.findAll({raw: true})

      let items = await fastify.db.models.product.findAll({
        include: [ 
          { model: fastify.db.models.image, as: "image", required: true },
          { model: fastify.db.models.user, as: "farmer", required: true },
          // { model: fastify.db.models.category, as: "category", required: true },
          // { model: fastify.db.models.subcategory, as: "subcategory", required: true },
        ],
        order: [
          ['created', 'DESC'], ['image', 'img_id', 'ASC']
        ],
      });

      let docs = [];
      items.forEach(async function(item){

        const id = item.old_id;
        const title = item.title;
        const description = item.description;
        let farmer = item.farmer.company_name;
        const category = item.category_id;
        const subcategory = item.subcategory_id;
        const price = item.price;
        const price_for = item.price_for;
        const quantity = item.quantity;
        const unit = item.unit;
        const active = item.active;
        const region = item.region_id;
        let start_date = item.start_date;
        let end_date = item.end_date;
        let created_date = item.created;
        const thumbnail = item.image[0].filename;
        const external = item.external;
        const external_url = item.external_url;
        const unlimited = item.unlimited;
        const flags = item.flags;

        if(!farmer){
          farmer = item.farmer.name;
        }

        if(!farmer){
          farmer = item.farmer.email;
        }

        console.log(item.farmer.name + ' / ' + item.farmer.company_name + ' / ' + farmer);

        // console.log('created_date', created_date);

        let m_start_date = moment.utc(start_date, "YYYY-MM-DD");
        let m_end_date = moment.utc(end_date, "YYYY-MM-DD");
        let m_created_date = moment.utc(created_date);

        start_date = m_start_date.toISOString();
        end_date = m_end_date.toISOString();
        created_date = m_created_date.toISOString();

        if(unlimited){
          start_date = null;
          end_date = null;
        }

        // const tags = ;
        // console.log('item', item.image[0].filename)

        // console.log('farmer', farmer);

        let doc = {
          id : id, 
          ge_id : {"set": id},
          ge_title : {"set": title},
          ge_description : {"set": description},
          ge_farmer : {"set": farmer},
          ge_category : {"set": category},
          ge_subcategory : {"set": subcategory},
          ge_price : {"set": price},
          ge_price_for : {"set": price_for},
          ge_quantity : {"set": quantity},
          ge_unit : {"set": unit},
          ge_active : {"set": active},
          ge_region : {"set": region},
          ge_thumbnail : {"set": thumbnail},
          ge_start_date : {"set": start_date},
          ge_end_date : {"set": end_date},
          ge_external : {"set": external},
          ge_external_url : {"set": external_url},
          ge_created_date : {"set": created_date},
        }

        if(flags && flags.length){

          let flag_ids = [];
          let flag_names = [];
          for (let index = 0; index < flags.length; index++) {
            const flag = flags[index];

            flag_ids.push(flag.id);
            flag_names.push(flag.ge_name);
          }

          doc.ge_flags = {"set": flag_ids};
          doc.ge_flag_names = {"set": flag_names};

        }

        docs.push(doc);

      })

      const obj = await client.add(docs);
      const commit = await client.commit();

      reply.send({ status: 'ok' })

    })

    fastify.get('/maintenance/search/update-allergens-data', async (request, reply) => {

      const client = solr.createClient({
        path: '/solr/odfarmara_allergens'
      });

      // const items = await fastify.db.models.product.findAll({raw: true})

      let items = await fastify.db.models.allergen.findAll({
        // include: [ 
        //   { model: fastify.db.models.user},
        // ],
      });

      console.log('items', items); 

      let docs = [];
      items.forEach(async function(item){

        const id = item.id;
        const name = item.name;
        const slug = item.slug;
        let description = item.description;
        let active = item.active;
        if(active){
          active = true;
        }
        else{
          active = false;
        }
        let created_date = item.created_at;
        let created_by = item.created_by;

        console.log('user', item.user);

        let m_created_date = moment(created_date);
        created_date = m_created_date.toISOString();

        let user_name = "";
        if(item.user){
          if(item.user.name == "" || item.user.name == null){
            user_name = item.user.company_name;
          }
          else{
            user_name = item.user.name;
          }
        }

        let doc = {
          id : id, 
          ge_id : {'set': id },
          ge_name : {'set': name },
          ge_slug :  {'set': slug },
          ge_active :  {'set': active },
          ge_created_date :  {'set': created_date },
          ge_user_name:  {'set': user_name },
        }

        docs.push(doc);
        
      })

      const obj = await client.add(docs);
      const commit = await client.commit();

      reply.send({ status: 'ok' })

    })

    fastify.get('/maintenance/search/update-nutritions-data', async (request, reply) => {

      const client = solr.createClient({
        path: '/solr/odfarmara_nutritions'
      });


      let items = await fastify.db.models.nutrition_value.findAll({
        // include: [ 
        //   { model: fastify.db.models.user},
        // ],
      });

      console.log('items', items); 

      let docs = [];
      for(let i = 0; i < items.length; i++){

        item = items[i].dataValues; 
        console.log('item', item);

        const id = item.id;
        const name = item.name;
        // const slug = item.slug;
        // let description = item.description;
        let active = item.active;
        if(active){
          active = true;
        }
        else{
          active = false;
        }
        let created_date = item.created_at;
        let created_by = item.created_by;

        let user = await fastify.db.models.user.findOne({
          where: {id:created_by}
        });

        console.log('user', user);
        user = user.dataValues;

        let m_created_date = moment(created_date);
        created_date = m_created_date.toISOString();

        let user_name = "";
        if(user){
          if(user.name == "" || user.name == null){
            user_name = user.company_name;
          }
          else{
            user_name = user.name;
          }
        }

        let doc = {
            id : id,
            ge_id : {'set': id},
            ge_name : {'set': name},
            ge_active : {'set': active},
            ge_created_date : {'set': created_date},
            ge_user_name: {'set': user_name},
        }

        console.log('doc', doc);

        docs.push(doc);
        
      }

      console.log('docs', docs);

      const obj = await client.add(docs);
      const commit = await client.commit();

      reply.send({ status: 'ok' })

    })

    fastify.get('/maintenance/search/update-flag-data', async (request, reply) => {

      const client = solr.createClient({
        path: '/solr/odfarmara_flags'
      });

      let items = await fastify.db.models.flag.findAll({
      });

      console.log('items', items); 

      let docs = [];
      items.forEach(async function(item){

        const id = item.id;
        const name = item.name;
        const slug = item.slug;
        let system = item.system;
        if(system){
          system = true;
        }
        else{
          system = false;
        }
        let active = item.active;
        if(active){
          active = true;
        }
        else{
          active = false;
        }

        let created_date = item.createdAt;

        let m_created_date = moment(created_date);
        created_date = m_created_date.toISOString();

        let doc = {
          id : id, 
          ge_id : {'set': id },
          ge_name : {'set': name },
          ge_slug : {'set': slug },
          ge_system : {'set': system },
          ge_active : {'set': active },
          ge_created_date : {'set': created_date },
        }

        docs.push(doc);
        
      })

      console.log('docs', docs);

      const obj = await client.add(docs);
      const commit = await client.commit();

      console.log('obj', obj);

      reply.send({ status: 'ok' })

    })

    fastify.get('/maintenance/search/update-tag-data', async (request, reply) => {

      const client = solr.createClient({
        path: '/solr/odfarmara_tags'
      });

      // const items = await fastify.db.models.product.findAll({raw: true})

      let items = await fastify.db.models.tag.findAll({
        include: [ 
          { model: fastify.db.models.user},
        ],
      });

      console.log('items', items); 

      let docs = [];
      items.forEach(async function(item){

        const id = item.id;
        const name = item.name;
        const slug = item.slug;
        let system = item.system;
        if(system){
          system = true;
        }
        else{
          system = false;
        }
        let active = item.active;
        if(active){
          active = true;
        }
        else{
          active = false;
        }
        let landing_page = item.landing_page;
        if(landing_page){
          landing_page = true;
        }
        else{
          landing_page = false;
        }

        let created_date = item.createdAt;

        console.log('user', item.user);

        let m_created_date = moment(created_date);
        created_date = m_created_date.toISOString();

        let user_name = "";
        if(item.user){
          if(item.user.name == "" || item.user.name == null){
            user_name = item.user.company_name;
          }
          else{
            user_name = item.user.name;
          }
        }

        let doc = {
          id : id, 
          ge_id : {'set': id },
          ge_name : {'set': name },
          ge_slug : {'set': slug },
          ge_system : {'set': system },
          ge_active : {'set': active },
          ge_front_page : {'set': landing_page },
          ge_created_date : {'set': created_date },
          ge_user_name: {'set': user_name },
        }

        docs.push(doc);
        
      })

      console.log('docs', docs);

      const obj = await client.add(docs);
      const commit = await client.commit();

      console.log('obj', obj);

      reply.send({ status: 'ok' })

    })

    fastify.get('/maintenance/search/update-seasonal-offer-data', async (request, reply) => {

      const client = solr.createClient({
        path: '/solr/odfarmara_seasonal_offers'
      });

      // const items = await fastify.db.models.product.findAll({raw: true})

      let items = await fastify.db.models.seasonal_offer.findAll({});

      console.log('items', items); 

      let docs = [];
      items.forEach(async function(item){

        const id = item.id;
        const name = item.name;
        const slug = item.slug;
        
        let active = item.active;
        if(active){
          active = true;
        }
        else{
          active = false;
        }

        const categories =  item.categories;
        const tags =  item.tags;
        let from_date =  item.display_from;
        let to_date =  item.display_to;

        console.log('from_date', from_date);
        console.log('to_date', to_date);

        let created_date = item.createdAt;

        let m_from_date = moment(from_date, "YYYY-MM-DD");
        from_date = m_from_date.toISOString();
        let m_to_date = moment(to_date, "YYYY-MM-DD");
        to_date = m_to_date.toISOString();
        let m_created_date = moment(created_date);
        created_date = m_created_date.toISOString();

        let tagsArr = [];
        if(tags && tags.length){
          for(let i = 0; i < tags.length; i++){
            let item = tags[i];
            tagsArr.push(item.id);
          }
        }

        let doc = {
          id : id, 
          ge_id : {'set': id },
          ge_name : {'set': name },
          ge_slug : {'set': slug },
          ge_category : {'set': categories },
          ge_tags : {'set': tagsArr },
          ge_active : {'set': active },
          ge_start_date : {'set': from_date },
          ge_end_date : {'set': to_date },
          ge_created_date : {'set': created_date },
        }

        docs.push(doc);
        
      })

      console.log('docs', docs);

      const obj = await client.add(docs);
      const commit = await client.commit();

      console.log('obj', obj);

      reply.send({ status: 'ok' })

    })

    fastify.get('/maintenance/password', async (request, reply) => {

      console.log('request.user', request.user);

      const password = "123456"
      const hash = await argon2.hash(password, {type: argon2.argon2id})

      reply.send({ password: hash })

    });

    fastify.get('/maintenance/tags', async (request, reply) => {

      await fastify.db.models.product.update({ 
        tags: [5,6]
      }, {
          where: {
            old_id: 67
          }
      })

      const product = await fastify.db.models.product.findOne({ where: { old_id: 67 } });

      const productFind = await fastify.db.models.product.findOne({
        where: {
          tags: {
            [Op.contains]: 5
          }
        }
      });

      reply.send({ 
        product: product,
        productFind: productFind,
       })

    });


    fastify.get('/maintenance/product', async (request, reply) => {

      const product = await fastify.db.models.product.findOne({ 
        where: { old_id: 806 },
        include: [ 
          { 
            model: fastify.db.models.tag,
          },
        ], 
      });

      reply.send({
        product: product,
      })

    });

    fastify.get('/maintenance/solr/delete', async (request, reply) => {

      let id = "8280";

      let doc = {
        id : id,
      }

      const client = solr.createClient({
          path: '/solr/odfarmara_farmers'
      });

      const query = 'id:' + id;
      // delete from solr
      const obj = await client.deleteByID(id);
      const commit = await client.commit();

      console.log('id', id);

      console.log('result', obj);

      reply.send({
        id: id,
      })

    });

    fastify.get('/maintenance/search/update-farmers-data', async (request, reply) => {

      const client = solr.createClient({
        path: '/solr/odfarmara_farmers'
      });
      client.autoCommit = true;

      let items = await fastify.db.models.user.findAll({
        include: [
          { model: fastify.db.models.region, as: 'region' },
        ],
        where: { 
          privilegs: 'farmer',
        },
      });
      
      let docs = [];
      for (let i=0; i<items.length; i++) {

        let item = items[i];

        // id, image, name, region, items count, account type, created at
        const id = item.id;
        const name = item.name;
        const company_name = item.company_name;
        const gps = item.gps;
        const profile_image = item.profile_image;
        const email = item.email;

        const region = item.region_id;
        let region_name = "";
        if(region){
          region_name = item.region.dataValues.name;
        }
        
        let active = item.active;
        let thumbnail = item.profile_image;
        
        const account_type = item.account_type;

        let created_date = item.created;
        if(!created_date){
          created_date = item.created_at;
        }
        let m_created_date = moment(created_date);
        created_date = m_created_date.toISOString();

        // let item_count = 0;
        let item_count = await fastify.db.models.product.count({
          where: { 
            user_id: id,
            active: 1
          }
        });

        let categories = [];
        let subcategories = [];

        let products = await fastify.db.models.product.findAll({
          where: { 
            user_id: id,
            active: 1
          }
        });

        if(products && products.length){

          for (let x=0; x<products.length; x++) {
            
            let category_id = products[x].category_id;
            let subcategory_id = products[x].subcategory_id;

            if(category_id){
              categories.push(category_id);
            }
            if(subcategory_id){
              subcategories.push(subcategory_id);
            }

          }

        }

        // add id from categories
        if(Array.isArray(item.categories) && item.categories.length){
          for (let y=0; y<item.categories.length; y++) {
            let id = item.categories[y].id;
            categories.push(id);
          }
        }

        // remove duplicates
        categories = [...new Set(categories)];
        subcategories = [...new Set(subcategories)];

        let doc = {
          id : id,
          ge_id: {"set": id},
          ge_name: {"set": name},
          ge_company_name: {"set": company_name},
          ge_company_name_keyword: {"set": company_name},
          ge_email: {"set": email},
          ge_active : {"set": active},
          ge_region : {"set": region},
          ge_region_name : {"set": region_name},
          ge_thumbnail : {"set": thumbnail},
          ge_item_count: {"set": item_count},
          ge_account_type: {"set": account_type},
          ge_created_date : {"set": created_date},
          ge_rating: {"set": item.rating_avg},
        }
        

        // console.log('doc', doc);

        if(categories && categories.length){
          doc.ge_category = {"set": categories};
        }

        if(subcategories && subcategories.length){
          doc.ge_subcategory = {"set": subcategories};
        }

        if(profile_image){
          doc.ge_profile_img = {"set": profile_image};
        }

        // console.log('gps', gps.length);

        //gps
        if(gps && gps.length){
          let gpsArray = gps.split(",");
          console.log('gpsArray', gpsArray);
          if(Array.isArray(gpsArray)){
            doc.ge_gps_lat = {"set": gpsArray[0]};
            doc.ge_gps_lng = {"set": gpsArray[1]};
          }
        }

        docs.push(doc);

      };

      console.log('docs', docs);

      const obj = await client.add(docs);
      const commit = await client.commit();

      reply.send({ status: 'ok' })

    })

    fastify.get('/maintenance/search/update-users-data', async (request, reply) => {

      const client = solr.createClient({
        path: '/solr/odfarmara_users'
      });
      client.autoCommit = true;

      let items = await fastify.db.models.user.findAll({
        where:{
          privilegs: {
            [Op.not]: 'farmer'
          },
        }
      });
      
      let docs = [];
      for (let i=0; i<items.length; i++) {

        let item = items[i];

        // id, image, name, region, items count, account type, created at
        const id = item.id;
        let name = item.name ? item.name : "";
        let company_name = item.company_name ? item.company_name : "";
        
        let active = item.active;
        let email = item.email;
        let thumbnail = item.profile_img_filename;
        if(!thumbnail){
          thumbnail = "";
        }

        let created_date = item.created;
        if(!created_date){
          created_date = item.created_at;
        }
        let m_created_date = moment(created_date);
        created_date = m_created_date.toISOString();

        let doc = {
          id : id,
          ge_id: {'set': id},
          ge_email: {"set": email},
          ge_name: {'set':name},
          ge_company_name: {'set':company_name},
          ge_active : {'set':active},
          ge_thumbnail : {'set':thumbnail},
          ge_created_date : {'set':created_date},
        }

        docs.push(doc);
      };

      console.log('docs', docs);

      const obj = await client.add(docs);
      const commit = await client.commit();

      reply.send({ status: 'ok' })

    })

    fastify.get('/maintenance/update-user-initials', async (request, reply) => {

      let items = await fastify.db.models.user.findAll();
      
      for (let i=0; i<items.length; i++) {

        let item = items[i];

        const id = item.id;
        const name = item.name;
        const company_name = item.company_name;
        let email = item.email;
        let initials = "";
        let names = [];

        if(name.length > 0){
          names = v(name).latinise().upperCase().words()
        }
        else{
          if(company_name.length > 0){
            names = v(company_name).latinise().upperCase().words()
          }
        }

        if(names.length > 0){
            for(let x=0; x < names.length; x++){
              if(x > 1){
                break;
              }
              let char = names[x].charAt(0);
              initials += char;
            }
        }

        if(initials.length == 0){
          initials = v(email).latinise().upperCase().charAt(0).value();
        }

        console.log('initials', initials);

        if(initials.length > 0){
          await fastify.db.models.user.update({ 
            initials: initials
          }, {
            where: {
              id: id
            }
          })
        }

      }

      reply.send({ status: 'ok' })

    })

    fastify.get('/maintenance/sync-opening-hours', async (request, reply) => {

      let items = await fastify.db.models.user.findAll();
      
      for (let i=0; i<items.length; i++) {

        let item = items[i];

        const id = item.id;
        let opening_hours_new = {};

        let oh = await fastify.db.models.opening_hours.findOne({
          where: {
            farmer_id: id
          }
        });

        if(oh){
          let monday_from = oh.monday_from;
          let monday_to = oh.monday_to;

          let tuesday_from = oh.tuesday_from;
          let tuesday_to = oh.tuesday_to;

          let wednesday_from = oh.wednesday_from;
          let wednesday_to = oh.wednesday_to;

          let thursday_from = oh.thursday_from;
          let thursday_to = oh.thursday_to;

          let friday_from = oh.friday_from;
          let friday_to = oh.friday_to;

          let saturday_from = oh.saturday_from;
          let saturday_to = oh.saturday_to;

          let sunday_from = oh.sunday_from;
          let sunday_to = oh.sunday_to;

          let used = false;
          if(monday_from || monday_to ||
            tuesday_from || tuesday_to  || 
            wednesday_from || wednesday_to ||
            thursday_from || thursday_to ||
            friday_from || friday_to ||
            saturday_from || saturday_to ||
            sunday_from || sunday_to
          ){
            used = true;
          }

          if(used){
            let opening_hours_new = {
              oh_1_from_h: monday_from,
              oh_1_from_m: 0,
              oh_1_to_h: monday_to,
              oh_1_to_m: 0,
              oh_2_from_h: tuesday_from,
              oh_2_from_m: 0,
              oh_2_to_h: tuesday_to,
              oh_2_to_m: 0,
              oh_3_from_h: thursday_from,
              oh_3_from_m: 0,
              oh_3_to_h: thursday_to,
              oh_3_to_m: 0,
              oh_4_from_h: wednesday_from,
              oh_4_from_m: 0,
              oh_4_to_h: wednesday_to,
              oh_4_to_m: 0,
              oh_5_from_h: friday_from,
              oh_5_from_m: 0,
              oh_5_to_h: friday_to,
              oh_5_to_m: 0,
              oh_6_from_h: saturday_from,
              oh_6_from_m: 0,
              oh_6_to_h: saturday_to,
              oh_6_to_m: 0,
              oh_7_from_h: sunday_from,
              oh_7_from_m: 0,
              oh_7_to_h: sunday_to,
              oh_7_to_m: 0,
            }

            await fastify.db.models.user.update({ 
              opening_hours_new: opening_hours_new
            }, {
              where: {
                id: id
              }
            })

          }
        }
        

      }

      reply.send({ status: 'ok' })

    })

    fastify.get('/maintenance/send-mail', async (request, reply) => {

      const { mailer } = fastify;

      mailer.sendMail({
        to: 'marian.michalovic@greyeminence.sk',
        subject: 'test',
        text: 'hello world !',
        html: 'hello world !',
      }, (errors, info) => {
        if (errors) {

          console.log(errors);
          fastify.log.error(errors)
    
          reply.status(500)
          return {
            status: 'error',
            message: 'Something went wrong'
          }
        }
    
        reply.send({ status: 'ok' })
      })

    })

    fastify.get('/maintenance/user/create', async (request, reply) => {

      // let newUser = await fastify.db.models.user.findOne({
      //   where: { email: 'info@odfarmara.sk' }
      // });

      // console.log('newUser', newUser);

      // let article = await fastify.db.models.user.update({ initials: 'RS' }, {
      //   where: {
      //     email: 'info@odfarmara.sk'
      //   }
      // });
      
      // const type = 'seller';
      // // const name = request.body.name;
      // const email = 'info@odfarmara.sk';
      const password = "31X9KzybJcF47pNoxRKZ6QXJT_";

      const hash = await argon2.hash(password, {type: argon2.argon2id});
      const uuid = uuidv4();

      const type = 'customer';
      const name = "Peter Nováčik";
      const email = "peter.novacik@unity.sk";

      let initials = "PN";

      let data = {
        username: email,
        email: email,
        active: "1",
        password_new: hash,
        uuid: uuid,
        privilegs: 'customer',
        admin: '1',
        initials: initials,
        name: name
      }

      // initials = v(email).latinise().upperCase().charAt(0).value();

      // if(type == "seller"){
      //   data.name = name;
      //   data.contact_name = name;
      //   data.contact_email = email;
      //   data.privilegs = 'farmer';

      //   // let names = v(name).latinise().upperCase().words();
      //   // if(names.length > 0){
      //   //   for(let x=0; x < names.length; x++){
      //   //     if(x > 1){
      //   //       break;
      //   //     }
      //   //     let char = names[x].charAt(0);
      //   //     initials += char;
      //   //   }
      //   // }
      // }

      // if(initials.length > 0){
      //   data.initials = initials;
      // }

      // data.admin = '1';

      console.log('data', data);
      console.log('type', type);

      // try {
      //   let newUser = await fastify.db.models.user.create(data);
      // }
      // catch (e) {
      //   console.log(e);
      // }

      // console.log('newUser', newUser);

      reply.send({ status: 'ok' })

    })

    fastify.get('/maintenance/product/create', async (request, reply) => {

      let data = {
        title: 'Jarna cibulka',
        category_id: 1,
        subcategory_id: 12,
        description: 'Jarna cibulka',
        short_description: 'Jarna cibulka',
        quantity: 50,
        min_quantity: 2,
        price: 55.55,
        price_for: 'ks',
        start_date: '2022-05-10',
        end_date: '2022-10-10',
        user_id: 3903,
        external: 0,
        active: 1,
        region_id: 1,
      }

      console.log('data', data);

      // save to db
      try {
      let created = await fastify.db.models.product.create(data);
      } catch (error) {
        console.log('error', error);
      }
      console.log('created', created);

      reply.send({ status: 'ok' })

    })

    fastify.get('/maintenance/update/category', async (request, reply) => {

      let categories = [{ "id": 222 }];

      let updated = await fastify.db.models.user.update({ 
        categories: categories 
      }, {
        where: {
          id: 6955
        }
      })
     
      reply.send({ status: updated })

    })

    fastify.get('/maintenance/search/update-recipes-data', async (request, reply) => {

      const client = solr.createClient({
        path: '/solr/odfarmara_recipes'
      });
      client.autoCommit = true;

      let items = await fastify.db.models.recipe.findAll({
        include: [ 
          { model: fastify.db.models.category, as: "category" },
        ],
      });
      
      let docs = [];
      for (let i=0; i<items.length; i++) {

        const item = items[i];

        const id = item.id;
        const name = item.title;
        let contents = item.perex + " " + item.contents;

        let active = item.active;
        let thumbnail = item.img;
        if(!thumbnail){
          thumbnail = "";
        }
        let created_date = item.created_at;
        let m_created_date = moment(created_date);
        created_date = m_created_date.toISOString();

        let category_id = item.category_id;

        let categories = [];
        categories.push(category_id);

        let ingredients = [];
        if(item.ingredients.length){
          for(let x=0; x < item.ingredients.length; x++){
            let title = item.ingredients[x].title;
            if(title){
              ingredients.push(title);
            }
          }
        }

        let doc = {
          id : id,
          ge_id: {'set': id},
          ge_name: {'set': name},
          ge_description: {'set': contents},
          ge_active : {'set': active},
          ge_thumbnail : {'set': thumbnail},
          ge_created_date : {'set': created_date},
          ge_ingredient: {'set': ingredients},
        }
        
        if(categories && categories.length){
          doc.ge_category = {'set': categories};
          doc.ge_category_name = {'set': item.category.name};
        }

        docs.push(doc);

      };

      console.log('docs', docs);

      const obj = await client.add(docs);
      const commit = await client.commit();

      reply.send({ status: 'ok' })

    })

    fastify.get('/maintenance/search/update-blog-data', async (request, reply) => {

      const client = solr.createClient({
        path: '/solr/odfarmara_blog'
      });
      client.autoCommit = true;

      let items = await fastify.db.models.article.findAll({
        include: [ 
          { model: fastify.db.models.category, as: "category" },
        ],
      });
      
      let docs = [];
      for (let i=0; i<items.length; i++) {

        const item = items[i];

        const id = item.article_id;
        const name = item.title;
        let contents = item.perex + " " + item.contents;

        let active = item.active;
        let thumbnail = item.blog_img;
        if(!thumbnail){
          thumbnail = "";
        }
        let created_date = item.date;
        let m_created_date = moment(created_date);
        created_date = m_created_date.toISOString();

        let category_id = item.cat_id;

        let categories = [];
        categories.push(category_id);

        let doc = {
          id : id,
          ge_id : {"set": id },
          ge_name: {"set": name },
          ge_description: {"set": contents },
          ge_active : {"set": active },
          ge_thumbnail : {"set": thumbnail },
          ge_created_date : {"set": created_date },
        }
        
        if(categories && categories.length){
          doc.ge_category = {"set": categories};
          doc.ge_category_name = {"set": item.category.name};
        }

        docs.push(doc);

      };

      console.log('docs', docs);

      const obj = await client.add(docs);
      const commit = await client.commit();

      reply.send({ status: 'ok' })

    })

    // fastify.get('/maintenance/test/user', async (request, reply) => {

    //   const user = await fastify.db.models.user.findOne({
    //     include: [ 
    //       { model: fastify.db.models.favorite, as: "favorites"},
    //     ],
    //     // raw: true,
    //     where: { id: 3418 }
    //   })

    //   reply.send({ user: user })
    
    // })

    fastify.get('/maintenance/map', async (request, reply) => {

      const axios = require('axios');

      let address = "Jánošíkova 86, 90101, Malacky, Slovenská republika";
      address = encodeURIComponent(address);

      let result = await axios.get('https://geocode.maps.co/search?q=' + address);
      console.log('result', result.data);

      reply.send({ result: 'ok' })
    
    })



    fastify.get('/maintenance/alter', async (request, reply) => {

    const [results, metadata] = await fastify.db.query("ALTER TABLE products ADD flags jsonb;");

    // let updated = await fastify.db.models.user.update({ 
    //   categories: [] 
    // }, {
    //   where: {
    //     id: 6955
    //   }
    // })

    reply.send({ result: 'ok' })
    
    })

    fastify.get('/maintenance/import/cities', async (request, reply) => {

      const slugify = require('slugify');

      let path = __dirname + "/../import/cities.xlsx";

      const workSheetsFromFile = xlsx.parse(path);
      console.log('workSheetsFromFile', workSheetsFromFile);

      for (let index = 0; index < workSheetsFromFile.length; index++) {
        const ws = workSheetsFromFile[index];
        if(ws.name == 'obce'){
          
          let data = ws.data;

          for (let x = 0; x < data.length; x++) {
            // header
            if (x == 0) {
              continue;
            }

            let name = data[x][1];
            let zip = data[x][3];
            if(zip){
              zip = zip.replace(/\s/g, "");
            }
            let region = data[x][7];

            // nitriansky_kraj
            if(region == 'NI'){
              region = 4;
            }
            // banskobystricky_kraj
            if(region == 'BC'){
              region = 1;
            }
            // trnavsky_kraj
            if(region == 'TA'){
              region = 7;
            }
            // presovsky_kraj
            if(region == 'PV'){
              region = 5;
            }
            // kosicky_kraj
            if(region == 'KI'){
              region = 3;
            }
            // trenciansky_kraj
            if(region == 'TC'){
              region = 6;
            }
            // bratislavsky_kraj
            if(region == 'BL'){
              region = 2;
            }
            // zilinsky_kraj
            if(region == 'ZI'){
              region = 2;
            }

            let slug = slugify(name, {
              lower: true,      // convert to lower case, defaults to `false`
              locale: 'sk'       // language code of the locale to use
            });

            // let created = await fastify.db.models.city.create({
            //   name: name,
            //   zip: zip,
            //   region: region,
            //   slug: slug,
            // });

            console.log(name, zip, region, slug);
          }

        }
      }
  
      reply.send({ result: 'ok' })
      
    })


    fastify.get('/maintenance/import/streets', async (request, reply) => {

      const slugify = require('slugify');

      let path = __dirname + "/../import/streets.xlsx";

      const workSheetsFromFile = xlsx.parse(path);
      console.log('workSheetsFromFile', workSheetsFromFile);

      for (let index = 0; index < workSheetsFromFile.length; index++) {
        const ws = workSheetsFromFile[index];
        if(ws.name == 'ulice'){

          let data = ws.data;
          let founded = 0;
          let lost = 0;

          for (let x = 0; x < data.length; x++) {
            // header
            if (x == 0) {
              continue;
            }

            let name = data[x][6];
            let zip = data[x][2];
            if(zip){
              zip = zip.replace(/\s/g, "");
            }

            // console.log( name + '-' + zip );
            
            let slug = slugify(name, {
              lower: true,      // convert to lower case, defaults to `false`
              locale: 'sk'       // language code of the locale to use
            });

            let region = '';
            let cityDb = await fastify.db.models.city.findOne({
              where: {
                slug: slug
              }
            });

            if(cityDb){
              founded++;
              region = cityDb.region;
            }
            else{
              if(slug == 'bratislava'){
                region = 2;
                founded++;
              }
              else if(slug == 'kosice'){
                region = 3;
                founded++;
              }
              else if(slug == 'nitrianske-hrnciarovce'){
                region = 4;
                founded++;
              }
              else{
                console.log('slug', slug);
                lost++;
              }
            }

            // console.log(name + ' - ' + zip + ' - ' + region + ' - ' + slug);

            // let created = await fastify.db.models.city.create({
            //   name: name,
            //   zip: zip,
            //   region: region,
            //   slug: slug,
            // });

          }

          console.log('founded', founded);
          console.log('lost', lost);

        }
      }

    })

    fastify.get('/maintenance/update-user-cemail', async (request, reply) => {


      const users = await fastify.db.models.user.findAll({raw: true})

      users.forEach(async function(user){
        
        const user_id = user.id;
        let contact_email = user.contact_email;
        if(!contact_email){
          contact_email = user.email;
        }

        await fastify.db.models.user.update({ contact_email: contact_email }, {
          where: {
            id: user_id
          }
        })

      })

      reply.send({ status: 'ok' })

    })

    fastify.get('/maintenance/contacts', async (request, reply) => {

      let user = request.user;
      let user_id = user.id;

      let data = {
        my_contacts : [ 7571, 7554 ],
        following_users : [ 7743, 7713 ],
        requesting_users : [ 7711 ],
      }

      await fastify.db.models.user.update({ contacts: data }, {
        where: {
          id: user_id
        }
      })


      reply.send({ status: 'ok' })

    })


    fastify.get('/maintenance/user/mm', async (request, reply) => {

        let name = 'Marián Michalovič';
        let username = 'marian.michalovic@greyeminence.sk';
        let email = 'marian.michalovic@greyeminence.sk';
        let active = "1";
        let admin = "1";
        let created = new Date();

        const password_hash = await argon2.hash('AZcIZJdFdcZjOsvG46jI_', { type: argon2.argon2id });

        let data = {
          name: name,
          username: username,
          password_new: password_hash,
          email: email,
          contact_email: email,
          active: active,
          privilegs: 'farmer',
          initials: 'MM',
          created: created,
          admin: admin
        }

        let newUser = await fastify.db.models.user.update(data, {
          where: {
            id: 3903
          }
        });

        reply.send({ status: 'ok' })

    })

    fastify.get('/maintenance/user/of', async (request, reply) => {

      let data = {
        admin: "1"
      }

      let newUser = await fastify.db.models.user.update(data, {
        where: {
          email: 'info@odfarmara.sk'
        }
      });

      reply.send({ status: 'ok' });

    });


    fastify.get('/maintenance/import/description', async (request, reply) => {

      let path = __dirname + "/../import/export.xlsx";

      const workSheetsFromFile = xlsx.parse(path);
      console.log('workSheetsFromFile', workSheetsFromFile);

      for (let index = 0; index < workSheetsFromFile.length; index++) {
        const ws = workSheetsFromFile[index];
        if(ws.name == 'export'){

          let data = ws.data;

          for (let x = 0; x < data.length; x++) {
            // header
            if (x == 0) {
              continue;
            }

            let id = data[x][0];
            let description = data[x][1];

            console.log(id);

            // let updated = await fastify.db.models.product.update({
            //   description: description
            // }, {where: {
            //   old_id: id
            // }});

          }

        }
      }

      reply.send({ status: 'ok' });

    })

    fastify.get('/maintenance/update/slug', async (request, reply) => {

      const slugify = require('slugify')

      const products = await fastify.db.models.product.findAll({raw: true})
      
      // save to solr
      const client = solr.createClient({
        path: '/solr/odfarmara_products'
      });
      client.autoCommit = true;

      products.forEach(async function(product){

        const slug = slugify(product.title, {
          lower: true,      // convert to lower case, defaults to `false`
          locale: 'sk'       // language code of the locale to use
        })

        await fastify.db.models.product.update({ slug: slug }, {
          where: {
            old_id: product.old_id
          }
        })

        let doc = {
            id :product.old_id,
            ge_slug : {"set": slug },
        }

        const obj = await client.add(doc);
        const commit = await client.commit();
      })

      const farmers = await fastify.db.models.user.findAll({raw: true, where:{
        privilegs: 'farmer',
      }})

      // save to solr
      const client2 = solr.createClient({
        path: '/solr/odfarmara_farmers'
      });
      client2.autoCommit = true;

      farmers.forEach(async function(farmer){

        let name = farmer.comapny_name;
        if(!name){
          name = farmer.name;
        }
        if(!name){
          name = farmer.email;
        }

        let slug = slugify(name, {
          lower: true,      // convert to lower case, defaults to `false`
          locale: 'sk'       // language code of the locale to use
        })

        slug = slug.replace("@", "-");
        console.log('slug', slug);

        await fastify.db.models.user.update({ slug: slug }, {
          where: {
            id: farmer.id
          }
        })

        let doc = {
            id : farmer.id,
            ge_slug : {"set": slug },
        }

        const obj = await client2.add(doc);
        const commit = await client2.commit();
      })

      reply.send({ status: 'ok' });

    })


    fastify.get('/maintenance/update/pt', async (request, reply) => {

      const users = await fastify.db.models.user.findAll();

      users.forEach(async function(item){

        // await fastify.db.models.user.update({ profile_type: 'free' }, {
        //   where: {
        //     id: item.id
        //   }
        // })

      });

      reply.send({ status: 'ok' });


    });


    fastify.get('/maintenance/test/delete-flag', async (request, reply) => {

      let id = "783375816356134913";

      const products = await fastify.db.models.product.findAll({
        where: {
          flags: {
            [Op.contains]: [
              {'id': id}
            ]
          }
        }
      });

      console.log('products', products);

      products.forEach(async function(item){

        let flags = item.flags;
        _.remove(flags, item => item.id === id);

        console.log('flags', flags);


        await fastify.db.models.user.update({ flags: flags }, {
          where: {
            id: item.id
          }
        })

      });

      reply.send({ status: 'ok' });

    });


    fastify.get('/maintenance/test/update-flag', async (request, reply) => {

      let id = "789813464036999169";

      const products = await fastify.db.models.product.findAll({
        where: {
          flags: {
            [Op.contains]: [
              {'id': id}
            ]
          }
        }
      });

      console.log('products', products);

      products.forEach(async function(item){

        let flags = item.flags;
        _.find(flags, { id: id }).ge_active = false

        console.log('flags', flags);


        // await fastify.db.models.user.update({ flags: flags }, {
        //   where: {
        //     id: item.id
        //   }
        // })

      });

      reply.send({ status: 'ok' });

    });

    fastify.get('/maintenance/delete-user', async (request, reply) => {

      const users = await fastify.db.models.user.findAll({
        where: {
          email: 'michalovicmarian@gmail.com'
        }
      });

      users.forEach(async function(item){

        let id = item.id;

        let client = solr.createClient({
            path: '/solr/odfarmara_farmers'
        });

        const query = 'id:' + id;
        // delete from solr
        const obj = await client.deleteByID(id);
        const commit = await client.commit();

        client = solr.createClient({
          path: '/solr/odfarmara_users'
        });

        const query1 = 'id:' + id;
        // delete from solr
        const obj1 = await client.deleteByID(id);
        const commit1 = await client.commit();

        await fastify.db.models.user.destroy({
          where: { id: id },
        })

      });

      reply.send({ status: 'ok' });

    });


    fastify.get('/maintenance/remail', async (request, reply) => {

      const email = "michalovicmarian@gmail.com";

      const findedUser = await fastify.db.models.user.findOne({
        where: { email: email }
      })

      // generate reset token
      // const resetToken = nanoid(128);
      // const resetTokenExpiration = moment().add(1, 'd').toDate();

      // send email
      if(findedUser){

        // // save token to db
        // const updatedUser = await fastify.db.models.user.update({
        //   reset_pass_token: resetToken,
        //   reset_pass_time: resetTokenExpiration
        // },{
        //   where: { id: findedUser.id }
        // })

        // send activation email
        const { mailer } = fastify;

        // const url = fastify.config.ROOT + "/obnova-hesla/" + resetToken;

        // mail template
        const mailText = "mailText";
        const mailHtml = "mailHtml";
        // const mailHtml = await fastify.view("/email/password_reset", { url: url });

        mailer.sendMail({
          to: 'michalovicmarian@gmail.com',
          // to: email,
          subject: 'Obnova hesla',
          text: mailText,
          html: mailHtml,
          }, (errors, info) => {
          if (errors) {
              fastify.log.error(errors)
      
              reply.status(500)
              return {
              status: 'error',
              message: 'Something went wrong'
              }
          }
      
        })

        reply.send({
          status: 'ok' 
        });

      }
      else{
        reply.send({
          status: 'false' 
        });
      }

    })

    fastify.get('/maintenance/import/acticles', async (request, reply) => {

      const slugify = require('slugify');

      let path = __dirname + "/../import/articles.xlsx";

      const workSheetsFromFile = xlsx.parse(path, {cellDates: true});
      console.log('workSheetsFromFile', workSheetsFromFile);

      for (let index = 0; index < workSheetsFromFile.length; index++) {
        const ws = workSheetsFromFile[index];
        if(ws.name == 'articles'){

          let data = ws.data;

          for (let x = 0; x < data.length; x++) {
            // header
            if (x == 0) {
              continue;
            }

            let article_id = data[x][0];
            let cat_id = data[x][1];
            let name = data[x][2];
            let seo_ext = data[x][3];
            let seo_title = data[x][4];
            let seo_description = data[x][5];
            let perex = data[x][8];
            let content = data[x][9];
            let img = data[x][10];
            let img_alt = data[x][11];
            let active = data[x][12];
            let date = data[x][13];
            let created = data[x][14];

            let date_formated = moment.utc(date).format('DD.MM.YYYY');
            let created_formated = moment.utc(created).format('DD.MM.YYYY');

            let filename = img.replace(/^.*[\\\/]/, '');

            console.log(article_id + ' - ' + name + ' - ' + date_formated + ' - ' + created_formated + ' - ' + filename);

            if(cat_id == 1){
              cat_id = 216;
            }
            if(cat_id == 6){
              cat_id = 217;
            }
            if(cat_id == 7){
              cat_id = 218;
            }
            if(cat_id == 10){
              cat_id = 219;
            }
            if(cat_id == 11){
              cat_id = 220;
            }
            if(cat_id == 12){
              cat_id = 221;
            }

            // get article
            const findedArticle = await fastify.db.models.article.findOne({
              where: { article_id: article_id }
            });

            // // update
            // if(findedArticle){
            //   const updatedArticle = await fastify.db.models.article.update({
            //     cat_id: cat_id,
            //     title: name,
            //     seo_ext: seo_ext,
            //     seo_title: seo_title,
            //     seo_description: seo_description,
            //     perex: perex,
            //     contents: content,
            //     blog_img: filename,
            //     blog_img_alt: img_alt,
            //     active: active,
            //     date: date,
            //     created: created
            //   }, {where: { article_id: article_id }});
            // }
            // // create
            // else{
            //   const newArticle = await fastify.db.models.article.create({
            //     article_id: article_id,
            //     cat_id: cat_id,
            //     title: name,
            //     seo_ext: seo_ext,
            //     seo_title: seo_title,
            //     seo_description: seo_description,
            //     perex: perex,
            //     contents: content,
            //     blog_img: filename,
            //     blog_img_alt: img_alt,
            //     active: active,
            //     date: date,
            //     created: created
            //   });
            // }

          }


        }
      }

      reply.send({
        status: 'ok' 
      });

    })

    fastify.get('/maintenance/get-product', async (request, reply) => {

      const product = await fastify.db.models.product.findOne({
        where: {
          old_id: 833
        }
      });

      reply.send({
        product: product,
        status: 'ok' 
      });

    })

    fastify.get('/maintenance/get-user', async (request, reply) => {

      const user = await fastify.db.models.user.findOne({
        where: {
          email: 'zakaznik1odfarma@proton.me'
        }
      });

      reply.send({
        //user: user,
        status: 'ok' 
      });

    })

  }
  
  module.exports = routes

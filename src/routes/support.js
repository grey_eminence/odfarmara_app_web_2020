
async function routes (fastify, options) {
    fastify.get('/podporte-projekt', async (request, reply) => {
      reply.view('/pages/support', { 
        title: 'Odfarmara.sk',
      })
    })
  }
  
  module.exports = routes
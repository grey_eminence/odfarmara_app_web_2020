
const argon2 = require('argon2')
const { v4: uuidv4 } = require('uuid')
const v = require('voca')
const moment = require('moment')
const { nanoid } = require('nanoid')
const Joi = require('joi')
const passwordComplexity = require("joi-password-complexity")
const slugify = require('slugify')
const solr = require('solr-client')

async function routes (fastify, options) {

    fastify.get('/registracia', async (request, reply) => {

      reply.view('/pages/registration_select', { 
        title: 'Odfarmara.sk',
      })
    })

    fastify.get('/registracia/zakaznik', async (request, reply) => {

      request.session.set('actionType', 'registration');
      request.session.set('registrationType', 'buyer');
      const registrationType = 'buyer';

      reply.view('/pages/registration', { 
        title: 'Odfarmara.sk',
        registrationType: registrationType,
        disabled: "disabled",
        checked: ""
      })
    })

    fastify.get('/registracia/farmar', async (request, reply) => {

      request.session.set('actionType', 'registration');
      request.session.set('registrationType', 'seller');
      const registrationType = 'seller';

      reply.view('/pages/registration', {
        title: 'Odfarmara.sk',
        registrationType: registrationType,
        disabled: "disabled",
        checked: ""
      })
    })

    fastify.post('/registracia', async (request, reply) => {

      const type = request.body.type;
      const name = request.body.name;
      const email = request.body.email;
      const password = request.body.password;
      const password_repeat = request.body.password_repeat;
      const accept = request.body.accept;

      let schema;
      // validation schema
      if(type == "seller"){
        schema = Joi.object({
          name: Joi.string().required().messages({
            'string.empty': 'pole meno je povinné pole',
            'any.required': 'pole meno je povinné pole'
          }),
          email: Joi.string().email().required().external(async function(value, helper){

            // email exists?
           const findedUser = await fastify.db.models.user.findOne({
             where: { email: value }
           })

           if(findedUser){
             throw new Joi.ValidationError(
               "string.email",
               [
                 {
                   message: "užívateľ s uvedeným emailom už existuje",
                   path: ["email"],
                   type: "string.email",
                   context: {
                     key: "email",
                     label: "email",
                     value,
                   },
                 },
               ],
               value
             );

           }
           else{
             return true;
           }

          }).messages({
            'string.empty': 'pole email je povinné pole',
            'string.email': 'pole email musí obsahovať platný email',
            'string.required': 'pole email je povinné pole',
          }),
          password: new passwordComplexity({
            min: 8,
            max: 30,
            lowerCase: 1,
            upperCase: 1,
            numeric: 1,
            symbol: 1,
            requirementCount: 4
          }).messages({
            '*': 'pole heslo musí obsahovať 8 až 30 znakov, malé aj veľké písmená, čísla aj symboly ako napr. # + - ! @',
          }),
          password_repeat: Joi.any().valid(Joi.ref('password')).required().messages({
            'string.empty': 'pole zopakovať heslo je povinné pole',
            'any.only': 'pole zopakovať heslo sa musí zhodovať s heslom',
            'any.required': 'pole zopakovať heslo je povinné pole'
          }),
          
        });
      }
      else{
        schema = Joi.object({
          email: Joi.string().email().required().external(async function(value, helper){

             // email exists?
            const findedUser = await fastify.db.models.user.findOne({
              where: { email: value }
            })

            if(findedUser){
              throw new Joi.ValidationError(
                "string.email",
                [
                  {
                    message: "užívateľ s uvedeným emailom už existuje",
                    path: ["email"],
                    type: "string.email",
                    context: {
                      key: "email",
                      label: "email",
                      value,
                    },
                  },
                ],
                value
              );

            }
            else{
              return true;
            }

          }).messages({
            'string.empty': 'pole email je povinné pole',
            'string.email': 'pole email musí obsahovať platný email',
            'string.required': 'pole email je povinné pole',
          }),
          password: new passwordComplexity({
            min: 8,
            max: 30,
            lowerCase: 1,
            upperCase: 1,
            numeric: 1,
            symbol: 1,
            requirementCount: 4
          }).messages({
            '*': 'pole heslo musí obsahovať 8 až 30 znakov, malé aj veľké písmená, čísla aj symboly ako napr. # + - ! @',
          }),
          password_repeat: Joi.any().valid(Joi.ref('password')).required().messages({
            'string.empty': 'pole zopakovať heslo sa musí zhodovať s heslom',
            'any.only': 'pole zopakovať heslo sa musí zhodovať s heslom',
            'any.required': 'pole zopakovať heslo sa musí zhodovať s heslom'
          }),
        });
      }

      // validation errors
      let validation_error = false;
      try {
          let data = { email: email, password: password, password_repeat: password_repeat };
          if(type == "seller"){
            data.name = name;
          }

          const validation = await schema.validateAsync(data, {abortEarly: false});
          
          // console.log('validation', validation);
      }
      catch (err) {
          // console.log('validation error', err);
          validation_error = err;
      }

      if(validation_error){

          let validation_error_sanitized = {};
          for(var key in validation_error.details){
              let message = validation_error.details[key].message;
              let path = validation_error.details[key].path[0];

              validation_error_sanitized[path] = message;
          }

          console.log('validation_error_sanitized', validation_error_sanitized);

          let disabled = "";
          let checked = "checked";
          if(!accept){
            disabled = "disabled";
            checked = "";
          }

          reply.view("/elements/content/form/registration", { 
            validationErrors: validation_error_sanitized, 
            registrationType: type,
            name: name,
            email: email,
            password: password,
            password_repeat: password_repeat,
            disabled: disabled,
            checked: checked
          });
      }
      else{
      
        const hash = await argon2.hash(password, {type: argon2.argon2id});
        const uuid = uuidv4();
        const activateToken = nanoid(128);
        const activateTokenExpiration = moment().add(7, 'd').toDate();

        let initials = "";

        let data = {
          username: email,
          email: email,
          active: '0',
          password_new: hash,
          uuid: uuid,
          privilegs: 'customer',
          profile_type: 'free',
          activate_token: activateToken,
          activate_token_expiration: activateTokenExpiration,
          created: new Date().toISOString(),
        }

        initials = v(email).latinise().upperCase().charAt(0).value();

        if(type == "seller"){
          data.name = name;
          data.company_name = name;
          data.contact_name = name;
          data.contact_email = email;
          data.privilegs = 'farmer';
          initials = ""; 

          let names = v(name).latinise().upperCase().words();
          if(names.length > 0){
            for(let x=0; x < names.length; x++){
              if(x > 1){
                break;
              }
              let char = names[x].charAt(0);
              initials += char;
            }
          }

          let slug = slugify(name, {
            lower: true,      // convert to lower case, defaults to `false`
            locale: 'sk'       // language code of the locale to use
          })
          data.slug = slug;
        }

        if(initials.length > 0){
          data.initials = initials;
        }

        console.log('data', data);
        console.log('type', type);

        let newUser = await fastify.db.models.user.create(data);

        // create solr index
        let m_created_date = moment(newUser.created);
        let created_date = m_created_date.toISOString();

        if(type == "seller"){

          const client = solr.createClient({
            path: '/solr/odfarmara_farmers'
          });

          let doc = {
              id : newUser.id,
              ge_id: {"set": newUser.id},
              ge_name : {"set": name},
              ge_company_name : {"set": name},
              ge_email : {"set": email},
              ge_active: {"set": '0'},
              ge_created_date: {"set": created_date},
          }

          // add and commit to solr
          const obj = await client.add(doc);
          const commit = await client.commit();

        }
        else{

          const client = solr.createClient({
            path: '/solr/odfarmara_users'
          });

          let doc = {
              id : newUser.id,
              ge_id: {"set": newUser.id},
              ge_email : {"set": email},
              ge_active: {"set": '0'},
              ge_created_date: {"set": created_date},
          }

          // add and commit to solr
          const obj = await client.add(doc);
          const commit = await client.commit();

        }

        // send activation email
        const { mailer } = fastify;

        const url = fastify.config.ROOT + "/registration/confirm/" + activateToken;

        // mail template
        const mailText = "";
        const mailHtml = await fastify.view("/email/registration_customer", { url: url });

        mailer.sendMail({
          //to: 'marian.michalovic@greyeminence.sk',
          to: email,
          subject: 'Potvrdenie registrácie',
          text: mailText,
          html: mailHtml,
          }, (errors, info) => {
          if (errors) {
              fastify.log.error(errors)
      
              reply.status(500)
              return {
              status: 'error',
              message: 'Something went wrong'
              }
          }
        })

        reply.view("/elements/content/form/registration", { success_message: "Ďakujeme za registráciu.", newUser: newUser });
      }

    })

    fastify.get('/registration/confirm/:token', async (request, reply) => {

      const token = request.params.token;
      let valid = false;

      const findedUser = await fastify.db.models.user.findOne({
        where: { activate_token: token }
      })

      if(findedUser){
        const activateTokenExpiration = findedUser.activate_token_expiration;
        const active = findedUser.active;
        if(!active){
          let after = moment().isSameOrAfter(activateTokenExpiration);

          const updated = await fastify.db.models.user.update({ active: '1' }, { where: { id: findedUser.id } });

          if(!after && updated){
            valid = true;
          }

          // update solr index
          if(findedUser.privilegs === "farmer"){
            const client = solr.createClient({
              path: '/solr/odfarmara_farmers'
            });

            let doc = {
                id : findedUser.id,
                ge_active: {"set": true},
            }

            // add and commit to solr
            const obj = await client.add(doc);
            const commit = await client.commit();
          }

          if(findedUser.privilegs === "customer"){
            const client = solr.createClient({
              path: '/solr/odfarmara_users'
            });

            let doc = {
                id : findedUser.id,
                ge_active: {"set": true},
            }

            // add and commit to solr
            const obj = await client.add(doc);
            const commit = await client.commit();
          }

        }
        else{
          valid = true;
        }
        
      }
      

      console.log('findedUser', findedUser);

      if(valid){
        reply.view("/pages/registration_confirm", { success_message: "Potvrdzujeme vašu registráciu, môžete sa prihlásiť." });
      }
      else{
        reply.view("/pages/registration_confirm", { false_message: "Bohužial, platnosť aktivácie vypršala. Skúste si obnoviť heslo." });
      }


    })

  }
  
  module.exports = routes
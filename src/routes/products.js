
const solr = require('solr-client')
const URI = require('urijs')
const { Op } = require("sequelize")

async function routes (fastify, options) {

    fastify.get('/home/search', async (request, reply) => {
      reply.redirect('/produkty');
    })

    fastify.get('/produkty', async (request, reply) => {

      const season_offers_categories = await fastify.getSeasonOffersCategories();
      const season_offers_products = await fastify.getSeasonOffersProducts();

      const user =  request.user;

      // favorite products
      let favoriteProducts = [];
      if(user && user.favorites.length){
        for(let a = 0; a < user.favorites.length; a++){
          if(user.favorites[a].id_offer){
            favoriteProducts.push(user.favorites[a].id_offer);
          }
        }
      }

      let docs = [];
      let query_text = "*:*";
      let ipp = 12;
      let startItem = 0;

      let uri = new URI(request.url);
      let urlQuery = uri.search(true);
      let c = urlQuery.c;
      let mc = urlQuery.mc;
      let e = urlQuery.e;
      let q = urlQuery.q;
      let p = urlQuery.p;
      let r = urlQuery.r;

      // selected categories
      let selectedCategories = [];
      let selectedMainCategories = [];
      let categories = [];
      categories = await fastify.db.models.category.findAll({
        where: { parent_id: {[Op.is]: null} },
        include: [ 
          { model: fastify.db.models.category, as: "subcategory"},
        ],
      });

      if(c){
          
        for(let x = 0; x < categories.length; x++){
          let id = categories[x].id;
          let name = categories[x].name;
  
          if(Array.isArray(c)){
            if(c.includes(String(id))){
              selectedCategories.push({id: id, name: name, subcategory: false});
            }
          }
          else{
            if(c == String(id)){
              selectedCategories.push({id: id, name: name, subcategory: false});
            }
          }
  
          let subcategories = categories[x].subcategory;
          if(subcategories.length){
            for(let y = 0; y < subcategories.length; y++){

              let id = subcategories[y].id;
              let name = subcategories[y].name;
      
              if(Array.isArray(c)){
                if(c.includes(String(id))){
                  selectedCategories.push({id: id, name: name, subcategory: true});
                }
              }
              else{
                if(c == String(id)){
                  selectedCategories.push({id: id, name: name, subcategory: true});
                }
              }

            }
          }
        }
      }

      if(mc){

        for(let x = 0; x < categories.length; x++){
          let id = categories[x].id;
          let name = categories[x].name;
  
          if(Array.isArray(mc)){
            if(mc.includes(String(id))){
              selectedMainCategories.push({id: id, name: name, subcategory: false});
            }
          }
          else{
            if(mc == String(id)){
              selectedMainCategories.push({id: id, name: name, subcategory: false});
            }
          }
        }
        
      }

      // default page
      if(!p){
        p = 1;
      }
      startItem = (ipp * (p-1));

      let sort = {ge_created_date : 'desc'};

      if(q){
        query_text = 'ge_title:"' + q + '", ' + 'ge_description:"' + q + '"';
        sort = {};
      }

      console.log('query_text', query_text);

      //Create a client
      const client = solr.createClient({
        path: '/solr/odfarmara_products'
      });

      const query = client
      .query()
      .q(query_text)
      .start(startItem)
      .sort(sort)
      .rows(ipp);

      // region filter
      if(r){
        query.matchFilter('ge_region', r );
      }

      if(mc){
        query.matchFilter('ge_category', mc );
      }

      if(c){
        query.matchFilter('ge_subcategory', c );
      }

      // from - to date filter
      // query.matchFilter('ge_start_date', '[* TO NOW]');
      // query.matchFilter('ge_end_date', '[NOW TO *]');

      // active filter
      query.matchFilter('ge_active', true);

      // regions
      let countryRegions = await fastify.db.models.region.findAll({
        where: { 
          active: '1',
        }
      });

      // selected regions
      let selectedRegions = [];
      if(r){
        for(let x = 0; x < countryRegions.length; x++){
          let id = countryRegions[x].region_id;
          let name = countryRegions[x].name;
          let icon = countryRegions[x].icon;
  
          if(Array.isArray(r)){
            if(r.includes(String(id))){
              selectedRegions.push({id: id, name: name, icon: icon});
            }
          }
          else{
            if(r == String(id)){
              selectedRegions.push({id: id, name: name, icon: icon});
            }
          }
        }
      }

      const data = await client.search(query);
      docs = data.response.docs;

      // set pages count
      let pages = Math.floor(data.response.numFound / ipp);
      let pages_modulo = data.response.numFound % ipp;
      if(pages_modulo){
        pages++;
      }

      let baseUrl = fastify.config.ROOT + "/produkty";

      let session_products = await fastify.db.models.product.findAll({
        limit: 9,
        include: [ 
          { model: fastify.db.models.image, as: "image", required: true },
          { model: fastify.db.models.user, as: "farmer", required: true },
        ],
        where: { 
          active: '1',
        },
        order: [
          ['created', 'DESC'], ['image', 'img_id', 'ASC']
        ]
      });

      reply.view('/pages/products', {
        title: 'Produkty - Odfarmara.sk',
        docs: docs,
        q: q,
        p: parseInt(p),
        c: c,
        mc: mc,
        e: e,
        r: r,
        pages: parseInt(pages),
        startItem: startItem,
        numFound: data.response.numFound,
        baseUrl: baseUrl,
        session_products: season_offers_products,
        sessional_offers_categories: season_offers_categories,
        current_url: request.url,
        selectedCategories: selectedCategories,
        selectedMainCategories: selectedMainCategories,
        selectedRegions: selectedRegions,
        countryRegions: countryRegions,
        localCategories: categories,
        favoriteProducts: favoriteProducts
      })
    })
  }
  
  module.exports = routes

const moment = require('moment')
const { nanoid } = require('nanoid')
const Joi = require('joi')
const argon2 = require('argon2')
const passwordComplexity = require("joi-password-complexity")
const passwordGenerator = require('generate-password')
const { v4: uuidv4 } = require('uuid')
const v = require('voca')
const solr = require('solr-client')

async function routes (fastify, options) {
    fastify.get('/prihlasenie', async (request, reply) => {

      request.session.set('actionType', 'login');

      reply.view('/pages/login', { 
        title: 'Odfarmara.sk',
        messages: reply.flash('error')
      })
      
    })

    fastify.get('/odhlasenie', async (request, reply) => {

      request.logout();
      reply.redirect('/');

    })

    fastify.get('/zabudnute-heslo', async (request, reply) => {

      reply.view("/pages/password_reset");

    })

    fastify.post('/zabudnute-heslo', async (request, reply) => {

      // parameters
      const email = request.body.email;

      // validation schema
      const schema = Joi.object({
        email: Joi.string().email().required().messages({
          'string.empty': 'pole email je povinné pole',
          'string.email': 'pole email musí obsahovať platný email',
          'any.required': 'pole email je povinné pole'
        }),
      });

      // validation errors
      let validation_error = false;
      try {
          const validation = await schema.validateAsync({ email: email }, {abortEarly: false});
          console.log('validation', validation);
      }
      catch (err) {
          console.log('validation error', err);
          validation_error = err;
      }

      if(validation_error){

          let validation_error_sanitized = {};
          for(var key in validation_error.details){
              let message = validation_error.details[key].message;
              let path = validation_error.details[key].path[0];

              validation_error_sanitized[path] = message;
          }

          console.log('validation_error_sanitized', validation_error_sanitized);

          reply.view("/pages/password_reset", { validationErrors: validation_error_sanitized, email: email });
      }
      else{

        const findedUser = await fastify.db.models.user.findOne({
          where: { email: email }
        })

        // generate reset token
        const resetToken = nanoid(128);
        const resetTokenExpiration = moment().add(1, 'd').toDate();

        // send email
        if(findedUser){

          // save token to db
          const updatedUser = await fastify.db.models.user.update({
            reset_pass_token: resetToken,
            reset_pass_time: resetTokenExpiration
          },{
            where: { id: findedUser.id }
          })

          // send activation email
          const { mailer } = fastify;

          const url = fastify.config.ROOT + "/obnova-hesla/" + resetToken;

          // mail template
          const mailText = "";
          const mailHtml = await fastify.view("/email/password_reset", { url: url });

          mailer.sendMail({
            // to: 'marian.michalovic@greyeminence.sk',
            to: email,
            subject: 'Obnova hesla',
            text: mailText,
            html: mailHtml,
            }, (errors, info) => {
            if (errors) {
                fastify.log.error(errors)
        
                reply.status(500)
                return {
                status: 'error',
                message: 'Something went wrong'
                }
            }
        
          })

        }
        // user not found
        else{
          reply.view("/pages/password_reset", { 'emailNotFound': true, email: email });
        }

        reply.view("/pages/password_reset", { 'success': true });
      }

    })

    fastify.get('/obnova-hesla/:token', async (request, reply) => {

      const token = request.params.token; 

      reply.view("/pages/password_restore", {token: token});

    })

    fastify.post('/obnova-hesla', async (request, reply) => {

      const token = request.body.token;
      const password = request.body.password;
      const password_repeat = request.body.password_repeat;

      // validation schema
      const schema = Joi.object({
        token: Joi.string().required().messages({
          'string.empty': 'pole token je povinné pole',
          'any.required': 'pole token je povinné pole'
        }),
        password: new passwordComplexity({
          min: 8,
          max: 30,
          lowerCase: 1,
          upperCase: 1,
          numeric: 1,
          symbol: 1,
          requirementCount: 4
        }).messages({
          '*': 'pole heslo musí obsahovať 8 až 30 znakov, malé aj veľké písmená, čísla aj symboly ako napr. # + - ! @',
        }),
        password_repeat: Joi.string().valid(Joi.ref('password')).messages({
          '*': 'pole zopakovať heslo musí mať rovnakú hodnotu ako pole heslo',
        }),
      });

      // validation errors
      let validation_error = false;
      try {
          const validation = await schema.validateAsync({ token: token, password: password, password_repeat: password_repeat }, {abortEarly: false});
      }
      catch (err) {
          validation_error = err;
      }

      if(validation_error){

          let validation_error_sanitized = {};
          for(var key in validation_error.details){
              let message = validation_error.details[key].message;
              let path = validation_error.details[key].path[0];

              validation_error_sanitized[path] = message;
          }

          reply.view("/pages/password_restore", {
            validationErrors: validation_error_sanitized,
            token: token,
            password: password,
            password_repeat: password_repeat
          });
      }
      else{
        const findedUser = await fastify.db.models.user.findOne({
          where: { reset_pass_token: token }
        })

        if(findedUser){
  
          const reset_pass_time = findedUser.reset_pass_time;
          let after = moment().isSameOrAfter(reset_pass_time);

          // if not expired save new password to db
          if(!after){
            const password_hash = await argon2.hash(password, {type: argon2.argon2id});

            const updatedUser = await fastify.db.models.user.update({
              reset_pass_token: null,
              reset_pass_time: null,
              password_new: password_hash
            },{
              where: { id: findedUser.id }
            })

            // send password change notification email
            const { mailer } = fastify;

            // mail template
            const mailText = "";
            const mailHtml = await fastify.view("/email/password_changed");

            mailer.sendMail({
              // to: 'marian.michalovic@greyeminence.sk',
              to: findedUser.email,
              subject: 'Zmenené heslo',
              text: mailText,
              html: mailHtml,
              }, (errors, info) => {
              if (errors) {
                  fastify.log.error(errors)
          
                  reply.status(500)
                  return {
                  status: 'error',
                  message: 'Something went wrong'
                  }
              }
          
            })

            reply.view("/pages/password_restore", { 
              successMessage: "Vaše heslo bolo úspešne zmenené, môžete sa prihlásiť novým heslom.", 
              token: token,
              password: password,
              password_repeat: password_repeat
            });
          }
          // token expired
          else{
            reply.view("/pages/password_restore", { 
              failedMessage: "Bohužial, platnosť obnovy hesla vypršala. Skúste znova obnoviť heslo cez funkciu zabudnuté heslo.",
              token: token,
              password: password,
              password_repeat: password_repeat
             });
          }
  
        }
        else{
          reply.view("/pages/password_restore", { 
            failedMessage: "Bohužial, platnosť obnovy hesla vypršala. Skúste znova obnoviť heslo cez funkciu zabudnuté heslo.",
            token: token,
            password: password,
            password_repeat: password_repeat
         });
        }
      }

    })

    fastify.get('/prihlasenie/profil', async (request, reply) => {

      reply.view("/pages/login_select");

    })

    fastify.get('/prihlasenie/zakaznik', async (request, reply) => {

      const profile = request.session.get('federatedProfile');

      if(!profile){
        reply.redirect('/');
      }

      let id = profile.id;
      let email = profile.emails[0].value;
      let displayName = profile.displayName;
      let familyName = profile.name.familyName;
      let givenName = profile.name.givenName;
      let photo = profile.photos[0].value;

      let existUser = await fastify.db.models.user.findOne({
        where:{ 
          email: email
        }
      });

      if(!existUser){

        //generate password
        const password = passwordGenerator.generate({
          length: 20,
          numbers: true,
          symbols: true,
        });
        const hash = await argon2.hash(password, {type: argon2.argon2id});
        const uuid = uuidv4();

        let data = {
          username: email,
          email: email,
          active: '1',
          password_new: hash,
          uuid: uuid,
          privilegs: 'customer',
          profile_type: 'free',
          name: displayName,
          created: new Date().toISOString(),
        }

        // calculate initials
        let initials = "";
        let names = v(displayName).latinise().upperCase().words();
        if(names.length > 0){
          for(let x=0; x < names.length; x++){
            if(x > 1){
              break;
            }
            let char = names[x].charAt(0);
            initials += char;
          }
        }
        if(initials == ""){
          initials = v(email).latinise().upperCase().charAt(0).value();
        }
        data.initials = initials;

        const newUser = await fastify.db.models.user.create(data);

        let m_created_date = moment(newUser.created);
        let created_date = m_created_date.toISOString();

        const client = solr.createClient({
          path: '/solr/odfarmara_users'
        });

        let doc = {
            id : newUser.id,
            ge_id: {"set": newUser.id},
            ge_name: {"set": displayName},
            ge_email : {"set": email},
            ge_active: {"set": true},
            ge_created_date: {"set": created_date},
        }

        // add and commit to solr
        const obj = await client.add(doc);
        const commit = await client.commit();

        request.session.set('federatedProfile', null);

        await request.login(newUser.dataValues);
        reply.redirect('/');

      }

    })

    fastify.get('/prihlasenie/farmar', async (request, reply) => {

      const profile = request.session.get('federatedProfile');

      if(!profile){
        reply.redirect('/');
      }

      let id = profile.id;
      let email = profile.emails[0].value;
      let displayName = profile.displayName;
      let familyName = profile.name.familyName;
      let givenName = profile.name.givenName;
      let photo = profile.photos[0].value;

      let existUser = await fastify.db.models.user.findOne({
        where:{ 
          email: email
        }
      });

      if(!existUser){

        //generate password
        const password = passwordGenerator.generate({
          length: 20,
          numbers: true,
          symbols: true,
        });
        const hash = await argon2.hash(password, {type: argon2.argon2id});
        const uuid = uuidv4();

        let data = {
          username: email,
          email: email,
          active: '1',
          password_new: hash,
          uuid: uuid,
          privilegs: 'farmer',
          profile_type: 'free',
          name: displayName,
          company_name: displayName,
          created: new Date().toISOString(),
        }

        // calculate initials
        let initials = "";
        let names = v(displayName).latinise().upperCase().words();
        if(names.length > 0){
          for(let x=0; x < names.length; x++){
            if(x > 1){
              break;
            }
            let char = names[x].charAt(0);
            initials += char;
          }
        }
        if(initials == ""){
          initials = v(email).latinise().upperCase().charAt(0).value();
        }
        data.initials = initials;

        const newUser = await fastify.db.models.user.create(data);

        let m_created_date = moment(newUser.created);
        let created_date = m_created_date.toISOString();

        const client = solr.createClient({
          path: '/solr/odfarmara_farmers'
        });

        let doc = {
            id : newUser.id,
            ge_id: {"set": newUser.id},
            ge_name : {"set": displayName},
            ge_company_name : {"set": displayName},
            ge_email : {"set": email},
            ge_active: {"set": true},
            ge_created_date: {"set": created_date},
        }

        // add and commit to solr
        const obj = await client.add(doc);
        const commit = await client.commit();

        request.session.set('federatedProfile', null);

        await request.login(newUser.dataValues);

        reply.redirect('/');

      }

    })
    
  }
  
  module.exports = routes
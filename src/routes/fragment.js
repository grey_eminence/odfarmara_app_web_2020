
const Joi = require('joi')

async function routes (fastify, options) {
    fastify.get('/fragment/next-items/:id', async (request, reply) => {

      const id = request.params.id;

      const next_products = await fastify.db.models.product.findAll({
        limit: 6,
        include: [ 
          { model: fastify.db.models.image, as: "image", required: true },
          { model: fastify.db.models.user, as: "farmer", required: true },
          { 
            model: fastify.db.models.category, 
            required: true,
            as: "category" ,
            where: {
              id: id
            }
          },
        ],
        where: { 
          active: '1',
        },
        order: [
          ['created', 'DESC'], ['image', 'img_id', 'ASC']
        ]
      });

      console.log('next_products', next_products);

      reply.view('/elements/content/next_categories', {
        id: id,
        next_products: next_products
      })

    })

    fastify.post('/fragment/newsletter', async (request, reply) => {

      const email = request.body.email;
      const clear = request.body.clear;

      if(!clear){
        // validation schema
        const schema = Joi.object({
          email: Joi.string().email().required().messages({
            'string.empty': 'pole email je povinné pole',
            'string.email': 'pole email musí obsahovať platný email',
            'any.required': 'pole email je povinné pole'
          }),
        });

        // validation errors
        let validation_error = false;
        try {
            const validation = await schema.validateAsync({ email: email }, {abortEarly: false});
            console.log('validation', validation);
        }
        catch (err) {
            console.log('validation error', err);
            validation_error = err;
        }

        if(validation_error){

            let validation_error_sanitized = {};
            for(var key in validation_error.details){
                let message = validation_error.details[key].message;
                let path = validation_error.details[key].path[0];

                validation_error_sanitized[path] = message;
            }

            console.log('validation_error_sanitized', validation_error_sanitized);

            reply.view('/elements/content/form/newsletter', {
              email: email,
              validationErrors: validation_error_sanitized
            })
        }
      }
      else{
        reply.view('/elements/content/form/newsletter');
      }

    })

    fastify.post('/fragment/profile/subcategory', async (request, reply) => {

      const category = parseInt(request.body.category);

      const subcategories = await fastify.db.models.category.findAll({
        where: { parent_id: category },
      })

      reply.view('/elements/content/form/add_product', {
        category: category,
        subcategories: subcategories
      });

    })
  }
  
  module.exports = routes
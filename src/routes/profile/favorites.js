const { nanoid } = require('nanoid');
const {promisify} = require('util');
const fs = require('fs');
const util = require('util')
const mv = promisify(fs.rename);
const process = require('process');
const { Op, Sequelize } = require("sequelize");
const Joi = require('joi').extend(require('@joi/date'));
const moment = require('moment');
const passwordComplexity = require("joi-password-complexity");
const argon2 = require('argon2');
const myCustomJoi = Joi.extend(require('joi-phone-number'));
const { pipeline } = require('stream');
const pump = util.promisify(pipeline);
const sharp = require("sharp");
const solr = require('solr-client');
const { v4: uuidv4 } = require('uuid');
const axios = require('axios');

async function routes (fastify, options) {

    fastify.get('/ucet/oblubene', async (request, reply) => {

        // console.log('request.user', request.user);
  
        const user = request.user;
  
        let favoriteProducts = [];
        if(user && user.favorites.length){
          for(let a = 0; a < user.favorites.length; a++){
            if(user.favorites[a].id_offer){
              favoriteProducts.push(user.favorites[a].id_offer);
            }
          }
        }
  
        let products = await fastify.db.models.product.findAll({
          include: [
            { model: fastify.db.models.image, as: "image" },
            { model: fastify.db.models.user, as: "farmer" },
          ],
          where: {
            old_id: {[Op.in]: favoriteProducts }
          },
        });
  
        const favoritesLength = products.length;
  
        console.log('products', products);
  
        reply.view('/pages/profile/favorites', {
          title: 'Odfarmara.sk',
          products: products,
          favoritesLength: favoritesLength,
          page: 'favorites'
        })
      
    })

    fastify.get('/ucet/oblubene/remove/:id', async (request, reply) => {

        // console.log('request.user', request.user);
  
        const user = request.user;
        const product_id = request.params.id;
  
        // remove favorite product
        await fastify.db.models.favorite.destroy({
          where: {
            user_id: user.id,
            id_offer: product_id
          },
        });
  
        // get favorite product
        const favoriteProductsQ = await fastify.db.models.favorite.findAll({
          where: {
            user_id: user.id
          },
        });
  
        let favoriteProducts = [];
        if(favoriteProductsQ){
          for(let a = 0; a < favoriteProductsQ.length; a++){
            favoriteProducts.push(favoriteProductsQ[a].id_offer);
          }
        }
  
        let products = await fastify.db.models.product.findAll({
          include: [
            { model: fastify.db.models.image, as: "image" },
            { model: fastify.db.models.user, as: "farmer" },
          ],
          where: {
            old_id: {[Op.in]: favoriteProducts }
          },
        });
  
        let favoritesLength = products.length;
  
        console.log('favoriteProducts', favoriteProducts);
        console.log('products', products);
  
        reply.view("/pages/profile/favorites", {
          products: products,
          favoritesLength: favoritesLength,
          page: 'favorites'
        });
  
      })

}

module.exports = routes
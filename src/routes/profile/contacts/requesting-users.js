const { nanoid } = require('nanoid');
const {promisify} = require('util');
const fs = require('fs');
const util = require('util')
const mv = promisify(fs.rename);
const process = require('process');
const { Op, Sequelize } = require("sequelize");
const Joi = require('joi').extend(require('@joi/date'));
const moment = require('moment');
const passwordComplexity = require("joi-password-complexity");
const argon2 = require('argon2');
const myCustomJoi = Joi.extend(require('joi-phone-number'));
const { pipeline } = require('stream');
const pump = util.promisify(pipeline);
const sharp = require("sharp");
const solr = require('solr-client');
const { v4: uuidv4 } = require('uuid');
const axios = require('axios');

async function routes (fastify, options) {
    fastify.get('/ucet/kontakty/ziadajuci-pouzivatelia', async (request, reply) => {

        let user = request.user;

        let contacts = [];
        let userContacts = [];

        if(user) {
            contacts = user.contacts;

            if(contacts && contacts.requesting_users && contacts.requesting_users.length){
                userContacts = await fastify.db.models.user.findAll({
                    where: {
                        id: {[Op.in]: contacts.requesting_users }
                    },
                })
            }
        }

        let replyData = {
            'contacts': userContacts
        }

        reply.view('/pages/profile/contacts/requesting-users', replyData);

    })
    
    fastify.post('/ucet/kontakt/schvalit', async (request, reply) => {

        const user = request.user;
        const id = parseInt(request.body.id);

        console.log('id', id);

        if(user){
            let contacts = request.user.contacts;

            if(contacts){
                let following_users = contacts.following_users;
                let my_contacts = contacts.my_contacts;
                _.remove(following_users, function(n) { return n == id;});
                my_contacts.push(id);

                await fastify.db.models.user.update({ 
                    contacts: contacts,
                }, {
                    where: {
                        id: user.id
                    },
                })
            }

            reply.send({ status: 'ok' });
        }
        else{
            reply.send({ status: 'logout' });
        }

    })
}

module.exports = routes
const { nanoid } = require('nanoid');
const {promisify} = require('util');
const fs = require('fs');
const util = require('util')
const mv = promisify(fs.rename);
const process = require('process');
const { Op, Sequelize } = require("sequelize");
const Joi = require('joi').extend(require('@joi/date'));
const moment = require('moment');
const passwordComplexity = require("joi-password-complexity");
const argon2 = require('argon2');
const myCustomJoi = Joi.extend(require('joi-phone-number'));
const { pipeline } = require('stream');
const pump = util.promisify(pipeline);
const sharp = require("sharp");
const solr = require('solr-client');
const { v4: uuidv4 } = require('uuid');
const axios = require('axios');

async function routes (fastify, options) {
    fastify.get('/ucet', async (request, reply) => {

      console.log('request.user', request.user);

      const user = request.user;

      let where = {};
      if(user.privilegs === 'farmer'){
        where.farmar_id = user.id;
      }
      if(user.privilegs === 'customer'){
        where.user_id = user.id;
      }

      let products = false;
      if(user.privilegs === 'farmer'){
        products = await fastify.db.models.product.findAll({
          include: [ 
            { model: fastify.db.models.image, as: "image" },
            { model: fastify.db.models.user, as: "farmer" },
          ],
          where: {user_id: user.id},
          order: [
            ['created', 'ASC'], ['image', 'img_id', 'ASC']
          ],
          limit: 2
        });
        
      }

      let favoriteProducts = [];
      if(user && user.favorites.length){
        for(let a = 0; a < user.favorites.length; a++){
          if(user.favorites[a].id_offer){
            favoriteProducts.push(user.favorites[a].id_offer);
          }
        }
      }
      const favoritesLength = favoriteProducts.length;

      const latestIds = await fastify.db.models.inbox.findAll({
        attributes: [[Sequelize.fn("max", Sequelize.col('id_inbox')), 'id_inbox']],
        group: ["id_demand"],
        where: where,
      });

      console.log('latestIds', latestIds);
      let latestIdsArr = [];
      for(let x=0;x<latestIds.length;x++) {
        latestIdsArr.push(latestIds[x].id_inbox);
      }

      console.log('latestIdsArr', latestIdsArr);

      // inbox
      let inbox = await fastify.db.models.inbox.findAll({
        where: {
          id_inbox: {[Op.in]: latestIdsArr }
        },
        include: [ 
          { model: fastify.db.models.user, as: "user" },
          { model: fastify.db.models.user, as: "farmer" },
          { 
            model: fastify.db.models.demand, 
            as: "demand", 
            include: [
              { model: fastify.db.models.product, as: "product" }
            ] 
          },
        ],
        limit: 5,
        order: [
          ['created', 'DESC'],
        ]
      });

      console.log('inbox', inbox);
      // console.log('inbox.demand.product', inbox[0].demand);

      reply.view('/pages/profile/index', {
        title: 'Odfarmara.sk',
        page: 'account',
        inbox: inbox,
        products: products,
        favoritesLength: favoritesLength
      })
    })

    fastify.post('/ucet/contact-form/send-message', async (request, reply) => {

        const category = request.body.category;
        const message = request.body.message;
        const user = request.user;
        let userLogged = false;
        let successProcessed = false;
  
        let responseData = {
          category: category,
          message: message,
        }
  
        if(user){
  
          userLogged = true;
  
          // validation schema
          const schema = Joi.object({
            category: Joi.string().required().messages({
              'string.empty': 'pole predmet je povinné pole',
              'any.required': 'pole predmet je povinné pole'
            }),
            message: Joi.string().required().messages({
              'string.empty': 'pole správa je povinné pole',
              'any.required': 'pole správa je povinné pole'
            }),
          });
  
          // validation errors
          let validation_error = false;
          let validation_error_sanitized = false;
          try {
              const validation = await schema.validateAsync({ category: category, message: message }, {abortEarly: false});
              console.log('validation', validation);
          }
          catch (err) {
              console.log('validation error', err);
              validation_error = err;
          }
  
          if(validation_error){
  
              validation_error_sanitized = {};
  
              for(var key in validation_error.details){
                  let message = validation_error.details[key].message;
                  let path = validation_error.details[key].path[0];
  
                  validation_error_sanitized[path] = message;
              }
  
              console.log('validation_error_sanitized', validation_error_sanitized);
  
              // reply.view("/pages/password_reset", { validationErrors: validation_error_sanitized, email: email });
          }
          else{
            // console.log('request.user', request.user);
  
            // save form submission to db
            // send email
            successProcessed = true;
  
          }
  
          if(validation_error){
            responseData.validationErrors = validation_error_sanitized;
          }
  
          if(successProcessed){
            responseData.successMessage = "Ďakujeme za odoslanie správy!";
          }
  
        }
  
        responseData.userLogged = userLogged;
  
        reply.view('/elements/content/form/profile_contact', responseData);
  
    })
}

module.exports = routes
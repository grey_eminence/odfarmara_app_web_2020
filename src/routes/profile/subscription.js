const { nanoid } = require('nanoid');
const {promisify} = require('util');
const fs = require('fs');
const util = require('util')
const mv = promisify(fs.rename);
const process = require('process');
const { Op, Sequelize } = require("sequelize");
const Joi = require('joi').extend(require('@joi/date'));
const moment = require('moment');
const passwordComplexity = require("joi-password-complexity");
const argon2 = require('argon2');
const myCustomJoi = Joi.extend(require('joi-phone-number'));
const { pipeline } = require('stream');
const pump = util.promisify(pipeline);
const sharp = require("sharp");
const solr = require('solr-client');
const { v4: uuidv4 } = require('uuid');
const axios = require('axios');

async function routes (fastify, options) {

fastify.get('/ucet/predplatne', async (request, reply) => {

    const user = request.user;

    let favoriteProducts = [];
    if(user && user.favorites.length){
      for(let a = 0; a < user.favorites.length; a++){
        if(user.favorites[a].id_offer){
          favoriteProducts.push(user.favorites[a].id_offer);
        }
      }
    }
    const favoritesLength = favoriteProducts.length;

    reply.view('/pages/profile/subscription', {
      title: 'Odfarmara.sk',
      favoritesLength: favoritesLength
    })
  })

}

module.exports = routes
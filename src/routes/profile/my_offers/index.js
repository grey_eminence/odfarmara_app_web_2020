const { nanoid } = require('nanoid');
const {promisify} = require('util');
const fs = require('fs');
const util = require('util')
const mv = promisify(fs.rename);
const process = require('process');
const { Op, Sequelize } = require("sequelize");
const Joi = require('joi').extend(require('@joi/date'));
const moment = require('moment');
const passwordComplexity = require("joi-password-complexity");
const argon2 = require('argon2');
const myCustomJoi = Joi.extend(require('joi-phone-number'));
const { pipeline } = require('stream');
const pump = util.promisify(pipeline);
const sharp = require("sharp");
const solr = require('solr-client');
const { v4: uuidv4 } = require('uuid');
const axios = require('axios');

async function routes (fastify, options) {

    fastify.get('/ucet/moja-ponuka', async (request, reply) => {

        const user = request.user;

        let favoriteProducts = [];
        if(user && user.favorites.length){
        for(let a = 0; a < user.favorites.length; a++){
            if(user.favorites[a].id_offer){
            favoriteProducts.push(user.favorites[a].id_offer);
            }
        }
        }
        const favoritesLength = favoriteProducts.length;

        let where = {};
        if(user.privilegs === 'farmer'){
        where.farmar_id = user.id;
        }
        if(user.privilegs === 'customer'){
        where.user_id = user.id;
        }

        let products = false;
        if(user.privilegs === 'farmer'){
        products = await fastify.db.models.product.findAll({
            include: [ 
            { model: fastify.db.models.image, as: "image" },
            { model: fastify.db.models.user, as: "farmer" },
            ],
            where: {user_id: user.id},
            order: [
            ['created', 'ASC'], ['image', 'img_id', 'ASC']
            ]
        });
        
        }

        reply.view('/pages/profile/my-offer', {
        title: 'Odfarmara.sk',
        page: 'products',
        products: products,
        favoritesLength: favoritesLength
        })
    })

    fastify.post('/ucet/produkt/odstranit', async (request, reply) => {

        const id = parseInt(request.body.id);
        const user = request.user;
  
        const product = await fastify.db.models.product.findOne({
          where: {old_id: id},
        });
  
        let productUserId = null;
        if(product){
          productUserId = product.user_id;
        }
  
        if(productUserId === user.id){
          product.destroy();
  
          const client = solr.createClient({
            path: '/solr/odfarmara_products'
          });
          client.autoCommit = true;
  
          const obj = await client.deleteByID(id);
          const commit = await client.commit();
  
          reply.send({status: 'ok'});
        }
        else{
          reply.send({status: 'permissionDenied'});
          // permission denied
        }
  
        // reply.view('/pages/profile/add-offer');
  
      })

}

module.exports = routes
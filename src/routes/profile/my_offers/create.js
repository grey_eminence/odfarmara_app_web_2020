const { nanoid } = require('nanoid');
const {promisify} = require('util');
const fs = require('fs');
const util = require('util')
const mv = promisify(fs.rename);
const process = require('process');
const { Op, Sequelize } = require("sequelize");
const Joi = require('joi').extend(require('@joi/date'));
const moment = require('moment');
const passwordComplexity = require("joi-password-complexity");
const argon2 = require('argon2');
const myCustomJoi = Joi.extend(require('joi-phone-number'));
const { pipeline } = require('stream');
const pump = util.promisify(pipeline);
const sharp = require("sharp");
const solr = require('solr-client');
const { v4: uuidv4 } = require('uuid');
const axios = require('axios');
const slugify = require('slugify');

async function routes (fastify, options) {

    fastify.get('/ucet/pridat-ponuku', async (request, reply) => {

        const user = request.user;

        // clear session if not unpoly ajax reload
        const reload = request.query.reload;
        if(!reload){
        console.log('clear session productImages');
        request.session.set('productImages', []);
        }

        const images = request.session.get('productImages');
        let uploadedPhotosValue = [];
        if(images.length){
        for(let x=0;x<images.length;x++){
            uploadedPhotosValue.push(images[x].id);
        }
        }
        uploadedPhotosValue = uploadedPhotosValue.toString();
    
        let favoriteProducts = [];
        if(user && user.favorites.length){
        for(let a = 0; a < user.favorites.length; a++){
            if(user.favorites[a].id_offer){
            favoriteProducts.push(user.favorites[a].id_offer);
            }
        }
        }
        const favoritesLength = favoriteProducts.length;

        const units = [
        { label: 'gram', value: 'g' },
        { label: 'kilogram', value: 'kg' },
        { label: 'liter', value: 'l' },
        { label: 'mililiter', value: 'ml' },
        { label: 'kus', value: 'ks' },
        { label: 'balenie', value: 'bal' },
        ];

        const topCategories = await fastify.db.models.category.findAll({
        where: { parent_id: {[Op.is]: null}, type: "product" },
        include: [
            { model: fastify.db.models.category, as: "subcategory"},
        ],
        })

        reply.view('/pages/profile/add-offer', {
        title: 'Odfarmara.sk',
        topCategories: topCategories,
        favoritesLength: favoritesLength,
        units: units,
        images: images,
        uploadedPhotosValue: uploadedPhotosValue
        })
    })

    fastify.post('/ucet/pridat-ponuku', async (request, reply) => {

        const product_title = request.body.product_title;
        const category = parseInt(request.body.category);
        const subcategory = parseInt(request.body.subcategory);
        const description = request.body.description;
        const short_description = request.body.short_description;
        const quantity = request.body.quantity;
        const min_quantity = request.body.min_quantity;
        let price = request.body.price;
        if(price){
          price = price.replace(',','.');
          price = parseFloat(price);
        }
        const price_for = request.body.price_for;
        const start_date = request.body.start_date;
        const end_date = request.body.end_date;
        const unit = request.body.unit;
        let unlimited = request.body.unlimited;
        if(unlimited){
          unlimited = true;
        }
        else{
          unlimited = false;
        }
  
        const user = request.user;
  
        const units = [
          { label: 'gram', value: 'g' },
          { label: 'kilogram', value: 'kg' },
          { label: 'liter', value: 'l' },
          { label: 'mililiter', value: 'ml' },
          { label: 'kus', value: 'ks' },
          { label: 'balenie', value: 'bal' },
        ];
  
        const topCategories = await fastify.db.models.category.findAll({
          where: { parent_id: {[Op.is]: null}, type: "product" },
          include: [
            { model: fastify.db.models.category, as: "subcategory"},
          ],
        })
  
        let subcategories = false;
        if(category){
          subcategories = await fastify.db.models.category.findAll({
            where: { parent_id: category },
          })
        }
  
        const images = request.session.get('productImages');
        let uploadedPhotosValue = [];
        if(images.length){
          for(let x=0;x<images.length;x++){
            uploadedPhotosValue.push(images[x].id);
          }
        }
        uploadedPhotosValue = uploadedPhotosValue.toString();
  
        let userLogged = false;
        let successProcessed = false;
  
        let responseData = {
          product_title: product_title,
          category: category,
          subcategory: subcategory,
          topCategories: topCategories,
          subcategories: subcategories,
          unit: unit,
          units: units,
          description: description,
          short_description: short_description,
          quantity: quantity,
          min_quantity: min_quantity,
          price: price.toString().replace('.', ','),
          price_for: price_for,
          start_date: start_date,
          end_date: end_date,
          images: images,
          uploadedPhotosValue: uploadedPhotosValue,
          unlimited: unlimited,
        }
  
        console.log('responseData', responseData);
  
        if(user && user.privilegs === 'farmer'){
  
          userLogged = true;
  
          // validation schema
          const schema = Joi.object({
            product_title: Joi.string().required().messages({
              'string.empty': 'pole názov produktu je povinné pole',
              'any.required': 'pole názov produktu je povinné pole',
            }),
            category: Joi.number().integer().greater(0).required().messages({
              'number.empty': 'pole kategória je povinné pole',
              'number.greater': 'pole kategória je povinné pole',
              'any.required': 'pole kategória je povinné pole'
            }),
            subcategory: Joi.number().integer().greater(0).required().messages({
              'number.empty': 'pole podkategória je povinné pole',
              'number.greater': 'pole podkategória je povinné pole',
              'any.required': 'pole podkategória je povinné pole'
            }),
            description: Joi.string().required().messages({
              'string.empty': 'pole dlhý popis je povinné pole',
              'any.required': 'pole dlhý popis je povinné pole'
            }),
            short_description: Joi.string().required().messages({
              'string.empty': 'pole krátky popis je povinné pole',
              'any.required': 'pole krátky popis je povinné pole'
            }),
            quantity: Joi.number().integer().greater(0).required().messages({
              'number.empty': 'pole celkové množstvo je povinné pole',
              'number.greater': 'pole celkové množstvo je povinné pole a musí byť celé číslo',
              'number.base': 'pole minimálny odber je povinné pole a musí byť celé číslo',
              'number.integer': 'pole musí byť celé číslo',
              'any.required': 'pole celkové množstvo je povinné pole'
            }),
            min_quantity: Joi.number().integer().greater(0).required().messages({
              'number.empty': 'pole minimálny odber je povinné pole',
              'number.greater': 'pole minimálny odber je povinné pole a musí byť celé číslo',
              'number.base': 'pole minimálny odber je povinné pole a musí byť celé číslo',
              'number.integer': 'pole musí byť celé číslo',
              'any.required': 'pole minimálny odber je povinné pole'
            }),
            price: Joi.number().precision(2).strict().positive().required().messages({
              'number.greater': 'pole cena je povinné pole a musí byť číslo s maximálne 2 desatinnými miestami',
              'number.base': 'pole cena je povinné pole a musí byť číslo s maximálne 2 desatinnými miestami',
              'number.precision': 'musí byť číslo s maximálne 2 desatinnými miestami',
              'any.required': 'pole cena je povinné pole a musí byť číslo s maximálne 2 desatinnými miestami'
            }),
            price_for: Joi.string().required().messages({
              'string.empty': 'pole v jednotke je povinné pole',
              'any.required': 'pole v jednotke je povinné pole',
            }),
            unit: Joi.number().integer().greater(0).required().messages({
              'number.empty': 'pole za množstvo je povinné pole',
              'number.greater': 'pole za množstvo je povinné pole a musí byť celé číslo',
              'number.base': 'pole za množstvo je povinné pole a musí byť celé číslo',
              'number.integer': 'pole musí byť celé číslo',
              'any.required': 'pole za množstvo  je povinné pole'
            }),
            start_date: Joi.alternatives().conditional('unlimited', { is: false, then: Joi.date().format('DD.MM.YYYY').required().messages({
              'date.empty': 'pole "platnosť ponuky od" je povinné pole',
              'date.format': 'pole "platnosť ponuky od" musí byť vo formáte DD.MM.RRRR',
              'any.required': 'pole "platnosť ponuky od" je povinné pole',
              }),
              otherwise: Joi.any()
            }),
            end_date: Joi.alternatives().conditional('unlimited', { is: false, then: Joi.date().format('DD.MM.YYYY').required().messages({
                'date.empty': 'pole "platnosť ponuky od" je povinné pole',
                'date.format': 'pole "platnosť ponuky od" musí byť vo formáte DD.MM.RRRR',
                'any.required': 'pole "platnosť ponuky od" je povinné pole',
                }),
                otherwise: Joi.any()
            }),
            unlimited: Joi.boolean().required(),
          });
  
          // validation errors
          let validation_error = false;
          let validation_error_sanitized = false;
          try {
              const validation = await schema.validateAsync({
                product_title: product_title,
                category: category,
                subcategory: subcategory,
                description: description,
                short_description: short_description,
                quantity: quantity,
                min_quantity: min_quantity,
                price: price,
                price_for: price_for,
                start_date: start_date,
                end_date: end_date,
                unit: unit,
                unlimited: unlimited,
              }, {abortEarly: false});
              console.log('validation', validation);
          }
          catch (err) {
              console.log('validation error', err);
              validation_error = err;
          }
  
          if(validation_error){
  
              validation_error_sanitized = {};
  
              for(var key in validation_error.details){
                  let message = validation_error.details[key].message;
                  let path = validation_error.details[key].path[0];
  
                  validation_error_sanitized[path] = message;
              }
  
              console.log('validation_error_sanitized', validation_error_sanitized);
  
          }
          else{
  
            let start_date_m = moment(start_date, "DD.MM.YYYY").format("YYYY-MM-DD");
            let end_date_m = moment(end_date, "DD.MM.YYYY").format("YYYY-MM-DD");
  
            if(unlimited){
              start_date_m = null;
              end_date_m = null;
            }

            let slug = slugify(product_title, {
                lower: true,      // convert to lower case, defaults to `false`
                locale: 'sk'       // language code of the locale to use
            })
  
            let data = {
              uuid: uuidv4(),
              title: product_title,
              slug: slug,
              category_id: category,
              subcategory_id: subcategory,
              description: description,
              short_description: short_description,
              quantity: parseInt(quantity),
              min_quantity: parseInt(min_quantity),
              price: price,
              price_for: price_for,
              unit: unit,
              start_date: start_date_m,
              end_date: end_date_m,
              user_id: user.id,
              external: 0,
              active: 1,
              unlimited: unlimited,
              created: new Date().toISOString()
            }
  
            console.log('data', data);
  
            // save to db
            const newProduct = await fastify.db.models.product.create(data);
  
            let thumbnail = "";
  
            if(newProduct){
              // create images
              if(images.length){
                for(let i = 0; i < images.length; i++){
                  let filename = images[i].name;
                  let imageData = {
                    product_id: newProduct.old_id,
                    user_id: user.id,
                    filename: filename
                  };
                  
                  let image = await fastify.db.models.image.create(imageData);
                }
  
                thumbnail = images[0].name;
              }
  
              // save to solr
              const client = solr.createClient({
                path: '/solr/odfarmara_products'
              });
              client.autoCommit = true;
  
              let m_start_date = null;
              let m_end_date = null;
              let start_date_iso = null;
              let end_date_iso = null;
              if(!unlimited){
                m_start_date = moment(start_date, "YYYY-MM-DD");
                m_end_date = moment(end_date, "YYYY-MM-DD");
                start_date_iso = m_start_date.toISOString();
                end_date_iso = m_end_date.toISOString();
              }
              let m_created_date = moment(newProduct.created);
  
              
              let created_date = m_created_date.toISOString();
  
              const farmer = user.company_name;
              const active = newProduct.active;
              const region = newProduct.region_id;
  
              let doc = {
                id : newProduct.old_id,
                ge_id : newProduct.old_id,
                ge_title : product_title,
                ge_slug : slug,
                ge_description : description,
                ge_farmer : farmer,
                ge_category : category,
                ge_subcategory : subcategory,
                ge_price : price,
                ge_price_for : price_for,
                ge_quantity : quantity,
                ge_unit : unit,
                ge_active : active,
                ge_region : region,
                ge_thumbnail : thumbnail,
                ge_start_date : start_date_iso,
                ge_end_date : end_date_iso,
                ge_created_date : created_date,
              }
  
              console.log('doc', doc);
  
              const obj = await client.add(doc);
              const commit = await client.commit();
  
              successProcessed = true;
            }
  
            // save form submission to db
            // send email
          }
  
          if(validation_error){
            responseData.validationErrors = validation_error_sanitized;
            reply.view('/elements/content/form/add_product', responseData);
          }
  
          if(successProcessed){
            // reset form data
            request.session.set('productImages', []);
            responseData = { 
              successMessage : "Produkt úspešne vytvorený",
              topCategories: topCategories,
              units: units,
            };
  
            responseData.userLogged = userLogged;
  
            let where = {};
            if(user.privilegs === 'farmer'){
              where.farmar_id = user.id;
            }
            if(user.privilegs === 'customer'){
              where.user_id = user.id;
            }
  
            let products = false;
            if(user.privilegs === 'farmer'){
              products = await fastify.db.models.product.findAll({
                include: [ 
                  { model: fastify.db.models.image, as: "image" },
                  { model: fastify.db.models.user, as: "farmer" },
                ],
                where: {user_id: user.id},
                order: [
                  ['created', 'ASC'], ['image', 'img_id', 'ASC']
                ],
                limit: 2
              });
              
            }
  
            let favoriteProducts = [];
            if(user && user.favorites.length){
              for(let a = 0; a < user.favorites.length; a++){
                if(user.favorites[a].id_offer){
                  favoriteProducts.push(user.favorites[a].id_offer);
                }
              }
            }
            const favoritesLength = favoriteProducts.length;
  
            const latestIds = await fastify.db.models.inbox.findAll({
              attributes: [[Sequelize.fn("max", Sequelize.col('id_inbox')), 'id_inbox']],
              group: ["id_demand"],
              where: where,
            });
  
            console.log('latestIds', latestIds);
            let latestIdsArr = [];
            for(let x=0;x<latestIds.length;x++) {
              latestIdsArr.push(latestIds[x].id_inbox);
            }
  
            console.log('latestIdsArr', latestIdsArr);
  
            // inbox
            let inbox = await fastify.db.models.inbox.findAll({
              where: {
                id_inbox: {[Op.in]: latestIdsArr }
              },
              include: [ 
                { model: fastify.db.models.user, as: "user" },
                { model: fastify.db.models.user, as: "farmer" },
                { 
                  model: fastify.db.models.demand, 
                  as: "demand", 
                  include: [
                    { model: fastify.db.models.product, as: "product" }
                  ] 
                },
              ],
              limit: 5,
              order: [
                ['created', 'DESC'],
              ]
            });
  
            console.log('inbox', inbox);
            // console.log('inbox.demand.product', inbox[0].demand);
  
            let offerCreatedMessage = "Ponuka bola úspešne vytvorená.";
  
            reply.code(200).headers({
              'X-Up-Location': '/ucet',
              'X-Up-Method': 'GET'
            }).view('/pages/profile/index', {
              title: 'Odfarmara.sk',
              page: 'account',
              inbox: inbox,
              products: products,
              favoritesLength: favoritesLength,
              offerCreatedMessage: offerCreatedMessage
            })
          }
  
        }
      })
}

module.exports = routes
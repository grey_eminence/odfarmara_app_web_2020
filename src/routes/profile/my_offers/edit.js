const { nanoid } = require('nanoid');
const {promisify} = require('util');
const fs = require('fs');
const util = require('util')
const mv = promisify(fs.rename);
const process = require('process');
const { Op, Sequelize } = require("sequelize");
const Joi = require('joi').extend(require('@joi/date'));
const moment = require('moment');
const passwordComplexity = require("joi-password-complexity");
const argon2 = require('argon2');
const myCustomJoi = Joi.extend(require('joi-phone-number'));
const { pipeline } = require('stream');
const pump = util.promisify(pipeline);
const sharp = require("sharp");
const solr = require('solr-client');
const { v4: uuidv4 } = require('uuid');
const axios = require('axios');

async function routes (fastify, options) {

    fastify.get('/ucet/moja-ponuka/upravit/:id', async (request, reply) => {
      
        const id = parseInt(request.params.id);
        const user = request.user;
  
        const product = await fastify.db.models.product.findOne({
          include: [ 
            { model: fastify.db.models.image, as: "image"},
            { 
              model: fastify.db.models.user, 
              as: "farmer"
            },
          ],
          where: {
            old_id: id
          },
        })
  
        let uploadedPhotosValue = [];
        let uploadedPhotos = [];
        if(product.image && product.image.length){
          for(let x=0;x<product.image.length;x++){
            let id = product.image[x].img_id;
            let name = product.image[x].filename;
            uploadedPhotosValue.push(id);
            uploadedPhotos.push({
              id: id,
              name: name
            });
          }
        }
  
        uploadedPhotosValue = uploadedPhotosValue.toString();
        request.session.set('productImages', uploadedPhotos);
  
        let product_title = product.title;
        let category = product.category_id;
        let subcategory = product.subcategory_id;
        let description = product.description;
        let short_description = product.short_description;
        let quantity = product.quantity;
        let min_quantity = product.min_quantity;
        let price = product.price;
        let price_for = product.price_for;
        let start_date = product.start_date;
        let end_date = product.end_date;
        let unit = product.unit;
        let unlimited = product.unlimited;
        start_date = moment(start_date, "YYYY-MM-DD").format("DD.MM.YYYY");
        end_date = moment(end_date, "YYYY-MM-DD").format("DD.MM.YYYY");
  
        let favoriteProducts = [];
        if(user && user.favorites.length){
          for(let a = 0; a < user.favorites.length; a++){
            if(user.favorites[a].id_offer){
              favoriteProducts.push(user.favorites[a].id_offer);
            }
          }
        }
        const favoritesLength = favoriteProducts.length;
  
        const units = [
          { label: 'gram', value: 'g' },
          { label: 'kilogram', value: 'kg' },
          { label: 'liter', value: 'l' },
          { label: 'mililiter', value: 'ml' },
          { label: 'kus', value: 'ks' },
          { label: 'balenie', value: 'bal' },
        ];
  
        const topCategories = await fastify.db.models.category.findAll({
          where: { parent_id: {[Op.is]: null}, type: "product" },
          include: [
            { model: fastify.db.models.category, as: "subcategory"},
          ],
        })
  
        const subcategories = await fastify.db.models.category.findAll({
          where: { parent_id: category },
        })
  
        let responseData = {
          title: 'Odfarmara.sk',
          id: id,
          product_title: product_title,
          category: category,
          subcategory: subcategory,
          topCategories: topCategories,
          subcategories: subcategories,
          units: units,
          unit: unit,
          description: description,
          short_description: short_description,
          quantity: quantity,
          min_quantity: min_quantity,
          price: price.toString().replace('.', ','),
          price_for: price_for,
          start_date: start_date,
          end_date: end_date,
          images: uploadedPhotos,
          uploadedPhotosValue: uploadedPhotosValue,
          favoritesLength: favoritesLength,
          unlimited: unlimited,
        }
  
        reply.view('/pages/profile/edit-offer', responseData)
  
    })

    fastify.post('/ucet/moja-ponuka/ulozit', async (request, reply) => {

        const id = parseInt(request.body.id);
        const product_title = request.body.product_title;
        const category = parseInt(request.body.category);
        const subcategory = parseInt(request.body.subcategory);
        const description = request.body.description;
        const short_description = request.body.short_description;
        const quantity = request.body.quantity;
        const min_quantity = request.body.min_quantity;
        let price = request.body.price;
        if(price){
          price = price.replace(',','.');
          price = parseFloat(price);
        }
        const price_for = request.body.price_for;
        const start_date = request.body.start_date;
        const end_date = request.body.end_date;
        const unit = request.body.unit;
        let unlimited = request.body.unlimited;
        if(unlimited){
          unlimited = true;
        }
        else{
          unlimited = false;
        }

        const user = request.user;

        const units = [
        { label: 'gram', value: 'g' },
        { label: 'kilogram', value: 'kg' },
        { label: 'liter', value: 'l' },
        { label: 'mililiter', value: 'ml' },
        { label: 'kus', value: 'ks' },
        { label: 'balenie', value: 'bal' },
        ];

        const topCategories = await fastify.db.models.category.findAll({
          where: { parent_id: {[Op.is]: null}, type: "product" },
          include: [
              { model: fastify.db.models.category, as: "subcategory"},
          ],
        })

        let subcategories = false;
        if(category){
          subcategories = await fastify.db.models.category.findAll({
              where: { parent_id: category },
          })
        }

        const product = await fastify.db.models.product.findOne({
          include: [ 
              { model: fastify.db.models.image, as: "image"},
              { 
              model: fastify.db.models.user, 
              as: "farmer"
              },
          ],
          where: {
              old_id: id
          },
        })

        let dbImageValues = [];
        if(product.image && product.image.length){
          for(let x=0;x<product.image.length;x++){
              let id = product.image[x].img_id;
              let name = product.image[x].filename;
              dbImageValues.push(id);
          }
        }
        dbImageValues = dbImageValues.toString();

        const images = request.session.get('productImages');
        // console.log(images, images);
        let uploadedPhotosValue = [];
        if(images && images.length){
          for(let x=0;x<images.length;x++){
              uploadedPhotosValue.push(images[x].id);
          }
        }
        uploadedPhotosValue = uploadedPhotosValue.toString();

        let userLogged = false;
        let successProcessed = false;

        let responseData = {
          id: id,
          product_title: product_title,
          category: category,
          subcategory: subcategory,
          topCategories: topCategories,
          subcategories: subcategories,
          units: units,
          unit: unit,
          description: description,
          short_description: short_description,
          quantity: quantity,
          min_quantity: min_quantity,
          price: price.toString().replace('.', ','),
          price_for: price_for,
          start_date: start_date,
          end_date: end_date,
          images: images,
          uploadedPhotosValue: uploadedPhotosValue,
          unlimited: unlimited
        }

        // console.log('responseData', responseData);

        if(user && user.privilegs === 'farmer'){

        userLogged = true;

        // validation schema
        const schema = Joi.object({
            id: Joi.number().integer().greater(0).required().messages({
            'number.empty': 'pole id je povinné pole',
            'number.greater': 'pole id je povinné pole',
            'any.required': 'pole id je povinné pole'
            }),
            product_title: Joi.string().required().messages({
            'string.empty': 'pole názov produktu je povinné pole',
            'any.required': 'pole názov produktu je povinné pole',
            }),
            category: Joi.number().integer().greater(0).required().messages({
            'number.empty': 'pole kategória je povinné pole',
            'number.greater': 'pole kategória je povinné pole',
            'any.required': 'pole kategória je povinné pole'
            }),
            subcategory: Joi.number().integer().greater(0).required().messages({
            'number.empty': 'pole podkategória je povinné pole',
            'number.greater': 'pole podkategória je povinné pole',
            'any.required': 'pole podkategória je povinné pole'
            }),
            description: Joi.string().required().messages({
            'string.empty': 'pole dlhý popis je povinné pole',
            'any.required': 'pole dlhý popis je povinné pole'
            }),
            short_description: Joi.string().required().messages({
            'string.empty': 'pole krátky popis je povinné pole',
            'any.required': 'pole krátky popis je povinné pole'
            }),
            quantity: Joi.number().integer().greater(0).required().messages({
            'number.empty': 'pole celkové množstvo je povinné pole',
            'number.greater': 'pole celkové množstvo je povinné pole a musí byť celé číslo',
            'number.base': 'pole minimálny odber je povinné pole a musí byť celé číslo',
            'number.integer': 'pole musí byť celé číslo',
            'any.required': 'pole celkové množstvo je povinné pole'
            }),
            min_quantity: Joi.number().integer().greater(0).required().messages({
            'number.empty': 'pole minimálny odber je povinné pole',
            'number.greater': 'pole minimálny odber je povinné pole a musí byť celé číslo',
            'number.base': 'pole minimálny odber je povinné pole a musí byť celé číslo',
            'number.integer': 'pole musí byť celé číslo',
            'any.required': 'pole minimálny odber je povinné pole'
            }),
            price: Joi.number().precision(2).strict().positive().required().messages({
            'number.greater': 'pole minimálny odber je povinné pole a musí byť číslo s maximálne 2 desatinnými miestami',
            'number.base': 'pole minimálny odber je povinné pole a musí byť číslo s maximálne 2 desatinnými miestami',
            'number.precision': 'musí byť číslo s maximálne 2 desatinnými miestami',
            'any.required': 'pole minimálny odber je povinné pole a musí byť číslo s maximálne 2 desatinnými miestami'
            }),
            price_for: Joi.string().required().messages({
            'string.empty': 'pole v jednotke je povinné pole',
            'any.required': 'pole v jednotke je povinné pole',
            }),
            unit: Joi.number().integer().greater(0).required().messages({
            'number.empty': 'pole za množstvo je povinné pole',
            'number.greater': 'pole za množstvo je povinné pole a musí byť celé číslo',
            'number.base': 'pole za množstvo je povinné pole a musí byť celé číslo',
            'number.integer': 'pole musí byť celé číslo',
            'any.required': 'pole za množstvo  je povinné pole'
            }),
            start_date: Joi.alternatives().conditional('unlimited', { is: false, then: Joi.date().format('DD.MM.YYYY').required().messages({
            'date.empty': 'pole "platnosť ponuky od" je povinné pole',
            'date.format': 'pole "platnosť ponuky od" musí byť vo formáte DD.MM.RRRR',
            'any.required': 'pole "platnosť ponuky od" je povinné pole',
            }),
            otherwise: Joi.any()
            }),
            end_date: Joi.alternatives().conditional('unlimited', { is: false, then: Joi.date().format('DD.MM.YYYY').required().messages({
                'date.empty': 'pole "platnosť ponuky od" je povinné pole',
                'date.format': 'pole "platnosť ponuky od" musí byť vo formáte DD.MM.RRRR',
                'any.required': 'pole "platnosť ponuky od" je povinné pole',
                }),
                otherwise: Joi.any()
            }),
            unlimited: Joi.boolean().required(),
        });

        // validation errors
        let validation_error = false;
        let validation_error_sanitized = false;
        try {
            const validation = await schema.validateAsync({
                id: id,
                product_title: product_title,
                category: category,
                subcategory: subcategory,
                description: description,
                short_description: short_description,
                quantity: quantity,
                min_quantity: min_quantity,
                price: price,
                price_for: price_for,
                unit: unit,
                start_date: start_date,
                end_date: end_date,
                unlimited: unlimited,
            }, {abortEarly: false});
            // console.log('validation', validation);
        }
        catch (err) {
            // console.log('validation error', err);
            validation_error = err;
        }

        if(validation_error){

            validation_error_sanitized = {};

            for(var key in validation_error.details){
                let message = validation_error.details[key].message;
                let path = validation_error.details[key].path[0];

                validation_error_sanitized[path] = message;
            }

            // console.log('validation_error_sanitized', validation_error_sanitized);

        }
        else{

            let start_date_m = moment(start_date, "DD.MM.YYYY").format("YYYY-MM-DD");
            let end_date_m = moment(end_date, "DD.MM.YYYY").format("YYYY-MM-DD");

            if(unlimited){
              start_date_m = null;
              end_date_m = null;
            }

            let data = {
              title: product_title,
              category_id: category,
              subcategory_id: subcategory,
              description: description,
              short_description: short_description,
              quantity: parseInt(quantity),
              min_quantity: parseInt(min_quantity),
              price: price,
              price_for: price_for,
              start_date: start_date_m,
              end_date: end_date_m,
              unit: unit,
              unlimited: unlimited,
              // user_id: user.id,
              // external: 0,
              // active: 1,
              // created: new Date().toISOString()
            }

            // console.log('data', data);

            // save to db
            const updatedProduct = await fastify.db.models.product.update(data, {
              where: {
                  old_id: id
              }
            });

            console.log('dbImageValues', dbImageValues);
            console.log('uploadedPhotosValue', uploadedPhotosValue);

            let thumbnail = null;

            // reordered images or changes
            if(uploadedPhotosValue != "" && dbImageValues != uploadedPhotosValue){

              // delete images
              let deleted = await fastify.db.models.image.destroy({
                where: {
                  product_id: id
                }
              })

              // create new
              if(images && images.length){

                  thumbnail = images[0].name;
                  
                  for(let i = 0; i < images.length; i++){
                    let filename = images[i].name;
                    let imageData = {
                        product_id: id,
                        user_id: user.id,
                        filename: filename
                    };
                    
                    let image = await fastify.db.models.image.create(imageData);
                  }

              }

            }
            else{
              if(!images || images.length === 0){
                // delete images
                let deleted = await fastify.db.models.image.destroy({
                  where: {
                    product_id: id
                  }
                })
              }
            }

            // save to solr
            const client = solr.createClient({
              path: '/solr/odfarmara_products'
            });
            client.autoCommit = true;

            let m_start_date = moment(start_date, "YYYY-MM-DD");
            let m_end_date = moment(end_date, "YYYY-MM-DD");

            let start_date_iso = m_start_date.toISOString();
            let end_date_iso = m_end_date.toISOString();

            const farmer = product.farmer.company_name;
            const active = product.active;
            const region = product.region_id;

            let doc = {
              id : id,
              ge_id : {"set": id },
              ge_title : {"set":product_title},
              ge_description : {"set":description},
              ge_farmer : {"set":farmer},
              ge_category : {"set":category},
              ge_subcategory : {"set":subcategory},
              ge_price : {"set":price},
              ge_price_for : {"set":price_for},
              ge_quantity : {"set":quantity},
              ge_unit : {"set":unit},
              ge_active : {"set":active},
              ge_region : {"set":region},
              ge_thumbnail : {"set":thumbnail},
              ge_start_date : {"set":start_date_iso},
              ge_end_date : {"set":end_date_iso}
            }

            console.log('doc', doc);

            const obj = await client.add(doc);
            const commit = await client.commit();

            successProcessed = true;

            // save form submission to db
            // send email

          }

          if(validation_error){
              responseData.validationErrors = validation_error_sanitized;
          }

          if(successProcessed){
              // reset form data
              // request.session.set('productImages', []);
              // responseData = { 
              //   successMessage : "Produkt úspešne vytvorený",
              //   topCategories: topCategories,
              //   units: units,
              // };

              responseData.successMessage = "Produkt úspešne uložený";
          }

        }

        responseData.userLogged = userLogged;

        reply.view('/elements/content/form/edit_product', responseData);

    })

}

module.exports = routes
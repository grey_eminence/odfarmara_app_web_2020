const { nanoid } = require('nanoid');
const {promisify} = require('util');
const fs = require('fs');
const util = require('util')
const mv = promisify(fs.rename);
const process = require('process');
const { Op, Sequelize } = require("sequelize");
const Joi = require('joi').extend(require('@joi/date'));
const moment = require('moment');
const passwordComplexity = require("joi-password-complexity");
const argon2 = require('argon2');
const myCustomJoi = Joi.extend(require('joi-phone-number'));
const { pipeline } = require('stream');
const pump = util.promisify(pipeline);
const sharp = require("sharp");
const solr = require('solr-client');
const { v4: uuidv4 } = require('uuid');
const axios = require('axios');

async function routes (fastify, options) {

fastify.get('/ucet/vlastnosti', async (request, reply) => {

    const user = request.user;

    let favoriteProducts = [];
    if(user && user.favorites.length){
      for(let a = 0; a < user.favorites.length; a++){
        if(user.favorites[a].id_offer){
          favoriteProducts.push(user.favorites[a].id_offer);
        }
      }
    }
    const favoritesLength = favoriteProducts.length;

    let name = request.user.name;
    let company_name = request.user.company_name;
    let short_description = request.user.short_description;
    let long_description = request.user.long_description;
    let street = request.user.street;
    let houseno = request.user.houseno;
    let zip = request.user.zip;
    let phone = request.user.phone;
    let city = request.user.city;
    let contact_email = request.user.contact_email;
    let region = request.user.region_id;
    let additional_information = request.user.additional_information;
    let opening_hours = request.user.opening_hours_new;
    let profile_image = request.user.profile_image;
    let intro_image = request.user.intro_image;
    let gps = request.user.gps;
    let gps_latitude = '';
    let gps_longitude = '';
    let website = request.user.website;
    if(gps && gps.length){
      let gps_arr = gps.split(',');
      gps_latitude = gps_arr[0];
      gps_longitude = gps_arr[1];
    }

    let invoice = request.user.invoice;
    let invoice_company_name = "";
    let invoice_business_id = "";
    let invoice_vat_id = "";
    let invoice_tax_id = "";
    let invoice_reg_id = "";
    
    if(invoice){
      invoice_company_name = invoice.invoice_company_name;
      invoice_business_id = invoice.invoice_business_id;
      invoice_vat_id = invoice.invoice_vat_id;
      invoice_tax_id = invoice.invoice_tax_id;
      invoice_reg_id = invoice.invoice_reg_id;
    };

    // const localCategories = await fastify.db.models.category.findAll({
    //   where: { parent_id: {[Op.is]: null} },
    //   include: [
    //     { model: fastify.db.models.category, as: "subcategory"},
    //   ],
    // })

    // regions
    const countryRegions = await fastify.db.models.region.findAll({
      where: {
        active: '1',
      }
    });

    reply.view('/pages/profile/profile', {
      title: 'Odfarmara.sk',
      page: 'profile',
      favoritesLength: favoritesLength,
      countryRegions: countryRegions,
      name: name,
      company_name: company_name,
      short_description: short_description,
      long_description: long_description,
      street: street,
      houseno: houseno,
      zip: zip,
      phone: phone,
      city: city,
      contact_email: contact_email,
      region: region,
      additional_information: additional_information,
      opening_hours: opening_hours,
      profile_image: profile_image,
      intro_image: intro_image,
      gps_latitude: gps_latitude,
      gps_longitude: gps_longitude,
      website: website,
      invoice_company_name: invoice_company_name,
      invoice_business_id: invoice_business_id,
      invoice_vat_id: invoice_vat_id,
      invoice_tax_id: invoice_tax_id,
      invoice_reg_id: invoice_reg_id,
    })
  })

  fastify.post('/ucet/vlastnosti', async (request, reply) => {

    let userLogged = false;
    let successProcessed = false;
    const user = request.user;
    let validation_error = false;
    let validation_error_sanitized = false;

    let name = request.body.name;

    let company_name = request.body.company_name;
    let short_description = request.body.short_description;
    let long_description = request.body.long_description;
    let street = request.body.street;
    let houseno = request.body.houseno;
    let zip = request.body.zip;
    let phone = request.body.phone;
    let city = request.body.city;
    let contact_email = request.body.contact_email;
    let region = request.body.region;

    let collection_person = request.body.collection_person ? true: false;
    let collection_courier = request.body.collection_courier ? true : false;
    let collection_regular_delivery = request.body.collection_regular_delivery ? true : false;

    let payment_cash = request.body.payment_cash ? true : false;
    let payment_card = request.body.payment_card ? true : false;
    let payment_courier = request.body.payment_courier ? true : false;
    let payment_crypto = request.body.payment_crypto ? true : false;

    let services_selfpicking = request.body.services_selfpicking ? true : false;
    let services_stay = request.body.services_stay ? true : false;
    let services_kitchen = request.body.services_kitchen ? true : false;
    let services_sale = request.body.services_sale ? true : false;

    let eco_friendly = request.body.eco_friendly ? true : false;
    let bio = request.body.bio ? true : false;
    let zero_waste = request.body.zero_waste ? true : false;

    let invoice_company_name = request.body.invoice_company_name;
    let invoice_business_id = request.body.invoice_business_id;
    let invoice_vat_id = request.body.invoice_vat_id;
    let invoice_tax_id = request.body.invoice_tax_id;
    let invoice_reg_id = request.body.invoice_reg_id;

    let oh_1_from_h = request.body.oh_1_from_h;
    let oh_1_from_m = request.body.oh_1_from_m;
    let oh_1_to_h = request.body.oh_1_to_h;
    let oh_1_to_m = request.body.oh_1_to_m;
    let oh_2_from_h = request.body.oh_2_from_h;
    let oh_2_from_m = request.body.oh_2_from_m;
    let oh_2_to_h = request.body.oh_2_to_h;
    let oh_2_to_m = request.body.oh_2_to_m;
    let oh_3_from_h = request.body.oh_3_from_h;
    let oh_3_from_m = request.body.oh_3_from_m;
    let oh_3_to_h = request.body.oh_3_to_h;
    let oh_3_to_m = request.body.oh_3_to_m;
    let oh_4_from_h = request.body.oh_4_from_h;
    let oh_4_from_m = request.body.oh_4_from_m;
    let oh_4_to_h = request.body.oh_4_to_h;
    let oh_4_to_m = request.body.oh_4_to_m;
    let oh_5_from_h = request.body.oh_5_from_h;
    let oh_5_from_m = request.body.oh_5_from_m;
    let oh_5_to_h = request.body.oh_5_to_h;
    let oh_5_to_m = request.body.oh_5_to_m;
    let oh_6_from_h = request.body.oh_6_from_h;
    let oh_6_from_m = request.body.oh_6_from_m;
    let oh_6_to_h = request.body.oh_6_to_h;
    let oh_6_to_m = request.body.oh_6_to_m;
    let oh_7_from_h = request.body.oh_7_from_h;
    let oh_7_from_m = request.body.oh_7_from_m;
    let oh_7_to_h = request.body.oh_7_to_h;
    let oh_7_to_m = request.body.oh_7_to_m;

    let gps = request.body.gps;
    let gps_latitude = request.body.gps_latitude;
    let gps_longitude = request.body.gps_longitude;
    let custom_gps = false;
    if(gps === 'custom' || gps == null) {
      custom_gps = true;

      if(gps_latitude && gps_longitude){
        gps = gps_latitude + ',' + gps_longitude;
      }
    }
    else{

      if(gps && gps.length){
        let gps_arr = gps.split(',');
        gps_latitude = gps_arr[0];
        gps_longitude = gps_arr[1];
      }
      
    }

    console.log('gps', gps);
    console.log('custom_gps', custom_gps);

    let opening_hours = {
      oh_1_from_h: oh_1_from_h,
      oh_1_from_m: oh_1_from_m,
      oh_1_to_h: oh_1_to_h,
      oh_1_to_m: oh_1_to_m,
      oh_2_from_h: oh_2_from_h,
      oh_2_from_m: oh_2_from_m,
      oh_2_to_h: oh_2_to_h,
      oh_2_to_m: oh_2_to_m,
      oh_3_from_h: oh_3_from_h,
      oh_3_from_m: oh_3_from_m,
      oh_3_to_h: oh_3_to_h,
      oh_3_to_m: oh_3_to_m,
      oh_4_from_h: oh_4_from_h,
      oh_4_from_m: oh_4_from_m,
      oh_4_to_h: oh_4_to_h,
      oh_4_to_m: oh_4_to_m,
      oh_5_from_h: oh_5_from_h,
      oh_5_from_m: oh_5_from_m,
      oh_5_to_h: oh_5_to_h,
      oh_5_to_m: oh_5_to_m,
      oh_6_from_h: oh_6_from_h,
      oh_6_from_m: oh_6_from_m,
      oh_6_to_h: oh_6_to_h,
      oh_6_to_m: oh_6_to_m,
      oh_7_from_h: oh_7_from_h,
      oh_7_from_m: oh_7_from_m,
      oh_7_to_h: oh_7_to_h,
      oh_7_to_m: oh_7_to_m,
    }

    let additional_information = {
      collection_person: collection_person,
      collection_courier: collection_courier,
      collection_regular_delivery: collection_regular_delivery,
      payment_cash: payment_cash,
      payment_card: payment_card,
      payment_courier: payment_courier,
      payment_crypto: payment_crypto,
      services_selfpicking: services_selfpicking,
      services_stay: services_stay,
      services_kitchen: services_kitchen,
      services_sale: services_sale,
      eco_friendly: eco_friendly,
      bio: bio,
      zero_waste: zero_waste,
    }

    let responseData = {
      name: name,
    }
    
    if(user && user.privilegs == 'farmer'){
      responseData.company_name = company_name;
      responseData.short_description = short_description;
      responseData.long_description = long_description;
      responseData.street = street;
      responseData.houseno = houseno;
      responseData.zip = zip;
      responseData.phone = phone;
      responseData.city = city;
      responseData.contact_email = contact_email;
      responseData.region = region;
      responseData.additional_information = additional_information;
      responseData.opening_hours = opening_hours;
      responseData.gps = gps;
      responseData.gps_longitude = gps_longitude;
      responseData.gps_latitude = gps_latitude;
      responseData.invoice_company_name = invoice_company_name;
      responseData.invoice_business_id = invoice_business_id;
      responseData.invoice_vat_id = invoice_vat_id;
      responseData.invoice_tax_id = invoice_tax_id;
      responseData.invoice_reg_id = invoice_reg_id;
    }

    if(user && user.privilegs == 'customer'){
      responseData.street = street;
      responseData.houseno = houseno;
      responseData.zip = zip;
      responseData.phone = phone;
      responseData.city = city;
      responseData.contact_email = contact_email;
      responseData.region = region;
    }

    if(user){
      const user_id = request.user.id;

      let profile_image = request.user.profile_image;
      let intro_image = request.user.intro_image;

      responseData.profile_image = profile_image;
      responseData.intro_image = intro_image;

      userLogged = true;

      // validation schema
      let schema;

      if(user && user.privilegs == 'customer'){
        schema = myCustomJoi.object({
          name: myCustomJoi.string().required().messages({
            'string.empty': 'pole meno prevádzkovateľa je povinné pole',
            'any.required': 'pole meno prevádzkovateľa je povinné pole',
          }),
          street: myCustomJoi.string().required().messages({
            'string.empty': 'pole ulica je povinné pole',
            'any.required': 'pole ulica je povinné pole',
          }),
          houseno: myCustomJoi.string().required().messages({
            'string.empty': 'pole číslo domu je povinné pole',
            'any.required': 'pole číslo domu je povinné pole',
          }),
          zip: myCustomJoi.string().required().messages({
            'string.empty': 'pole číslo domu je povinné pole',
            'any.required': 'pole číslo domu je povinné pole',
          }),
          city: myCustomJoi.string().required().messages({
            'string.empty': 'pole mesto je povinné pole',
            'any.required': 'pole mesto je povinné pole',
          }),
          region: myCustomJoi.number().greater(0).required().messages({
            'string.empty': 'pole región je povinné pole',
            'any.required': 'pole región je povinné pole',
          }),
          phone: myCustomJoi.string().phoneNumber({ defaultCountry: 'SK', format: 'international', strict: true }).messages({
            'string.empty': 'pole telefón je povinné pole',
            'phoneNumber.invalid': 'pole telefón musí mať platný formát telefónneho čísla',
            'string.required': 'pole telefón je povinné pole'
          }),
          contact_email: myCustomJoi.string().email().required().messages({
            'any.required': 'pole email je povinné pole',
            'string.empty': 'pole email je povinné pole',
            'string.required': 'pole email je povinné pole',
            'string.email': 'pole email musí obsahovať platný email',
          }),
        })
      }

      if(user && user.privilegs == 'farmer'){
        schema = myCustomJoi.object({
          company_name: myCustomJoi.string().required().messages({
            'string.empty': 'pole meno farmy je povinné pole',
            'any.required': 'pole meno farmy je povinné pole',
          }),
          name: myCustomJoi.string().required().messages({
            'string.empty': 'pole meno prevádzkovateľa je povinné pole',
            'any.required': 'pole meno prevádzkovateľa je povinné pole',
          }),
          short_description: myCustomJoi.string().required().messages({
            'string.empty': 'pole krátky popis je povinné pole',
            'any.required': 'pole krátky popis je povinné pole',
          }),
          long_description: myCustomJoi.string().required().messages({
            'string.empty': 'pole dlhý popis je povinné pole',
            'any.required': 'pole dlhý popis je povinné pole',
          }),
          street: myCustomJoi.string().required().messages({
            'string.empty': 'pole ulica je povinné pole',
            'any.required': 'pole ulica je povinné pole',
          }),
          houseno: myCustomJoi.string().required().messages({
            'string.empty': 'pole číslo domu je povinné pole',
            'any.required': 'pole číslo domu je povinné pole',
          }),
          zip: myCustomJoi.string().required().messages({
            'string.empty': 'pole číslo domu je povinné pole',
            'any.required': 'pole číslo domu je povinné pole',
          }),
          city: myCustomJoi.string().required().messages({
            'string.empty': 'pole mesto je povinné pole',
            'any.required': 'pole mesto je povinné pole',
          }),
          region: myCustomJoi.number().greater(0).required().messages({
            'string.empty': 'pole región je povinné pole',
            'any.required': 'pole región je povinné pole',
          }),
          phone: myCustomJoi.string().phoneNumber({ defaultCountry: 'SK', format: 'international', strict: true }).messages({
            'string.empty': 'pole telefón je povinné pole',
            'phoneNumber.invalid': 'pole telefón musí mať platný formát telefónneho čísla',
            'string.required': 'pole telefón je povinné pole'
          }),
          contact_email: myCustomJoi.string().email().required().messages({
            'string.empty': 'pole email je povinné pole',
            'string.required': 'pole email je povinné pole',
            'string.email': 'pole email musí obsahovať platný email',
          }),
          // invoice_company_name: myCustomJoi.string()
          //     .required()
          //     .messages({
          //         'string.empty': 'povinné pole',
          //         'any.required': 'povinné pole'
          //     }),
          // invoice_business_id: myCustomJoi.string()
          //     .required()
          //     .messages({
          //         'string.empty': 'povinné pole',
          //         'any.required': 'povinné pole'
          //     }),
          // invoice_vat_id: myCustomJoi.string()
          //     .required()
          //     .messages({
          //         'string.empty': 'povinné pole',
          //         'any.required': 'povinné pole'
          //     }),
          // invoice_tax_id: myCustomJoi.string()
          //     .required()
          //     .messages({
          //         'string.empty': 'povinné pole',
          //         'any.required': 'povinné pole'
          //     }),
          // invoice_reg_id: myCustomJoi.string()
          //     .required()
          //     .messages({
          //         'string.empty': 'povinné pole',
          //         'any.required': 'povinné pole'
          //     }),
          collection_person: Joi.boolean().required().messages({
            '*': 'pole je povinné',
          }),
          collection_courier: Joi.boolean().required().messages({
            '*': 'pole je povinné',
          }),
          collection_regular_delivery: Joi.boolean().required().messages({
            '*': 'pole je povinné',
          }),
          payment_cash: Joi.boolean().required().messages({
            '*': 'pole je povinné',
          }),
          payment_card: Joi.boolean().required().messages({
            '*': 'pole je povinné',
          }),
          payment_courier: Joi.boolean().required().messages({
            '*': 'pole je povinné',
          }),
          payment_crypto: Joi.boolean().required().messages({
            '*': 'pole je povinné',
          }),
          services_selfpicking: Joi.boolean().required().messages({
            '*': 'pole je povinné',
          }),
          services_stay: Joi.boolean().required().messages({
            '*': 'pole je povinné',
          }),
          services_kitchen: Joi.boolean().required().messages({
            '*': 'pole je povinné',
          }),
          services_sale: Joi.boolean().required().messages({
            '*': 'pole je povinné',
          }),
          eco_friendly: Joi.boolean().required().messages({
            '*': 'pole je povinné',
          }),
          bio: Joi.boolean().required().messages({
            '*': 'pole je povinné',
          }),
          zero_waste: Joi.boolean().required().messages({
            '*': 'pole je povinné',
          }),
          oh_1_from_h: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_1_from_m: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_1_to_h: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_1_to_m: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_2_from_h: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_2_from_m: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_2_to_h: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_2_to_m: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_3_from_h: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_3_from_m: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_3_to_h: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_3_to_m: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_4_from_h: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_4_from_m: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_4_to_h: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_4_to_m: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_5_from_h: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_5_from_m: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_5_to_h: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_5_to_m: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_6_from_h: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_6_from_m: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_6_to_h: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_6_to_m: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_7_from_h: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_7_from_m: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_7_to_h: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
          oh_7_to_m: Joi.number().required().messages({
            '*': 'pole je povinné',
          }),
        });
      }

      let validationData = {};

      if(user && user.privilegs == 'customer'){
        validationData = {
          name: name,
          street: street,
          houseno: houseno,
          zip: zip,
          phone: phone,
          contact_email: contact_email,
          region: region,
          city: city,
        }
      }

      if(user && user.privilegs == 'farmer'){
        validationData = {
          company_name: company_name,
          name: name,
          short_description: short_description,
          long_description: long_description,
          street: street,
          houseno: houseno,
          zip: zip,
          phone: phone,
          contact_email: contact_email,
          region: region,
          city: city,
          // invoice_company_name: invoice_company_name,
          // invoice_business_id: invoice_business_id,
          // invoice_vat_id: invoice_vat_id,
          // invoice_tax_id: invoice_tax_id,
          // invoice_reg_id: invoice_reg_id,
          collection_person: collection_person,
          collection_courier: collection_courier,
          collection_regular_delivery: collection_regular_delivery,
          payment_cash: payment_cash,
          payment_card: payment_card,
          payment_courier: payment_courier,
          payment_crypto: payment_crypto,
          services_selfpicking: services_selfpicking,
          services_stay: services_stay,
          services_kitchen: services_kitchen,
          services_sale: services_sale,
          eco_friendly: eco_friendly,
          bio: bio,
          zero_waste: zero_waste,
          oh_1_from_h: oh_1_from_h,
          oh_1_from_m: oh_1_from_m,
          oh_1_to_h: oh_1_to_h,
          oh_1_to_m: oh_1_to_m,
          oh_2_from_h: oh_2_from_h,
          oh_2_from_m: oh_2_from_m,
          oh_2_to_h: oh_2_to_h,
          oh_2_to_m: oh_2_to_m,
          oh_3_from_h: oh_3_from_h,
          oh_3_from_m: oh_3_from_m,
          oh_3_to_h: oh_3_to_h,
          oh_3_to_m: oh_3_to_m,
          oh_4_from_h: oh_4_from_h,
          oh_4_from_m: oh_4_from_m,
          oh_4_to_h: oh_4_to_h,
          oh_4_to_m: oh_4_to_m,
          oh_5_from_h: oh_5_from_h,
          oh_5_from_m: oh_5_from_m,
          oh_5_to_h: oh_5_to_h,
          oh_5_to_m: oh_5_to_m,
          oh_6_from_h: oh_6_from_h,
          oh_6_from_m: oh_6_from_m,
          oh_6_to_h: oh_6_to_h,
          oh_6_to_m: oh_6_to_m,
          oh_7_from_h: oh_7_from_h,
          oh_7_from_m: oh_7_from_m,
          oh_7_to_h: oh_7_to_h,
          oh_7_to_m: oh_7_to_m,
        }
      }

      try {
        const validation = await schema.validateAsync(validationData, {abortEarly: false});
        console.log('validation', validation);
      }
      catch (err) {
          console.log('validation error', err);
          validation_error = err;
      }

      // validate region

      // get zip, normalize
      let normalized_zip = zip.replace(/\s/g, '');
      // find in db
      const finded_zip = await fastify.db.models.city.findOne({
        where: { zip: normalized_zip }
      });

      if(finded_zip){
        // compare with selected region
        if(finded_zip.dataValues.region != region){
          if(!validation_error){
            validation_error = {};
            validation_error.details = [];
          }
          validation_error.details['region'] = {path:['region'], message: 'Nesprávny región.'};
        }
      }
      else{
        if(!validation_error){
          validation_error = {};
          validation_error.details = [];
        }
        validation_error.details['zip'] = {path:['zip'], message: 'Musí byť platné PSČ.'};
      }

      if(validation_error){

          validation_error_sanitized = {};

          for(var key in validation_error.details){
              let message = validation_error.details[key].message;
              let path = validation_error.details[key].path[0];

              validation_error_sanitized[path] = message;
          }

          console.log('validation_error_sanitized', validation_error_sanitized);

      }
      else{

        let updated = false;

        if(user && user.privilegs == 'customer'){
          updated = await fastify.db.models.user.update({
            name: name,
            street: street,
            houseno: houseno,
            zip: zip,
            phone: phone,
            city: city,
            contact_email: contact_email,
          }, {
            where: {
              id: user_id
            }
          })
        }

        if(user && user.privilegs == 'farmer'){

          let invoice = {
            invoice_company_name: invoice_company_name,
            invoice_business_id: invoice_business_id,
            invoice_vat_id: invoice_vat_id,
            invoice_tax_id: invoice_tax_id,
            invoice_reg_id: invoice_reg_id,
          };

          let data = {
            name: name,
            company_name: company_name,
            short_description: short_description,
            long_description: long_description,
            street: street,
            houseno: houseno,
            zip: zip,
            phone: phone,
            city: city,
            contact_email: contact_email,
            region_id: region,
            additional_information: additional_information,
            opening_hours_new: opening_hours,
            gps: gps,
            invoice: invoice
          }
         
          updated = await fastify.db.models.user.update(data, {
            where: {
              id: user_id
            }
          })
        }

        const updated_user = await fastify.db.models.user.findOne({ 
          include: [ 
              { model: fastify.db.models.category, as: "category" },
              { model: fastify.db.models.region, as: 'region' },
          ],
          where: { id: user_id }
        });


        // save to solr
        const client = solr.createClient({
            path: '/solr/odfarmara_farmers'
        });
        client.autoCommit = true;

        let m_created_date = moment(updated_user.dataValues.created);
        let created_date = m_created_date.toISOString();
        const region_id = updated_user.dataValues.region_id;

        // let account_type = '1';
        // if(profile_type == 'premium'){
        //     account_type = '2';
        // }

        let region_name = '';
        if(region_id && region_id > 0){
            region_name = updated_user.dataValues.region.name;
        }

        if(user && user.privilegs == 'farmer'){
          let doc = {
            id : user_id,
            ge_title : {"set": name},
            ge_description : {"set": long_description},
            // ge_category : category_id,
            ge_name: {"set": updated_user.dataValues.name},
            ge_company_name : {"set": company_name},
            ge_company_name_keyword : {"set": company_name},
            ge_region_name : {"set": region_name},
            ge_thumbnail : {"set": request.user.intro_image},
            ge_profile_img : {"set": request.user.profile_image},
            // ge_account_type : account_type,
            ge_active : {"set": updated_user.dataValues.active},
            ge_region : {"set": region_id},
            ge_created_date : {"set": created_date},
            ge_rating: {"set": updated_user.rating_avg},
          }

          if(gps_latitude && gps_longitude){
            doc.ge_gps_lat = {"set": gps_latitude};
            doc.ge_gps_lng = {"set": gps_longitude};
          }

          console.log('doc', doc);

          const obj = await client.add(doc);
          const commit = await client.commit();
        }
        
        
        if(updated){
          successProcessed = true;
        }

      }

      if(validation_error){
        responseData.validationErrors = validation_error_sanitized;
      }

    }

    // regions
    const countryRegions = await fastify.db.models.region.findAll({
      where: {
        active: '1',
      }
    });

    responseData.userLogged = userLogged;
    responseData.save_success = successProcessed;
    responseData.countryRegions = countryRegions;

    reply.view('/pages/profile/profile', responseData);

  })

  fastify.post('/ucet/gps', async (request, reply) => {

    const user = request.user;
    const street = request.body.street;
    const city = request.body.city;
    const houseno = request.body.houseno;
    const zip = request.body.zip;

    let address = street + " " + houseno + ", " + zip + " " + city + ", Slovenská republika";
    // console.log('address', address);
    address = encodeURIComponent(address);

    let result = await axios.get('https://geocode.maps.co/search?q=' + address);
    console.log('result', result.data);

    let data = [];
    let empty = true; 
    if(result.data.length){
      data = result.data;
      empty = false;

      request.session.set('profileGPS', data);
    }

    let gps = user.gps;
    let gps_latitude = '';
    let gps_longitude = '';
    if(gps && gps.length){
      let gps_arr = gps.split(',');
      gps_latitude = gps_arr[0];
      gps_longitude = gps_arr[1];
    }

    reply.view('/elements/content/profile/gps.njk', {
      data: data,
      resultEmpty: empty,
      gps_latitude: gps_latitude,
      gps_longitude: gps_longitude
    });

  })

}

module.exports = routes
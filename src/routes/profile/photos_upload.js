const { nanoid } = require('nanoid');
const {promisify} = require('util');
const fs = require('fs');
const util = require('util')
const mv = promisify(fs.rename);
const process = require('process');
const { Op, Sequelize } = require("sequelize");
const Joi = require('joi').extend(require('@joi/date'));
const moment = require('moment');
const passwordComplexity = require("joi-password-complexity");
const argon2 = require('argon2');
const myCustomJoi = Joi.extend(require('joi-phone-number'));
const { pipeline } = require('stream');
const pump = util.promisify(pipeline);
const sharp = require("sharp");
const solr = require('solr-client');
const { v4: uuidv4 } = require('uuid');
const axios = require('axios');

async function routes (fastify, options) {

  fastify.post('/ucet/upload', async (request, reply) => {

    const user = request.user;
    let success = false;

    //request.session.set('data', request.body);

    const data = await request.file()

    console.log('data', data);

    const fileName = data.filename;

    let fileId = await nanoid();
    let fileExt = fileName.split(".").pop();
    let newFileName = fileId + "." + fileExt;

    const destPath = process.cwd() + '/public/assets_web/thumb_large/' + newFileName;
    const absoluteFileUrl = fastify.config.ROOT + '/assets/assets_web/thumb_large/' + newFileName;
    const relativeFileUrl = 'assets_web/thumb_large/' + newFileName;

    // todo validate mime and filesize
    // todo handle logout user

    const mimetype = data.mimetype; // 'image/jpeg'
    const length = data.file.length; // file size in bytes

    await pump(data.file, fs.createWriteStream(destPath));

    // generate sizes from large size
    try {
      await sharp('public/assets_web/thumb_large/' + newFileName)
        .resize({
          height: 300,
        })
        .toFile('public/assets_web/thumb_small/' + newFileName);
      await sharp('public/assets_web/thumb_large/' + newFileName)
        .resize({
          height: 600,
        })
        .toFile('public/assets_web/thumb_medium/' + newFileName);
    } catch (error) {
      console.log(error);
    }

    if(user){

      const client = solr.createClient({
        path: '/solr/odfarmara_farmers'
      });
      client.autoCommit = true;

      const user_id = user.id;

      let doc = {
        id : user_id,
      }

      let savedData = {};
      // profile photo
      if(data.fieldname === 'profile_photo'){
        savedData.profile_image = newFileName;
        doc.ge_profile_img = {"set": newFileName};
      }
      // intro photo
      if(data.fieldname === 'intro_photo'){
        savedData.intro_image = newFileName;
        doc.ge_thumbnail = {"set": newFileName};
      }

      success = await fastify.db.models.user.update( savedData, {
        where: {
          id: user_id,
        }
      })

      const obj = await client.add(doc);
      const commit = await client.commit();

    }

    reply.send({fileUrl: absoluteFileUrl});

  })

  fastify.post('/ucet/upload/multiple', async (request, reply) => {

    const user = request.user;
    let success = false;
    let images = request.session.get('productImages');
    if(!images){
      images = [];
    }

    // files
    if(user){
      const parts = request.files();
      console.log('parts', parts);
    
      for await (const part of parts) {
        const fileName = part.filename;
        let fileId = await nanoid();

        let fileExt = fileName.split(".").pop();
        let newFileName = fileId + "." + fileExt;

        // large size
        const destPath = process.cwd() + '/public/assets_web/thumb_large/' + newFileName;
        const absoluteFileUrl = fastify.config.ROOT + '/assets/assets_web/thumb_large/' + newFileName;
        const relativeFileUrl = 'assets_web/thumb_large/' + newFileName;

        // todo validate mime and filesize
        // todo handle logout user

        const mimetype = part.mimetype; // 'image/jpeg'
        const length = part.file.length; // file size in bytes

        console.log('part', part);

        let image = {
          id: await nanoid(),
          name: newFileName
        };

        await pump(part.file, fs.createWriteStream(destPath))

        // generate sizes from large size
        try {
          await sharp('public/assets_web/thumb_large/' + newFileName)
            .resize({
              height: 300,
            })
            .toFile('public/assets_web/thumb_small/' + newFileName);
          await sharp('public/assets_web/thumb_large/' + newFileName)
            .resize({
              height: 600,
            })
            .toFile('public/assets_web/thumb_medium/' + newFileName);
        } catch (error) {
          console.log(error);
        }

        images.push(image);
      }
      console.log('images', images);
      request.session.set('productImages', images);
    }

    reply.send({status: 'ok'});

  })

  fastify.post('/ucet/produkt/odstranit-fotku', async (request, reply) => {

    const id = request.body.id;
    let newOfferUpload = request.session.get('productImages');
    console.log('newOfferUpload', newOfferUpload);
    let indexToRemove = null;
    for(let x=0;x < newOfferUpload.length;x++) {
      if(newOfferUpload[x].id == id) {
        indexToRemove = x;
        console.log('x', x);
        break;
      }
    }

    if(indexToRemove != null){
      newOfferUpload.splice(indexToRemove, 1);
    }

    console.log('indexToRemove', indexToRemove);
    console.log('newOfferUploadDeleted', newOfferUpload);
    request.session.set('productImages', newOfferUpload);

    reply.send({status: 'ok'});
  })

  fastify.post('/ucet/produkt/zoradit-fotky', async (request, reply) => {

    const uploadedPhotosValue = request.body.uploadedPhotosValue;
    const uploadedPhotosValueArray = uploadedPhotosValue.split(",");
    const newOfferUpload = request.session.get('productImages');

    console.log('newOfferUpload', newOfferUpload);
    console.log('uploadedPhotosValue', uploadedPhotosValue);

    let newOfferUploadSorted = [];
    for(let x=0;x < uploadedPhotosValueArray.length;x++){
      let sortedId = uploadedPhotosValueArray[x];
      let findedObject = newOfferUpload.find(item => item.id == sortedId);
      if(findedObject){
        newOfferUploadSorted.push(findedObject);
      }
    }

    console.log('newOfferUploadSorted', newOfferUploadSorted);

    request.session.set('productImages', newOfferUploadSorted);

    reply.send({status: 'ok'});
    
  })

  fastify.post('/ucet/produkt/odstranit-fotky', async (request, reply) => {
    request.session.set('productImages', []);
    reply.view('/pages/profile/add-offer');
  })

}

module.exports = routes
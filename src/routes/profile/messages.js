const { nanoid } = require('nanoid');
const {promisify} = require('util');
const fs = require('fs');
const util = require('util')
const mv = promisify(fs.rename);
const process = require('process');
const { Op, Sequelize } = require("sequelize");
const Joi = require('joi').extend(require('@joi/date'));
const moment = require('moment');
const passwordComplexity = require("joi-password-complexity");
const argon2 = require('argon2');
const myCustomJoi = Joi.extend(require('joi-phone-number'));
const { pipeline } = require('stream');
const pump = util.promisify(pipeline);
const sharp = require("sharp");
const solr = require('solr-client');
const { v4: uuidv4 } = require('uuid');
const axios = require('axios');

async function routes (fastify, options) {
fastify.get('/ucet/spravy', async (request, reply) => {

    const user = request.user;

    let favoriteProducts = [];
    if(user && user.favorites.length){
      for(let a = 0; a < user.favorites.length; a++){
        if(user.favorites[a].id_offer){
          favoriteProducts.push(user.favorites[a].id_offer);
        }
      }
    }
    const favoritesLength = favoriteProducts.length;

    let where = {};
    if(user.privilegs === 'farmer'){
      where.farmar_id = user.id;
    }
    if(user.privilegs === 'customer'){
      where.user_id = user.id;
    }

    const latestIds = await fastify.db.models.inbox.findAll({
      attributes: [[Sequelize.fn("max", Sequelize.col('id_inbox')), 'id_inbox']],
      group: ["id_demand"],
      where: where,
    });

    let latestIdsArr = [];
    for(let x=0;x<latestIds.length;x++) {
      latestIdsArr.push(latestIds[x].id_inbox);
    }

    // inbox
    let inbox = await fastify.db.models.inbox.findAll({
      where: {
        id_inbox: {[Op.in]: latestIdsArr }
      },
      include: [ 
        { model: fastify.db.models.user, as: "user" },
        { model: fastify.db.models.user, as: "farmer" },
        { 
          model: fastify.db.models.demand, 
          as: "demand", 
          include: [
            { model: fastify.db.models.product, as: "product" }
          ] 
        },
      ],
      // limit: 5,
      order: [
        ['created', 'DESC'],
      ]
    });

    reply.view('/pages/profile/messages', {
      title: 'Odfarmara.sk',
      page: 'messages',
      inbox: inbox,
      favoritesLength: favoritesLength
    })
  })

  fastify.get('/ucet/sprava/:id', async (request, reply) => {

    const user = request.user;
    const id = parseInt(request.params.id);

    let favoriteProducts = [];
    if(user && user.favorites.length){
      for(let a = 0; a < user.favorites.length; a++){
        if(user.favorites[a].id_offer){
          favoriteProducts.push(user.favorites[a].id_offer);
        }
      }
    }
    const favoritesLength = favoriteProducts.length;

    // inbox
    let inbox = await fastify.db.models.inbox.findOne({
      where: {
        id_inbox: id,
      },
      include: [ 
        { model: fastify.db.models.user, as: "user" },
        { model: fastify.db.models.user, as: "farmer" },
        { 
          model: fastify.db.models.demand, 
          as: "demand", 
          include: [
            { model: fastify.db.models.product, as: "product" }
          ] 
        },
      ],
      // limit: 5,
      order: [
        ['created', 'ASC'],
      ]
    });

    console.log('inbox', inbox);

    let messages = [];
    if(inbox){

      let where = {
        farmar_id: inbox.farmar_id,
        user_id: user.id,
        id_demand: {[Op.is]: null}
      };

      if(inbox.id_demand){
        where.id_demand = inbox.id_demand;
      }

      console.log('where', where);

      messages = await fastify.db.models.inbox.findAll({
        where: where,
        include: [ 
          { model: fastify.db.models.user, as: "user" },
          { model: fastify.db.models.user, as: "farmer" },
          { 
            model: fastify.db.models.demand, 
            as: "demand", 
            include: [
              { model: fastify.db.models.product, as: "product" }
            ] 
          },
        ],
        // limit: 5,
        order: [
          ['created', 'ASC'],
        ]
      });

      console.log('messages', messages);
    }
    
    reply.view('/pages/profile/message', {
      title: 'Odfarmara.sk',
      page: 'messages',
      id: id,
      messages: messages,
      favoritesLength: favoritesLength
    })

  })
  
  fastify.post('/ucet/sprava/odoslat', async (request, reply) => {

    const user = request.user;

    let favoriteProducts = [];
    if(user && user.favorites.length){
      for(let a = 0; a < user.favorites.length; a++){
        if(user.favorites[a].id_offer){
          favoriteProducts.push(user.favorites[a].id_offer);
        }
      }
    }
    const favoritesLength = favoriteProducts.length;

    const id = parseInt(request.body.id);
    const message = request.body.message;

    // inbox
    let inbox = await fastify.db.models.inbox.findOne({
      where: {
        id_inbox: id,
      },
      include: [ 
        { model: fastify.db.models.user, as: "user" },
        { model: fastify.db.models.user, as: "farmer" },
        { 
          model: fastify.db.models.demand, 
          as: "demand", 
          include: [
            { model: fastify.db.models.product, as: "product" }
          ] 
        },
      ],
      // limit: 5,
      order: [
        ['created', 'ASC'],
      ]
    });

    console.log('inbox', inbox);

    let messages = [];
    if(inbox){

      let where = {
        farmar_id: inbox.farmar_id,
        user_id: user.id,
        id_demand: {[Op.is]: null}
      };

      if(inbox.id_demand){
        where.id_demand = inbox.id_demand;
      }

      console.log('where', where);

      let inboxData = {
        content: message,
        farmar_id: inbox.farmar_id,
        user_id: user.id,
        status: '0',
        seen: '0',
        seen_farmar: '0',
        hidden_to: '0',
        flag: '0'
      };

      if(inbox.id_demand){
        inboxData.id_demand = inbox.id_demand;
      }

      await fastify.db.models.inbox.create(inboxData);

      messages = await fastify.db.models.inbox.findAll({
        where: where,
        include: [ 
          { model: fastify.db.models.user, as: "user" },
          { model: fastify.db.models.user, as: "farmer" },
          { 
            model: fastify.db.models.demand, 
            as: "demand", 
            include: [
              { model: fastify.db.models.product, as: "product" }
            ] 
          },
        ],
        // limit: 5,
        order: [
          ['created', 'ASC'],
        ]
      });

      console.log('messages', messages);
    }

    reply.view('/pages/profile/message', {
      title: 'Odfarmara.sk',
      page: 'products',
      id: id,
      messages: messages,
      favoritesLength: favoritesLength
    })

  })
}

module.exports = routes
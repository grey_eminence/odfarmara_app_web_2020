const { nanoid } = require('nanoid');
const {promisify} = require('util');
const fs = require('fs');
const util = require('util')
const mv = promisify(fs.rename);
const process = require('process');
const { Op, Sequelize } = require("sequelize");
const Joi = require('joi').extend(require('@joi/date'));
const moment = require('moment');
const passwordComplexity = require("joi-password-complexity");
const argon2 = require('argon2');
const myCustomJoi = Joi.extend(require('joi-phone-number'));
const { pipeline } = require('stream');
const pump = util.promisify(pipeline);
const sharp = require("sharp");
const solr = require('solr-client');
const { v4: uuidv4 } = require('uuid');
const axios = require('axios');

async function routes (fastify, options) {

    fastify.get('/ucet/nastavenia', async (request, reply) => {

      const user = request.user;
      let favoriteProducts = [];
      if(user && user.favorites.length){
        for(let a = 0; a < user.favorites.length; a++){
          if(user.favorites[a].id_offer){
            favoriteProducts.push(user.favorites[a].id_offer);
          }
        }
      }
      const favoritesLength = favoriteProducts.length;

      const email = request.user.email;
      const notification_email = request.user.notification_email;
      const notification_only_week = request.user.notification_only_week;
      const notification_new_products = request.user.notification_new_products;

      reply.view('/pages/profile/settings', {
        title: 'Odfarmara.sk',
        page: 'settings',
        favoritesLength: favoritesLength,
        email: email,
        notification_email: notification_email,
        notification_only_week: notification_only_week,
        notification_new_products: notification_new_products
      })
    })

    fastify.post('/ucet/heslo', async (request, reply) => {

        console.log('/ucet/heslo');
  
        let password = request.body.password;
        let password_repeat = request.body.password_repeat;
  
        const user = request.user;
  
        let password_success = false;
        let userLogged = false;
        let validation_error = false;
        let validation_error_sanitized = false;
  
        let responseData = {
          password: password,
          password_repeat: password_repeat,
        }
  
        if(user){
  
          userLogged = true;
          const user_id = request.user.id;
  
          responseData.email = request.user.email;
  
          // validation schema
          const schema = Joi.object({
            password: new passwordComplexity({
              min: 8,
              max: 30,
              lowerCase: 1,
              upperCase: 1,
              numeric: 1,
              symbol: 1,
              requirementCount: 4
            }).messages({
              '*': 'pole heslo musí obsahovať 8 až 30 znakov, malé aj veľké písmená, čísla aj symboly ako napr. # + - ! @',
            }),
            password_repeat: Joi.any().valid(Joi.ref('password')).required().messages({
              'string.empty': 'pole zopakovať heslo sa musí zhodovať s heslom',
              'any.only': 'pole zopakovať heslo sa musí zhodovať s heslom',
              'any.required': 'pole zopakovať heslo sa musí zhodovať s heslom'
            }),
          });
   
          // validation errors
          try {
              const data = {
                password: password, 
                password_repeat: password_repeat
              };
              const validation = await schema.validateAsync(data, {abortEarly: false});
              console.log('data', data);
              console.log('validation', validation);
          }
          catch (err) {
              validation_error = err;
          }
  
          if(validation_error){
  
            validation_error_sanitized = {};
  
            for(var key in validation_error.details){
                let message = validation_error.details[key].message;
                let path = validation_error.details[key].path[0];
  
                validation_error_sanitized[path] = message;
            }
  
            console.log('validation_error_sanitized', validation_error_sanitized);
  
          }
          else{
            const password_hash = await argon2.hash(password, {type: argon2.argon2id});
            password_success = await fastify.db.models.user.update({
              password_new: password_hash
            }, {
              where: {
                id: user_id
              }
            })
  
            if(password_success){
              password_success = true;
              responseData.password = null;
              responseData.password_repeat = null;
            }
          }
        }
  
        if(validation_error){
          responseData.validationErrors = validation_error_sanitized;
        }
  
        responseData.password_success = password_success;
        responseData.userLogged = userLogged;
  
        reply.view('/elements/content/form/change_password', responseData);
      })

      fastify.post('/ucet/notifikacie', async (request, reply) => {

        const user = request.user;
        let notification_email = request.body.notification_email ? true : false;
        let notification_only_week = null;
        let notification_new_products = null;
        let notification_success = false;
        let userLogged = false;
        let validation_error = false;
        let validation_error_sanitized = false;
  
        let responseData = {
          notification_email: notification_email,
          
        }
  
        if(user && user.privilegs === 'customer'){
          notification_only_week = request.body.notification_only_week ? true : false;
          notification_new_products = request.body.notification_new_products ? true : false;
  
          responseData.notification_only_week = notification_only_week;
          responseData.notification_new_products = notification_new_products;
        }
  
        if(user){
  
          userLogged = true;
          const user_id = request.user.id;
  
          let schema = {};
          let data = {};
          // validation schema
          if(user.privilegs === 'customer'){
            schema = Joi.object({
              notification_email: Joi.boolean().required().messages({
                '*': 'pole je povinné',
              }),
              notification_only_week: Joi.boolean().required().messages({
                '*': 'pole je povinné',
              }),
              notification_new_products: Joi.boolean().required().messages({
                '*': 'pole je povinné',
              }),
            });
  
            data = {
              notification_email: notification_email,
              notification_only_week: notification_only_week,
              notification_new_products: notification_new_products,
            }
  
          }
  
          if(user.privilegs === 'farmer'){
            schema = Joi.object({
              notification_email: Joi.boolean().required().messages({
                '*': 'pole je povinné',
              }),
            });
  
            data = {
              notification_email: notification_email,
            }
          }
  
          // validation errors
          try {
              const validation = await schema.validateAsync(data, {abortEarly: false});
              console.log('validation', validation);
          }
          catch (err) {
              validation_error = err;
          }
  
          if(validation_error){
  
            validation_error_sanitized = {};
  
            for(var key in validation_error.details){
                let message = validation_error.details[key].message;
                let path = validation_error.details[key].path[0];
  
                validation_error_sanitized[path] = message;
            }
  
            console.log('validation_error_sanitized', validation_error_sanitized);
  
          }
          else{
  
            let updateData = {
              notification_email: notification_email,
            };
  
            if(user.privilegs === 'customer'){
              updateData.notification_only_week = notification_only_week;
              updateData.notification_new_products = notification_new_products;
            }
  
            notification_success = await fastify.db.models.user.update(updateData, {
              where: {
                id: user_id
              }
            });
  
            if(notification_success){
              notification_success = true;
            }
  
          }
  
        }
  
        if(validation_error){
          responseData.validationErrors = validation_error_sanitized;
        }
  
        responseData.notification_success = notification_success;
        responseData.userLogged = userLogged;
  
        console.log('responseData', responseData);
  
        reply.view('/elements/content/form/notifications', responseData);
  
      })

}

module.exports = routes
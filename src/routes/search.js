
const argon2 = require('argon2')
const solr = require('solr-client')

async function routes (fastify, options) {
    fastify.get('/hladat', async (req, reply) => {

    let q = req.query.q;
    let c = req.query.c;
    let p = req.query.p;
    let docs = [];
    let query_text = "*:*";
    let ipp = 12;
    let used = false;
    let startItem = 0;

    // default page
    if(!p){
      p = 1;
    }

    if(q){
      query_text = 'ge_title:' + q + ', ' + 'ge_description:' + q;
    }

    let query_params = '';
    let query_pagination = '';
    if(q || c || p){
      query_params += '?';
    }

    if(q){
      query_params += 'q=' + q;
      used = true;
    }

    if(used){
      query_params += '&';
    }

    if(c){
      query_params += 'c=' + c;
      used = true;
    }

    if(used){
      query_params += '&';
    }

    if(p){
      query_params += 'p=' + p;
      used = true;
      startItem = (ipp * (p-1));
    }

    //Create a client
    const client = solr.createClient({
      path: '/solr/odfarmara_products'
    });

    const query = client
    .query()
    .q(query_text)
    .matchFilter('ge_category', '(6 OR 7)')
    // .dismax()
    //.qf({ title_t: 0.2, description_t: 3.3 })
    //.mm(2)
    .start(startItem)
    .sort({ ge_title : 'asc'})
    .rows(ipp);

    const data = await client.search(query);
    docs = data.response.docs;
    console.log("result", data.response);

    // set pages count
    let pages = Math.floor(data.response.numFound / ipp);
    let pages_modulo = data.response.numFound % ipp;
    if(pages_modulo){
      pages++;
    }

    let baseUrl = fastify.config.ROOT + "/hladat";

    reply.view('/pages/search', { 
        title: 'Odfarmara.sk',
        docs: docs,
        q: q,
        p: parseInt(p),
        c: c,
        pages: parseInt(pages),
        queryParams: query_params,
        startItem: startItem,
        numFound: data.response.numFound,
        baseUrl: baseUrl
      })
    });
  }
  
  module.exports = routes
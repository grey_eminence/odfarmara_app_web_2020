
const { Op } = require("sequelize");
const URI = require('urijs');
const moment = require('moment');
const _ = require('lodash');

async function routes (fastify, options) {
  
  fastify.get('/', async (request, reply) => {

    const season_offers_categories = await fastify.getSeasonOffersCategories();
    const season_offers_products = await fastify.getSeasonOffersProducts();

    const user =  request.user;
    
    // favorite products
    let favoriteProducts = [];
    if(user && user.favorites.length){
      for(let a = 0; a < user.favorites.length; a++){
        if(user.favorites[a].id_offer){
          favoriteProducts.push(user.favorites[a].id_offer);
        }
      }
    }

    const tags = await fastify.db.models.tag.findAll({
      where: { 
        active: '1',
        landing_page: '1',
      },
    });

    let products_count = await fastify.db.models.product.count()
    let customers_count = await fastify.db.models.user.count({ where: { privilegs: 'customer' } })
    let farmers_count = await fastify.db.models.user.count({ where: { privilegs: 'farmer' } })


    // // sessional offers
    // let current_date = moment();
    // let current_date_formated = moment().format('YYYY-MM-DD'); 
    // let sessional_offers = await fastify.db.models.seasonal_offer.findAll({
    //   where: {
    //     active: '1',
    //     display_from: {
    //       [Op.lte] : current_date_formated
    //     },
    //     display_to: {
    //       [Op.gte] : current_date_formated
    //     },
    //   }
    // });

    // sessional_offers_categories = [];
    // for(let x = 0; x < sessional_offers.length; x++){
    //   let item = sessional_offers[x];
    //   if(item.categories && item.categories.length){
    //     for(let y = 0; y < item.categories.length; y++){
    //       sessional_offers_categories.push(item.categories[y]);
    //     }
    //   }
    // }

    // // unique
    // sessional_offers_categories = _.uniq(sessional_offers_categories);

    // let sessional_offers_categories_db = [];
    // if(sessional_offers_categories.length){
    //   sessional_offers_categories_db = await fastify.db.models.category.findAll({
    //     where: {
    //       id: {[Op.in]: sessional_offers_categories }
    //     }
    //   })
    // }

    // // console.log('sessional_offers.categories', sessional_offers_categories);
    // // console.log('sessional_offers_categories_db', sessional_offers_categories_db);

    // let session_products = await fastify.db.models.product.findAll({
    //   limit: 9,
    //   include: [ 
    //     // { model: fastify.db.models.image, as: "image", required: true },
    //     { model: fastify.db.models.image, as: "image" },
    //     { model: fastify.db.models.user, as: "farmer", required: true },
    //   ],
    //   where: { 
    //     active: '1',
    //   },
    //   order: [
    //     ['created', 'DESC'], ['image', 'img_id', 'ASC']
    //   ]
    // });

    let new_products = await fastify.db.models.product.findAll({
      limit: 5,
      include: [ 
        // { model: fastify.db.models.image, as: "image", required: true },
        { model: fastify.db.models.image, as: "image" },
        { model: fastify.db.models.user, as: "farmer", required: true },
      ],
      where: { 
        active: '1',
      },
      order: [
        ['created', 'DESC'], ['image', 'img_id', 'ASC']
      ]
    });

    let vegetable_products = await fastify.db.models.product.findAll({
      limit: 6,
      include: [ 
        // { model: fastify.db.models.image, as: "image", required: true },
        { model: fastify.db.models.image, as: "image" },
        { model: fastify.db.models.user, as: "farmer", required: true },
        { 
          model: fastify.db.models.category, 
          required: true,
          as: "category" ,
          where: {
            id: 1
          }
        },
      ],
      where: { 
        active: '1',
      },
      order: [
        ['created', 'DESC'], ['image', 'img_id', 'ASC']
      ]
    });

    let fruit_products = await fastify.db.models.product.findAll({
      limit: 6,
      include: [ 
        // { model: fastify.db.models.image, as: "image", required: true },
        { model: fastify.db.models.image, as: "image" },
        { model: fastify.db.models.user, as: "farmer", required: true },
        { 
          model: fastify.db.models.category, 
          required: true,
          as: "category" ,
          where: {
            id: 2
          }
        },
      ],
      where: { 
        active: '1',
      },
      order: [
        ['created', 'DESC'], ['image', 'img_id', 'ASC']
      ]
    });

    let next_products = await fastify.db.models.product.findAll({
      limit: 6,
      include: [ 
        // { model: fastify.db.models.image, as: "image", required: true },
        { model: fastify.db.models.image, as: "image" },
        { model: fastify.db.models.user, as: "farmer", required: true },
        { 
          model: fastify.db.models.category, 
          required: true,
          as: "category" ,
          where: {
            id: 3
          }
        },
      ],
      where: { 
        active: '1',
      },
      order: [
        ['created', 'DESC'], ['image', 'img_id', 'ASC']
      ]
    });

    let farms = await fastify.db.models.user.findAll({
      include: [ 
        {
          model: fastify.db.models.farmer_rating, 
          as: "rating" ,
        },
      ],
      where: { 
        privilegs: 'farmer',
        gps: {
          [Op.not]: null,    
        }
      },
      order: [
        ['created', 'DESC']
      ]
    });

    let new_farms = await fastify.db.models.user.findAll({
      where: { 
        privilegs: 'farmer',
      },
      limit: 4,
      order: [
        ['created', 'DESC']
      ]
    });

    let recipes = await fastify.db.models.recipe.findAll({
      limit: 4,
      include: [ 
        {
          model: fastify.db.models.category, 
          as: "category" ,
        },
      ],
      order: [
        ['created_at', 'DESC']
      ],
      where: { 
        active: '1',
      },
    });

    let articles = await fastify.db.models.article.findAll({
      limit: 3,
      order: [
        ['created', 'DESC']
      ],
      where: { 
        active: '1',
      },
    });

    let docs = [];
    let query_text = "*:*";
    let ipp = 12;
    let startItem = 0;

    let uri = new URI(request.url);
    let urlQuery = uri.search(true);
    let c = urlQuery.c;
    let mc = urlQuery.mc;
    let e = urlQuery.e;
    let q = urlQuery.q;
    let p = urlQuery.p;
    let r = urlQuery.r;


    reply.view('/pages/index', { 
      title: 'Odfarmara.sk - Nakupujte priamo od farmárov',
      description: 'Nájdite lokálne vypestované ovocie, zeleninu a výrobky vo vašom okolí.',
      keywords: 'odfarmara.sk, od farmara, farmarske produkty, farmarske vyrobky, lokalne potraviny, lokalne vypestovane, lokalne dopestovane, farmarska aplikacia, od farmarov, od slovenskych vyrobcov, slovenske farmy, slovenski vyrobcovia',
      favoriteProducts: favoriteProducts,
      articles: articles,
      recipes: recipes,
      new_farms: new_farms,
      farms: farms,
      next_products: next_products,
      fruit_products: fruit_products,
      vegetable_products: vegetable_products,
      new_products: new_products,
      session_products: season_offers_products,
      sessional_offers_categories: season_offers_categories,
      products_count: products_count,
      customers_count: customers_count,
      farmers_count: farmers_count,
      docs: docs,
      q: q,
      p: parseInt(p),
      c: c,
      mc: mc,
      e: e,
      r: r,
      tags: tags
    })

  })
}

module.exports = routes
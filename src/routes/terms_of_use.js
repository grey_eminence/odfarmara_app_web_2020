
async function routes (fastify, options) {
    fastify.get('/podmienky-pouzivania', async (request, reply) => {
      
      reply.view('/pages/terms_of_use', { 
        title: 'Podmienky používania - Odfarmara.sk',
      })

    })
  }
  
  module.exports = routes
const { Op } = require("sequelize");

async function routes (fastify, options) {
    fastify.get('/recept/:id', async (request, reply) => {

      const id = request.params.id;

      const categories = await fastify.db.models.category.findAll({
        where: { parent_id: {[Op.is]: null}, type: "recipe" },
      }) 

      const recipe = await fastify.db.models.recipe.findOne({
        include: [ 
          { model: fastify.db.models.category, as: "category" },
        ],
        where: { 
          id: id,
        },
      });

      let baseUrl = fastify.config.ROOT + "/recepty";

      reply.view('/pages/recipe', { 
        title: recipe.title  + ' - Odfarmara.sk',
        categories: categories,
        recipe: recipe,
        baseUrl: baseUrl,
        current_url: request.url,
      })
    })
  }
  
  module.exports = routes
const solr = require('solr-client')
const fs = require('fs')
const util = require('util')
const path = require('path')
const { nanoid } = require('nanoid')
const { pipeline } = require('stream')
const pump = util.promisify(pipeline)
const process = require('process')
const moment = require('moment')
const Joi = require('joi')
const slugify = require('slugify')
const _ = require('lodash')
const { Op } = require('sequelize')

async function routes (fastify, options) {

    fastify.post('/admin/flags/select', async (request, reply) => {

        const user_id = request.user.id;
        let docs = [];
        let query_text = "*:*";
        let startItem = 0;

        const request_query = request.body;
        let ipp = 100;
        let to = false;
        if(request_query.to){
            to = parseInt(request_query.to);
            ipp = 100 * Math.ceil(to / 100);
        }
        const search = request_query.search
        const page = request_query.page
        if (page) {
            startItem = (page * ipp) - ipp;
        }

        console.log('search', search);

        const order_by = request_query.sortBy;

        let order_type = '';
        if (order_by) {
            const desc = request_query.descending
            
            if(desc){
                order_type = 'desc'
            }
            else {
                order_type = 'asc'
            }
        }

        if(search){
            query_text = 'ge_name:' + search + ', ' + 'ge_slug:' + search;
        }

        let sortParams = {}; 
        sortParams[order_by] = order_type;

        const client = solr.createClient({
            path: '/solr/odfarmara_flags'
        });

        if(to > 90){
            ipp = ipp + 100;
        }

        const query = client
        .query()
        .q(query_text)
        .start(startItem)
        .rows(ipp);

        if(order_by){
            query.sort(sortParams);
        }

        const data = await client.search(query);
        docs = data.response.docs;

        // set pages count
        let pages = Math.floor(data.response.numFound / ipp);
        let pages_modulo = data.response.numFound % ipp;
        if(pages_modulo){
        pages++;
        }

        console.log('user_id', user_id);
        reply.send({ 
            user: user_id,
            docs: docs,
            pages: parseInt(pages),
            count: data.response.numFound,
            max_to: ipp
        })

    })

    fastify.post('/admin/flags', async (request, reply) => {

        const user_id = request.user.id;
        let docs = [];
        let query_text = "*:*";
        let startItem = 0;

        const request_query = request.body;
        const ipp = parseInt(request_query.rowsPerPage)
        const search = request_query.search
        const page = request_query.page
        if (page) {
            startItem = (page * ipp) - ipp;
        }

        console.log('search', search);

        const order_by = request_query.sortBy;

        let order_type = '';
        if (order_by) {
            const desc = request_query.descending
            
            if(desc){
                order_type = 'desc'
            }
            else {
                order_type = 'asc'
            }
        }

        if(search){
            query_text = 'ge_name:' + search + ', ' + 'ge_slug:' + search;
        }

        let sortParams = {}; 
        sortParams[order_by] = order_type;

        const client = solr.createClient({
            path: '/solr/odfarmara_flags'
        });

        const query = client
        .query()
        .q(query_text)
        .start(startItem)
        .rows(ipp);

        if(order_by){
            query.sort(sortParams);
        }

        const data = await client.search(query);
        docs = data.response.docs;

        // set pages count
        let pages = Math.floor(data.response.numFound / ipp);
        let pages_modulo = data.response.numFound % ipp;
        if(pages_modulo){
        pages++;
        }

        console.log('user_id', user_id);
        reply.send({ 
            user: user_id,
            docs: docs,
            pages: parseInt(pages),
            count: data.response.numFound
        })

    })

    fastify.get('/admin/flag/:id', async (request, reply) => {

        const user_id = request.user.id;
        const id = request.params.id;

        const flag = await fastify.db.models.flag.findOne({
            where: {
                id: id
            },
        });

        reply.send({
            user: user_id, 
            flag: flag 
        })
    
    })

    fastify.route({
        method: 'POST',
        url: '/admin/flag/:id',
        handler: async function (request, reply) {

            const user_id = request.user.id
            const id = request.params.id
            const name = request.body.name
            const slug = request.body.slug
            const system = request.body.system
            const active = request.body.active

            // validation
            const schema = Joi.object({
                name: Joi.string()
                .min(2)
                .max(1024)
                .required()
                .messages({
                    'string.empty': '"názov" je povinné pole',
                    'string.min': '"názov" nemôže byť kratší ako {#limit}',
                    'any.required': '"názov" je povinné pole'
                }),
                slug: Joi.string()
                .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
                .required()
                .messages({
                    'string.empty': '"slug" je povinné pole',
                    'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
                }),
                system: Joi.boolean()
                .required(),
                active: Joi.boolean()
                .required(),
            });

            let validation_error = false;
            try {
                const validation = await schema.validateAsync({ name: name, slug: slug, system: system, active: active }, {abortEarly: false});
                console.log('validation', validation);
            }
            catch (err) {
                console.log('validation error', err);
                validation_error = err;
            }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                // update in DB
                await fastify.db.models.flag.update({ 
                    name: name,
                    slug: slug,
                    system: system,
                    active: active,
                }, {
                    where: {
                        id: id
                    }
                })

                // update products
                const products = await fastify.db.models.product.findAll({
                    where: {
                      flags: {
                        [Op.contains]: [
                          {'id': id}
                        ]
                      }
                    }
                });

                products.forEach(async function(item){
            
                    let flags = item.flags;
                    _.find(flags, { id: id }).ge_active = active;
                    _.find(flags, { id: id }).ge_name = name;
                    _.find(flags, { id: id }).ge_slug = slug;
            
                    console.log('flags', flags);

                    await fastify.db.models.product.update({ flags: flags }, {
                        where: {
                            old_id: item.old_id
                        }
                    })

                    const client2 = solr.createClient({
                        path: '/solr/odfarmara_products'
                    });

                    // solr flags
                    if(flags && flags.length){

                        let flag_ids = [];
                        let flag_names = [];
                        let doc = {
                            id : item.old_id,
                        };

                        for (let index = 0; index < flags.length; index++) {
                          const flag = flags[index];
              
                          flag_ids.push(flag.id);
                          flag_names.push(flag.ge_name);
                        }
              
                        doc.ge_flags = {"set": flag_ids};
                        doc.ge_flag_names = {"set": flag_names};

                        const obj = await client2.add(doc);
                        const commit = await client2.commit();
              
                    }
                    
                });

                // add doc to solr
                const flag = await fastify.db.models.flag.findOne({
                    where: {
                        id: id
                    },
                });

                let doc_system = flag.system;
                if(system){
                    doc_system = true;
                }
                else{
                    doc_system = false;
                }
                let created_date = flag.createdAt;
                let m_created_date = moment(created_date);
                created_date = m_created_date.toISOString();

                const client = solr.createClient({
                    path: '/solr/odfarmara_flags'
                });

                let doc = {
                    id : flag.id,
                    ge_id: {"set": flag.id},
                    ge_name : {"set": flag.name},
                    ge_slug : {"set": flag.slug},
                    ge_system : {"set": doc_system},
                    ge_created_date : {"set": created_date},
                    ge_active: {"set": active},
                }

                // add and commit to solr
                const obj = await client.add(doc);
                const commit = await client.commit();

                reply.send({    
                    user: user_id,
                    status: 'success' 
                })
            }

        }
    })

    fastify.route({
        method: 'POST',
        url: '/admin/flag/create',
        handler: async function (request, reply) {

            const user_id = request.user.id
            const name = request.body.name
            const slug = request.body.slug
            const system = request.body.system
            const active = request.body.active

            // validation
            const schema = Joi.object({
                name: Joi.string()
                .min(2)
                .max(1024)
                .required()
                .messages({
                    'string.empty': '"názov" je povinné pole',
                    'string.min': '"názov" nemôže byť kratší ako {#limit}',
                    'any.required': '"názov" je povinné pole'
                }),
                slug: Joi.string()
                .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
                .required()
                .messages({
                    'string.empty': '"slug" je povinné pole',
                    'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
                }),
                system: Joi.boolean()
                .required(),
                active: Joi.boolean()
                .required(),
            });

            let validation_error = false;
            try {
                const validation = await schema.validateAsync({ name: name, slug: slug, system: system, active: active }, {abortEarly: false});
                console.log('validation', validation);
            }
            catch (err) {
                console.log('validation error', err);
                validation_error = err;
            }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                // update in DB
                const new_flag = await fastify.db.models.flag.create({ 
                    name: name,
                    slug: slug,
                    system: system,
                    active: active,
                });

                let doc_system = new_flag.system;
                if(doc_system){
                    doc_system = true;
                }
                else{
                    doc_system = false;
                }
               
                let created_date = new_flag.created_at;
                let m_created_date = moment(created_date);
                created_date = m_created_date.toISOString();

                const client = solr.createClient({
                    path: '/solr/odfarmara_flags'
                });

                let doc = {
                    id : new_flag.id,
                    ge_id : parseInt(new_flag.id),
                    ge_name : new_flag.name,
                    ge_slug : new_flag.slug,
                    ge_system : doc_system,
                    ge_active : new_flag.active,
                    ge_created_date : created_date,
                }

                // add and commit to solr
                const obj = await client.add(doc);
                const commit = await client.commit();

                reply.send({    
                    user: user_id,
                    status: 'success' 
                })
            }

        }
    })

    fastify.route({
        method: 'POST',
        url: '/admin/flag/quick-create',
        handler: async function (request, reply) {

            const user_id = request.user.id;
            const new_flag = request.body.name;

            // validation
            const schema = Joi.object({
                new_flag: Joi.string()
                .min(2)
                .max(1024)
                .required()
                .messages({
                    'string.empty': '"názov" je povinné pole',
                    'string.min': '"názov" nemôže byť kratší ako {#limit}',
                    'any.required': '"názov" je povinné pole'
                }),
            });

            let validation_error = false;
            try {
                const validation = await schema.validateAsync({ new_flag: new_flag }, {abortEarly: false});
                console.log('validation', validation);
            }
            catch (err) {
                console.log('validation error', err);
                validation_error = err;
            }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                const slug = slugify(new_flag, {
                    lower: true,
                    locale: 'sk'
                })

                console.log('slug', slug);

                let exist = await fastify.db.models.flag.findAll({
                    where: { slug: slug }
                });

                console.log('exist', exist);

                if(exist.length){
                    reply.send({    
                        validation_error: {'new_flag': 'uvedený príznak už existuje'},
                    })
                }
                else{

                    // update in DB
                    const flag_created = await fastify.db.models.flag.create({ 
                        name: new_flag,
                        slug: slug,
                        system: false,
                        active: true,
                        landing_page: false,
                        user_id: user_id,
                    });

                    let doc_system = flag_created.system;
                    if(doc_system){
                        doc_system = true;
                    }
                    else{
                        doc_system = false;
                    }
                
                    let created_date = flag_created.created_at;
                    let m_created_date = moment(created_date);
                    created_date = m_created_date.toISOString();

                    const client = solr.createClient({
                        path: '/solr/odfarmara_flags'
                    });

                    let doc = {
                        id : flag_created.id,
                        ge_id : flag_created.id,
                        ge_name : flag_created.name,
                        ge_slug : flag_created.slug,
                        ge_system : doc_system,
                        ge_active : flag_created.active,
                        ge_created_date : created_date,
                        ge_user_name: user_name
                    }

                    // add and commit to solr
                    const obj = await client.add(doc);
                    const commit = await client.commit();

                    reply.send({    
                        user: user_id,
                        status: 'success' 
                    })
                }
            }

        }
    })

    fastify.route({
        method: 'DELETE',
        url: '/admin/flag/:id',
        handler: async function (request, reply) {

            const user_id = request.user.id;
            const id = request.params.id;

            
            
            // validation
            // const schema = Joi.object({
            //     id: Joi.string()
            //     .min(2)
            //     .max(1024)
            //     .required()
            //     .messages({
            //         'string.empty': '"názov" je povinné pole',
            //         'string.min': '"názov" nemôže byť kratší ako {#limit}',
            //         'any.required': '"názov" je povinné pole'
            //     }),
            //     slug: Joi.string()
            //     .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
            //     .required()
            //     .messages({
            //         'string.empty': '"slug" je povinné pole',
            //         'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
            //     }),
            //     system: Joi.boolean()
            //     .required(),
            //     active: Joi.boolean()
            //     .required(),
            // });

            

            let validation_error = false;
            // try {
            //     const validation = await schema.validateAsync({ name: name, slug: slug, system: system, active: active }, {abortEarly: false});
            //     console.log('validation', validation);
            // }
            // catch (err) {
            //     console.log('validation error', err);
            //     validation_error = err;
            // }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                // delete in DB
                const deleted = await fastify.db.models.flag.destroy({
                    where: {
                      id: id
                    }
                });

                const client = solr.createClient({
                    path: '/solr/odfarmara_flags'
                });


                // delete from solr
                const obj = await client.deleteByID(id.toString());
                const commit = await client.commit();

                
                // todo delete all flags from products

                const products = await fastify.db.models.product.findAll({
                    where: {
                      flags: {
                        [Op.contains]: [
                          {'id': id}
                        ]
                      }
                    }
                });

                const client2 = solr.createClient({
                    path: '/solr/odfarmara_products'
                });
            
                products.forEach(async function(item){
            
                    let flags = item.flags;
                    _.remove(flags, item => item.id === id);
            
                    console.log('flags', flags);

                    await fastify.db.models.product.update({ flags: flags }, {
                        where: {
                            old_id: item.old_id
                        }
                    })

                    // solr flags
                    if(flags && flags.length){

                        let flag_ids = [];
                        let flag_names = [];
                        let doc = {
                            id : item.old_id,
                        };

                        for (let index = 0; index < flags.length; index++) {
                          const flag = flags[index];
              
                          flag_ids.push(flag.id);
                          flag_names.push(flag.ge_name);
                        }
              
                        doc.ge_flags = {"set": flag_ids};
                        doc.ge_flag_names = {"set": flag_names};

                        const obj = await client2.add(doc);
                        const commit = await client2.commit();
              
                    }
                    else{
                        let doc = {
                            id : item.old_id,
                        };
                        doc.ge_flags = {"set": null};
                        doc.ge_flag_names = {"set": null};

                        const obj = await client2.add(doc);
                        const commit = await client2.commit();
                    }
                    
                });

                reply.send({    
                    user: user_id,
                    status: 'success' 
                })

            }

        }
    })

  }
  
  module.exports = routes
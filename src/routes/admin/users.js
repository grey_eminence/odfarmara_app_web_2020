const solr = require('solr-client')
const fs = require('fs')
const util = require('util')
const path = require('path')
const { nanoid } = require('nanoid')
const { pipeline } = require('stream')
const pump = util.promisify(pipeline)
const process = require('process')
const moment = require('moment')
const Joi = require('joi')
const { v4: uuidv4 } = require('uuid')
const { Op } = require("sequelize")
const argon2 = require('argon2')
const passwordComplexity = require("joi-password-complexity");

async function routes (fastify, options) {

    fastify.get('/admin/user/:id', async (request, reply) => {

        const id = parseInt(request.params.id);
        const user = request.user;

        const dbUser = await fastify.db.models.user.findOne({
            where: {
                id: id
            },
        })

        reply.send({
            user_id: user.id,
            user: dbUser
        });

    });


    fastify.get('/admin/user/current', async (request, reply) => {

        const user = request.user;
        if(user && user.admin){
            reply.send(user);
        }
        else{
            reply.send({});
        }

    });

    fastify.route({
        method: 'POST',
        url: '/admin/user/create',
        handler: async function (request, reply) {

            const user = request.user
            const user_id = user.id

            const name = request.body.name
            const street = request.body.street
            const houseno = request.body.houseno
            const zip = request.body.zip
            const phone = request.body.phone
            const city = request.body.city
            const region_id = request.body.region_id
            const active = request.body.active
            const profile_image = request.body.profile_image
            const email = request.body.email

            const password = request.body.password
            const password_repeat = request.body.password_repeat

            // validation
            const schema = Joi.object({
                email: Joi.string().email().required().external(async function(value, helper){

                    // email exists?
                    const findedUser = await fastify.db.models.user.findOne({
                        where: { email: value }
                    })
            
                    if(findedUser){
                     throw new Joi.ValidationError(
                       "string.email",
                       [
                         {
                           message: "užívateľ s uvedeným emailom už existuje",
                           path: ["email"],
                           type: "string.email",
                           context: {
                             key: "email",
                             label: "email",
                             value,
                           },
                         },
                       ],
                       value
                     );
        
                   }
                   else{
                     return true;
                   }
                }).messages({
                    'string.empty': 'pole email je povinné pole',
                    'string.email': 'pole email musí obsahovať platný email',
                    'string.required': 'pole email je povinné pole',
                }),
                password: new passwordComplexity({
                    min: 8,
                    max: 30,
                    lowerCase: 1,
                    upperCase: 1,
                    numeric: 1,
                    symbol: 1,
                    requirementCount: 4
                }).messages({
                    '*': 'pole heslo musí obsahovať 8 až 30 znakov, malé aj veľké písmená, čísla aj symboly ako napr. # + - ! @',
                }),
                password_repeat: Joi.any().valid(Joi.ref('password')).required().messages({
                    'string.empty': 'pole zopakovať heslo sa musí zhodovať s heslom',
                    'any.only': 'pole zopakovať heslo sa musí zhodovať s heslom',
                    'any.required': 'pole zopakovať heslo sa musí zhodovať s heslom'
                }),
                active: Joi.boolean()
                .required(),
            })

            let validation_error = false;
            try {
                const validation = await schema.validateAsync({ 
                    email: email,
                    active: active,
                    password: password,
                    password_repeat: password_repeat,
                }, {abortEarly: false});
                console.log('validation', validation);
            }
            catch (err) {
                console.log('validation error', err);
                validation_error = err;
            }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                const password_hash = await argon2.hash(password, {type: argon2.argon2id});

                let data = {
                    name: name,
                    street: street,
                    houseno: houseno,
                    zip: zip,
                    phone: phone,
                    city: city,
                    region_id: region_id,
                    active: active,
                    profile_image: profile_image,
                    password_new: password_hash,
                    email: email,
                    privilegs: 'customer',
                    created: new Date().toISOString(),
                }

                data.active = '0';
                if(active){
                    data.active = '1';
                }

                console.log('data', data);

                // update in DB
                let newUser = await fastify.db.models.user.create(data);

                if(newUser){

                    let m_created_date = moment(newUser.created);
                    let created_date = m_created_date.toISOString();

                    const client = solr.createClient({
                        path: '/solr/odfarmara_users'
                    });

                    let doc = {
                        id : newUser.id,
                        ge_id: {"set": newUser.id},
                        ge_name : {"set": name},
                        ge_email : {"set": email},
                        ge_thumbnail : {"set": profile_image},
                        ge_active: {"set": active},
                        ge_created_date: {"set": created_date},
                    }

                    // add and commit to solr
                    const obj = await client.add(doc);
                    const commit = await client.commit();

                    reply.send({    
                        user: user_id,
                        status: 'success' 
                    })
                }
            }

        }
    })

    fastify.route({
        method: 'POST',
        url: '/admin/user/:id',
        handler: async function (request, reply) {

            const user = request.user
            const user_id = user.id
            const id = parseInt(request.params.id)

            const name = request.body.name
            const street = request.body.street
            const houseno = request.body.houseno
            const zip = request.body.zip
            const phone = request.body.phone
            const city = request.body.city
            const region_id = request.body.region_id
            const active = request.body.active
            const profile_image = request.body.profile_image

            // validation
            const schema = Joi.object({
                // name: Joi.string()
                // .min(2)
                // .max(1024)
                // .required()
                // .messages({
                //     'string.empty': '"názov" je povinné pole',
                //     'string.min': '"názov" nemôže byť kratší ako {#limit}',
                //     'any.required': '"názov" je povinné pole'
                // }),
                // slug: Joi.string()
                // .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
                // .required()
                // .messages({
                //     'string.empty': '"slug" je povinné pole',
                //     'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
                // }),
                // mainCategory: Joi.boolean(),
                // parent_id: Joi.number(),
                // type: Joi.string().required().valid('product', 'farmer', 'blog', 'recipe').messages({
                //     'string.empty': '"typ" je povinné pole',
                //     'any.required': '"typ" je povinné pole'
                // }),
                active: Joi.boolean()
                .required(),
            })
            // }).when(Joi.object({ type: Joi.valid('product'), mainCategory: Joi.valid(false) }).unknown(), {
            //     then: Joi.object({ parent_id: Joi.number().greater(0).required().messages({
            //         'number.greater': '"nadradená kategória" je povinné pole',
            //     }) })
                
            // });

            let validation_error = false;
            // try {
            //     const validation = await schema.validateAsync({ 
            //         name: name,
            //         slug: slug,
            //         active: active,
            //         type: type,
            //         parent_id: parent_id,
            //         mainCategory: mainCategory,
            //     }, {abortEarly: false});
            //     console.log('validation', validation);
            // }
            // catch (err) {
            //     console.log('validation error', err);
            //     validation_error = err;
            // }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                let data = {
                    name: name,
                    street: street,
                    houseno: houseno,
                    zip: zip,
                    phone: phone,
                    city: city,
                    region_id: region_id,
                    active: active,
                    profile_image: profile_image
                }

                data.active = '0';
                if(active){
                    data.active = '1';
                }

                console.log('data', data);

                // update in DB
                await fastify.db.models.user.update(data, {
                    where: {
                        id: id
                    }
                })

                const client = solr.createClient({
                    path: '/solr/odfarmara_users'
                });

                let doc = {
                    id : id,
                    ge_id: {"set": id},
                    ge_name : {"set": name},
                    ge_thumbnail : {"set": profile_image},
                    ge_active: {"set": active},
                }

                // add and commit to solr
                const obj = await client.add(doc);
                const commit = await client.commit();

                reply.send({    
                    user: user_id,
                    status: 'success' 
                })
            }

        }
    })

    fastify.post('/admin/users', async (request, reply) => {

        const user_id = request.user.id;
        let docs = [];
        let query_text = "*:*";
        let startItem = 0;

        const request_query = request.body;
        console.log('request_query', request_query);
        const ipp = parseInt(request_query.rowsPerPage)
        const search = request_query.search
        const page = request_query.page
        if (page) {
            startItem = (page * ipp) - ipp;
        }

        const order_by = request_query.sortBy;

        let order_type = '';
        if (order_by) {
            const desc = request_query.descending
            
            if(desc){
                order_type = 'desc'
            }
            else {
                order_type = 'asc'
            }
        }

        if(search){
            query_text = 'ge_name:"' + search + '", ' + 'ge_company_name:"' + search + '", ' + 'ge_email:"' + search + '"';
        }

        console.log('query_text', query_text);

        let sortParams = {}; 
        sortParams[order_by] = order_type;

        const client = solr.createClient({
            path: '/solr/odfarmara_users'
        });

        const query = client
        .query()
        .q(query_text)
        .start(startItem)
        .rows(ipp);

        if(order_by){
            query.sort(sortParams);
        }

        console.log('query', query);

        const data = await client.search(query);
        docs = data.response.docs;

        // set pages count
        let pages = Math.floor(data.response.numFound / ipp);
        let pages_modulo = data.response.numFound % ipp;
        if(pages_modulo){
        pages++;
        }

        // add missing data to result
        // for(let i = 0; i < docs.length; i++){
        //     if(!docs[i].hasOwnProperty('ge_name')){
        //         docs[i].ge_name = null;
        //     }
        //     if(!docs[i].hasOwnProperty('ge_thumbnail')){
        //         docs[i].ge_thumbnail = null;
        //     }
        //     if(!docs[i].hasOwnProperty('ge_company_name')){
        //         docs[i].ge_company_name = null;
        //     }
        // }

        console.log('docs', docs);

        console.log('user_id', user_id);
        reply.send({ 
            user: user_id,
            docs: docs,
            pages: parseInt(pages),
            count: data.response.numFound
        })

    })

    fastify.post('/admin/users/select/farmers', async (request, reply) => {

        const user_id = request.user.id;

        let docs = [];
        let query_text = "*:*";
        let startItem = 0;

        const request_query = request.body;
        //const ipp = parseInt(request_query.rowsPerPage)
        let ipp = 100;
        let to = false;
        if(request_query.to){
            to = parseInt(request_query.to);
            ipp = 100 * Math.ceil(to / 100);
        }
        
        const search = request_query.search
        const page = request_query.page
        if (page) {
            startItem = (page * ipp) - ipp;
        }

        const order_by = request_query.sortBy;

        let order_type = '';
        if (order_by) {
            const desc = request_query.descending
            
            if(desc){
                order_type = 'desc'
            }
            else {
                order_type = 'asc'
            }
        }

        if(search){
            query_text = 'ge_name:' + search + ', ' + 'ge_company_name:' + search;
        }

        let sortParams = {}; 
        sortParams[order_by] = order_type;

        const client = solr.createClient({
            path: '/solr/odfarmara_farmers'
        });

        if(to > 90){
            ipp = ipp + 100;
        }

        const query = client
        .query()
        .q(query_text)
        .start(startItem)
        .rows(ipp);

        if(order_by){
            query.sort(sortParams);
        }

        const data = await client.search(query);
        docs = data.response.docs;

        // set pages count
        let pages = Math.floor(data.response.numFound / ipp);
        let pages_modulo = data.response.numFound % ipp;
        if(pages_modulo){
        pages++;
        }

        reply.send({ 
            user: user_id,
            docs: docs,
            pages: parseInt(pages),
            count: data.response.numFound,
            max_to: ipp
        })

    })


    fastify.route({
        method: 'POST',
        url: '/admin/user/password/:id',
        handler: async function (request, reply) {

            const user = request.user
            const user_id = user.id
            const id = parseInt(request.params.id)

            const password = request.body.password
            const password_repeat = request.body.password_repeat

            // validation schema
            const schema = Joi.object({
                password: new passwordComplexity({
                    min: 8,
                    max: 30,
                    lowerCase: 1,
                    upperCase: 1,
                    numeric: 1,
                    symbol: 1,
                    requirementCount: 4
                }).messages({
                    '*': 'pole heslo musí obsahovať 8 až 30 znakov, malé aj veľké písmená, čísla aj symboly ako napr. # + - ! @',
                }),
                password_repeat: Joi.any().valid(Joi.ref('password')).required().messages({
                    'string.empty': 'pole zopakovať heslo sa musí zhodovať s heslom',
                    'any.only': 'pole zopakovať heslo sa musí zhodovať s heslom',
                    'any.required': 'pole zopakovať heslo sa musí zhodovať s heslom'
                }),
            });

            let validation_error = false;
            try {
                const validation = await schema.validateAsync({ 
                    password: password,
                    password_repeat: password_repeat,
                }, {abortEarly: false});
                console.log('validation', validation);
            }
            catch (err) {
                console.log('validation error', err);
                validation_error = err;
            }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                const password_hash = await argon2.hash(password, {type: argon2.argon2id});
                const password_success = await fastify.db.models.user.update({
                    password_new: password_hash
                }, {
                    where: {
                        id: id
                    }
                })

                reply.send({    
                    user: user_id,
                    status: 'success' 
                })
            }

        }
    })


    fastify.route({
        method: 'DELETE',
        url: '/admin/user/:id',
        handler: async function (request, reply) {

            const user_id = request.user.id;
            const id = request.params.id;

            
            // validation
            // const schema = Joi.object({
            //     id: Joi.string()
            //     .min(2)
            //     .max(1024)
            //     .required()
            //     .messages({
            //         'string.empty': '"názov" je povinné pole',
            //         'string.min': '"názov" nemôže byť kratší ako {#limit}',
            //         'any.required': '"názov" je povinné pole'
            //     }),
            //     slug: Joi.string()
            //     .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
            //     .required()
            //     .messages({
            //         'string.empty': '"slug" je povinné pole',
            //         'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
            //     }),
            //     system: Joi.boolean()
            //     .required(),
            //     active: Joi.boolean()
            //     .required(),
            // });

            

            let validation_error = false;
            // try {
            //     const validation = await schema.validateAsync({ name: name, slug: slug, system: system, active: active }, {abortEarly: false});
            //     console.log('validation', validation);
            // }
            // catch (err) {
            //     console.log('validation error', err);
            //     validation_error = err;
            // }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                // delete in DB
                const deleted = await fastify.db.models.user.destroy({
                    where: {
                      id: id
                    }
                });

                const client = solr.createClient({
                    path: '/solr/odfarmara_users'
                });


                // delete from solr
                const obj = await client.deleteByID(id.toString());
                const commit = await client.commit();

                reply.send({    
                    user: user_id,
                    status: 'success' 
                })


            }

        }
    })


  }
  
  module.exports = routes
const solr = require('solr-client')
const fs = require('fs')
const util = require('util')
const path = require('path')
const { nanoid } = require('nanoid')
const { pipeline } = require('stream')
const pump = util.promisify(pipeline)
const process = require('process')
const moment = require('moment')
const Joi = require('joi')
const { v4: uuidv4 } = require('uuid')
const { Op } = require("sequelize")

async function routes (fastify, options) {
    fastify.post('/admin/blogs', async (request, reply) => {

        // console.log('request', request);
  
        const user_id = request.user.id;
        let docs = [];
        let query_text = "*:*";
        let startItem = 0;

        const request_query = request.body;
        const ipp = parseInt(request_query.rowsPerPage)
        const search = request_query.search
        const page = request_query.page
        if (page) {
            startItem = (page * ipp) - ipp;
        }

        console.log('search', search);

        const order_by = request_query.sortBy;

        let order_type = '';
        if (order_by) {
            const desc = request_query.descending
            
            if(desc){
                order_type = 'desc'
            }
            else {
                order_type = 'asc'
            }
        }

        if(search){
            query_text = 'ge_title:' + search + ', ' + 'ge_description:' + search;
        }

        let sortParams = {}; 
        sortParams[order_by] = order_type;

        const client = solr.createClient({
            path: '/solr/odfarmara_blog'
        });

        const query = client
        .query()
        .q(query_text)
        .start(startItem)
        .rows(ipp);

        if(order_by){
            query.sort(sortParams);
        }

        const data = await client.search(query);
        docs = data.response.docs;

        // set pages count
        let pages = Math.floor(data.response.numFound / ipp);
        let pages_modulo = data.response.numFound % ipp;
        if(pages_modulo){
        pages++;
        }

        console.log('user_id', user_id);
        reply.send({ 
            user: user_id,
            docs: docs,
            pages: parseInt(pages),
            count: data.response.numFound
        })

    })

    fastify.get('/admin/blog/:id', async (request, reply) => {

        const user_id = request.user.id;
        const id = request.params.id

        const blog = await fastify.db.models.article.findOne({
            include: [ 
                { model: fastify.db.models.category, as: "category" },
            ],
            where: {
                article_id: id
            },
        })

        reply.send({    
            user: user_id, 
            blog: blog
        })
    
    })

    fastify.route({
        method: 'POST',
        url: '/admin/blog/create',
        handler: async function (request, reply) {

            const user = request.user;
            const user_id = user.id;
            const title = request.body.title;
            const slug = request.body.slug;
            const perex = request.body.perex;
            const contents = request.body.contents;
            const category_id = request.body.category_id;
            const tags = request.body.tags;
            const active = request.body.active;
            const blog_img = request.body.blog_img;

            // validation
            const schema = Joi.object({
                title: Joi.string()
                .min(2)
                .max(1024)
                .required()
                .messages({
                    'string.empty': '"názov" je povinné pole',
                    'string.min': '"názov" nemôže byť kratší ako {#limit}',
                    'any.required': '"názov" je povinné pole'
                }),
                slug: Joi.string()
                .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
                .required()
                .messages({
                    'string.base': '"slug" je povinné pole',
                    'string.empty': '"slug" je povinné pole',
                    'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
                }),
                category_id: Joi.number()
                .required()
                .messages({
                    'number.base': '"kategória" je povinné pole',
                    'number.empty': '"kategória" je povinné pole',
                    'any.required': '"kategória" je povinné pole'
                }),
                active: Joi.boolean()
                .required(),
            });

            let validation_error = false;
            try {
                const validation = await schema.validateAsync({ 
                    title: title, 
                    slug: slug, 
                    category_id: category_id,
                    active: active,
                }, {abortEarly: false});
                console.log('validation', validation);
            }
            catch (err) {
                console.log('validation error', err);
                validation_error = err;
            }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                let active_value = '0';
                if(active){
                    active_value = '1';
                }

                let data = {
                    title: title,
                    slug: slug,
                    perex: perex,
                    contents: contents,
                    cat_id: category_id,
                    tags: tags,
                    blog_img: blog_img,
                    active: active_value,
                    created: new Date().toISOString(),
                    date: new Date().toISOString(),
                }
    
                let new_article = await fastify.db.models.article.create(data);

                new_article = await fastify.db.models.article.findOne({ 
                    include: [ 
                        { model: fastify.db.models.category, as: "category" },
                    ],
                    where: { article_id: new_article.article_id }
                });

                 // save to solr
                 const client = solr.createClient({
                    path: '/solr/odfarmara_blog'
                });
                client.autoCommit = true;

                let m_created_date = moment(new_article.dataValues.created);
                let created_date = m_created_date.toISOString();

                let doc = {
                    id : new_article.article_id,
                    ge_id : new_article.article_id,
                    ge_title : title,
                    ge_description : contents,
                    ge_category : category_id,
                    ge_category_name: new_article.category.name,
                    ge_name: title,
                    ge_thumbnail : blog_img,
                    ge_active : active,
                    ge_created_date : created_date,
                }

                console.log('doc', doc);

                const obj = await client.add(doc);
                const commit = await client.commit();

            }
    
            reply.send({    
                user: user_id,
                status: 'success' 
            })

        }
    })

    fastify.route({
        method: 'POST',
        url: '/admin/blog/:id',
        handler: async function (request, reply) {

            const user = request.user;
            const user_id = user.id;
            const id = request.params.id;
            const title = request.body.title;
            const slug = request.body.slug;
            const perex = request.body.perex;
            const contents = request.body.contents;
            const category_id = request.body.category_id;
            const tags = request.body.tags;
            const active = request.body.active;
            const blog_img = request.body.blog_img;

            // validation
            const schema = Joi.object({
                title: Joi.string()
                .min(2)
                .max(1024)
                .required()
                .messages({
                    'string.empty': '"názov" je povinné pole',
                    'string.min': '"názov" nemôže byť kratší ako {#limit}',
                    'any.required': '"názov" je povinné pole'
                }),
                slug: Joi.string()
                .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
                .required()
                .messages({
                    'string.base': '"slug" je povinné pole',
                    'string.empty': '"slug" je povinné pole',
                    'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
                }),
                category_id: Joi.number()
                .required()
                .messages({
                    'number.base': '"kategória" je povinné pole',
                    'number.empty': '"kategória" je povinné pole',
                    'any.required': '"kategória" je povinné pole'
                }),
                active: Joi.boolean()
                .required(),
            });

            let validation_error = false;
            try {
                const validation = await schema.validateAsync({ 
                    title: title, 
                    slug: slug, 
                    category_id: category_id,
                    active: active,
                }, {abortEarly: false});
                console.log('validation', validation);
            }
            catch (err) {
                console.log('validation error', err);
                validation_error = err;
            }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                let active_value = '0';
                if(active){
                    active_value = '1';
                }

                let data = {
                    title: title,
                    slug: slug,
                    perex: perex,
                    contents: contents,
                    cat_id: category_id,
                    tags: tags,
                    blog_img: blog_img,
                    active: active_value
                }
    
                await fastify.db.models.article.update(data, {
                    where: {
                        article_id: id
                    }
                })

                const updated_article = await fastify.db.models.article.findOne({ 
                    include: [ 
                        { model: fastify.db.models.category, as: "category" },
                    ],
                    where: { article_id: id }
                });

                 // save to solr
                 const client = solr.createClient({
                    path: '/solr/odfarmara_blog'
                });
                client.autoCommit = true;

                let m_created_date = moment(updated_article.dataValues.created);
                let created_date = m_created_date.toISOString();

                let doc = {
                    id : id,
                    ge_id: {"set": id },
                    ge_title : {"set": title },
                    ge_description : {"set": contents },
                    ge_category : {"set": category_id },
                    ge_category_name: {"set": updated_article.category.name },
                    ge_name: {"set": title },
                    ge_thumbnail : {"set": blog_img },
                    ge_active : {"set": active },
                    ge_created_date : {"set": created_date },
                }

                console.log('doc', doc);

                const obj = await client.add(doc);
                const commit = await client.commit();

            }
    
            reply.send({    
                user: user_id,
                status: 'success' 
            })

        }
    })

    fastify.route({
        method: 'DELETE',
        url: '/admin/blog/:id',
        handler: async function (request, reply) {

            const user_id = request.user.id;
            const id = request.params.id;
            
            // validation
            // const schema = Joi.object({
            //     id: Joi.string()
            //     .min(2)
            //     .max(1024)
            //     .required()
            //     .messages({
            //         'string.empty': '"názov" je povinné pole',
            //         'string.min': '"názov" nemôže byť kratší ako {#limit}',
            //         'any.required': '"názov" je povinné pole'
            //     }),
            //     slug: Joi.string()
            //     .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
            //     .required()
            //     .messages({
            //         'string.empty': '"slug" je povinné pole',
            //         'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
            //     }),
            //     system: Joi.boolean()
            //     .required(),
            //     active: Joi.boolean()
            //     .required(),
            // });

            

            let validation_error = false;
            // try {
            //     const validation = await schema.validateAsync({ name: name, slug: slug, system: system, active: active }, {abortEarly: false});
            //     console.log('validation', validation);
            // }
            // catch (err) {
            //     console.log('validation error', err);
            //     validation_error = err;
            // }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                // delete in DB
                const deleted = await fastify.db.models.article.destroy({
                    where: {
                      article_id: id
                    }
                });

                const client = solr.createClient({
                    path: '/solr/odfarmara_blog'
                });

                // delete from solr
                const obj = await client.deleteByID(id.toString());
                const commit = await client.commit();

                reply.send({    
                    user: user_id,
                    status: 'success' 
                })

                // todo delete all tags from products, farmers, recipes, blog articles
                

            }

        }
    })
    
  }
  
  module.exports = routes
const solr = require('solr-client')
const fs = require('fs')
const util = require('util')
const path = require('path')
const { nanoid } = require('nanoid')
const { pipeline } = require('stream')
const pump = util.promisify(pipeline)
const process = require('process')
const moment = require('moment')
const Joi = require('joi')
const { v4: uuidv4 } = require('uuid')
const { Op } = require("sequelize")

async function routes (fastify, options) {

    fastify.post('/admin/nutritions/select', async (request, reply) => {

        const user_id = request.user.id;
        let docs = [];
        let query_text = "*:*";
        let startItem = 0;

        const request_query = request.body;
        let ipp = 100;
        let to = false;
        if(request_query.to){
            to = parseInt(request_query.to);
            ipp = 100 * Math.ceil(to / 100);
        }
        const search = request_query.search
        const page = request_query.page
        if (page) {
            startItem = (page * ipp) - ipp;
        }

        console.log('search', search);

        const order_by = request_query.sortBy;

        let order_type = '';
        if (order_by) {
            const desc = request_query.descending
            
            if(desc){
                order_type = 'desc'
            }
            else {
                order_type = 'asc'
            }
        }

        if(search){
            query_text = 'ge_name:' + search + ', ' + 'ge_slug:' + search;
        }

        let sortParams = {}; 
        sortParams[order_by] = order_type;

        const client = solr.createClient({
            path: '/solr/odfarmara_nutritions'
        });

        if(to > 90){
            ipp = ipp + 100;
        }

        const query = client
        .query()
        .q(query_text)
        .start(startItem)
        .rows(ipp);

        if(order_by){
            query.sort(sortParams);
        }

        const data = await client.search(query);
        docs = data.response.docs;

        // set pages count
        let pages = Math.floor(data.response.numFound / ipp);
        let pages_modulo = data.response.numFound % ipp;
        if(pages_modulo){
        pages++;
        }

        console.log('user_id', user_id);
        reply.send({ 
            user: user_id,
            docs: docs,
            pages: parseInt(pages),
            count: data.response.numFound,
            max_to: ipp
        })

    })

    fastify.post('/admin/nutritions', async (request, reply) => {

        const user_id = request.user.id;
        let docs = [];
        let query_text = "*:*";
        let startItem = 0;

        const request_query = request.body;
        const ipp = parseInt(request_query.rowsPerPage)
        const search = request_query.search
        const page = request_query.page
        if (page) {
            startItem = (page * ipp) - ipp;
        }

        console.log('search', search);

        const order_by = request_query.sortBy;

        let order_type = '';
        if (order_by) {
            const desc = request_query.descending
            
            if(desc){
                order_type = 'desc'
            }
            else {
                order_type = 'asc'
            }
        }

        if(search){
            query_text = 'ge_name:' + search;
        }

        let sortParams = {}; 
        sortParams[order_by] = order_type;

        const client = solr.createClient({
            path: '/solr/odfarmara_nutritions'
        });

        const query = client
        .query()
        .q(query_text)
        .start(startItem)
        .rows(ipp);

        if(order_by){
            query.sort(sortParams);
        }

        const data = await client.search(query);
        docs = data.response.docs;

        // set pages count
        let pages = Math.floor(data.response.numFound / ipp);
        let pages_modulo = data.response.numFound % ipp;
        if(pages_modulo){
        pages++;
        }

        console.log('user_id', user_id);
        reply.send({ 
            user: user_id,
            docs: docs,
            pages: parseInt(pages),
            count: data.response.numFound
        })

    })

    fastify.get('/admin/nutrition/category/:id', async (request, reply) => {

        const user_id = request.user.id;
        const id = request.params.id

        let nutrition = await fastify.db.models.nutrition_value.findOne({
            where: {
                category_id: id
            },
        })

        console.log('nutrition', nutrition);

        reply.send({
            user: user_id,
            nutrition: nutrition 
        })
    
    })

    fastify.get('/admin/nutrition/:id', async (request, reply) => {

        const user_id = request.user.id;
        const id = request.params.id

        let nutrition = await fastify.db.models.nutrition_value.findOne({
            include: [ 
                { 
                    model: fastify.db.models.category, 
                    as: "category",
                    include: [ { 
                        model: fastify.db.models.category, 
                        as: "parentCategory",
                    } ]
                },
              ],
            where: {
                id: id
            },
        })

        console.log('nutrition', nutrition);

        reply.send({
            user: user_id,
            nutrition: nutrition 
        })
    
    })

    fastify.route({
        method: 'POST',
        url: '/admin/nutrition/:id',
        handler: async function (request, reply) {

            const user = request.user
            const user_id = user.id
            const id = request.params.id

            const category = request.body.category
            const nutritionalInformation = request.body.nutritionalInformation
            const minerals = request.body.minerals
            const vitamins = request.body.vitamins

            // validation
            const schema = Joi.object({
                category: Joi.number().integer().greater(0).required().external(async function(value, helper){

                    // exists?
                   const findedCategory = await fastify.db.models.nutrition_value.findOne({
                     where: { category_id: value, id: {[Op.ne]: id} }
                   })
       
                   if(findedCategory){
                     throw new Joi.ValidationError(
                       "number.base",
                       [
                         {
                           message: "nutričná hodnota pre uvedenú kategóriu už existuje",
                           path: ["category"],
                           type: "number.base",
                           context: {
                             key: "number",
                             label: "number",
                             value,
                           },
                         },
                       ],
                       value
                     );
       
                   }
                   else{
                     return true;
                   }
       
                 })
                .messages({
                    'number.base': '"kategória" je povinné pole',
                    'number.empty': '"kategória" je povinné pole',
                    'any.required': '"kategória" je povinné pole'
                }),
                nutritionalInformation: Joi.array().items(
                    Joi.object({
                        title: Joi.string().required(),
                        value: Joi.string().required(),
                        unit: Joi.string().required(),
                    })
                    .messages({
                        'string.empty': '"výživové údaje" nesmú obsahovať nevyplnené pole',
                        'any.required': '"výživové údaje" je povinné pole'
                    }),
                ),
                minerals: Joi.array().items(
                    Joi.object({
                        title: Joi.string().required(),
                        value: Joi.string().required(),
                        unit: Joi.string().required(),
                    })
                    .messages({
                        'string.empty': '"minerály" nesmú obsahovať nevyplnené pole',
                        'any.required': '"minerály" je povinné pole'
                    }),
                ),
                vitamins: Joi.array().items(
                    Joi.object({
                        title: Joi.string().required(),
                        value: Joi.string().required(),
                        unit: Joi.string().required(),
                    })
                    .messages({
                        'string.empty': '"vitamíny" nesmú obsahovať nevyplnené pole',
                        'any.required': '"vitamíny" je povinné pole'
                    }),
                )
            });

            let validation_error = false;
            try {
                const validation = await schema.validateAsync({ 
                    category: category, 
                    nutritionalInformation: nutritionalInformation,
                    minerals: minerals,
                    vitamins: vitamins,
                }, {abortEarly: false});
                console.log('validation', validation);
            }
            catch (err) {
                console.log('validation error', err);
                validation_error = err;
            }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                const findedCategory = await fastify.db.models.category.findOne({
                    where:{ id: category}
                });

                let name = findedCategory.name;
                let values = {
                    'nutritionalInformation': nutritionalInformation,
                    'minerals': minerals,
                    'vitamins': vitamins,
                };

                // update in DB
                await fastify.db.models.nutrition_value.update({ 
                    name: name,
                    category_id: category,
                    values: values,
                }, {
                    where: {
                        id: id
                    }
                })

                // add doc to solr
                const nutrition = await fastify.db.models.nutrition_value.findOne({
                    where: {
                        id: id
                    },
                });

                const userCreated = await fastify.db.models.user.findOne({
                    where: {
                        id: nutrition.created_by
                    },
                });

                let created_date = nutrition.created_at;
                let m_created_date = moment(created_date);
                created_date = m_created_date.toISOString();

                let user_name = "";
                if(userCreated){
                    if(userCreated.name == "" || userCreated.name == null){
                        user_name = userCreated.company_name;
                    }
                    else{
                        user_name = userCreated.name;
                    }
                }

                const client = solr.createClient({
                    path: '/solr/odfarmara_nutritions'
                });

                let doc = {
                    id : nutrition.id,
                    ge_id : {"set": nutrition.id},
                    ge_name : {"set": nutrition.name},
                    ge_slug : {"set": nutrition.slug},
                    ge_description : {"set": nutrition.description},
                    ge_created_date : {"set": created_date},
                    ge_user_name: {"set": user_name},
                    ge_active: {"set": nutrition.active},
                }

                // add and commit to solr
                const obj = await client.add(doc);
                const commit = await client.commit();

                reply.send({    
                    user: user_id,
                    status: 'success' 
                })
            }

        }
    })

    fastify.route({
        method: 'POST',
        url: '/admin/nutrition/create',
        handler: async function (request, reply) {

            const user = request.user
            const user_id = user.id

            const category = request.body.category
            const nutritionalInformation = request.body.nutritionalInformation
            const minerals = request.body.minerals
            const vitamins = request.body.vitamins

            // validation
            const schema = Joi.object({
                category: Joi.number().integer().greater(0).required().external(async function(value, helper){

                    // exists?
                   const findedCategory = await fastify.db.models.nutrition_value.findOne({
                     where: { category_id: value }
                   })
       
                   if(findedCategory){
                     throw new Joi.ValidationError(
                       "number.base",
                       [
                         {
                           message: "nutričná hodnota pre uvedenú kategóriu už existuje",
                           path: ["category"],
                           type: "number.base",
                           context: {
                             key: "number",
                             label: "number",
                             value,
                           },
                         },
                       ],
                       value
                     );
       
                   }
                   else{
                     return true;
                   }
       
                 })
                .messages({
                    'number.base': '"kategória" je povinné pole',
                    'number.empty': '"kategória" je povinné pole',
                    'any.required': '"kategória" je povinné pole'
                }),
                nutritionalInformation: Joi.array().items(
                    Joi.object({
                        title: Joi.string().required(),
                        value: Joi.string().required(),
                        unit: Joi.string().required(),
                    })
                    .messages({
                        'string.empty': '"výživové údaje" nesmú obsahovať nevyplnené pole',
                        'any.required': '"výživové údaje" je povinné pole'
                    }),
                ),
                minerals: Joi.array().items(
                    Joi.object({
                        title: Joi.string().required(),
                        value: Joi.string().required(),
                        unit: Joi.string().required(),
                    })
                    .messages({
                        'string.empty': '"minerály" nesmú obsahovať nevyplnené pole',
                        'any.required': '"minerály" je povinné pole'
                    }),
                ),
                vitamins: Joi.array().items(
                    Joi.object({
                        title: Joi.string().required(),
                        value: Joi.string().required(),
                        unit: Joi.string().required(),
                    })
                    .messages({
                        'string.empty': '"vitamíny" nesmú obsahovať nevyplnené pole',
                        'any.required': '"vitamíny" je povinné pole'
                    }),
                )
            });

            let validation_error = false;
            try {
                const validation = await schema.validateAsync({ 
                    category: category, 
                    nutritionalInformation: nutritionalInformation,
                    minerals: minerals,
                    vitamins: vitamins,
                }, {abortEarly: false});
                console.log('validation', validation);
            }
            catch (err) {
                console.log('validation error', err);
                validation_error = err;
            }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                const findedCategory = await fastify.db.models.category.findOne({
                    where:{ id: category}
                });

                let name = findedCategory.name;
                let values = {
                    'nutritionalInformation': nutritionalInformation,
                    'minerals': minerals,
                    'vitamins': vitamins,
                };

                // create in DB
                const new_nutrition = await fastify.db.models.nutrition_value.create({ 
                    name: name,
                    category_id: category,
                    values: values,
                    active: '1',
                    created_by: user_id,
                    uuid: uuidv4()
                });

                let created_date = new_nutrition.created_at;
                let m_created_date = moment(created_date);
                created_date = m_created_date.toISOString();

                let user = await fastify.db.models.user.findOne({
                    where: {
                        id: user_id
                    },
                });

                let user_name = "";
                if(user){
                    if(user.name == "" || user.name == null){
                        user_name = user.company_name;
                    }
                    else{
                        user_name = user.name;
                    }
                }

                const client = solr.createClient({
                    path: '/solr/odfarmara_nutritions'
                });

                let doc = {
                    id : new_nutrition.id,
                    ge_name : new_nutrition.name,
                    ge_active : new_nutrition.active,
                    ge_created_date : created_date,
                    ge_user_name: user_name
                }

                // add and commit to solr
                const obj = await client.add(doc);
                const commit = await client.commit();

                reply.send({    
                    user: user_id,
                    status: 'success' 
                })
            }

        }
    })

    fastify.route({
        method: 'DELETE',
        url: '/admin/nutrition/:id',
        handler: async function (request, reply) {

            const user_id = request.user.id;
            const id = request.params.id;
            
            // validation
            // const schema = Joi.object({
            //     id: Joi.string()
            //     .min(2)
            //     .max(1024)
            //     .required()
            //     .messages({
            //         'string.empty': '"názov" je povinné pole',
            //         'string.min': '"názov" nemôže byť kratší ako {#limit}',
            //         'any.required': '"názov" je povinné pole'
            //     }),
            //     slug: Joi.string()
            //     .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
            //     .required()
            //     .messages({
            //         'string.empty': '"slug" je povinné pole',
            //         'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
            //     }),
            //     system: Joi.boolean()
            //     .required(),
            //     active: Joi.boolean()
            //     .required(),
            // });

            

            let validation_error = false;
            // try {
            //     const validation = await schema.validateAsync({ name: name, slug: slug, system: system, active: active }, {abortEarly: false});
            //     console.log('validation', validation);
            // }
            // catch (err) {
            //     console.log('validation error', err);
            //     validation_error = err;
            // }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                // delete in DB
                const deleted = await fastify.db.models.nutrition_value.destroy({
                    where: {
                      id: id
                    }
                });

                const client = solr.createClient({
                    path: '/solr/odfarmara_nutritions'
                });

                // delete from solr
                const obj = await client.deleteByID(id.toString());
                const commit = await client.commit();

                reply.send({    
                    user: user_id,
                    status: 'success' 
                })

                // todo delete all tags from products, farmers, recipes, blog articles
                

            }

        }
    })

  }
  
  module.exports = routes
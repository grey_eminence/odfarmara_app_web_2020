
async function routes (fastify, options) {
  fastify.get('/admin/maintenance', async (request, reply) => {

    const user_id = request.user.id;
    console.log('user_id', user_id);
    reply.send({ user: user_id })

  })
}

module.exports = routes
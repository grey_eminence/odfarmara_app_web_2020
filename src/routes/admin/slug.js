const slugify = require('slugify')

async function routes (fastify, options) {
    fastify.post('/admin/slug/generate', async (request, reply) => {

        const user_id = request.user.id;
        const text = request.body.text;
        // const type = request.params.type;

        console.log('text', text);

        const slug = slugify(text, {
            lower: true,
            locale: 'sk'
        })

        // let safe = false;
        // if(type == 'product'){

        //     while(safe){
        //         let f
        //         let items = await fastify.db.models.product.findAll({
        //         where: { 
        //             slug: slug
        //         }
        //         });
        //     }

        // }

        // let items = await fastify.db.models.category.findAll({
        //     // limit: 6,
        //     order: [
        //         ['created', 'DESC']
        //     ]
        // });

        

        reply.send({
            slug: slug
        })
        
    })
  }
  
  module.exports = routes
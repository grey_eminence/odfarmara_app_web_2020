
async function routes (fastify, options) {
  fastify.get('/administracia', async (request, reply) => {

    const user = request.user;
    if(user && user.admin){
      return reply.sendFile('/admin/index.html');
    }

    if(user && !user.admin){
      return reply.redirect('/');
    }

    console.log('user ', user);

    if(!user){
      return reply.redirect('/prihlasenie');
    }

  })
}

module.exports = routes
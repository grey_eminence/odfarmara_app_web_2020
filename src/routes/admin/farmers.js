const solr = require('solr-client')
const fs = require('fs')
const util = require('util')
const path = require('path')
const { nanoid } = require('nanoid')
const { pipeline } = require('stream')
const pump = util.promisify(pipeline)
const process = require('process')
const moment = require('moment')
const Joi = require('joi')
const { v4: uuidv4 } = require('uuid')
const { Op } = require("sequelize")
const sharp = require("sharp");
const myCustomJoi = Joi.extend(require('joi-phone-number'));
const _ = require('lodash');

async function routes (fastify, options) {
    fastify.post('/admin/farmers', async (request, reply) => {

        // console.log('request', request);

        const user_id = request.user.id;
        let docs = [];
        let query_text = "*:*";
        let startItem = 0;

        const request_query = request.body;
        const ipp = parseInt(request_query.rowsPerPage)
        const search = request_query.search
        const page = request_query.page
        if (page) {
            startItem = (page * ipp) - ipp;
        }

        console.log('search', search);

        const order_by = request_query.sortBy;

        let order_type = '';
        if (order_by) {
            const desc = request_query.descending

            if(desc){
                order_type = 'desc'
            }
            else {
                order_type = 'asc'
            }
        }

        let sortParams = {};
        sortParams[order_by] = order_type;

        if(search){
            query_text = 'ge_company_name_keyword:"' + search + '", ge_name:"' + search + '", ' + 'ge_company_name:"' + search + '", ' + 'ge_email:"' + search + '"';
            order_type = '';
        }

        const client = solr.createClient({
            path: '/solr/odfarmara_farmers',
        });

        const query = client.query()
        .q(query_text)
        // .dismax()
        // .qf({ge_company_name : 3})
        .start(startItem)
        .rows(ipp)
        .debugQuery();

        if(order_by && !search){
            query.sort(sortParams);
        }

        const data = await client.search(query);
        docs = data.response.docs;

        console.log('data', data);

        // set pages count
        let pages = Math.floor(data.response.numFound / ipp);
        let pages_modulo = data.response.numFound % ipp;
        if(pages_modulo){
        pages++;
        }

        console.log('user_id', user_id);
        reply.send({
            user: user_id,
            docs: docs,
            pages: parseInt(pages),
            count: data.response.numFound
        })

    })

    fastify.get('/admin/farmer/:id', async (request, reply) => {

        const user_id = request.user.id;
        const id = request.params.id

        let farmer = await fastify.db.models.user.findOne({
            include: [
                { model: fastify.db.models.category, as: "category" },
            ],
            where: {
                id: id
            },
        })

        let tags_db = [];

        // add tags data
        if(farmer){

            let tags = farmer.tags;
            let tagsId = [];

            if(tags){
                for (let index = 0; index < tags.length; index++) {
                    const item = tags[index];
                    tagsId.push(item.id);
                }
            }

            console.log('tagsId', tagsId);

            tags_db = await fastify.db.models.tag.findAll({
                where: { id: {[Op.in]: tagsId} },
                raw: true,
            })

        }

        let data = {
            user: user_id,
            farmer: farmer,
            tags: tags_db
        }

        reply.send(data);

    })

    fastify.route({
        method: 'POST',
        url: '/admin/farmer/create',
        handler: async function (request, reply) {

            const user = request.user;
            const user_id = user.id;
            const email = request.body.email;
            const company_name = request.body.company_name;
            const slug = request.body.slug;
            const long_description = request.body.long_description;
            const short_description = request.body.short_description;
            const categories = request.body.categories;
            const active = request.body.active;
            const tags = request.body.tags;
            const city = request.body.city;
            const houseno = request.body.houseno;
            const zip = request.body.zip;
            const street = request.body.street;
            const phone = request.body.phone;
            const additional_information = request.body.additional_information;
            const opening_hours_new = request.body.opening_hours_new;
            const profile_type = request.body.profile_type;
            const region_id = request.body.region_id;
            const profile_image = request.body.profile_image;
            const intro_image = request.body.intro_image;
            const website = request.body.website;
            const awards = request.body.awards;

            const invoice_company_name = request.body.invoice_company_name;
            const invoice_business_id = request.body.invoice_business_id;
            const invoice_vat_id = request.body.invoice_vat_id;
            const invoice_tax_id = request.body.invoice_tax_id;
            const invoice_reg_id = request.body.invoice_reg_id;


            // validation
            const schema = myCustomJoi.object({
                // company_name: myCustomJoi.string()
                // .min(2)
                // .max(1024)
                // .required()
                // .messages({
                //     'string.empty': '"názov" je povinné pole',
                //     'string.min': '"názov" nemôže byť kratší ako {#limit}',
                //     'any.required': '"názov" je povinné pole'
                // }),
                // slug: myCustomJoi.string()
                // .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
                // .required()
                // .messages({
                //     'string.empty': '"slug" je povinné pole',
                //     'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
                // }),
                // invoice_company_name: myCustomJoi.string()
                // .required()
                // .messages({
                //     'string.empty': 'povinné pole',
                //     'any.required': 'povinné pole'
                // }),
                // invoice_business_id: myCustomJoi.string()
                // .required()
                // .messages({
                //     'string.empty': 'povinné pole',
                //     'any.required': 'povinné pole'
                // }),
                // invoice_vat_id: myCustomJoi.string()
                // .required()
                // .messages({
                //     'string.empty': 'povinné pole',
                //     'any.required': 'povinné pole'
                // }),
                // invoice_tax_id: myCustomJoi.string()
                // .required()
                // .messages({
                //     'string.empty': 'povinné pole',
                //     'any.required': 'povinné pole'
                // }),
                // invoice_reg_id: myCustomJoi.string()
                // .required()
                // .messages({
                //     'string.empty': 'povinné pole',
                //     'any.required': 'povinné pole'
                // }),
                // street: myCustomJoi.string()
                // .required()
                // .messages({
                //     'string.empty': '"ulica" je povinné pole',
                //     'any.required': '"ulica" je povinné pole'
                // }),
                // houseno: myCustomJoi.string()
                // .required()
                // .messages({
                //     'string.empty': '"číslo domu" je povinné pole',
                //     'any.required': '"číslo domu" je povinné pole'
                // }),
                // city: myCustomJoi.string()
                // .required()
                // .messages({
                //     'string.empty': '"mesto" je povinné pole',
                //     'any.required': '"mesto" je povinné pole'
                // }),
                // zip: myCustomJoi.string()
                // .required()
                // .messages({
                //     'string.empty': '"PSČ" je povinné pole',
                //     'any.required': '"PSČ" je povinné pole'
                // }),
                // region_id: myCustomJoi.number()
                // .required()
                // .messages({
                //     'number.empty': '"región" je povinné pole',
                //     'any.required': '"región" je povinné pole'
                // }),
                // category_id: myCustomJoi.number()
                // .required()
                // .messages({
                //     'number.base': '"kategória" je povinné pole',
                //     'number.empty': '"kategória" je povinné pole',
                //     'any.required': '"kategória" je povinné pole'
                // }),
                // phone: myCustomJoi.string().phoneNumber({ defaultCountry: 'SK', format: 'international', strict: true }).messages({
                //     'string.empty': 'pole telefón je povinné pole',
                //     'phoneNumber.invalid': 'pole telefón musí mať platný formát telefónneho čísla',
                //     'string.required': 'pole telefón je povinné pole'
                // }),
                email: Joi.string().email().required().external(async function(value, helper){

                    // email exists?
                   const findedUser = await fastify.db.models.user.findOne({
                     where: { 
                        email: value,
                    }
                   })

                   console.log('findedUser', findedUser);
       
                   if(findedUser){
                     throw new Joi.ValidationError(
                       "string.email",
                       [
                         {
                           message: "užívateľ s uvedeným emailom už existuje",
                           path: ["email"],
                           type: "string.email",
                           context: {
                             key: "email",
                             label: "email",
                             value,
                           },
                         },
                       ],
                       value
                     );
       
                   }
                   else{
                     return true;
                   }
       
                 }).messages({
                   'string.empty': 'pole email je povinné pole',
                   'string.email': 'pole email musí obsahovať platný email',
                   'string.required': 'pole email je povinné pole',
                   'any.required': 'pole email je povinné pole',
                 }),
                profile_type: myCustomJoi.string()
                .required()
                .messages({
                    'string.empty': '"typ profilu" je povinné pole',
                    'any.required': '"typ profilu" je povinné pole'
                }),
                // website: myCustomJoi.string().uri().messages({
                //     'string.uri': '"externá url" musí byť platná url',
                // }),
                active: myCustomJoi.boolean()
                .required(),
            });

            let validation_error = false;
            try {
                const validation = await schema.validateAsync({
                    email: email,
                    // company_name: company_name,
                    // slug: slug,
                    // street: street,
                    // houseno: houseno,
                    // city: city,
                    // zip: zip,
                    // region_id: region_id,
                    // phone: phone,
                    profile_type: profile_type,
                    active: active,
                    // invoice_company_name: invoice_company_name,
                    // invoice_business_id: invoice_business_id,
                    // invoice_vat_id: invoice_vat_id,
                    // invoice_tax_id: invoice_tax_id,
                    // invoice_reg_id: invoice_reg_id,
                    // category_id: category_id,
                    // website: website,
                }, {abortEarly: false});
                console.log('validation', validation);
            }
            catch (err) {
                console.log('validation error', err);
                validation_error = err;
            }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                let invoice = {
                    invoice_company_name: invoice_company_name,
                    invoice_business_id: invoice_business_id,
                    invoice_vat_id: invoice_vat_id,
                    invoice_tax_id: invoice_tax_id,
                    invoice_reg_id: invoice_reg_id,
                };

                let data = {
                    email: email,
                    username: email,
                    company_name: company_name,
                    slug: slug,
                    long_description: long_description,
                    short_description: short_description,
                    categories: categories,
                    active: active,
                    tags: tags,
                    city: city,
                    houseno: houseno,
                    zip: zip,
                    street: street,
                    phone: phone,
                    additional_information: additional_information,
                    opening_hours_new: opening_hours_new,
                    profile_type: profile_type,
                    region_id: region_id,
                    profile_image: profile_image,
                    intro_image: intro_image,
                    website: website,
                    awards: awards,
                    created: new Date().toISOString(),
                    invoice: invoice
                };

                data.active = '0';
                if(active){
                    data.active = '1';
                }

                console.log('data', data);

                const created = await fastify.db.models.user.create(data, {})

                if(created){
                    const created_farmer = await fastify.db.models.user.findOne({
                        include: [
                            { model: fastify.db.models.category, as: "category" },
                            { model: fastify.db.models.region, as: 'region' },
                        ],
                        where: { id: created.id }
                    });

                    // save to solr
                    const client = solr.createClient({
                        path: '/solr/odfarmara_farmers'
                    });
                    client.autoCommit = true;

                    let m_created_date = moment(created_farmer.dataValues.created);
                    let created_date = m_created_date.toISOString();
                    const region = created_farmer.dataValues.region_id;

                    let account_type = '1';
                    if(profile_type == 'premium'){
                        account_type = '2';
                    }

                    let region_name = '';
                    if(region_id != 0){
                        region_name = created_farmer.dataValues.region.name;
                    }

                    let doc = {
                        id : created_farmer.id,
                        ge_id : created_farmer.id,
                        ge_description : long_description,
                        ge_email : email,
                        // ge_category : category_id,
                        ge_name: created_farmer.dataValues.name,
                        ge_company_name : company_name,
                        ge_company_name_keyword : company_name,
                        ge_region_name : region_name,
                        ge_thumbnail : profile_image,
                        ge_account_type : account_type,
                        ge_active : active,
                        ge_region : region,
                        ge_created_date : created_date,
                        ge_rating: created_farmer.rating_avg
                    }

                    console.log('doc', doc);

                    const obj = await client.add(doc);
                    const commit = await client.commit();

                    // todo treba vytvoriť aj usera, mail pass atď
                }

            }

            reply.send({
                user: user_id,
                status: 'success'
            })

        }
    })

    fastify.route({
        method: 'POST',
        url: '/admin/farmer/:id',
        handler: async function (request, reply) {

            const user = request.user;
            const user_id = user.id;
            const id = request.params.id;
            const email = request.body.email;
            const company_name = request.body.company_name;
            const slug = request.body.slug;
            const long_description = request.body.long_description;
            const short_description = request.body.short_description;
            const categories = request.body.categories;
            const active = request.body.active;
            const tags = request.body.tags;
            const city = request.body.city;
            const houseno = request.body.houseno;
            const zip = request.body.zip;
            const street = request.body.street;
            const phone = request.body.phone;
            const additional_information = request.body.additional_information;
            const opening_hours_new = request.body.opening_hours_new;
            const profile_type = request.body.profile_type;
            const region_id = request.body.region_id;
            const profile_image = request.body.profile_image;
            const intro_image = request.body.intro_image;
            const website = request.body.website;
            const awards = request.body.awards;

            const invoice_company_name = request.body.invoice_company_name;
            const invoice_business_id = request.body.invoice_business_id;
            const invoice_vat_id = request.body.invoice_vat_id;
            const invoice_tax_id = request.body.invoice_tax_id;
            const invoice_reg_id = request.body.invoice_reg_id;

            console.log("email", email);

            // validation
            const schema = myCustomJoi.object({
                // company_name: myCustomJoi.string()
                // .min(2)
                // .max(1024)
                // .required()
                // .messages({
                //     'string.empty': '"názov" je povinné pole',
                //     'string.min': '"názov" nemôže byť kratší ako {#limit}',
                //     'any.required': '"názov" je povinné pole'
                // }),
                // slug: myCustomJoi.string()
                // .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
                // .required()
                // .messages({
                //     'string.empty': '"slug" je povinné pole',
                //     'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
                // }),
                // invoice_company_name: myCustomJoi.string()
                // .required()
                // .messages({
                //     'string.empty': 'povinné pole',
                //     'any.required': 'povinné pole'
                // }),
                // invoice_business_id: myCustomJoi.string()
                // .required()
                // .messages({
                //     'string.empty': 'povinné pole',
                //     'any.required': 'povinné pole'
                // }),
                // invoice_vat_id: myCustomJoi.string()
                // .required()
                // .messages({
                //     'string.empty': 'povinné pole',
                //     'any.required': 'povinné pole'
                // }),
                // invoice_tax_id: myCustomJoi.string()
                // .required()
                // .messages({
                //     'string.empty': 'povinné pole',
                //     'any.required': 'povinné pole'
                // }),
                // invoice_reg_id: myCustomJoi.string()
                // .required()
                // .messages({
                //     'string.empty': 'povinné pole',
                //     'any.required': 'povinné pole'
                // }),
                // street: myCustomJoi.string()
                // .required()
                // .messages({
                //     'string.empty': '"ulica" je povinné pole',
                //     'any.required': '"ulica" je povinné pole'
                // }),
                // houseno: myCustomJoi.string()
                // .required()
                // .messages({
                //     'string.empty': '"číslo domu" je povinné pole',
                //     'any.required': '"číslo domu" je povinné pole'
                // }),
                // city: myCustomJoi.string()
                // .required()
                // .messages({
                //     'string.empty': '"mesto" je povinné pole',
                //     'any.required': '"mesto" je povinné pole'
                // }),
                // zip: myCustomJoi.string()
                // .required()
                // .messages({
                //     'string.empty': '"PSČ" je povinné pole',
                //     'any.required': '"PSČ" je povinné pole'
                // }),
                // region_id: myCustomJoi.number()
                // .required()
                // .messages({
                //     'number.empty': '"región" je povinné pole',
                //     'any.required': '"región" je povinné pole'
                // }),
                // category_id: myCustomJoi.number()
                // .required()
                // .messages({
                //     'number.base': '"kategória" je povinné pole',
                //     'number.empty': '"kategória" je povinné pole',
                //     'any.required': '"kategória" je povinné pole'
                // }),
                // phone: myCustomJoi.string().phoneNumber({ defaultCountry: 'SK', format: 'international', strict: true }).messages({
                //     'string.empty': 'pole telefón je povinné pole',
                //     'phoneNumber.invalid': 'pole telefón musí mať platný formát telefónneho čísla',
                //     'string.required': 'pole telefón je povinné pole'
                // }),
                email: Joi.string().email().required().external(async function(value, helper){

                    // email exists?
                   const findedUser = await fastify.db.models.user.findOne({
                     where: { 
                        email: value, 
                        id: {
                            [Op.ne]: id
                        }
                    }
                   })

                   console.log('findedUser', findedUser);
       
                   if(findedUser){
                     throw new Joi.ValidationError(
                       "string.email",
                       [
                         {
                           message: "užívateľ s uvedeným emailom už existuje",
                           path: ["email"],
                           type: "string.email",
                           context: {
                             key: "email",
                             label: "email",
                             value,
                           },
                         },
                       ],
                       value
                     );
       
                   }
                   else{
                     return true;
                   }
       
                 }).messages({
                   'string.empty': 'pole email je povinné pole',
                   'string.email': 'pole email musí obsahovať platný email',
                   'string.required': 'pole email je povinné pole',
                   'any.required': 'pole email je povinné pole',
                 }),
                profile_type: myCustomJoi.string()
                .required()
                .messages({
                    'string.empty': '"typ profilu" je povinné pole',
                    'any.required': '"typ profilu" je povinné pole'
                }),
                // website: myCustomJoi.string().uri().messages({
                //     'string.uri': '"externá url" musí byť platná url',
                // }),
                active: myCustomJoi.boolean()
                .required(),
            });

            let validation_error = false;
            try {
                const validation = await schema.validateAsync({
                    email: email,
                    // company_name: company_name,
                    // slug: slug,
                    // street: street,
                    // houseno: houseno,
                    // city: city,
                    // zip: zip,
                    // region_id: region_id,
                    // phone: phone,
                    // contact_email: contact_email,
                    profile_type: profile_type,
                    active: active,
                    // invoice_company_name: invoice_company_name,
                    // invoice_business_id: invoice_business_id,
                    // invoice_vat_id: invoice_vat_id,
                    // invoice_tax_id: invoice_tax_id,
                    // invoice_reg_id: invoice_reg_id,
                    // category_id: category_id,
                    // website: website,
                }, {abortEarly: false});
                console.log('validation', validation);
            }
            catch (err) {
                console.log('validation error', err);
                validation_error = err;
            }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                let cleanCategories = _.without(categories, undefined, null, "");

                let invoice = {
                    invoice_company_name: invoice_company_name,
                    invoice_business_id: invoice_business_id,
                    invoice_vat_id: invoice_vat_id,
                    invoice_tax_id: invoice_tax_id,
                    invoice_reg_id: invoice_reg_id,
                };

                let data = {
                    email: email,
                    username: email,
                    company_name: company_name,
                    slug: slug,
                    long_description: long_description,
                    short_description: short_description,
                    categories: cleanCategories,
                    active: active,
                    tags: tags,
                    city: city,
                    houseno: houseno,
                    zip: zip,
                    street: street,
                    phone: phone,
                    additional_information: additional_information,
                    opening_hours_new: opening_hours_new,
                    profile_type: profile_type,
                    region_id: region_id,
                    profile_image: profile_image,
                    intro_image: intro_image,
                    website: website,
                    awards: awards,
                    invoice: invoice
                };

                data.active = '0';
                if(active){
                    data.active = '1';
                }

                console.log('data', data);

                await fastify.db.models.user.update(data, {
                    where: {
                        id: id
                    }
                })

                const updated_user = await fastify.db.models.user.findOne({
                    include: [
                        { model: fastify.db.models.category, as: "category" },
                        { model: fastify.db.models.region, as: 'region' },
                    ],
                    where: { id: id }
                });

                // save to solr
                const client = solr.createClient({
                    path: '/solr/odfarmara_farmers'
                });
                client.autoCommit = true;

                let created = updated_user.dataValues.created;
                if(!created){
                    created = updated_user.dataValues.created_at;
                }
                let m_created_date = moment(created);
                let created_date = m_created_date.toISOString();
                const region = updated_user.dataValues.region_id;

                let account_type = '1';
                if(profile_type == 'premium'){
                    account_type = '2';
                }

                let region_name = '';
                if(region_id && region_id != 0){
                    region_name = updated_user.dataValues.region.name;
                }

                let products = await fastify.db.models.product.findAll({
                    where:{
                        user_id: id
                    }
                })

                let tmp_categories = [];
                let tmp_subcategories = [];
                for (let index = 0; index < products.length; index++) {
                    let item = products[index];
                    let category_id = item.category_id;
                    let subcategory_id = item.subcategory_id;
                    tmp_categories.push(category_id);
                    tmp_subcategories.push(subcategory_id);
                }

                if(cleanCategories && cleanCategories.length > 0){
                    let categories_db = await fastify.db.models.category.findAll({
                        where: { id: {[Op.in]: cleanCategories} },
                    })

                    if(categories_db){
                        for (let index = 0; index < categories_db.length; index++) {
                            let item_db = categories_db[index];
                            let category_id_db = item_db.parent_id;
                            let subcategory_id_db = item_db.id;
                            tmp_categories.push(category_id_db);
                            tmp_subcategories.push(subcategory_id_db);
                        }
                    }
                }

                // remove duplicates
                tmp_categories = _.uniq(tmp_categories);
                tmp_subcategories = _.uniq(tmp_subcategories);

                let doc = {
                    id : id,
                    ge_id:  {"set": id},
                    ge_email : {"set": email},
                    ge_company_name : {"set": company_name},
                    ge_company_name_keyword :  {"set": company_name},
                    ge_description : {"set": long_description},
                    ge_category : {"set": tmp_categories},
                    ge_subcategory : {"set": tmp_subcategories},
                    ge_name: {"set": updated_user.dataValues.name},
                    ge_region_name : {"set": region_name},
                    ge_thumbnail : {"set": profile_image},
                    ge_account_type : {"set": account_type},
                    ge_active : {"set": active},
                    ge_region : {"set": region},
                    ge_created_date : {"set": created_date},
                    ge_rating: {"set": updated_user.rating_avg}
                }

                console.log('doc', doc);

                const obj = await client.add(doc);
                const commit = await client.commit();

            }

            reply.send({
                user: user_id,
                status: 'success'
            })

        }
    })

    fastify.route({
        method: 'DELETE',
        url: '/admin/farmer/:id',
        handler: async function (request, reply) {

            const user_id = request.user.id;
            const id = request.params.id;

            // validation
            // const schema = Joi.object({
            //     id: Joi.string()
            //     .min(2)
            //     .max(1024)
            //     .required()
            //     .messages({
            //         'string.empty': '"názov" je povinné pole',
            //         'string.min': '"názov" nemôže byť kratší ako {#limit}',
            //         'any.required': '"názov" je povinné pole'
            //     }),
            //     slug: Joi.string()
            //     .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
            //     .required()
            //     .messages({
            //         'string.empty': '"slug" je povinné pole',
            //         'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
            //     }),
            //     system: Joi.boolean()
            //     .required(),
            //     active: Joi.boolean()
            //     .required(),
            // });



            let validation_error = false;
            // try {
            //     const validation = await schema.validateAsync({ name: name, slug: slug, system: system, active: active }, {abortEarly: false});
            //     console.log('validation', validation);
            // }
            // catch (err) {
            //     console.log('validation error', err);
            //     validation_error = err;
            // }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                // delete farmer from db
                const deleted = await fastify.db.models.user.destroy({
                    where: {
                      id: id
                    }
                });

                const client = solr.createClient({
                    path: '/solr/odfarmara_farmers'
                });

                // delete farmer from solr
                const obj = await client.deleteByID(id.toString());
                const commit = await client.commit();

                const products = await fastify.db.models.product.findAll({
                    where: {
                      user_id: id
                    }
                });

                // delete products from db
                const deleted_products = await fastify.db.models.product.destroy({
                    where: {
                      user_id: id
                    }
                });

                // delete products from solr
                if(products && products.length){

                    const client = solr.createClient({
                        path: '/solr/odfarmara_products'
                    });

                    for (let index = 0; index < products.length; index++) {

                        const item = products[index];
                        let id = item.old_id;

                        // delete from solr
                        const obj = await client.deleteByID(id.toString());
                        const commit = await client.commit();

                    }

                }

                reply.send({
                    user: user_id,
                    status: 'success'
                })
            }

        }
    })

  }

  module.exports = routes
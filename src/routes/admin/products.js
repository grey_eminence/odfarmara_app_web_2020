const solr = require('solr-client')
const fs = require('fs')
const util = require('util')
const path = require('path')
const { nanoid } = require('nanoid')
const { pipeline } = require('stream')
const pump = util.promisify(pipeline)
const process = require('process')
const moment = require('moment')
const Joi = require('joi').extend(require('@joi/date'));
const { v4: uuidv4 } = require('uuid')
const { Op } = require("sequelize")
const sharp = require("sharp");
const slugify = require('slugify');

async function routes (fastify, options) {
    fastify.post('/admin/products', async (request, reply) => {

        // console.log('request', request);
  
        const user_id = request.user.id;
        let docs = [];
        let query_text = "*:*";
        let startItem = 0;

        const request_query = request.body;
        const ipp = parseInt(request_query.rowsPerPage)
        const search = request_query.search
        const page = request_query.page
        if (page) {
            startItem = (page * ipp) - ipp;
        }

        console.log('search', search);

        const order_by = request_query.sortBy;

        let order_type = '';
        if (order_by) {
            const desc = request_query.descending
            
            if(desc){
                order_type = 'desc'
            }
            else {
                order_type = 'asc'
            }
        }

        if(search){
            query_text = 'ge_title:' + search + ', ' + 'ge_description:' + search + ', ' + 'ge_farmer:' + search;
        }

        let sortParams = {}; 
        sortParams[order_by] = order_type;

        const client = solr.createClient({
            path: '/solr/odfarmara_products'
        });

        const query = client
        .query()
        .q(query_text)
        .start(startItem)
        .rows(ipp);

        if(order_by){
            query.sort(sortParams);
        }

        const data = await client.search(query);
        docs = data.response.docs;

        // set pages count
        let pages = Math.floor(data.response.numFound / ipp);
        let pages_modulo = data.response.numFound % ipp;
        if(pages_modulo){
        pages++;
        }

        console.log('user_id', user_id);
        reply.send({ 
            user: user_id,
            docs: docs,
            pages: parseInt(pages),
            count: data.response.numFound
        })

    })

    fastify.get('/admin/product/:id', async (request, reply) => {

        const user_id = request.user.id
        const id = request.params.id

        const product = await fastify.db.models.product.findOne({
            include: [ 
            { model: fastify.db.models.category, as: "category" },
            { model: fastify.db.models.category, as: "subcategory" },
            { model: fastify.db.models.image, as: "image"},
            // { model: fastify.db.models.tag },
            { 
                model: fastify.db.models.user, 
                as: "farmer"
            },
            ],
            where: {
                old_id: id
            },
        })

        reply.send({    
            user: user_id, 
            product: product
        })
    
    })

    fastify.route({
        method: 'POST',
        url: '/admin/product/:id',
        handler: async function (request, reply) {

            const user = request.user;
            const user_id = user.id;
            const id = request.params.id;
            const title = request.body.title;
            let slug = request.body.slug;
            const type = request.body.type;
            const external_url = request.body.external_url;
            const description = request.body.description;
            const short_description = request.body.short_description;
            let price = request.body.price;
            if(price){
                price = price.toString().replace(',', '.');
                price = parseFloat(price);
            }
            const price_for = request.body.price_for;
            const unit = request.body.unit;
            const farmer_id = request.body.farmer_id;
            const category_id = request.body.category_id;
            const subcategory_id = request.body.subcategory_id;
            const tags = request.body.tags;
            const flags = request.body.flags;
            const allergens = request.body.allergens;
            const nutritionValueType = request.body.nutritionValueType;
            const gallery = request.body.gallery;
            const quantity = request.body.quantity;
            const min_quantity = request.body.min_quantity;
            const active = request.body.active;

            const nutritionalInformation = request.body.nutritionalInformation;
            const minerals = request.body.minerals;
            const vitamins = request.body.vitamins;
            const start_date = request.body.start_date;
            const end_date = request.body.end_date;
            const unlimited = request.body.unlimited;

            // validation
            const schema = Joi.object({
                title: Joi.string()
                .min(2)
                .max(1024)
                .required()
                .messages({
                    'string.empty': '"názov" je povinné pole',
                    'string.min': '"názov" nemôže byť kratší ako {#limit}',
                    'any.required': '"názov" je povinné pole'
                }),
                farmer_id: Joi.number().integer().greater(0).required().messages({
                    'number.empty': 'pole "farmár" je povinné pole',
                    'number.greater': 'pole "farmár" je povinné pole',
                    'any.required': 'pole "farmár" je povinné pole'
                }),
                price: Joi.number().precision(2).strict().positive().required().messages({
                    'number.greater': 'pole "cena" je povinné pole a musí byť číslo s maximálne 2 desatinnými miestami',
                    'number.base': 'pole "cena" je povinné pole a musí byť číslo s maximálne 2 desatinnými miestami',
                    'number.precision': 'musí byť číslo s maximálne 2 desatinnými miestami',
                    'any.required': 'pole "cena" je povinné pole a musí byť číslo s maximálne 2 desatinnými miestami'
                }),
                price_for: Joi.string().required().messages({
                    'string.empty': 'pole "v jednotke" je povinné pole',
                    'any.required': 'pole "v jednotke" je povinné pole',
                }),
                unit: Joi.number().integer().greater(0).required().messages({
                    'number.empty': 'pole "za množstvo" je povinné pole',
                    'number.greater': 'pole "za množstvo" je povinné pole a musí byť celé číslo',
                    'number.base': 'pole "za množstvo" je povinné pole a musí byť celé číslo',
                    'number.integer': 'pole musí byť celé číslo',
                    'any.required': 'pole "za množstvo" je povinné pole'
                }),
                quantity: Joi.number().integer().greater(0).required().messages({
                    'number.empty': 'pole "dostupné množstvo" je povinné pole',
                    'number.greater': 'pole "dostupné množstvo" je povinné pole a musí byť celé číslo',
                    'number.base': 'pole "dostupné množstvo" je povinné pole a musí byť celé číslo',
                    'number.integer': 'pole musí byť celé číslo',
                    'any.required': 'pole "dostupné množstvo" je povinné pole'
                }),
                min_quantity: Joi.number().integer().greater(0).required().messages({
                    'number.empty': 'pole "minimálny odber" je povinné pole',
                    'number.greater': 'pole "minimálny odber" je povinné pole a musí byť celé číslo',
                    'number.base': 'pole "minimálny odber" je povinné pole a musí byť celé číslo',
                    'number.integer': 'pole musí byť celé číslo',
                    'any.required': 'pole "minimálny odber" je povinné pole'
                }),
                category_id: Joi.number().integer().greater(0).required().messages({
                    'number.empty': 'pole "kategória" je povinné pole',
                    'number.greater': 'pole "kategória" je povinné pole a musí byť celé číslo',
                    'number.base': 'pole "kategória" je povinné pole a musí byť celé číslo',
                    'number.integer': 'pole musí byť celé číslo',
                    'any.required': 'pole "kategória" je povinné pole'
                }),
                subcategory_id: Joi.number().integer().greater(0).required().messages({
                    'number.empty': 'pole "kategória" je povinné pole',
                    'number.greater': 'pole "kategória" je povinné pole a musí byť celé číslo',
                    'number.base': 'pole "kategória" je povinné pole a musí byť celé číslo',
                    'number.integer': 'pole musí byť celé číslo',
                    'any.required': 'pole "kategória" je povinné pole'
                }),
                type: Joi.string().valid('internal', 'external'),
                // description:  Joi.alternatives().conditional('type', { is: 'internal', then:Joi.string(), otherwise: Joi.any() }),
                // slug: Joi.alternatives().conditional('type', { is: 'internal', then: Joi.string().pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$')).required().messages({
                //         'string.base': '"slug" je povinné pole',
                //         'string.empty': '"slug" je povinné pole',
                //         'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
                //     }),
                //     otherwise: Joi.any()
                // }),
                external_url: Joi.alternatives().conditional('type', { is: 'external', then: Joi.string().uri().required().messages({
                        'string.base': '"externá url" je povinné pole',
                        'string.uri': '"externá url" musí byť platná url',
                        'string.empty': '"externá url" je povinné pole',
                        'string.pattern.base': '"externá url" môže obsahovať len alfanumerické znaky a znak "-"'
                    }),
                    otherwise: Joi.any()
                }),
                start_date: Joi.alternatives().conditional('unlimited', { is: false, then: Joi.date().format('DD.MM.YYYY').required().messages({
                    'date.empty': 'pole "platnosť ponuky od" je povinné pole',
                    'date.format': 'pole "platnosť ponuky od" musí byť vo formáte DD.MM.RRRR',
                    'any.required': 'pole "platnosť ponuky od" je povinné pole',
                    }),
                    otherwise: Joi.any()
                }),
                end_date: Joi.alternatives().conditional('unlimited', { is: false, then: Joi.date().format('DD.MM.YYYY').required().messages({
                    'date.empty': 'pole "platnosť ponuky od" je povinné pole',
                    'date.format': 'pole "platnosť ponuky od" musí byť vo formáte DD.MM.RRRR',
                    'any.required': 'pole "platnosť ponuky od" je povinné pole',
                    }),
                    otherwise: Joi.any()
                }),
                unlimited: Joi.boolean().required(),
                active: Joi.boolean()
                .required(),
            });

            let validation_error = false;
            try {
                const validation = await schema.validateAsync({ 
                    title: title, 
                    // slug: slug, 
                    price: price,
                    price_for: price_for,
                    active: active,
                    unit: unit,
                    quantity: quantity,
                    min_quantity: min_quantity,
                    farmer_id: farmer_id,
                    // description: description,
                    // short_description: short_description,
                    category_id: category_id,
                    subcategory_id: subcategory_id,
                    type: type,
                    external_url: external_url,
                    start_date: start_date,
                    end_date: end_date,
                    unlimited: unlimited
                }, {abortEarly: false});
                console.log('validation', validation);
            }
            catch (err) {
                console.log('validation error', err);
                validation_error = err;
            }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                let start_date_m = moment(start_date, "DD.MM.YYYY").format("YYYY-MM-DD");
                let end_date_m = moment(end_date, "DD.MM.YYYY").format("YYYY-MM-DD");

                if(unlimited){
                    start_date_m = null;
                    end_date_m = null;
                }

                if(!slug){
                    slug = slugify(title, {
                        lower: true,      // convert to lower case, defaults to `false`
                        locale: 'sk'       // language code of the locale to use
                    })
                }

                let data = {
                    title: title,
                    slug: slug,
                    price: price,
                    price_for: price_for,
                    unit: unit,
                    quantity: quantity,
                    min_quantity: min_quantity,
                    user_id: farmer_id,
                    description: description,
                    short_description: short_description,
                    category_id: category_id,
                    subcategory_id: subcategory_id,
                    start_date: start_date_m,
                    end_date: end_date_m,
                    unlimited: unlimited,
                }

                data.external = false;
                if(type == 'external'){
                    data.external = true;
                    data.external_url = external_url;
                }

                data.active = 0;
                if(active){
                    data.active = 1;
                }

                data.nutritions = { type: nutritionValueType};
                if(nutritionValueType == 'custom'){
                    data.nutritions.values = {
                        nutritionalInformation: nutritionalInformation,
                        minerals: minerals,
                        vitamins: vitamins,
                    };
                }

                data.allergens = allergens;
                data.tags = tags;
                data.flags = flags;
    
                await fastify.db.models.product.update(data, {
                    where: {
                    old_id: id
                    }
                })

                // gallery

                // delete images
                await fastify.db.models.image.destroy({
                    where: {
                        product_id: id
                    }
                })
  
                // create new
                if(gallery && gallery.length){
                
                    for(let i = 0; i < gallery.length; i++){
                        let filename = gallery[i].filename;
                        let imageData = {
                            product_id: id,
                            user_id: user_id,
                            filename: filename
                        };
                    
                        let image = await fastify.db.models.image.create(imageData);
                    }
  
                }

                let product = await fastify.db.models.product.findOne({
                    include: [ 
                        { model: fastify.db.models.image, as: "image" },
                        { model: fastify.db.models.user, as: "farmer" },
                        { model: fastify.db.models.category, as: "category" },
                    ],
                    where: {
                        old_id: id
                    }
                });

                console.log('product', product);

                // save to solr
                const client = solr.createClient({
                    path: '/solr/odfarmara_products'
                });
                client.autoCommit = true;

                console.log('start_date', start_date);
                console.log('end_date', end_date);

                let m_start_date = moment.utc(start_date, "DD.MM.YYYY");
                let m_end_date = moment.utc(end_date, "DD.MM.YYYY");
                let m_created_date = moment.utc(product.dataValues.created);

                let start_date_iso = m_start_date.toISOString();
                let end_date_iso = m_end_date.toISOString();
                let created_date = m_created_date.toISOString();

                start_date_iso = {"set": start_date_iso};
                end_date_iso = {"set": end_date_iso};

                if(unlimited){
                    start_date_iso = {"set": null};
                    end_date_iso = {"set": null};
                }

                let farmer = product.farmer.dataValues.company_name;
                if(!farmer){
                    farmer = product.farmer.name;
                }
                if(!farmer){
                    farmer = product.farmer.email;
                }
                console.log('product.dataValues', product.dataValues);
                const region = product.dataValues.region_id;

                let doc = {
                    id : id,
                    ge_title : {"set": title },
                    ge_slug : {"set": slug },
                    ge_description : {"set": description},
                    ge_farmer : {"set": farmer},
                    ge_category : {"set": category_id},
                    ge_subcategory : {"set": subcategory_id},
                    ge_price : {"set": price},
                    ge_price_for : {"set": price_for},
                    ge_quantity : {"set": quantity},
                    ge_unit : {"set": unit},
                    ge_active : {"set": active},
                    ge_region : {"set": region},
                    ge_start_date : start_date_iso,
                    ge_end_date : end_date_iso,
                    ge_created_date : {"set": created_date},
                }
                

                if(gallery && gallery.length){
                    doc.ge_thumbnail = {"set": gallery[0].filename};
                }

                if(type == 'external'){
                    doc.ge_external = {"set": true};
                    doc.ge_external_url = {"set": external_url};
                }
                else{
                    doc.ge_external = {"set": false};
                }

                // solr flags
                if(flags && flags.length){

                    let flag_ids = [];
                    let flag_names = [];

                    for (let index = 0; index < flags.length; index++) {
                      const flag = flags[index];
          
                      flag_ids.push(flag.id);
                      flag_names.push(flag.ge_name);
                    }
          
                    doc.ge_flags = {"set": flag_ids};
                    doc.ge_flag_names = {"set": flag_names};
                }
                else{
                    doc.ge_flags = {"set": null};
                    doc.ge_flag_names = {"set": null};
                }

                console.log('doc', doc);

                const obj = await client.add(doc);
                const commit = await client.commit();

            }
    
            reply.send({    
                user: user_id,
                status: 'success' 
            })

        }
    })

    fastify.route({
        method: 'POST',
        url: '/admin/product/create',
        handler: async function (request, reply) {

            const user = request.user;
            const user_id = user.id;
            const title = request.body.title;
            let slug = request.body.slug;
            const type = request.body.type;
            const external_url = request.body.external_url;
            const description = request.body.description;
            const short_description = request.body.short_description;
            let price = request.body.price;
            if(price){
                price = price.toString().replace(',', '.');
                price = parseFloat(price);
            }
            const price_for = request.body.price_for;
            const unit = request.body.unit;
            const farmer_id = request.body.farmer_id;
            const category_id = request.body.category_id;
            const subcategory_id = request.body.subcategory_id;
            const tags = request.body.tags;
            const flags = request.body.flags;
            const allergens = request.body.allergens;
            const nutritionValueType = request.body.nutritionValueType;
            const gallery = request.body.gallery;
            const quantity = request.body.quantity;
            const min_quantity = request.body.min_quantity;
            const active = request.body.active;

            const nutritionalInformation = request.body.nutritionalInformation;
            const minerals = request.body.minerals;
            const vitamins = request.body.vitamins;
            const start_date = request.body.start_date;
            const end_date = request.body.end_date;
            const unlimited = request.body.unlimited;

            // validation
            const schema = Joi.object({
                title: Joi.string()
                .min(2)
                .max(1024)
                .required()
                .messages({
                    'string.empty': '"názov" je povinné pole',
                    'string.min': '"názov" nemôže byť kratší ako {#limit}',
                    'any.required': '"názov" je povinné pole'
                }),
                farmer_id: Joi.number().integer().greater(0).required().messages({
                    'number.base': 'pole "farmár" je povinné pole',
                    'number.empty': 'pole "farmár" je povinné pole',
                    'number.greater': 'pole "farmár" je povinné pole',
                    'any.required': 'pole "farmár" je povinné pole'
                }),
                price: Joi.number().precision(2).strict().positive().required().messages({
                    'number.greater': 'pole "cena" je povinné pole a musí byť číslo s maximálne 2 desatinnými miestami',
                    'number.base': 'pole "cena" je povinné pole a musí byť číslo s maximálne 2 desatinnými miestami',
                    'number.precision': 'musí byť číslo s maximálne 2 desatinnými miestami',
                    'any.required': 'pole "cena" je povinné pole a musí byť číslo s maximálne 2 desatinnými miestami'
                }),
                price_for: Joi.string().required().messages({
                    'string.base': 'pole "v jednotke" je povinné pole',
                    'string.empty': 'pole "v jednotke" je povinné pole',
                    'any.required': 'pole "v jednotke" je povinné pole',
                }),
                unit: Joi.number().integer().greater(0).required().messages({
                    'number.empty': 'pole "za množstvo" je povinné pole',
                    'number.greater': 'pole "za množstvo" je povinné pole a musí byť celé číslo',
                    'number.base': 'pole "za množstvo" je povinné pole a musí byť celé číslo',
                    'number.integer': 'pole musí byť celé číslo',
                    'any.required': 'pole "za množstvo" je povinné pole'
                }),
                quantity: Joi.number().integer().greater(0).required().messages({
                    'number.empty': 'pole "dostupné množstvo" je povinné pole',
                    'number.greater': 'pole "dostupné množstvo" je povinné pole a musí byť celé číslo',
                    'number.base': 'pole "dostupné množstvo" je povinné pole a musí byť celé číslo',
                    'number.integer': 'pole musí byť celé číslo',
                    'any.required': 'pole "dostupné množstvo" je povinné pole'
                }),
                min_quantity: Joi.number().integer().greater(0).required().messages({
                    'number.empty': 'pole "minimálny odber" je povinné pole',
                    'number.greater': 'pole "minimálny odber" je povinné pole a musí byť celé číslo',
                    'number.base': 'pole "minimálny odber" je povinné pole a musí byť celé číslo',
                    'number.integer': 'pole musí byť celé číslo',
                    'any.required': 'pole "minimálny odber" je povinné pole'
                }),
                category_id: Joi.number().integer().greater(0).required().messages({
                    'number.empty': 'pole "kategória" je povinné pole',
                    'number.greater': 'pole "kategória" je povinné pole',
                    'number.base': 'pole "kategória" je povinné pole',
                    'number.integer': 'pole musí byť celé číslo',
                    'any.required': 'pole "kategória" je povinné pole'
                }),
                subcategory_id: Joi.number().integer().greater(0).required().messages({
                    'number.empty': 'pole "kategória" je povinné pole',
                    'number.greater': 'pole "kategória" je povinné pole a musí byť celé číslo',
                    'number.base': 'pole "kategória" je povinné pole a musí byť celé číslo',
                    'number.integer': 'pole musí byť celé číslo',
                    'any.required': 'pole "kategória" je povinné pole'
                }),
                // short_description: Joi.string(),
                type: Joi.string().valid('internal', 'external'),
                // description:  Joi.alternatives().conditional('type', { is: 'internal', then:Joi.string(), otherwise: Joi.any() }),
                // slug: Joi.alternatives().conditional('type', { is: 'internal', then: Joi.string().pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$')).required().messages({
                //         'string.base': '"slug" je povinné pole',
                //         'string.empty': '"slug" je povinné pole',
                //         'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
                //     }),
                //     otherwise: Joi.any()
                // }),
                external_url: Joi.alternatives().conditional('type', { is: 'external', then: Joi.string().uri().required().messages({
                        'string.base': '"externá url" je povinné pole',
                        'string.uri': '"externá url" musí byť platná url',
                        'string.empty': '"externá url" je povinné pole',
                        'string.pattern.base': '"externá url" môže obsahovať len alfanumerické znaky a znak "-"'
                    }),
                    otherwise: Joi.any()
                }),
                start_date: Joi.alternatives().conditional('unlimited', { is: false, then: Joi.date().format('DD.MM.YYYY').required().messages({
                    'date.empty': 'pole "platnosť ponuky od" je povinné pole',
                    'date.format': 'pole "platnosť ponuky od" musí byť vo formáte DD.MM.RRRR',
                    'any.required': 'pole "platnosť ponuky od" je povinné pole',
                    }),
                    otherwise: Joi.any()
                }),
                end_date: Joi.alternatives().conditional('unlimited', { is: false, then: Joi.date().format('DD.MM.YYYY').required().messages({
                    'date.empty': 'pole "platnosť ponuky od" je povinné pole',
                    'date.format': 'pole "platnosť ponuky od" musí byť vo formáte DD.MM.RRRR',
                    'any.required': 'pole "platnosť ponuky od" je povinné pole',
                    }),
                    otherwise: Joi.any()
                }),
                unlimited: Joi.boolean().required(),
                active: Joi.boolean()
                .required(),
            });

            let validation_error = false;
            try {
                const validation = await schema.validateAsync({ 
                    title: title, 
                    // slug: slug, 
                    price: price,
                    price_for: price_for,
                    active: active,
                    unit: unit,
                    quantity: quantity,
                    min_quantity: min_quantity,
                    farmer_id: farmer_id,
                    // description: description,
                    // short_description: short_description,
                    category_id: category_id,
                    subcategory_id: subcategory_id,
                    type: type,
                    start_date: start_date,
                    end_date: end_date,
                    external_url: external_url,
                    unlimited: unlimited,
                }, {abortEarly: false});
                console.log('validation', validation);
            }
            catch (err) {
                console.log('validation error', err);
                validation_error = err;
            }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                let start_date_m = moment(start_date, "DD.MM.YYYY").format("YYYY-MM-DD");
                let end_date_m = moment(end_date, "DD.MM.YYYY").format("YYYY-MM-DD");

                if(unlimited){
                    start_date_m = null;
                    end_date_m = null;
                }

                if(!slug){
                    slug = slugify(title, {
                        lower: true,      // convert to lower case, defaults to `false`
                        locale: 'sk'       // language code of the locale to use
                    })
                }

                let data = {
                    uuid: uuidv4(),
                    title: title,
                    slug: slug,
                    price: price,
                    price_for: price_for,
                    unit: unit,
                    quantity: quantity,
                    min_quantity: min_quantity,
                    user_id: farmer_id,
                    description: description,
                    short_description: short_description,
                    category_id: category_id,
                    subcategory_id: subcategory_id,
                    created_by: user_id,
                    start_date: start_date_m,
                    end_date: end_date_m,
                    created: new Date().toISOString(),
                    unlimited: unlimited
                }

                data.external = false;
                if(type == 'external'){
                    data.external = true;
                    data.external_url = external_url;
                }

                data.active = 0;
                if(active){
                    data.active = 1;
                }

                data.nutritions = { type: nutritionValueType};
                if(nutritionValueType == 'custom'){
                    data.nutritions.values = {
                        nutritionalInformation: nutritionalInformation,
                        minerals: minerals,
                        vitamins: vitamins,
                    };
                }

                data.allergens = allergens;
                data.tags = tags;
                data.flags = flags;
    
                let product = await fastify.db.models.product.create(data);

                if(product){
                    // gallery
    
                    // create new
                    if(gallery && gallery.length){
                    
                        for(let i = 0; i < gallery.length; i++){
                            let filename = gallery[i].filename;
                            let imageData = {
                                product_id: product.old_id,
                                user_id: user_id,
                                filename: filename
                            };
                        
                            let image = await fastify.db.models.image.create(imageData);
                        }
    
                    }

                    let product_id = product.old_id;
                    product = await fastify.db.models.product.findOne({ 
                        include: [ 
                            { model: fastify.db.models.user, as: "farmer", required: true },
                        ],
                        where: { old_id: product_id }
                    });

                     // save to solr
                    const client = solr.createClient({
                        path: '/solr/odfarmara_products'
                    });
                    client.autoCommit = true;

                    let m_start_date = moment.utc(start_date, "DD.MM.YYYY");
                    let m_end_date = moment.utc(end_date, "DD.MM.YYYY");
                    let m_created_date = moment.utc(product.created);

                    let start_date_iso = m_start_date.toISOString();
                    let end_date_iso = m_end_date.toISOString();
                    let created_date = m_created_date.toISOString();

                    let farmer = product.farmer.company_name;
                    if(!farmer){
                        farmer = product.farmer.name;
                    }
                    if(!farmer){
                        farmer = product.farmer.email;
                    }
                    const active = product.active;
                    const region = product.region_id;

                    let doc = {
                        id : product_id,
                        ge_id : product_id,
                        ge_title : title,
                        ge_slug : slug,
                        ge_description : description,
                        ge_farmer : farmer,
                        ge_category : category_id,
                        ge_subcategory : subcategory_id,
                        ge_price : price,
                        ge_price_for : price_for,
                        ge_quantity : quantity,
                        ge_unit : unit,
                        ge_active : active,
                        ge_region : region,
                        ge_start_date : start_date_iso,
                        ge_end_date : end_date_iso,
                        ge_created_date : created_date,
                    }

                    if(gallery && gallery.length){
                        doc.ge_thumbnail = gallery[0].filename;
                    }

                    if(type == 'external'){
                        doc.ge_external = 1;
                        doc.ge_external_url = external_url;
                    }

                    // solr flags
                    if(flags && flags.length){

                        let flag_ids = [];
                        let flag_names = [];

                        for (let index = 0; index < flags.length; index++) {
                          const flag = flags[index];
              
                          flag_ids.push(flag.id);
                          flag_names.push(flag.ge_name);
                        }
              
                        doc.ge_flags = {"set": flag_ids};
                        doc.ge_flag_names = {"set": flag_names};
                    }

                    console.log('doc', doc);

                    const obj = await client.add(doc);
                    const commit = await client.commit();

                }

            }
    
            reply.send({    
                user: user_id,
                status: 'success' 
            })

        }
    })

    fastify.route({
        method: 'DELETE',
        url: '/admin/product/:id',
        handler: async function (request, reply) {

            const user_id = request.user.id;
            const id = request.params.id;
            
            // validation
            // const schema = Joi.object({
            //     id: Joi.string()
            //     .min(2)
            //     .max(1024)
            //     .required()
            //     .messages({
            //         'string.empty': '"názov" je povinné pole',
            //         'string.min': '"názov" nemôže byť kratší ako {#limit}',
            //         'any.required': '"názov" je povinné pole'
            //     }),
            //     slug: Joi.string()
            //     .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
            //     .required()
            //     .messages({
            //         'string.empty': '"slug" je povinné pole',
            //         'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
            //     }),
            //     system: Joi.boolean()
            //     .required(),
            //     active: Joi.boolean()
            //     .required(),
            // });

            

            let validation_error = false;
            // try {
            //     const validation = await schema.validateAsync({ name: name, slug: slug, system: system, active: active }, {abortEarly: false});
            //     console.log('validation', validation);
            // }
            // catch (err) {
            //     console.log('validation error', err);
            //     validation_error = err;
            // }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                // delete in DB
                const deleted = await fastify.db.models.product.destroy({
                    where: {
                      old_id: id
                    }
                });

                const client = solr.createClient({
                    path: '/solr/odfarmara_products'
                });

                // delete from solr
                const obj = await client.deleteByID(id.toString());
                const commit = await client.commit();

                reply.send({    
                    user: user_id,
                    status: 'success' 
                })

                // todo delete all tags from products, farmers, recipes, blog articles
                

            }

        }
    })

    fastify.route({
        method: 'POST',
        url: '/admin/image/upload',
        handler: async function (request, reply) {

            const user_id = request.user.id
            const data = await request.file()

            const fileName = data.filename;
            
            let fileId = await nanoid();
            let fileExt = fileName.split(".").pop();
            let newFileName = fileId + "." + fileExt;

            const destPath = process.cwd() + '/public/assets_web/thumb_large/' + newFileName;
            const absoluteFileUrl = fastify.config.ROOT + '/assets/assets_web/thumb_large/' + newFileName;
            const relativeFileUrl = 'assets_web/thumb_large/' + newFileName;

            let globalError = false;
            await pump(data.file, fs.createWriteStream(destPath));

            if(!globalError) {
                try {
                    await sharp('public/assets_web/thumb_large/' + newFileName)
                    .resize({
                        height: 300,
                    })
                    .toFile('public/assets_web/thumb_small/' + newFileName);
                    await sharp('public/assets_web/thumb_large/' + newFileName)
                    .resize({
                        height: 600,
                    })
                    .toFile('public/assets_web/thumb_medium/' + newFileName);
                } catch (error) {
                    console.log(error);
                    globalError = error
                }
            }

            if(!globalError){
                reply.send({
                    user: user_id,
                    absoluteFileUrl: absoluteFileUrl,
                    relativeFileUrl: relativeFileUrl,
                    location: absoluteFileUrl,
                    filename: newFileName,
                    status: 'success' 
                })
            }
            else{
                reply.send({
                    user: user_id,
                    error: error,
                    status: 'error' 
                })
            }

        }
    })
    
  }
  
  module.exports = routes
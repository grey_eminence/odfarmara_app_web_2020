const solr = require('solr-client')
const fs = require('fs')
const util = require('util')
const path = require('path')
const { nanoid } = require('nanoid')
const { pipeline } = require('stream')
const pump = util.promisify(pipeline)
const process = require('process')
const moment = require('moment')
const Joi = require('joi')
const { v4: uuidv4 } = require('uuid')

async function routes (fastify, options) {

    fastify.post('/admin/allergens/select', async (request, reply) => {

        const user_id = request.user.id;
        let docs = [];
        let query_text = "*:*";
        let startItem = 0;

        const request_query = request.body;
        let ipp = 100;
        let to = false;
        if(request_query.to){
            to = parseInt(request_query.to);
            ipp = 100 * Math.ceil(to / 100);
        }
        const search = request_query.search
        const page = request_query.page
        if (page) {
            startItem = (page * ipp) - ipp;
        }

        console.log('search', search);

        const order_by = request_query.sortBy;

        let order_type = '';
        if (order_by) {
            const desc = request_query.descending
            
            if(desc){
                order_type = 'desc'
            }
            else {
                order_type = 'asc'
            }
        }

        if(search){
            query_text = 'ge_name:' + search + ', ' + 'ge_slug:' + search;
        }

        let sortParams = {}; 
        sortParams[order_by] = order_type;

        const client = solr.createClient({
            path: '/solr/odfarmara_allergens'
        });

        if(to > 90){
            ipp = ipp + 100;
        }

        const query = client
        .query()
        .q(query_text)
        .start(startItem)
        .rows(ipp);

        if(order_by){
            query.sort(sortParams);
        }

        const data = await client.search(query);
        docs = data.response.docs;

        // set pages count
        let pages = Math.floor(data.response.numFound / ipp);
        let pages_modulo = data.response.numFound % ipp;
        if(pages_modulo){
        pages++;
        }

        console.log('user_id', user_id);
        reply.send({ 
            user: user_id,
            docs: docs,
            pages: parseInt(pages),
            count: data.response.numFound,
            max_to: ipp
        })

    })

    fastify.post('/admin/allergens', async (request, reply) => {

        const user_id = request.user.id;
        let docs = [];
        let query_text = "*:*";
        let startItem = 0;

        const request_query = request.body;
        const ipp = parseInt(request_query.rowsPerPage)
        const search = request_query.search
        const page = request_query.page
        if (page) {
            startItem = (page * ipp) - ipp;
        }

        console.log('search', search);

        const order_by = request_query.sortBy;

        let order_type = '';
        if (order_by) {
            const desc = request_query.descending
            
            if(desc){
                order_type = 'desc'
            }
            else {
                order_type = 'asc'
            }
        }

        if(search){
            query_text = 'ge_name:' + search + ', ' + 'ge_slug:' + search;
        }

        let sortParams = {}; 
        sortParams[order_by] = order_type;

        const client = solr.createClient({
            path: '/solr/odfarmara_allergens'
        });

        const query = client
        .query()
        .q(query_text)
        .start(startItem)
        .rows(ipp);

        if(order_by){
            query.sort(sortParams);
        }

        const data = await client.search(query);
        docs = data.response.docs;

        // set pages count
        let pages = Math.floor(data.response.numFound / ipp);
        let pages_modulo = data.response.numFound % ipp;
        if(pages_modulo){
        pages++;
        }

        console.log('user_id', user_id);
        reply.send({ 
            user: user_id,
            docs: docs,
            pages: parseInt(pages),
            count: data.response.numFound
        })

    })

    fastify.get('/admin/allergen/:id', async (request, reply) => {

        const user_id = request.user.id;
        const id = request.params.id

        const allergen = await fastify.db.models.allergen.findOne({
            where: {
                id: id
            },
        })

        reply.send({
            user: user_id,
            allergen: allergen 
        })
    
    })

    fastify.route({
        method: 'POST',
        url: '/admin/allergen/:id',
        handler: async function (request, reply) {

            const user = request.user
            const user_id = user.id
            const id = request.params.id
            const name = request.body.name
            const slug = request.body.slug
            const description = request.body.description
            const active = request.body.active

            // validation
            const schema = Joi.object({
                name: Joi.string()
                .min(2)
                .max(1024)
                .required()
                .messages({
                    'string.empty': '"názov" je povinné pole',
                    'string.min': '"názov" nemôže byť kratší ako {#limit}',
                    'any.required': '"názov" je povinné pole'
                }),
                slug: Joi.string()
                .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
                .required()
                .messages({
                    'string.empty': '"slug" je povinné pole',
                    'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
                }),
                active: Joi.boolean()
                .required(),
            });

            let validation_error = false;
            try {
                const validation = await schema.validateAsync({ 
                    name: name, 
                    slug: slug, 
                    active: active
                }, {abortEarly: false});
                console.log('validation', validation);
            }
            catch (err) {
                console.log('validation error', err);
                validation_error = err;
            }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                // update in DB
                await fastify.db.models.allergen.update({ 
                    name: name,
                    slug: slug,
                    description: description,
                    active: active,
                }, {
                    where: {
                        id: id
                    }
                })

                // add doc to solr
                const allergen = await fastify.db.models.allergen.findOne({
                    where: {
                        id: id
                    },
                });

                const userCreated = await fastify.db.models.user.findOne({
                    where: {
                        id: allergen.created_by
                    },
                });

                let created_date = allergen.created_at;
                let m_created_date = moment(created_date);
                created_date = m_created_date.toISOString();

                let user_name = "";
                if(userCreated){
                    if(userCreated.name == "" || userCreated.name == null){
                        user_name = userCreated.company_name;
                    }
                    else{
                        user_name = userCreated.name;
                    }
                }

                const client = solr.createClient({
                    path: '/solr/odfarmara_allergens'
                });

                let doc = {
                    id : allergen.id,
                    ge_id : {"set": allergen.id},
                    ge_name : {"set": allergen.name},
                    ge_slug : {"set": allergen.slug},
                    ge_description : {"set": allergen.description},
                    ge_created_date : {"set": created_date},
                    ge_user_name: {"set": user_name},
                    ge_active: {"set": allergen.active},
                }

                // add and commit to solr
                const obj = await client.add(doc);
                const commit = await client.commit();

                reply.send({    
                    user: user_id,
                    status: 'success' 
                })
            }

        }
    })

    fastify.route({
        method: 'POST',
        url: '/admin/allergen/create',
        handler: async function (request, reply) {

            const user = request.user
            const user_id = user.id

            const name = request.body.name
            const slug = request.body.slug
            const description = request.body.description
            const active = request.body.active

            // validation
            const schema = Joi.object({
                name: Joi.string()
                .min(2)
                .max(1024)
                .required()
                .messages({
                    'string.empty': '"názov" je povinné pole',
                    'string.min': '"názov" nemôže byť kratší ako {#limit}',
                    'any.required': '"názov" je povinné pole'
                }),
                slug: Joi.string()
                .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
                .required()
                .messages({
                    'string.empty': '"slug" je povinné pole',
                    'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
                }),
                active: Joi.boolean()
                .required(),
            });

            let validation_error = false;
            try {
                const validation = await schema.validateAsync({ 
                    name: name, 
                    slug: slug,
                    active: active, 
                }, {abortEarly: false});
                console.log('validation', validation);
            }
            catch (err) {
                console.log('validation error', err);
                validation_error = err;
            }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                // create in DB
                const new_tag = await fastify.db.models.allergen.create({ 
                    name: name,
                    slug: slug,
                    description: description,
                    active: active,
                    created_by: user_id,
                    uuid: uuidv4()
                });

                let created_date = new_tag.created_at;
                let m_created_date = moment(created_date);
                created_date = m_created_date.toISOString();

                let user = await fastify.db.models.user.findOne({
                    where: {
                        id: user_id
                    },
                });

                let user_name = "";
                if(user){
                    if(user.name == "" || user.name == null){
                        user_name = user.company_name;
                    }
                    else{
                        user_name = user.name;
                    }
                }

                const client = solr.createClient({
                    path: '/solr/odfarmara_allergens'
                });

                let doc = {
                    id : new_tag.id,
                    ge_id : new_tag.id,
                    ge_name : new_tag.name,
                    ge_slug : new_tag.slug,
                    ge_active : new_tag.active,
                    ge_description : new_tag.description,
                    ge_created_date : created_date,
                    ge_user_name: user_name
                }

                // add and commit to solr
                const obj = await client.add(doc);
                const commit = await client.commit();

                reply.send({    
                    user: user_id,
                    status: 'success' 
                })
            }

        }
    })

    fastify.route({
        method: 'DELETE',
        url: '/admin/allergen/:id',
        handler: async function (request, reply) {

            const user_id = request.user.id;
            const id = request.params.id;
            
            // validation
            // const schema = Joi.object({
            //     id: Joi.string()
            //     .min(2)
            //     .max(1024)
            //     .required()
            //     .messages({
            //         'string.empty': '"názov" je povinné pole',
            //         'string.min': '"názov" nemôže byť kratší ako {#limit}',
            //         'any.required': '"názov" je povinné pole'
            //     }),
            //     slug: Joi.string()
            //     .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
            //     .required()
            //     .messages({
            //         'string.empty': '"slug" je povinné pole',
            //         'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
            //     }),
            //     system: Joi.boolean()
            //     .required(),
            //     active: Joi.boolean()
            //     .required(),
            // });

            

            let validation_error = false;
            // try {
            //     const validation = await schema.validateAsync({ name: name, slug: slug, system: system, active: active }, {abortEarly: false});
            //     console.log('validation', validation);
            // }
            // catch (err) {
            //     console.log('validation error', err);
            //     validation_error = err;
            // }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                // delete in DB
                const deleted = await fastify.db.models.allergen.destroy({
                    where: {
                      id: id
                    }
                });

                const client = solr.createClient({
                    path: '/solr/odfarmara_allergens'
                });

                // delete from solr
                const obj = await client.deleteByID(id.toString());
                const commit = await client.commit();

                reply.send({    
                    user: user_id,
                    status: 'success' 
                })

                // todo delete all tags from products, farmers, recipes, blog articles
                

            }

        }
    })

  }
  
  module.exports = routes
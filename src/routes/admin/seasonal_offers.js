const solr = require('solr-client')
const fs = require('fs')
const util = require('util')
const path = require('path')
const { nanoid } = require('nanoid')
const { pipeline } = require('stream')
const pump = util.promisify(pipeline)
const process = require('process')
const moment = require('moment')
const Joi = require('joi')
const slugify = require('slugify')
const _ = require('lodash')

async function routes (fastify, options) {

    fastify.post('/admin/seasonal-offers', async (request, reply) => {

        const user_id = request.user.id;
        let docs = [];
        let query_text = "*:*";
        let startItem = 0;

        const request_query = request.body;
        const ipp = parseInt(request_query.rowsPerPage)
        const search = request_query.search
        const page = request_query.page
        if (page) {
            startItem = (page * ipp) - ipp;
        }

        console.log('search', search);

        const order_by = request_query.sortBy;

        let order_type = '';
        if (order_by) {
            const desc = request_query.descending
            
            if(desc){
                order_type = 'desc'
            }
            else {
                order_type = 'asc'
            }
        }

        if(search){
            query_text = 'ge_name:' + search + ', ' + 'ge_slug:' + search;
        }

        let sortParams = {}; 
        sortParams[order_by] = order_type;

        const client = solr.createClient({
            path: '/solr/odfarmara_seasonal_offers'
        });

        const query = client
        .query()
        .q(query_text)
        .start(startItem)
        .rows(ipp);

        if(order_by){
            query.sort(sortParams);
        }

        const data = await client.search(query);
        docs = data.response.docs;

        // set pages count
        let pages = Math.floor(data.response.numFound / ipp);
        let pages_modulo = data.response.numFound % ipp;
        if(pages_modulo){
        pages++;
        }

        console.log('user_id', user_id);
        reply.send({ 
            user: user_id,
            docs: docs,
            pages: parseInt(pages),
            count: data.response.numFound
        })

    })

    fastify.get('/admin/seasonal-offer/:id', async (request, reply) => {

        const user_id = request.user.id;
        const id = request.params.id;

        console.log('id', id);

        const seasonal_offer = await fastify.db.models.seasonal_offer.findOne({
            where: {
                id: id
            },
        })

        console.log('seasonal_offer', seasonal_offer);

        reply.send({
            user: user_id, 
            seasonal_offer: seasonal_offer 
        })
    
    })

    fastify.route({
        method: 'POST',
        url: '/admin/seasonal-offer/:id',
        handler: async function (request, reply) {

            const user_id = request.user.id
            const id = request.params.id
            const name = request.body.name
            const slug = request.body.slug
            const description = request.body.description
            const active = request.body.active
            const categories = request.body.categories;
            const tags = request.body.tags;
            const start_date = request.body.start_date;
            const end_date = request.body.end_date;

            // validation
            const schema = Joi.object({
                name: Joi.string()
                .min(2)
                .max(1024)
                .required()
                .messages({
                    'string.empty': '"názov" je povinné pole',
                    'string.min': '"názov" nemôže byť kratší ako {#limit}',
                    'any.required': '"názov" je povinné pole'
                }),
                slug: Joi.string()
                .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
                .required()
                .messages({
                    'string.empty': '"slug" je povinné pole',
                    'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
                }),
                active: Joi.boolean()
                .required(),
            });

            let validation_error = false;
            try {
                const validation = await schema.validateAsync({ name: name, slug: slug, active: active }, {abortEarly: false});
                console.log('validation', validation);
            }
            catch (err) {
                console.log('validation error', err);
                validation_error = err;
            }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                let start_date_m = moment(start_date, "DD.MM.YYYY").format("YYYY-MM-DD");
                let end_date_m = moment(end_date, "DD.MM.YYYY").format("YYYY-MM-DD");

                let cleanCategories = _.without(categories, undefined, null, "");

                let data = { 
                    name: name,
                    slug: slug,
                    description: description,
                    categories: cleanCategories,
                    tags: tags,
                    display_from: start_date_m,
                    display_to: end_date_m,
                    active: active,
                };

                // create in DB
                const updated = await fastify.db.models.seasonal_offer.update(data, 
                    { 
                        where: {
                            id: id
                        }
                    }
                );

                if(updated){

                    let from_date =  start_date;
                    let to_date =  end_date;

                    let m_from_date = moment(from_date, "YYYY-MM-DD");
                    from_date = m_from_date.toISOString();
                    let m_to_date = moment(to_date, "YYYY-MM-DD");
                    to_date = m_to_date.toISOString();
                   
                    let tagsArr = [];
                    if(tags && tags.length){
                        for(let i = 0; i < tags.length; i++){
                            let item = tags[i];
                            tagsArr.push(item.id);
                        }
                    }

                    const client = solr.createClient({
                        path: '/solr/odfarmara_seasonal_offers'
                    });

                    let doc = {
                        id : id,
                        ge_id : {'set': id },
                        ge_name : {'set': name },
                        ge_slug : {'set': slug },
                        ge_category : {'set': cleanCategories },
                        ge_tags : {'set': tagsArr },
                        ge_active : {'set': active },
                        ge_start_date : {'set': from_date },
                        ge_end_date : {'set': to_date }
                    }

                    // add and commit to solr
                    const obj = await client.add(doc);
                    const commit = await client.commit();

                }

                reply.send({    
                    user: user_id,
                    status: 'success' 
                })
            }

        }
    })

    fastify.route({
        method: 'POST',
        url: '/admin/seasonal-offer/create',
        handler: async function (request, reply) {

            const user_id = request.user.id
            const name = request.body.name
            const slug = request.body.slug
            const description = request.body.description
            const active = request.body.active
            const categories = request.body.categories;
            const tags = request.body.tags;
            const start_date = request.body.start_date;
            const end_date = request.body.end_date;

            // validation
            const schema = Joi.object({
                name: Joi.string()
                .min(2)
                .max(1024)
                .required()
                .messages({
                    'string.empty': '"názov" je povinné pole',
                    'string.min': '"názov" nemôže byť kratší ako {#limit}',
                    'any.required': '"názov" je povinné pole'
                }),
                slug: Joi.string()
                .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
                .required()
                .messages({
                    'string.empty': '"slug" je povinné pole',
                    'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
                }),
                active: Joi.boolean()
                .required(),
            });

            let validation_error = false;
            try {
                const validation = await schema.validateAsync({ name: name, slug: slug, active: active }, {abortEarly: false});
                console.log('validation', validation);
            }
            catch (err) {
                console.log('validation error', err);
                validation_error = err;
            }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                let start_date_m = moment(start_date, "DD.MM.YYYY").format("YYYY-MM-DD");
                let end_date_m = moment(end_date, "DD.MM.YYYY").format("YYYY-MM-DD");

                // create in DB
                const new_so = await fastify.db.models.seasonal_offer.create({ 
                    name: name,
                    slug: slug,
                    description: description,
                    categories: categories,
                    tags: tags,
                    display_from: start_date_m,
                    display_to: end_date_m,
                    active: active,
                });

                if(new_so){

                    let from_date =  start_date;
                    let to_date =  end_date;
                    let created_date = new_so.createdAt;

                    let m_from_date = moment(from_date, "YYYY-MM-DD");
                    from_date = m_from_date.toISOString();
                    let m_to_date = moment(to_date, "YYYY-MM-DD");
                    to_date = m_to_date.toISOString();
                    let m_created_date = moment(created_date);
                    created_date = m_created_date.toISOString();

                    let tagsArr = [];
                    if(tags && tags.length){
                        for(let i = 0; i < tags.length; i++){
                            let item = tags[i];
                            tagsArr.push(item.id);
                        }
                    }

                    const client = solr.createClient({
                        path: '/solr/odfarmara_seasonal_offers'
                    });

                    let doc = {
                        id : new_so.id, 
                        ge_id : {'set': new_so.id },
                        ge_name : {'set': name },
                        ge_slug : {'set': slug },
                        ge_category : {'set': categories },
                        ge_tags : {'set': tagsArr },
                        ge_active : {'set': active },
                        ge_start_date : {'set': from_date },
                        ge_end_date : {'set': to_date },
                        ge_created_date : {'set': created_date },
                    }

                    // add and commit to solr
                    const obj = await client.add(doc);
                    const commit = await client.commit();

                }

                reply.send({    
                    user: user_id,
                    status: 'success' 
                })
            }

        }
    })

    fastify.route({
        method: 'DELETE',
        url: '/admin/seasonal-offer/:id',
        handler: async function (request, reply) {

            const user_id = request.user.id;
            const id = request.params.id;
            
            // validation
            // const schema = Joi.object({
            //     id: Joi.string()
            //     .min(2)
            //     .max(1024)
            //     .required()
            //     .messages({
            //         'string.empty': '"názov" je povinné pole',
            //         'string.min': '"názov" nemôže byť kratší ako {#limit}',
            //         'any.required': '"názov" je povinné pole'
            //     }),
            //     slug: Joi.string()
            //     .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
            //     .required()
            //     .messages({
            //         'string.empty': '"slug" je povinné pole',
            //         'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
            //     }),
            //     system: Joi.boolean()
            //     .required(),
            //     active: Joi.boolean()
            //     .required(),
            // });

            

            let validation_error = false;
            // try {
            //     const validation = await schema.validateAsync({ name: name, slug: slug, system: system, active: active }, {abortEarly: false});
            //     console.log('validation', validation);
            // }
            // catch (err) {
            //     console.log('validation error', err);
            //     validation_error = err;
            // }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                // delete in DB
                const deleted = await fastify.db.models.seasonal_offer.destroy({
                    where: {
                      id: id
                    }
                });

                const client = solr.createClient({
                    path: '/solr/odfarmara_seasonal_offers'
                });


                // delete from solr
                const obj = await client.deleteByID(id);
                const commit = await client.commit();

                reply.send({    
                    user: user_id,
                    status: 'success' 
                })

                // todo delete all tags from products, farmers, recipes, blog articles
                

            }

        }
    })

  }
  
  module.exports = routes
const solr = require('solr-client')
const fs = require('fs')
const util = require('util')
const path = require('path')
const { nanoid } = require('nanoid')
const { pipeline } = require('stream')
const pump = util.promisify(pipeline)
const process = require('process')
const moment = require('moment')
const Joi = require('joi')
const slugify = require('slugify')

async function routes (fastify, options) {

    fastify.post('/admin/tags/select', async (request, reply) => {

        const user_id = request.user.id;
        let docs = [];
        let query_text = "*:*";
        let startItem = 0;

        const request_query = request.body;
        let ipp = 100;
        let to = false;
        if(request_query.to){
            to = parseInt(request_query.to);
            ipp = 100 * Math.ceil(to / 100);
        }
        const search = request_query.search
        const page = request_query.page
        if (page) {
            startItem = (page * ipp) - ipp;
        }

        console.log('search', search);

        const order_by = request_query.sortBy;

        let order_type = '';
        if (order_by) {
            const desc = request_query.descending
            
            if(desc){
                order_type = 'desc'
            }
            else {
                order_type = 'asc'
            }
        }

        if(search){
            query_text = 'ge_name:' + search + ', ' + 'ge_slug:' + search;
        }

        let sortParams = {}; 
        sortParams[order_by] = order_type;

        const client = solr.createClient({
            path: '/solr/odfarmara_tags'
        });

        if(to > 90){
            ipp = ipp + 100;
        }

        const query = client
        .query()
        .q(query_text)
        .start(startItem)
        .rows(ipp);

        if(order_by){
            query.sort(sortParams);
        }

        const data = await client.search(query);
        docs = data.response.docs;

        // set pages count
        let pages = Math.floor(data.response.numFound / ipp);
        let pages_modulo = data.response.numFound % ipp;
        if(pages_modulo){
        pages++;
        }

        console.log('user_id', user_id);
        reply.send({ 
            user: user_id,
            docs: docs,
            pages: parseInt(pages),
            count: data.response.numFound,
            max_to: ipp
        })

    })

    fastify.post('/admin/tags', async (request, reply) => {

        const user_id = request.user.id;
        let docs = [];
        let query_text = "*:*";
        let startItem = 0;

        const request_query = request.body;
        const ipp = parseInt(request_query.rowsPerPage)
        const search = request_query.search
        const page = request_query.page
        if (page) {
            startItem = (page * ipp) - ipp;
        }

        console.log('search', search);

        const order_by = request_query.sortBy;

        let order_type = '';
        if (order_by) {
            const desc = request_query.descending
            
            if(desc){
                order_type = 'desc'
            }
            else {
                order_type = 'asc'
            }
        }

        if(search){
            query_text = 'ge_name:' + search + ', ' + 'ge_slug:' + search;
        }

        let sortParams = {}; 
        sortParams[order_by] = order_type;

        const client = solr.createClient({
            path: '/solr/odfarmara_tags'
        });

        const query = client
        .query()
        .q(query_text)
        .start(startItem)
        .rows(ipp);

        if(order_by){
            query.sort(sortParams);
        }

        const data = await client.search(query);
        docs = data.response.docs;

        // set pages count
        let pages = Math.floor(data.response.numFound / ipp);
        let pages_modulo = data.response.numFound % ipp;
        if(pages_modulo){
        pages++;
        }

        console.log('user_id', user_id);
        reply.send({ 
            user: user_id,
            docs: docs,
            pages: parseInt(pages),
            count: data.response.numFound
        })

    })

    fastify.get('/admin/tag/:id', async (request, reply) => {

        const user_id = request.user.id;
        const id = request.params.id

        const tag = await fastify.db.models.tag.findOne({
            include: [ 
            { model: fastify.db.models.user },
            ],
            where: {
                id: id
            },
        })

        reply.send({
            user: user_id, 
            tag: tag 
        })
    
    })

    fastify.route({
        method: 'POST',
        url: '/admin/tag/:id',
        handler: async function (request, reply) {

            const user_id = request.user.id
            const id = request.params.id
            const name = request.body.name
            const slug = request.body.slug
            const description = request.body.description
            const system = request.body.system
            const active = request.body.active
            const landing_page = request.body.landing_page

            // validation
            const schema = Joi.object({
                name: Joi.string()
                .min(2)
                .max(1024)
                .required()
                .messages({
                    'string.empty': '"názov" je povinné pole',
                    'string.min': '"názov" nemôže byť kratší ako {#limit}',
                    'any.required': '"názov" je povinné pole'
                }),
                slug: Joi.string()
                .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
                .required()
                .messages({
                    'string.empty': '"slug" je povinné pole',
                    'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
                }),
                system: Joi.boolean()
                .required(),
                active: Joi.boolean()
                .required(),
                landing_page: Joi.boolean()
                .required(),
            });

            let validation_error = false;
            try {
                const validation = await schema.validateAsync({ name: name, slug: slug, system: system, active: active, landing_page: landing_page }, {abortEarly: false});
                console.log('validation', validation);
            }
            catch (err) {
                console.log('validation error', err);
                validation_error = err;
            }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                // update in DB
                await fastify.db.models.tag.update({ 
                    name: name,
                    slug: slug,
                    description: description,
                    system: system,
                    active: active,
                    landing_page: landing_page
                }, {
                    where: {
                    id: id
                    }
                })

                // add doc to solr
                const tag = await fastify.db.models.tag.findOne({
                    include: [ 
                    { model: fastify.db.models.user},
                    ],
                    where: {
                        id: id
                    },
                });

                let doc_system = tag.system;
                if(system){
                    doc_system = true;
                }
                else{
                    doc_system = false;
                }
                let created_date = tag.createdAt;
                let m_created_date = moment(created_date);
                created_date = m_created_date.toISOString();

                let user_name = "";
                if(tag.user){
                    if(tag.user.name == "" || tag.user.name == null){
                        user_name = tag.user.company_name;
                    }
                    else{
                        user_name = tag.user.name;
                    }
                }

                const client = solr.createClient({
                    path: '/solr/odfarmara_tags'
                });

                let doc = {
                    id : tag.id,
                    ge_id: {"set": tag.id},
                    ge_name : {"set": tag.name},
                    ge_slug : {"set": tag.slug},
                    ge_system : {"set": doc_system},
                    ge_created_date : {"set": created_date},
                    ge_user_name: {"set": user_name},
                    ge_active: {"set": active},
                    ge_front_page: {"set": landing_page}
                }

                // add and commit to solr
                const obj = await client.add(doc);
                const commit = await client.commit();

                reply.send({    
                    user: user_id,
                    status: 'success' 
                })
            }

        }
    })

    fastify.route({
        method: 'POST',
        url: '/admin/tag/create',
        handler: async function (request, reply) {

            const user_id = request.user.id
            const name = request.body.name
            const slug = request.body.slug
            const description = request.body.description
            const system = request.body.system
            const active = request.body.active
            const landing_page = request.body.landing_page

            // validation
            const schema = Joi.object({
                name: Joi.string()
                .min(2)
                .max(1024)
                .required()
                .messages({
                    'string.empty': '"názov" je povinné pole',
                    'string.min': '"názov" nemôže byť kratší ako {#limit}',
                    'any.required': '"názov" je povinné pole'
                }),
                slug: Joi.string()
                .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
                .required()
                .messages({
                    'string.empty': '"slug" je povinné pole',
                    'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
                }),
                system: Joi.boolean()
                .required(),
                landing_page: Joi.boolean()
                .required(),
                active: Joi.boolean()
                .required(),
            });

            let validation_error = false;
            try {
                const validation = await schema.validateAsync({ name: name, slug: slug, system: system, active: active, landing_page: landing_page }, {abortEarly: false});
                console.log('validation', validation);
            }
            catch (err) {
                console.log('validation error', err);
                validation_error = err;
            }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                // update in DB
                const new_tag = await fastify.db.models.tag.create({ 
                    name: name,
                    slug: slug,
                    description: description,
                    system: system,
                    active: active,
                    landing_page: landing_page,
                    user_id: user_id,
                });

                let doc_system = new_tag.system;
                if(doc_system){
                    doc_system = true;
                }
                else{
                    doc_system = false;
                }
               
                let created_date = new_tag.created_at;
                let m_created_date = moment(created_date);
                created_date = m_created_date.toISOString();

                let user = await fastify.db.models.user.findOne({
                    where: {
                        id: user_id
                    },
                });

                let user_name = "";
                if(user){
                    if(user.name == "" || user.name == null){
                        user_name = user.company_name;
                    }
                    else{
                        user_name = user.name;
                    }
                }

                const client = solr.createClient({
                    path: '/solr/odfarmara_tags'
                });

                let doc = {
                    id : new_tag.id,
                    ge_id : new_tag.id,
                    ge_name : new_tag.name,
                    ge_slug : new_tag.slug,
                    ge_system : doc_system,
                    ge_active : new_tag.active,
                    ge_front_page: new_tag.landing_page,
                    ge_created_date : created_date,
                    ge_user_name: user_name
                }

                // add and commit to solr
                const obj = await client.add(doc);
                const commit = await client.commit();

                reply.send({    
                    user: user_id,
                    status: 'success' 
                })
            }

        }
    })

    fastify.route({
        method: 'POST',
        url: '/admin/tag/quick-create',
        handler: async function (request, reply) {

            const user_id = request.user.id;
            const new_tag = request.body.name;

            // validation
            const schema = Joi.object({
                new_tag: Joi.string()
                .min(2)
                .max(1024)
                .required()
                .messages({
                    'string.empty': '"názov" je povinné pole',
                    'string.min': '"názov" nemôže byť kratší ako {#limit}',
                    'any.required': '"názov" je povinné pole'
                }),
            });

            let validation_error = false;
            try {
                const validation = await schema.validateAsync({ new_tag: new_tag }, {abortEarly: false});
                console.log('validation', validation);
            }
            catch (err) {
                console.log('validation error', err);
                validation_error = err;
            }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                const slug = slugify(new_tag, {
                    lower: true,
                    locale: 'sk'
                })

                console.log('slug', slug);

                let exist = await fastify.db.models.tag.findAll({
                    where: { slug: slug }
                });

                console.log('exist', exist);

                if(exist.length){
                    reply.send({    
                        validation_error: {'new_tag': 'uvedený tag už existuje'},
                    })
                }
                else{

                    // update in DB
                    const tag_created = await fastify.db.models.tag.create({ 
                        name: new_tag,
                        slug: slug,
                        system: false,
                        active: true,
                        landing_page: false,
                        user_id: user_id,
                    });

                    let doc_system = tag_created.system;
                    if(doc_system){
                        doc_system = true;
                    }
                    else{
                        doc_system = false;
                    }
                
                    let created_date = tag_created.created_at;
                    let m_created_date = moment(created_date);
                    created_date = m_created_date.toISOString();

                    let user = await fastify.db.models.user.findOne({
                        where: {
                            id: user_id
                        },
                    });

                    let user_name = "";
                    if(user){
                        if(user.name == "" || user.name == null){
                            user_name = user.company_name;
                        }
                        else{
                            user_name = user.name;
                        }
                    }

                    const client = solr.createClient({
                        path: '/solr/odfarmara_tags'
                    });

                    let doc = {
                        id : tag_created.id,
                        ge_id : tag_created.id,
                        ge_name : tag_created.name,
                        ge_slug : tag_created.slug,
                        ge_system : doc_system,
                        ge_active : tag_created.active,
                        ge_front_page: tag_created.landing_page,
                        ge_created_date : created_date,
                        ge_user_name: user_name
                    }

                    // add and commit to solr
                    const obj = await client.add(doc);
                    const commit = await client.commit();

                    reply.send({    
                        user: user_id,
                        status: 'success' 
                    })
                }
            }

        }
    })

    fastify.route({
        method: 'DELETE',
        url: '/admin/tag/:id',
        handler: async function (request, reply) {

            const user_id = request.user.id;
            const id = request.params.id;

            
            
            // validation
            // const schema = Joi.object({
            //     id: Joi.string()
            //     .min(2)
            //     .max(1024)
            //     .required()
            //     .messages({
            //         'string.empty': '"názov" je povinné pole',
            //         'string.min': '"názov" nemôže byť kratší ako {#limit}',
            //         'any.required': '"názov" je povinné pole'
            //     }),
            //     slug: Joi.string()
            //     .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
            //     .required()
            //     .messages({
            //         'string.empty': '"slug" je povinné pole',
            //         'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
            //     }),
            //     system: Joi.boolean()
            //     .required(),
            //     active: Joi.boolean()
            //     .required(),
            // });

            

            let validation_error = false;
            // try {
            //     const validation = await schema.validateAsync({ name: name, slug: slug, system: system, active: active }, {abortEarly: false});
            //     console.log('validation', validation);
            // }
            // catch (err) {
            //     console.log('validation error', err);
            //     validation_error = err;
            // }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                // delete in DB
                const deleted = await fastify.db.models.tag.destroy({
                    where: {
                      id: id
                    }
                });

                const client = solr.createClient({
                    path: '/solr/odfarmara_tags'
                });


                // delete from solr
                const obj = await client.deleteByID(id.toString());
                const commit = await client.commit();

                reply.send({    
                    user: user_id,
                    status: 'success' 
                })

                // let products = 

                // todo delete all tags from products, farmers, recipes, blog articles
                // let exist = await fastify.db.models.tag.findAll({
                //     where: { slug: slug }
                // });
                

            }

        }
    })

  }
  
  module.exports = routes
const solr = require('solr-client')
const fs = require('fs')
const util = require('util')
const path = require('path')
const { nanoid } = require('nanoid')
const { pipeline } = require('stream')
const pump = util.promisify(pipeline)
const process = require('process')
const moment = require('moment')
const Joi = require('joi')
const { v4: uuidv4 } = require('uuid')
const { Op } = require("sequelize")

async function routes (fastify, options) {
    fastify.post('/admin/recipes', async (request, reply) => {

        // console.log('request', request);
  
        const user_id = request.user.id;
        let docs = [];
        let query_text = "*:*";
        let startItem = 0;

        const request_query = request.body;
        const ipp = parseInt(request_query.rowsPerPage)
        const search = request_query.search
        const page = request_query.page
        if (page) {
            startItem = (page * ipp) - ipp;
        }

        console.log('search', search);

        const order_by = request_query.sortBy;

        let order_type = '';
        if (order_by) {
            const desc = request_query.descending
            
            if(desc){
                order_type = 'desc'
            }
            else {
                order_type = 'asc'
            }
        }

        if(search){
            query_text = 'ge_title:' + search + ', ' + 'ge_description:' + search;
        }

        let sortParams = {}; 
        sortParams[order_by] = order_type;

        const client = solr.createClient({
            path: '/solr/odfarmara_recipes'
        });

        const query = client
        .query()
        .q(query_text)
        .start(startItem)
        .rows(ipp);

        if(order_by){
            query.sort(sortParams);
        }

        const data = await client.search(query);
        docs = data.response.docs;

        // set pages count
        let pages = Math.floor(data.response.numFound / ipp);
        let pages_modulo = data.response.numFound % ipp;
        if(pages_modulo){
        pages++;
        }

        console.log('user_id', user_id);
        reply.send({ 
            user: user_id,
            docs: docs,
            pages: parseInt(pages),
            count: data.response.numFound
        })

    })

    fastify.get('/admin/recipe/:id', async (request, reply) => {

        const user_id = request.user.id;
        const id = request.params.id;

        // const product = await fastify.db.models.recipe.findOne({
        //     include: [ 
        //     { model: fastify.db.models.category, as: "category" },
        //     { model: fastify.db.models.category, as: "subcategory" },
        //     { model: fastify.db.models.image, as: "image"},
        //     { model: fastify.db.models.tag },
        //     { 
        //         model: fastify.db.models.user, 
        //         as: "farmer"
        //     },
        //     ],
        //     where: {
        //         old_id: id
        //     },
        // })

        const recipe = await fastify.db.models.recipe.findOne({
            include: [{ model: fastify.db.models.category, as: "category" }],
            where: {
                id: id
            },
        });

        reply.send({    
            user: user_id, 
            recipe: recipe
        });
    
    })

    fastify.route({
        method: 'POST',
        url: '/admin/recipe/create',
        handler: async function (request, reply) {

            const user = request.user;
            const user_id = user.id;
            const title = request.body.title;
            const slug = request.body.slug;
            const perex = request.body.perex;
            const contents = request.body.contents;
            const category_id = request.body.category_id;
            const tags = request.body.tags;
            const img = request.body.img;
            const calories = request.body.calories;
            const procedure = request.body.procedure;
            const ingredients = request.body.ingredients;
            const time_cooking = request.body.time_cooking;
            const time_preparation = request.body.time_preparation;
            const time_overall = request.body.time_overall;
            const active = request.body.active;

            // validation
            const schema = Joi.object({
                title: Joi.string()
                .min(2)
                .max(1024)
                .required()
                .messages({
                    'string.empty': '"názov" je povinné pole',
                    'string.min': '"názov" nemôže byť kratší ako {#limit}',
                    'any.required': '"názov" je povinné pole'
                }),
                slug: Joi.string()
                .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
                .required()
                .messages({
                    'string.base': '"slug" je povinné pole',
                    'string.empty': '"slug" je povinné pole',
                    'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
                }),
                // perex: Joi.string()
                // .required()
                // .messages({
                //     'string.base': '"perex" je povinné pole',
                //     'string.empty': '"perex" je povinné pole',
                //     'any.required': '"perex" je povinné pole'
                // }),
                // contents: Joi.string()
                // .min(2)
                // .required()
                // .messages({
                //     'string.base': '"perex" je povinné pole',
                //     'string.empty': '"perex" je povinné pole',
                //     'any.required': '"perex" je povinné pole'
                // }),
                category_id: Joi.number()
                .required()
                .messages({
                    'number.base': '"kategória" je povinné pole',
                    'number.empty': '"kategória" je povinné pole',
                    'any.required': '"kategória" je povinné pole'
                }),
                time_cooking: Joi.number()
                .required()
                .messages({
                    'number.base': '"varenie" je povinné pole',
                    'number.empty': '"varenie" je povinné pole',
                    'any.required': '"varenie" je povinné pole'
                }),
                time_overall: Joi.number()
                .required()
                .messages({
                    'number.base': '"celkový čas" je povinné pole',
                    'number.empty': '"celkový čas" je povinné pole',
                    'any.required': '"celkový čas" je povinné pole'
                }),
                time_preparation: Joi.number()
                .required()
                .messages({
                    'number.base': '"príprava" je povinné pole',
                    'number.empty': '"príprava" je povinné pole',
                    'any.required': '"príprava" je povinné pole'
                }),
                calories: Joi.number()
                .required()
                .messages({
                    'number.base': '"kalórie" je povinné pole',
                    'number.empty': '"kalórie" je povinné pole',
                    'any.required': '"kalórie" je povinné pole'
                }),
                active: Joi.boolean()
                .required(),
            });

            let validation_error = false;
            try {
                const validation = await schema.validateAsync({ 
                    title: title, 
                    slug: slug, 
                    active: active,
                    // contents: contents,
                    category_id: category_id,
                    time_cooking: time_cooking,
                    time_overall: time_overall,
                    time_preparation: time_preparation,
                    calories: calories,
                    // perex: perex,
                }, {abortEarly: false});
                console.log('validation', validation);
            }
            catch (err) {
                console.log('validation error', err);
                validation_error = err;
            }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                let active_value = '0';
                if(active){
                    active_value = '1';
                }

                let time = {
                    cooking: time_cooking,
                    preparation: time_preparation,
                    overall: time_overall
                }

                let data = {
                    title: title,
                    active: active_value,
                    slug: slug,
                    perex: perex,
                    contents: contents,
                    category_id: category_id,
                    tags: tags,
                    img: img,
                    calories: calories,
                    procedure: procedure,
                    ingredients: request.body.ingredients,
                    time: time
                }

                console.log('data', data);
    
                let new_recipe = await fastify.db.models.recipe.create(data);
                
                const recipe = await fastify.db.models.recipe.findOne({
                    include: [{ model: fastify.db.models.category, as: "category" }],
                    where: {
                        id: new_recipe.id
                    },
                });

                // save to solr
                const client = solr.createClient({
                    path: '/solr/odfarmara_recipes'
                });
                client.autoCommit = true;

                let m_created_date = moment(recipe.dataValues.created);
                let created_date = m_created_date.toISOString();

                let doc = {
                    id : recipe.id,
                    ge_id: recipe.id,
                    ge_name : title,
                    ge_description : contents,
                    ge_category : category_id,
                    ge_category_name: recipe.category.name,
                    ge_thumbnail : img,
                    ge_active : active,
                    ge_created_date : created_date,
                }

                console.log('doc', doc);

                const obj = await client.add(doc);
                const commit = await client.commit();

            }
    
            reply.send({    
                user: user_id,
                status: 'success' 
            })

        }
    })

    fastify.route({
        method: 'POST',
        url: '/admin/recipe/:id',
        handler: async function (request, reply) {

            const user = request.user;
            const user_id = user.id;
            const id = request.params.id;
            const title = request.body.title;
            const slug = request.body.slug;
            const perex = request.body.perex;
            const contents = request.body.contents;
            const category_id = request.body.category_id;
            const tags = request.body.tags;
            const img = request.body.img;
            const calories = request.body.calories;
            const procedure = request.body.procedure;
            const ingredients = request.body.ingredients;
            const time_cooking = request.body.time_cooking;
            const time_preparation = request.body.time_preparation;
            const time_overall = request.body.time_overall;
            const active = request.body.active;

            // validation
            const schema = Joi.object({
                title: Joi.string()
                .min(2)
                .max(1024)
                .required()
                .messages({
                    'string.empty': '"názov" je povinné pole',
                    'string.min': '"názov" nemôže byť kratší ako {#limit}',
                    'any.required': '"názov" je povinné pole'
                }),
                slug: Joi.string()
                .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
                .required()
                .messages({
                    'string.base': '"slug" je povinné pole',
                    'string.empty': '"slug" je povinné pole',
                    'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
                }),
                // perex: Joi.string()
                // .required()
                // .messages({
                //     'string.base': '"perex" je povinné pole',
                //     'string.empty': '"perex" je povinné pole',
                //     'any.required': '"perex" je povinné pole'
                // }),
                // contents: Joi.string()
                // .min(2)
                // .required()
                // .messages({
                //     'string.base': '"perex" je povinné pole',
                //     'string.empty': '"perex" je povinné pole',
                //     'any.required': '"perex" je povinné pole'
                // }),
                category_id: Joi.number()
                .required()
                .messages({
                    'number.base': '"kategória" je povinné pole',
                    'number.empty': '"kategória" je povinné pole',
                    'any.required': '"kategória" je povinné pole'
                }),
                time_cooking: Joi.number()
                .required()
                .messages({
                    'number.base': '"varenie" je povinné pole',
                    'number.empty': '"varenie" je povinné pole',
                    'any.required': '"varenie" je povinné pole'
                }),
                time_overall: Joi.number()
                .required()
                .messages({
                    'number.base': '"celkový čas" je povinné pole',
                    'number.empty': '"celkový čas" je povinné pole',
                    'any.required': '"celkový čas" je povinné pole'
                }),
                time_preparation: Joi.number()
                .required()
                .messages({
                    'number.base': '"príprava" je povinné pole',
                    'number.empty': '"príprava" je povinné pole',
                    'any.required': '"príprava" je povinné pole'
                }),
                calories: Joi.number()
                .required()
                .messages({
                    'number.base': '"kalórie" je povinné pole',
                    'number.empty': '"kalórie" je povinné pole',
                    'any.required': '"kalórie" je povinné pole'
                }),
                active: Joi.boolean()
                .required(),
            });

            let validation_error = false;
            try {
                const validation = await schema.validateAsync({ 
                    title: title, 
                    slug: slug, 
                    active: active,
                    // contents: contents,
                    category_id: category_id,
                    time_cooking: time_cooking,
                    time_overall: time_overall,
                    time_preparation: time_preparation,
                    calories: calories,
                    // perex: perex,
                }, {abortEarly: false});
                console.log('validation', validation);
            }
            catch (err) {
                console.log('validation error', err);
                validation_error = err;
            }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                let active_value = '0';
                if(active){
                    active_value = '1';
                }

                let time = {
                    cooking: time_cooking,
                    preparation: time_preparation,
                    overall: time_overall
                }

                let data = {
                    title: title,
                    active: active_value,
                    slug: slug,
                    perex: perex,
                    contents: contents,
                    category_id: category_id,
                    tags: tags,
                    img: img,
                    calories: calories,
                    procedure: procedure,
                    ingredients: request.body.ingredients,
                    time: time
                }

                console.log('data', data);
    
                let updated = await fastify.db.models.recipe.update(data, {
                    where: {
                        id: id
                    }
                })

                const updated_recipe = await fastify.db.models.recipe.findOne({ 
                    include: [ 
                        { model: fastify.db.models.category, as: "category" },
                    ],
                    where: { id: id }
                });

                // save to solr
                const client = solr.createClient({
                    path: '/solr/odfarmara_recipes'
                });
                client.autoCommit = true;

                let m_created_date = moment(updated_recipe.dataValues.created);
                let created_date = m_created_date.toISOString();

                let doc = {
                    id : id,
                    ge_id: {"set": id},
                    ge_name : {"set": title},
                    ge_description : {"set": contents},
                    ge_category : {"set": category_id},
                    ge_category_name: {"set": updated_recipe.category.name},
                    ge_thumbnail : {"set": img},
                    ge_active : {"set": active},
                    ge_created_date : {"set": created_date},
                }

                console.log('doc', doc);

                const obj = await client.add(doc);
                const commit = await client.commit();

            }
    
            reply.send({    
                user: user_id,
                status: 'success' 
            })

        }
    })

    fastify.route({
        method: 'DELETE',
        url: '/admin/recipe/:id',
        handler: async function (request, reply) {

            const user_id = request.user.id;
            const id = request.params.id;
            
            // validation
            // const schema = Joi.object({
            //     id: Joi.string()
            //     .min(2)
            //     .max(1024)
            //     .required()
            //     .messages({
            //         'string.empty': '"názov" je povinné pole',
            //         'string.min': '"názov" nemôže byť kratší ako {#limit}',
            //         'any.required': '"názov" je povinné pole'
            //     }),
            //     slug: Joi.string()
            //     .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
            //     .required()
            //     .messages({
            //         'string.empty': '"slug" je povinné pole',
            //         'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
            //     }),
            //     system: Joi.boolean()
            //     .required(),
            //     active: Joi.boolean()
            //     .required(),
            // });

            

            let validation_error = false;
            // try {
            //     const validation = await schema.validateAsync({ name: name, slug: slug, system: system, active: active }, {abortEarly: false});
            //     console.log('validation', validation);
            // }
            // catch (err) {
            //     console.log('validation error', err);
            //     validation_error = err;
            // }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                // delete in DB
                const deleted = await fastify.db.models.recipe.destroy({
                    where: {
                      id: id
                    }
                });

                const client = solr.createClient({
                    path: '/solr/odfarmara_recipes'
                });

                // delete from solr
                const obj = await client.deleteByID(id.toString());
                const commit = await client.commit();

                reply.send({    
                    user: user_id,
                    status: 'success' 
                })

                // todo delete all tags from products, farmers, recipes, blog articles
                

            }

        }
    })
    
  }
  
  module.exports = routes
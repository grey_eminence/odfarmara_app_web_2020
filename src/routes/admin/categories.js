const solr = require('solr-client')
const fs = require('fs')
const util = require('util')
const path = require('path')
const { nanoid } = require('nanoid')
const { pipeline } = require('stream')
const pump = util.promisify(pipeline)
const process = require('process')
const moment = require('moment')
const Joi = require('joi')
const { v4: uuidv4 } = require('uuid')
const { Op } = require("sequelize")


  async function routes (fastify, options) {

    fastify.get('/admin/category/:id', async (request, reply) => {

        const user_id = request.user.id;
        const id = request.params.id

        const category = await fastify.db.models.category.findOne({
            include: [ { 
                model: fastify.db.models.category, 
                as: "parentCategory",
            } ],
            where: {
                id: id
            },
        })

        const mainCategories = await fastify.db.models.category.findAll({
            where: {
                parent_id: {[Op.is]: null},
            },
        })

        reply.send({
            user: user_id, 
            category: category,
            mainCategories: mainCategories
        })
    
    })

    fastify.post('/admin/categories', async (request, reply) => {

        // const user_id = request.user.id;
        // const id = request.params.id

        console.log('/admin/categories');

        let items = await fastify.db.models.category.findAll({
        });
        let itemsProduct = await fastify.db.models.category.findAll({
            where: {
                type: 'product',
            }
        });
        let itemsFarmer = await fastify.db.models.category.findAll({
            where: {
                type: 'farmer',
            }
        });
        let itemsBlog = await fastify.db.models.category.findAll({
            where: {
                type: 'blog',
            }
        });
        let itemsRecipe = await fastify.db.models.category.findAll({
            where: {
                type: 'recipe',
            }
        });

        reply.send({
            items: items,
            itemsProduct: itemsProduct,
            itemsFarmer: itemsFarmer,
            itemsBlog: itemsBlog,
            itemsRecipe: itemsRecipe,
        });
        
    })

    fastify.post('/admin/categories/product', async (request, reply) => {

        // const user_id = request.user.id;
        // const id = request.params.id

        console.log('/admin/categories/product');

        let items = await fastify.db.models.category.findAll({
            where: {
                type: 'product',
            }
        })

        reply.send({
            items: items
        })
        
    })

    fastify.post('/admin/categories/farmer', async (request, reply) => {

        // const user_id = request.user.id;
        // const id = request.params.id

        console.log('/admin/categories/farmer');

        let items = await fastify.db.models.category.findAll({
            where: {
                type: 'farmer',
            }
        })

        reply.send({
            items: items
        })
        
    })

    fastify.post('/admin/categories/recipe', async (request, reply) => {

        // const user_id = request.user.id;
        // const id = request.params.id

        console.log('/admin/categories/recipe');

        let items = await fastify.db.models.category.findAll({
            where: {
                type: 'recipe',
            }
        })

        reply.send({
            items: items
        })
        
    })

    fastify.post('/admin/categories/blog', async (request, reply) => {

        // const user_id = request.user.id;
        // const id = request.params.id

        console.log('/admin/categories/blog');

        let items = await fastify.db.models.category.findAll({
            where: {
                type: 'blog',
            }
        })

        reply.send({
            items: items
        })
        
    })

    fastify.route({
        method: 'POST',
        url: '/admin/category/create',
        handler: async function (request, reply) {

            const user = request.user
            const user_id = user.id

            const name = request.body.name
            const slug = request.body.slug
            const active = request.body.active
            const parent_id = request.body.parent_id ? parseInt(request.body.parent_id): 0;
            const mainCategory = request.body.mainCategory
            const type = request.body.type

            // validation
            const schema = Joi.object({
                name: Joi.string()
                .min(2)
                .max(1024)
                .required()
                .messages({
                    'string.empty': '"názov" je povinné pole',
                    'string.min': '"názov" nemôže byť kratší ako {#limit}',
                    'any.required': '"názov" je povinné pole'
                }),
                slug: Joi.string()
                .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
                .required()
                .messages({
                    'string.empty': '"slug" je povinné pole',
                    'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
                }),
                mainCategory: Joi.boolean(),
                parent_id: Joi.number(),
                type: Joi.string().required().valid('product', 'farmer', 'blog', 'recipe').messages({
                    'string.empty': '"typ" je povinné pole',
                    'any.required': '"typ" je povinné pole'
                }),
                active: Joi.boolean()
                .required(),
            }).when(Joi.object({ type: Joi.valid('product'), mainCategory: Joi.valid(false) }).unknown(), {
                then: Joi.object({ parent_id: Joi.number().greater(0).required().messages({
                    'number.greater': '"nadradená kategória" je povinné pole',
                }) }),
            });

            let validation_error = false;
            try {
                const validation = await schema.validateAsync({ 
                    name: name,
                    slug: slug,
                    active: active,
                    type: type,
                    parent_id: parent_id,
                    mainCategory: mainCategory,
                }, {abortEarly: false});
                console.log('validation', validation);
            }
            catch (err) {
                console.log('validation error', err);
                validation_error = err;
            }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                let data = {
                    name: name,
                    slug: slug,
                    active: active,
                    type: type,
                    uuid: uuidv4()
                }

                if(!mainCategory && parent_id){
                    data.parent_id = parent_id
                }

                console.log('data', data);

                // create in DB
                const new_category = await fastify.db.models.category.create(data);

                console.log('new_category', new_category);

                // const client = solr.createClient({
                //     path: '/solr/odfarmara_categories'
                // });

                // let doc = {
                //     id : new_tag.id,
                //     ge_name : new_tag.name,
                //     ge_slug : new_tag.slug,
                //     ge_active : new_tag.active,
                //     ge_description : new_tag.description,
                //     ge_created_date : created_date,
                //     ge_user_name: user_name
                // }

                // // add and commit to solr
                // const obj = await client.add(doc);
                // const commit = await client.commit();

                reply.send({    
                    user: user_id,
                    status: 'success' 
                })
            }

        }
    })

    fastify.route({
        method: 'POST',
        url: '/admin/category/:id',
        handler: async function (request, reply) {

            const user = request.user
            const user_id = user.id
            const id = parseInt(request.params.id)

            const name = request.body.name
            const slug = request.body.slug
            const active = request.body.active
            const parent_id = request.body.parent_id ? parseInt(request.body.parent_id): 0;
            const mainCategory = request.body.mainCategory
            const type = request.body.type

            // validation
            const schema = Joi.object({
                name: Joi.string()
                .min(2)
                .max(1024)
                .required()
                .messages({
                    'string.empty': '"názov" je povinné pole',
                    'string.min': '"názov" nemôže byť kratší ako {#limit}',
                    'any.required': '"názov" je povinné pole'
                }),
                slug: Joi.string()
                .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
                .required()
                .messages({
                    'string.empty': '"slug" je povinné pole',
                    'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
                }),
                mainCategory: Joi.boolean(),
                parent_id: Joi.number(),
                type: Joi.string().required().valid('product', 'farmer', 'blog', 'recipe').messages({
                    'string.empty': '"typ" je povinné pole',
                    'any.required': '"typ" je povinné pole'
                }),
                active: Joi.boolean()
                .required(),
            }).when(Joi.object({ type: Joi.valid('product'), mainCategory: Joi.valid(false) }).unknown(), {
                then: Joi.object({ parent_id: Joi.number().greater(0).required().messages({
                    'number.greater': '"nadradená kategória" je povinné pole',
                }) })
                
            });

            let validation_error = false;
            try {
                const validation = await schema.validateAsync({ 
                    name: name,
                    slug: slug,
                    active: active,
                    type: type,
                    parent_id: parent_id,
                    mainCategory: mainCategory,
                }, {abortEarly: false});
                console.log('validation', validation);
            }
            catch (err) {
                console.log('validation error', err);
                validation_error = err;
            }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                let data = {
                    name: name,
                    slug: slug,
                    active: active,
                    // type: type,
                    uuid: uuidv4()
                }

                if(!mainCategory && parent_id){
                    data.parent_id = parent_id
                }

                console.log('data', data);

                // update in DB
                await fastify.db.models.category.update(data, {
                    where: {
                        id: id
                    }
                })

                // const client = solr.createClient({
                //     path: '/solr/odfarmara_categories'
                // });

                // let doc = {
                //     id : new_tag.id,
                //     ge_name : new_tag.name,
                //     ge_slug : new_tag.slug,
                //     ge_active : new_tag.active,
                //     ge_description : new_tag.description,
                //     ge_created_date : created_date,
                //     ge_user_name: user_name
                // }

                // // add and commit to solr
                // const obj = await client.add(doc);
                // const commit = await client.commit();

                reply.send({    
                    user: user_id,
                    status: 'success' 
                })
            }

        }
    })

    fastify.route({
        method: 'DELETE',
        url: '/admin/category/:id',
        handler: async function (request, reply) {

            const user_id = request.user.id;
            const id = request.params.id;
            
            // validation
            // const schema = Joi.object({
            //     id: Joi.string()
            //     .min(2)
            //     .max(1024)
            //     .required()
            //     .messages({
            //         'string.empty': '"názov" je povinné pole',
            //         'string.min': '"názov" nemôže byť kratší ako {#limit}',
            //         'any.required': '"názov" je povinné pole'
            //     }),
            //     slug: Joi.string()
            //     .pattern(new RegExp('^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$'))
            //     .required()
            //     .messages({
            //         'string.empty': '"slug" je povinné pole',
            //         'string.pattern.base': '"slug" môže obsahovať len alfanumerické znaky a znak "-"'
            //     }),
            //     system: Joi.boolean()
            //     .required(),
            //     active: Joi.boolean()
            //     .required(),
            // });

            

            let validation_error = false;
            // try {
            //     const validation = await schema.validateAsync({ name: name, slug: slug, system: system, active: active }, {abortEarly: false});
            //     console.log('validation', validation);
            // }
            // catch (err) {
            //     console.log('validation error', err);
            //     validation_error = err;
            // }

            if(validation_error){

                let validation_error_sanitized = {};
                for(var key in validation_error.details){
                    let message = validation_error.details[key].message;
                    let path = validation_error.details[key].path[0];

                    validation_error_sanitized[path] = message;
                }

                reply.send({    
                    validation_error: validation_error_sanitized,
                })
            }
            else{

                // delete in DB
                const deleted = await fastify.db.models.category.destroy({
                    where: {
                      id: id
                    }
                });

                reply.send({    
                    user: user_id,
                    status: 'success' 
                })

                // todo delete all tags from products, farmers, recipes, blog articles
                

            }

        }
    })


  }
  
  module.exports = routes
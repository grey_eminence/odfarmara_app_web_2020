const { Op } = require("sequelize")
const solr = require('solr-client')
var URI = require('urijs')

async function routes (fastify, options) {

  fastify.get('/cms/blog', async (request, reply) => {
    reply.redirect('/blog');
  });

  fastify.get('/cms/allblogs', async (request, reply) => {
    reply.redirect('/blog');
  });

  fastify.get('/blog', async (request, reply) => {

    const categories = await fastify.db.models.category.findAll({
      where: { parent_id: {[Op.is]: null}, type: "blog" },
    })

    const latestArticles = await fastify.db.models.article.findAll({
      include: [ 
        { model: fastify.db.models.category, as: "category" },
      ],
      limit: 3,
      order: [
        ['created', 'DESC']
      ],
      where: { 
        active: '1',
      },
    });

    const popularArticles = await fastify.db.models.article.findAll({
      include: [ 
        { model: fastify.db.models.category, as: "category" },
      ],
      limit: 3,
      order: [
        ['counter_view', 'DESC']
      ],
      where: { 
        active: '1',
      },
    });

    let docs = [];
    let query_text = "*:*";
    let ipp = 9;
    let startItem = 0;

    let uri = new URI(request.url);
    let urlQuery = uri.search(true);
    let c = urlQuery.c;
    let mc = urlQuery.mc;
    let e = urlQuery.e;
    let q = urlQuery.q;
    let p = urlQuery.p;
    let r = urlQuery.r;
    let pages = false;

    // selected categories
    let baseUrl = fastify.config.ROOT + "/blog";

    let sort = {ge_created_date : 'desc'};

    let searching = false;
    if(q || mc){
      searching = true;

      // default page
      if(!p){
        p = 1;
      }
      startItem = (ipp * (p-1));

      if(q){
        query_text = 'ge_name:"' + q + '", ' + 'ge_description:"' + q + '"';
        sort = {};
      }

      //Create a client
      const client = solr.createClient({
        path: '/solr/odfarmara_blog'
      });

      const query = client
      .query()
      .q(query_text)
      .start(startItem)
      .sort(sort)
      .rows(ipp);

      if(mc){
        query.matchFilter('ge_category', mc );
      }

      query.matchFilter('ge_active', true);

      const data = await client.search(query);
      docs = data.response.docs;

      pages = Math.floor(data.response.numFound / ipp);
      let pages_modulo = data.response.numFound % ipp;
      if(pages_modulo){
        pages++;
      }
    }


    const articles = await fastify.db.models.article.findAll({
      where: { 
        active: '1',
      },
    });

    let tags = [];
    if(articles && articles.length){
      for(let a = 0; a < articles.length; a++){
        let item = articles[a];
        let item_tags = item.tags;

        if(item_tags && item_tags.length){
          for(let b = 0; b < item_tags.length; b++){
            let tag_obj = {slug: item_tags[b].ge_slug, name: item_tags[b].ge_name};

            if (tags.filter(e => e.slug === item_tags[b].ge_slug).length === 0) {
              tags.push(tag_obj);
            }

          }
        }
      }
    }

    console.log('tags', tags);
    
    reply.view('/pages/blog', { 
      title: 'Blog - Odfarmara.sk',
      latestArticles: latestArticles,
      popularArticles: popularArticles,
      categories: categories,
      baseUrl: baseUrl,
      current_url: request.url,
      searching: searching,
      q: q,
      p: parseInt(p),
      c: c,
      mc: mc,
      e: e,
      r: r,
      docs: docs,
      tags: tags,
      pages: parseInt(pages),
    })

  })

  }
  
  module.exports = routes
// Require the framework and instantiate it
const fastifyEnv = require('fastify-env')
const app = require('fastify')({
  logger: false
})

const fileUpload = require('fastify-file-upload')
app.register(fileUpload,{
	limits: { fileSize: 50 * 1024 * 1024 },
	preserveExtension: true,
	useTempFiles: true,
	createParentPath: true
})


const schema = {
  type: 'object',
  required: [ 'PORT' ],
  properties: {
    PORT: {
      type: 'string',
      default: 3000
    },
    ROOT: {
      type: 'string',
      default: '/'
    },
    ASSETS: {
      type: 'string',
      default: '/public'
    }
  }
}

const options = {
  confKey: 'config', // optional, default: 'config'
  schema: schema,
  dotenv: true
}

app.register(fastifyEnv, options)
  .ready((err) => {
    if (err) console.error(err)

    console.log(app.config); // or fastify[options.confKey] 

    try {
      app.listen(app.config.PORT) 
    } catch (err) {
      app.log.error(err)
      process.exit(1) 
    }

  })
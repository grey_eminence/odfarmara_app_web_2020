const defaultTheme = require('tailwindcss/defaultTheme');
const colors = require('tailwindcss/colors')

module.exports = {
  // prefix: 'tw-',
  content: ['./src/views/**/*.njk'],
  theme: {
    extend: {
      fontFamily: {
          sans: ['Poppins', ...defaultTheme.fontFamily.sans],
      },
      height: {
          product_image: '570px',
      },
    },
    fontSize: {
      'xxs': '.65rem',
      'xs': '.75rem',
      'sm': '.875rem',
      'tiny': '.875rem',
      'base': '1rem',
      'lg': '1.125rem',
      'xl': '1.25rem',
      '2xl': '1.5rem',
      '3xl': '1.875rem',
      '4xl': '2.25rem',
      '5xl': '3rem',
      '6xl': '4rem',
      '7xl': '5rem',
    },
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      white: colors.white,
      gray: colors.neutral,
      indigo: colors.indigo,
      red: colors.rose,
      yellow: colors.amber,
      green: colors.green,
      'red-of-bg': '#FDF3F3',
      'red-of-bg-2': '#f6caca',
      'red-darken-of-bg': '#F8DDDD',
      'green-of-bg': '#F3F9EB',
      'green-of-button-2': '#8bc53f',
      'greygreen-of-bg': '#E5EBEB',
      'greygreen-light-of-bg': '#f2f7f4',
      'greygreen-of': '#9bacac',
      'green-of-app-bg': '#DDEDC3',
      'green-of-button': '#375531',
      'red-of': '#E54F4E',
      'gray-of': '#f2f6fa',
      'gray-dark-of': '#e4e7eb',
      'yellow-of': "#db9500"
    },
    backgroundSize: {
      'auto': 'auto',
      'cover': 'cover',
      'contain': 'contain',
      '50%': '50%',
      '70%': '70%',
    }
  },
  variants: {
    extend: {},
  },
  safelist: [
    'w-20',
    'h-20',
  ],
  plugins: [
    require("daisyui"),
    require('@tailwindcss/forms')
  ],
}

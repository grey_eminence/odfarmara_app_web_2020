const fastifyEnv = require('fastify-env')
const app = require('fastify')({
  logger: true
})
const path = require('path')
const fs = require('fs')

const { Op, QueryTypes, Sequelize } = require("sequelize");
const sharp = require("sharp");

// db 
const fsequelize = require('./src/plugins/cockroachdb')
const sequelizeConfig = {
  instance: 'db', // name of instance will be mapped to fastify
  // autoConnect: true, // auto authentication and test connection on first run
  dialect: 'postgres',
  host: 'localhost',
  port: '26257',
  username: 'root',
  password: '',
  database: 'odfarmara_imported',
  timezone: 'Europe/Bratislava',
  dialectOptions: {
    timezone: "local",
    // ssl: {
    //   rejectUnauthorized: false,
    //   // For secure connection:
    //   /*ca: fs.readFileSync('certs/ca.crt').toString()*/
    // },
  },
  // timout for long running queries
  pool: {
    max: 10,
    min: 0,
    idle: 100000
  },
  logging: false,
}
app.register(fsequelize, sequelizeConfig).ready()

app.register(require('./src/models/farmer_rating'))
app.register(require('./src/models/user'))

async function start() {
    let users = await app.db.models.user.findAll({
      where: { 
        privilegs: 'farmer',
      },
    });

    for(x in users){

        let user_id = users[x].id;

        const result = await app.db.query("SELECT AVG(rating) as rating FROM farmer_rating WHERE farmer_rating.farmer_id = " + user_id, { type: QueryTypes.SELECT });
        const rating = result[0].rating;

        let value = 0;
        if(rating){
            value = rating;
            let afterDecimal = parseFloat(value) % 1;
            let decimal = value - afterDecimal;
            afterDecimal = Math.round(afterDecimal * 2) / 2;
            value = decimal + afterDecimal;
        }

        let user = await app.db.models.user.update({ rating_avg: value }, {
            where: {
              id: user_id
            }
        });

    }
}

const schema = {
    type: 'object',
    required: [ 'PORT' ],
    properties: {
        PORT: {
            type: 'string',
            default: 3000
        },
        ROOT: {
            type: 'string',
            default: '/'
        },
        ASSETS: {
            type: 'string',
            default: '/public'
        }
    }
}


const options = {
    confKey: 'config',
    schema: schema,
    dotenv: true
}

app.register(fastifyEnv, options)
.ready((err) => {
    start();
})



const fastifyEnv = require('fastify-env')
const app = require('fastify')({
  logger: true
})
const path = require('path')
const fs = require('fs')

const { Op } = require("sequelize");
const sharp = require("sharp");

// db 
const fsequelize = require('./src/plugins/cockroachdb')
const sequelizeConfig = {
  instance: 'db', // name of instance will be mapped to fastify
  // autoConnect: true, // auto authentication and test connection on first run
  dialect: 'postgres',
  host: 'localhost',
  port: '26257',
  username: 'root',
  password: '',
  database: 'odfarmara_imported',
  timezone: 'Europe/Bratislava',
  dialectOptions: {
    timezone: "local",
    // ssl: {
    //   rejectUnauthorized: false,
    //   // For secure connection:
    //   /*ca: fs.readFileSync('certs/ca.crt').toString()*/
    // },
  },
  // timout for long running queries
  pool: {
    max: 10,
    min: 0,
    idle: 100000
  },
  logging: false,
}
app.register(fsequelize, sequelizeConfig).ready()

app.register(require('./src/models/article'))

async function start() {
    let articles = await app.db.models.article.findAll();

    for(x in articles){

        let filename = articles[x].blog_img.replace(/^.*[\\\/]/, '');
        let article_id = articles[x].article_id;
        console.log(filename);

        let article = await app.db.models.article.update({ filename: filename }, {
            where: {
              article_id: article_id
            }
        });

    }
}

const schema = {
    type: 'object',
    required: [ 'PORT' ],
    properties: {
        PORT: {
            type: 'string',
            default: 3000
        },
        ROOT: {
            type: 'string',
            default: '/'
        },
        ASSETS: {
            type: 'string',
            default: '/public'
        }
    }
}


const options = {
    confKey: 'config',
    schema: schema,
    dotenv: true
}

app.register(fastifyEnv, options)
.ready((err) => {
    start();
})



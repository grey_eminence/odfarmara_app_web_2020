//setup unpoly
up.fragment.config.runScripts = true;
up.fragment.config.badResponseTime = 300;

const rootUrl = document.getElementById("app-script").getAttribute("data-url");

up.compiler('.swiper-container', function(element) {

    const swiper = new Swiper('.swiper-container', {
        // Optional parameters
        direction: 'horizontal',
        loop: true,

        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
        },

        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },

        // And if we need scrollbar
        scrollbar: {
            el: '.swiper-scrollbar',
        },
    });
});

// up.compiler('#reg-form', function(element) {

//     const buyerBtn = document.querySelector('.buyer-btn');
//     const sellerBtn = document.querySelector('.seller-btn');
//     const nameField = document.querySelector('.name-field');
//     const registrationType = document.getElementById('registrationType');

//     buyerBtn.addEventListener("click", function(event){
//         sellerBtn.classList.remove('text-red-of');
//         buyerBtn.classList.add('text-red-of');
//         nameField.classList.add('hidden');
//         registrationType.value= 'buyer';
//     });

//     sellerBtn.addEventListener("click", function(event){
//         sellerBtn.classList.add('text-red-of');
//         buyerBtn.classList.remove('text-red-of');
//         nameField.classList.remove('hidden');
//         registrationType.value= 'seller';
//     });

// });

up.compiler('.anim-head-shake', function(element) {

    element.addEventListener("mouseenter", function(event){
        const arrow = element.querySelector('.arrow');
        arrow.classList.add("animate__headShake", "animate__animated", "animate__slow");
    });
   
});

up.compiler('.arrow', function(element) {

    element.addEventListener("webkitAnimationEnd", function(event){
        element.classList.remove("animate__headShake", "animate__animated", "animate__slow");
    });
    element.addEventListener("mozAnimationEnd", function(event){
        element.classList.remove("animate__headShake", "animate__animated", "animate__slow");
    });
    element.addEventListener("oAnimationEnd", function(event){
        element.classList.remove("animate__headShake", "animate__animated", "animate__slow");
    });
    element.addEventListener("animationEnd", function(event){
        element.classList.remove("animate__headShake", "animate__animated", "animate__slow");
    });

});

up.compiler('.show-full-category', function(element) {

    element.addEventListener("mouseenter", function(event){
        const arrow = element.querySelector('.arrow-white');
        arrow.classList.add("animate__headShake", "animate__animated", "animate__slow");
    });
   
});

up.compiler('.arrow-white', function(element) {

    element.addEventListener("webkitAnimationEnd", function(event){
        element.classList.remove("animate__headShake", "animate__animated", "animate__slow");
    });
    element.addEventListener("mozAnimationEnd", function(event){
        element.classList.remove("animate__headShake", "animate__animated", "animate__slow");
    });
    element.addEventListener("oAnimationEnd", function(event){
        element.classList.remove("animate__headShake", "animate__animated", "animate__slow");
    });
    element.addEventListener("animationEnd", function(event){
        element.classList.remove("animate__headShake", "animate__animated", "animate__slow");
    });

});

up.compiler('.marker-anim', function(element) {    
    element.addEventListener("mouseenter", function(event){
        const marker = element.querySelector('.marker');
        marker.classList.add("animate__bounce", "animate__animated");
    });
});

up.compiler('.marker', function(element) {

    element.addEventListener("webkitAnimationEnd", function(event){
        element.classList.remove("animate__bounce", "animate__animated",);
    });
    element.addEventListener("mozAnimationEnd", function(event){
        element.classList.remove("animate__bounce", "animate__animated",);
    });
    element.addEventListener("oAnimationEnd", function(event){
        element.classList.remove("animate__bounce", "animate__animated",);
    });
    element.addEventListener("animationEnd", function(event){
        element.classList.remove("animate__bounce", "animate__animated",);
    });

});

up.compiler('#map', function(element) {

    console.log("map render");

    if(element){
        mapboxgl.accessToken = 'pk.eyJ1IjoibWFyaWFubWljaGFsb3ZpYyIsImEiOiJja3RhbmdxaHgxbndjMnZuOXBhbWU1a2dmIn0.ldsc33qUVIrN5cWHxFqkXA';
        var map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/marianmichalovic/cktc6fzi402sx17p9rh4kylxq',
            center: [19.468, 48.731],
            zoom: 6.5
        });

        map.on('load', () => {
            if (typeof farms !== 'undefined') {
                for (let i = 0; i < farms.length; i++) {
                    
                    let name = farms[i].name;
                    let gps = farms[i].gps.split(',');
                    let url = farms[i].url;
                    let img = farms[i].img;
                    let img_src = farms[i].img_src;
                    const el = document.createElement('div');
                    el.className = 'marker';
                    el.style.backgroundImage = 'url(/assets/img/map_marker_icon.svg)';
                    el.style.width = '70px';
                    el.style.height = '70px';
                    el.style.backgroundSize = '100%';

                    let html = "";
                    if(img){
                        html += '<div class="map-popup-img" style="background-image: url(' + img_src + ');"></div>';
                    }
                    html += '<div class="title">' + name + '</div><a href="' + url + '" class="popup-button">Zobraziť farmu</a>';
                    // create the popup
                    const popup = new mapboxgl.Popup().setHTML(html);
                
                    new mapboxgl.Marker(el).setLngLat([gps[1], gps[0]]).setPopup(popup).addTo(map);
                    
                }
            }
        });

    }

});

up.compiler('.cart-qty-up', function(element) {

    element.addEventListener("click", function(event){

        let qtyElModal = document.getElementById('cart-qty-value-modal');
        let qtyEl = document.getElementById('cart-qty-value');
        let qtyValue = qtyEl.value;
        qtyValue = parseInt(qtyValue);
        let unit = parseInt(qtyEl.getAttribute('data-unit'));
        console.log('up');
        qtyValue++;
        qtyElModal.value = qtyValue;
        qtyEl.value = qtyValue;

        let priceValue = qtyEl.dataset.itemPrice;
        let newPriceValue = parseFloat(priceValue) * (qtyValue / unit);

        let priceOverallElModal = document.getElementById('cart-price-overall-modal');
        let priceOverallEl = document.getElementById('cart-price-overall');
        let newPriceValueFormated = new Intl.NumberFormat('sk-SK', { style: 'currency', currency: 'EUR' }).format(parseFloat(newPriceValue));
        priceOverallElModal.innerHTML = newPriceValueFormated;
        priceOverallEl.innerHTML = newPriceValueFormated;

    });

});

up.compiler('.cart-qty-up-modal', function(element) {

    element.addEventListener("click", function(event){

        let qtyElModal = document.getElementById('cart-qty-value-modal');
        let qtyEl = document.getElementById('cart-qty-value');
        let qtyValue = qtyElModal.value;
        qtyValue = parseInt(qtyValue);
        console.log('up');
        let unit = parseInt(qtyEl.getAttribute('data-unit'));
        qtyValue++;
        qtyElModal.value = qtyValue;
        qtyEl.value = qtyValue;

        let priceValue = qtyElModal.dataset.itemPrice;
        let newPriceValue = parseFloat(priceValue) * (qtyValue / unit);

        let priceOverallElModal = document.getElementById('cart-price-overall-modal');
        let priceOverallEl = document.getElementById('cart-price-overall');
        let newPriceValueFormated = new Intl.NumberFormat('sk-SK', { style: 'currency', currency: 'EUR' }).format(parseFloat(newPriceValue));
        priceOverallElModal.innerHTML = newPriceValueFormated;
        priceOverallEl.innerHTML = newPriceValueFormated;

    });

});

up.compiler('.cart-qty-down', function(element) {

    element.addEventListener("click", function(event){

        let qtyElModal = document.getElementById('cart-qty-value-modal');
        let qtyEl = document.getElementById('cart-qty-value');

        let qtyValue = qtyEl.value;
        qtyValue = parseInt(qtyValue);
        let minQuantity = parseInt(qtyEl.getAttribute('data-min-quantity'));
        let unit = parseInt(qtyEl.getAttribute('data-unit'));
        console.log('down');
        if(qtyValue > 1 && qtyValue > minQuantity){
            qtyValue--;
        }
        qtyElModal.value = qtyValue;
        qtyEl.value = qtyValue;

        let priceValue = qtyEl.dataset.itemPrice;
        let newPriceValue = parseFloat(priceValue) * (qtyValue / unit);

        let priceOverallElModal = document.getElementById('cart-price-overall-modal');
        let priceOverallEl = document.getElementById('cart-price-overall');
        let newPriceValueFormated = new Intl.NumberFormat('sk-SK', { style: 'currency', currency: 'EUR' }).format(parseFloat(newPriceValue));
        priceOverallElModal.innerHTML = newPriceValueFormated;
        priceOverallEl.innerHTML = newPriceValueFormated;

    });

});

up.compiler('.cart-qty-down-modal', function(element) {

    element.addEventListener("click", function(event){

        let qtyElModal = document.getElementById('cart-qty-value-modal');
        let qtyEl = document.getElementById('cart-qty-value');
        let qtyValue = qtyElModal.value;
        qtyValue = parseInt(qtyValue);
        let minQuantity = parseInt(qtyEl.getAttribute('data-min-quantity'));
        console.log('down');
        let unit = parseInt(qtyEl.getAttribute('data-unit'));
        if(qtyValue > 1 && qtyValue > minQuantity){
            qtyValue--;
        }
        qtyElModal.value = qtyValue;
        qtyEl.value = qtyValue;

        let priceValue = qtyElModal.dataset.itemPrice;
        let newPriceValue = parseFloat(priceValue) * (qtyValue / unit);

        let priceOverallElModal = document.getElementById('cart-price-overall-modal');
        let priceOverallEl = document.getElementById('cart-price-overall');
        let newPriceValueFormated = new Intl.NumberFormat('sk-SK', { style: 'currency', currency: 'EUR' }).format(parseFloat(newPriceValue));
        priceOverallElModal.innerHTML = newPriceValueFormated;
        priceOverallEl.innerHTML = newPriceValueFormated;

    });

});

up.compiler('#product-tab', function(element) {

    let btnMoreInfoEl = document.getElementById('tab-btn-more-info');
    let moreInfoEl = document.getElementById('tab-more-info');
    let btnNutritionEl = document.getElementById('tab-btn-nutrition');
    let nutritionEl = document.getElementById('tab-nutrition');

    let btnMoreInfoSvgEl = document.querySelector('#tab-btn-more-info svg');
    let btnNutritionSvgEl = document.querySelector('#tab-btn-nutrition svg');

    if (btnMoreInfoEl) {
        btnMoreInfoEl.addEventListener("click", function(event){
            moreInfoEl.classList.remove("hidden");
            btnMoreInfoEl.classList.add("text-red-of", "border-b-2");
            nutritionEl.classList.add("hidden");
            btnNutritionEl.classList.remove("text-red-of", "border-b-2");

            btnMoreInfoSvgEl.classList.add("text-red-of");
            btnNutritionSvgEl.classList.remove("text-red-of");
        })
    }

    if (btnNutritionEl) {
        btnNutritionEl.addEventListener("click", function(event){
            moreInfoEl.classList.add("hidden");
            btnMoreInfoEl.classList.remove("text-red-of", "border-b-2");
            nutritionEl.classList.remove("hidden");
            btnNutritionEl.classList.add("text-red-of", "border-b-2");

            btnMoreInfoSvgEl.classList.remove("text-red-of");
            btnNutritionSvgEl.classList.add("text-red-of");
        })
    }

});

up.compiler('#farm-tab', function(element) {

    let btnProfileEl = document.getElementById('tab-btn-profile');
    let btnAddressEl = document.getElementById('tab-btn-address');
    let btnOpenEl = document.getElementById('tab-btn-open');
    let btnMessageEl = document.getElementById('tab-btn-message');

    let btnProfileSvgEl = document.querySelector('#tab-btn-profile svg');
    let btnAddressSvgEl = document.querySelector('#tab-btn-address svg');
    let btnOpenSvgEl = document.querySelector('#tab-btn-open svg');
    let btnMessageSvgEl = document.querySelector('#tab-btn-message svg');

    let tabProfileEl = document.getElementById('tab-profile');
    let tabAddressEl = document.getElementById('tab-address');
    let tabOpenEl = document.getElementById('tab-open');
    let tabMessageEl = document.getElementById('tab-message');

    btnProfileEl.addEventListener("click", function(event){
        
        tabProfileEl.classList.add("hidden");
        if(tabAddressEl){
            tabAddressEl.classList.add("hidden");
        }
        tabOpenEl.classList.add("hidden");
        if(tabMessageEl){
            tabMessageEl.classList.add("hidden");
        }

        btnProfileEl.classList.remove("text-red-of", "border-b-2");
        if(btnAddressEl){
            btnAddressEl.classList.remove("text-red-of", "border-b-2");
        }
        btnOpenEl.classList.remove("text-red-of", "border-b-2");
        if(btnMessageEl){
            btnMessageEl.classList.remove("text-red-of", "border-b-2");
        }

        btnProfileSvgEl.classList.remove("text-red-of");
        if(btnAddressSvgEl){
            btnAddressSvgEl.classList.remove("text-red-of");
        }
        btnOpenSvgEl.classList.remove("text-red-of");
        if(btnMessageEl){
            btnMessageSvgEl.classList.remove("text-red-of");
        }

        tabProfileEl.classList.remove("hidden");
        btnProfileEl.classList.add("text-red-of", "border-b-2");
        btnProfileSvgEl.classList.add("text-red-of");

    })

    if(btnAddressEl){
        btnAddressEl.addEventListener("click", function(event){
            
            tabProfileEl.classList.add("hidden");
            if(tabAddressEl){
                tabAddressEl.classList.add("hidden");
            }
            tabOpenEl.classList.add("hidden");
            if(tabMessageEl){
                tabMessageEl.classList.add("hidden");
            }

            btnProfileEl.classList.remove("text-red-of", "border-b-2");
            if(btnAddressEl){
                btnAddressEl.classList.remove("text-red-of", "border-b-2");
            }
            btnOpenEl.classList.remove("text-red-of", "border-b-2");
            if(btnMessageEl){
                btnMessageEl.classList.remove("text-red-of", "border-b-2");
            }

            btnProfileSvgEl.classList.remove("text-red-of");
            btnAddressSvgEl.classList.remove("text-red-of");
            btnOpenSvgEl.classList.remove("text-red-of");
            if(btnMessageEl){
                btnMessageSvgEl.classList.remove("text-red-of");
            }

            tabAddressEl.classList.remove("hidden");
            btnAddressEl.classList.add("text-red-of", "border-b-2");
            btnAddressSvgEl.classList.add("text-red-of");

            const element = document.getElementById("map-farmer");
            if(!element.children.length){

                mapboxgl.accessToken = 'pk.eyJ1IjoibWFyaWFubWljaGFsb3ZpYyIsImEiOiJja3RhbmdxaHgxbndjMnZuOXBhbWU1a2dmIn0.ldsc33qUVIrN5cWHxFqkXA';
                var map = new mapboxgl.Map({
                    container: 'map-farmer',
                    style: 'mapbox://styles/marianmichalovic/cktc6fzi402sx17p9rh4kylxq',
                    center: [19.468, 48.731],
                    zoom: 6
                });

                map.on('load', () => {
                    const el = document.createElement('div');
                    el.className = 'marker';
                    el.style.backgroundImage = 'url(/assets/img/map_marker_icon.svg)';
                    el.style.width = '70px';
                    el.style.height = '70px';
                    el.style.backgroundSize = '100%';
        
                    const lng = element.getAttribute('data-gps-lng');
                    const lat = element.getAttribute('data-gps-lat');
        
                    // console.log('lng', lng);
                    // console.log('lat', lat);
                
                    new mapboxgl.Marker(el).setLngLat([lng, lat]).addTo(map);
                })
                
            }

        })
    }

    btnOpenEl.addEventListener("click", function(event){
        
        tabProfileEl.classList.add("hidden");
        if(tabAddressEl){
            tabAddressEl.classList.add("hidden");
        }
        tabOpenEl.classList.add("hidden");
        if(tabMessageEl){
            tabMessageEl.classList.add("hidden");
        }

        btnProfileEl.classList.remove("text-red-of", "border-b-2");
        if(btnAddressEl){
            btnAddressEl.classList.remove("text-red-of", "border-b-2");
        }
        btnOpenEl.classList.remove("text-red-of", "border-b-2");
        if(btnMessageEl){
            btnMessageEl.classList.remove("text-red-of", "border-b-2");
        }

        btnProfileSvgEl.classList.remove("text-red-of");
        if(btnAddressSvgEl){
            btnAddressSvgEl.classList.remove("text-red-of");
        }
        btnOpenSvgEl.classList.remove("text-red-of");
        if(btnMessageEl){
            btnMessageSvgEl.classList.remove("text-red-of");
        }

        tabOpenEl.classList.remove("hidden");
        btnOpenEl.classList.add("text-red-of", "border-b-2");
        btnOpenSvgEl.classList.add("text-red-of");

    })

    if(btnMessageEl){
        btnMessageEl.addEventListener("click", function(event){
            
            tabProfileEl.classList.add("hidden");
            tabAddressEl.classList.add("hidden");
            tabOpenEl.classList.add("hidden");
            tabMessageEl.classList.add("hidden");

            btnProfileEl.classList.remove("text-red-of", "border-b-2");
            btnAddressEl.classList.remove("text-red-of", "border-b-2");
            btnOpenEl.classList.remove("text-red-of", "border-b-2");
            btnMessageEl.classList.remove("text-red-of", "border-b-2");

            btnProfileSvgEl.classList.remove("text-red-of");
            btnAddressSvgEl.classList.remove("text-red-of");
            btnOpenSvgEl.classList.remove("text-red-of");
            btnMessageSvgEl.classList.remove("text-red-of");

            tabMessageEl.classList.remove("hidden");
            btnMessageEl.classList.add("text-red-of", "border-b-2");
            btnMessageSvgEl.classList.add("text-red-of");

        })
    }

});

up.compiler('.main-category', function(element) {

    element.addEventListener("click", function(event){

        event.preventDefault();

        if(element.parentElement.classList.contains('expanded')){
            element.parentElement.classList.remove("expanded");
        }
        else{
            element.parentElement.classList.add("expanded");
        }

        let expandedArr = [];
        let expandedEls = document.querySelectorAll(".menu-item.expanded");
        // console.log('expandedEls', expandedEls);
        for (let i = 0; i < expandedEls.length; i++) {
            let catId = expandedEls[i].getAttribute('data-id');
            expandedArr.push(catId);
        }

        // console.log('expandedArr', expandedArr);
        
        // update category href
        let els = document.querySelectorAll("a.category");
        for (let i = 0; i < els.length; i++) {

            let sHref = els[i].getAttribute('href');
            let sUri = new URI(sHref);
            let sQuery = sUri.search(true);
            
            if(expandedArr.length){
                sQuery.e = expandedArr;
            }
            else{
                sQuery.e = '';
            }

            sUri.search(sQuery);
            sUri.normalizeSearch();
            els[i].setAttribute('href', sUri);

        }

        // update pagination href
        let elsPagination = document.querySelectorAll(".pagination a");
        for (let x = 0; x < elsPagination.length; x++) {
            
            let pHref = elsPagination[x].getAttribute('href');
            let pUri = new URI(pHref);
            let pQuery = pUri.search(true);
            
            if(expandedArr.length){
                pQuery.e = expandedArr;
            }
            else{
                pQuery.e = '';
            }

            pUri.search(pQuery);
            pUri.normalizeSearch();
            elsPagination[x].setAttribute('href', pUri);
        }

        //update search form
        let elsInputs = document.querySelectorAll(".main-search-form input[name='e']");
        Array.prototype.forEach.call( elsInputs, function( node ) {
            node.parentNode.removeChild(node);
        })
        if(expandedArr.length){
            let form = document.querySelector(".main-search-form");
            for (let z = 0; z < expandedArr.length; z++) {
                let input = document.createElement("input");
                input.type = "hidden";
                input.name = "e";
                input.value = expandedArr[z];
                form.appendChild(input); 
            }
        }
    })

})

up.compiler('.menu-item .submenu a', function(element) {

    element.addEventListener("click", function(event){
        element.classList.add("bg-gray-600");
        element.classList.add("text-white");
        element.classList.add("cursor-not-allowed");
        element.classList.remove("bg-red-of-bg-2");
        element.classList.remove("hover:bg-green-of-button");
    })

})

up.compiler('a.region-item', function(element) {

    element.addEventListener("click", function(event){
        element.classList.add("bg-gray-600");
        element.classList.add("text-white");
        element.classList.add("cursor-not-allowed");
        element.classList.add("selection-none");
        element.classList.add("rounded-md");
        element.classList.remove("bg-red-of-bg-2");
        element.classList.remove("hover:bg-green-of-button");
    })

})

up.compiler('a.remove-category', function(element) {

    element.addEventListener("click", function(event){
        event.preventDefault();
        element.parentNode.parentNode.parentNode.removeChild(element.parentNode.parentNode);
        console.log('remove category');
    })

})

up.compiler('a.remove-region', function(element) {

    element.addEventListener("click", function(event){
        event.preventDefault();
        element.parentNode.parentNode.parentNode.removeChild(element.parentNode.parentNode);
        console.log('remove region');
    })

})

up.compiler('#oh_1_from', function(element) {
    
    const oh_1_from_h = document.querySelector("input[name='oh_1_from_h']");
    const oh_1_from_m = document.querySelector("input[name='oh_1_from_m']");

    const TimePicker = tui.TimePicker;
    const instance = new TimePicker(element, {
        inputType: 'spinbox',
        showMeridiem: false,
        format: 'hh:mm',
        initialHour: parseInt(oh_1_from_h.value),
        initialMinute: parseInt(oh_1_from_m.value),
    });


    instance.on('change', (e) => {
        oh_1_from_h.value = e.hour;
        oh_1_from_m.value = e.minute;
    });

})

up.compiler('#oh_1_to', function(element) {

    const oh_1_to_h = document.querySelector("input[name='oh_1_to_h']");
    const oh_1_to_m = document.querySelector("input[name='oh_1_to_m']");

    const TimePicker = tui.TimePicker;
    const instance = new TimePicker(element, {
        inputType: 'spinbox',
        showMeridiem: false,
        format: 'hh:mm',
        initialHour: parseInt(oh_1_to_h.value),
        initialMinute: parseInt(oh_1_to_m.value),
    });

    instance.on('change', (e) => {
        oh_1_to_h.value = e.hour;
        oh_1_to_m.value = e.minute;
    });

})

up.compiler('#oh_2_from', function(element) {

    const oh_2_from_h = document.querySelector("input[name='oh_2_from_h']");
    const oh_2_from_m = document.querySelector("input[name='oh_2_from_m']");

    const TimePicker = tui.TimePicker;
    const instance = new TimePicker(element, {
        inputType: 'spinbox',
        showMeridiem: false,
        format: 'hh:mm',
        initialHour: parseInt(oh_2_from_h.value),
        initialMinute: parseInt(oh_2_from_m.value),
    });


    instance.on('change', (e) => {
        oh_2_from_h.value = e.hour;
        oh_2_from_m.value = e.minute;
    });

})

up.compiler('#oh_2_to', function(element) {

    const oh_2_to_h = document.querySelector("input[name='oh_2_to_h']");
    const oh_2_to_m = document.querySelector("input[name='oh_2_to_m']");

    const TimePicker = tui.TimePicker;
    const instance = new TimePicker(element, {
        inputType: 'spinbox',
        showMeridiem: false,
        format: 'hh:mm',
        initialHour: parseInt(oh_2_to_h.value),
        initialMinute: parseInt(oh_2_to_m.value),
    });


    instance.on('change', (e) => {
        oh_2_to_h.value = e.hour;
        oh_2_to_m.value = e.minute;
    });

})

up.compiler('#oh_3_from', function(element) {

    const oh_3_from_h = document.querySelector("input[name='oh_3_from_h']");
    const oh_3_from_m = document.querySelector("input[name='oh_3_from_m']");

    const TimePicker = tui.TimePicker;
    const instance = new TimePicker(element, {
        inputType: 'spinbox',
        showMeridiem: false,
        format: 'hh:mm',
        initialHour: parseInt(oh_3_from_h.value),
        initialMinute: parseInt(oh_3_from_m.value),
    });


    instance.on('change', (e) => {
        oh_3_from_h.value = e.hour;
        oh_3_from_m.value = e.minute;
    });

})

up.compiler('#oh_3_to', function(element) {

    const oh_3_to_h = document.querySelector("input[name='oh_3_to_h']");
    const oh_3_to_m = document.querySelector("input[name='oh_3_to_m']");

    const TimePicker = tui.TimePicker;
    const instance = new TimePicker(element, {
        inputType: 'spinbox',
        showMeridiem: false,
        format: 'hh:mm',
        initialHour: parseInt(oh_3_to_h.value),
        initialMinute: parseInt(oh_3_to_m.value),
    });


    instance.on('change', (e) => {
        oh_3_to_h.value = e.hour;
        oh_3_to_m.value = e.minute;
    });

})

up.compiler('#oh_4_from', function(element) {

    const oh_4_from_h = document.querySelector("input[name='oh_4_from_h']");
    const oh_4_from_m = document.querySelector("input[name='oh_4_from_m']");

    const TimePicker = tui.TimePicker;
    const instance = new TimePicker(element, {
        inputType: 'spinbox',
        showMeridiem: false,
        format: 'hh:mm',
        initialHour: parseInt(oh_4_from_h.value),
        initialMinute: parseInt(oh_4_from_m.value),
    });


    instance.on('change', (e) => {
        oh_4_from_h.value = e.hour;
        oh_4_from_m.value = e.minute;
    });

})

up.compiler('#oh_4_to', function(element) {

    const oh_4_to_h = document.querySelector("input[name='oh_4_to_h']");
    const oh_4_to_m = document.querySelector("input[name='oh_4_to_m']");

    const TimePicker = tui.TimePicker;
    const instance = new TimePicker(element, {
        inputType: 'spinbox',
        showMeridiem: false,
        format: 'hh:mm',
        initialHour: parseInt(oh_4_to_h.value),
        initialMinute: parseInt(oh_4_to_m.value),
    });


    instance.on('change', (e) => {
        oh_4_to_h.value = e.hour;
        oh_4_to_m.value = e.minute;
    });

})

up.compiler('#oh_5_from', function(element) {

    const oh_5_from_h = document.querySelector("input[name='oh_5_from_h']");
    const oh_5_from_m = document.querySelector("input[name='oh_5_from_m']");

    const TimePicker = tui.TimePicker;
    const instance = new TimePicker(element, {
        inputType: 'spinbox',
        showMeridiem: false,
        format: 'hh:mm',
        initialHour: parseInt(oh_5_from_h.value),
        initialMinute: parseInt(oh_5_from_m.value),
    });


    instance.on('change', (e) => {
        oh_5_from_h.value = e.hour;
        oh_5_from_m.value = e.minute;
    });

})

up.compiler('#oh_5_to', function(element) {

    const oh_5_to_h = document.querySelector("input[name='oh_5_to_h']");
    const oh_5_to_m = document.querySelector("input[name='oh_5_to_m']");

    const TimePicker = tui.TimePicker;
    const instance = new TimePicker(element, {
        inputType: 'spinbox',
        showMeridiem: false,
        format: 'hh:mm',
        initialHour: parseInt(oh_5_to_h.value),
        initialMinute: parseInt(oh_5_to_m.value),
    });


    instance.on('change', (e) => {
        oh_5_to_h.value = e.hour;
        oh_5_to_m.value = e.minute;
    });

})

up.compiler('#oh_6_from', function(element) {

    const oh_6_from_h = document.querySelector("input[name='oh_6_from_h']");
    const oh_6_from_m = document.querySelector("input[name='oh_6_from_m']");

    const TimePicker = tui.TimePicker;
    const instance = new TimePicker(element, {
        inputType: 'spinbox',
        showMeridiem: false,
        format: 'hh:mm',
        initialHour: parseInt(oh_6_from_h.value),
        initialMinute: parseInt(oh_6_from_m.value),
    });


    instance.on('change', (e) => {
        oh_6_from_h.value = e.hour;
        oh_6_from_m.value = e.minute;
    });

})

up.compiler('#oh_6_to', function(element) {

    const oh_6_to_h = document.querySelector("input[name='oh_6_to_h']");
    const oh_6_to_m = document.querySelector("input[name='oh_6_to_m']");

    const TimePicker = tui.TimePicker;
    const instance = new TimePicker(element, {
        inputType: 'spinbox',
        showMeridiem: false,
        format: 'hh:mm',
        initialHour: parseInt(oh_6_to_h.value),
        initialMinute: parseInt(oh_6_to_m.value),
    });


    instance.on('change', (e) => {
        oh_6_to_h.value = e.hour;
        oh_6_to_m.value = e.minute;
    });

})

up.compiler('#oh_7_from', function(element) {

    const oh_7_from_h = document.querySelector("input[name='oh_7_from_h']");
    const oh_7_from_m = document.querySelector("input[name='oh_7_from_m']");

    const TimePicker = tui.TimePicker;
    const instance = new TimePicker(element, {
        inputType: 'spinbox',
        showMeridiem: false,
        format: 'hh:mm',
        initialHour: parseInt(oh_7_from_h.value),
        initialMinute: parseInt(oh_7_from_m.value),
    });


    instance.on('change', (e) => {
        oh_7_from_h.value = e.hour;
        oh_7_from_m.value = e.minute;
    });

})

up.compiler('#oh_7_to', function(element) {

    const oh_7_to_h = document.querySelector("input[name='oh_7_to_h']");
    const oh_7_to_m = document.querySelector("input[name='oh_7_to_m']");

    const TimePicker = tui.TimePicker;
    const instance = new TimePicker(element, {
        inputType: 'spinbox',
        showMeridiem: false,
        format: 'hh:mm',
        initialHour: parseInt(oh_7_to_h.value),
        initialMinute: parseInt(oh_7_to_m.value),
    });


    instance.on('change', (e) => {
        oh_7_to_h.value = e.hour;
        oh_7_to_m.value = e.minute;
    });

})

let uppy_profile = null;
up.compiler('.uppy-profile', function(element) {

    uppy_profile = new Uppy.Core({
        id: 'uppy-profile',
        autoProceed: true,
        debug: true,
        restrictions: {
            maxFileSize: 5242880,
            maxNumberOfFiles: 1,
            allowedFileTypes: ['.png','.jpg', '.jpeg'],
        },
        locale: {
            strings: {
                exceedsSize: '%{file}, prekročená maximálna povolená veľkosť súboru %{size}',
            }
        }
    })
    // uppy.use(Uppy.DragDrop, { target: '.uppy' })
    uppy_profile.use(Uppy.FileInput, { 
        target: '.uppy-profile',
        locale: { 
            strings: {
                // The same key is used for the same purpose by @uppy/robodog's `form()` API, but our
                // locale pack scripts can't access it in Robodog. If it is updated here, it should
                // also be updated there!
                chooseFiles: 'Nahrať foto',
            },
        },
    })
    uppy_profile.use(Uppy.XHRUpload, { 
        formData: true,
        fieldName: 'profile_photo',
        endpoint: rootUrl + '/ucet/upload'
    })

    uppy_profile.use(Uppy.ProgressBar, {
        target: '.UppyProgressBar',
        hideAfterFinish: true,
    })

    uppy_profile.on('upload-success', (file, response) => {
        up.render({ target: '#profile-photo, #profile-photo-thumb', url: rootUrl + '/ucet/vlastnosti' });
        uppy_profile.reset();
    })

    uppy_profile.on('upload-error', (file, error, response) => {
        console.log('error with file:', file.id)
        console.log('error message:', error)
        uppy_profile.reset();
    })
  
    uppy_profile.on('error', (file, error, response) => {
        console.log('error with file:', file.id)
        console.log('error message:', error)
        uppy_profile.reset();
    })

    uppy_profile.on('upload-retry', (fileID) => {
        console.log('upload retried:', fileID)
    })

    uppy_profile.on('restriction-failed', (file, error) => {
        // do some customized logic like showing system notice to users
        console.log('restriction failed:', file, error);

        document.getElementById("profile_image_max").innerHTML = error;
        document.getElementById("profile_image_max").classList.remove("hidden");
    })

    uppy_profile.on('file-added', (file) => {
        document.getElementById("profile_image_max").classList.add("hidden");
    })

})

let uppy_intro = null;
up.compiler('.uppy-intro', function(element) {

    uppy_intro = new Uppy.Core({
        id: 'uppy-intro',
        autoProceed: true,
        debug: true,
        restrictions: {
            maxFileSize: 5242880,
            maxNumberOfFiles: 1,
            allowedFileTypes: ['.png','.jpg', '.jpeg'],
        },
        locale: {
            strings: {
                exceedsSize: '%{file}, prekročená maximálna povolená veľkosť súboru %{size}',
            }
        }
    })
    // uppy.use(Uppy.DragDrop, { target: '.uppy' })
    uppy_intro.use(Uppy.FileInput, {
        target: '.uppy-intro',
        locale: { 
            strings: {
                // The same key is used for the same purpose by @uppy/robodog's `form()` API, but our
                // locale pack scripts can't access it in Robodog. If it is updated here, it should
                // also be updated there!
                chooseFiles: 'Nahrať foto',
            },
        },
    })
    uppy_intro.use(Uppy.XHRUpload, { 
        formData: true,
        fieldName: 'intro_photo',
        endpoint: rootUrl + '/ucet/upload'
    })

    uppy_intro.use(Uppy.ProgressBar, {
        target: '.UppyProgressBar',
        hideAfterFinish: true,
    })

    uppy_intro.on('upload-success', (file, response) => {
        up.render({ target: '#intro-photo', url: rootUrl + '/ucet/vlastnosti' });
        uppy_intro.reset();
    })

    uppy_intro.on('upload-error', (file, error, response) => {
        console.log('error with file:', file.id)
        console.log('error message:', error)
        uppy_profile.reset();
    })
  
    uppy_intro.on('error', (file, error, response) => {
        console.log('error with file:', file.id)
        console.log('error message:', error)
        uppy_profile.reset();
    })

    uppy_intro.on('upload-retry', (fileID) => {
        console.log('upload retried:', fileID)
    })

    uppy_intro.on('restriction-failed', (file, error) => {
        // do some customized logic like showing system notice to users
        console.log('restriction failed:', file, error);

        document.getElementById("intro_image_max").innerHTML = error;
        document.getElementById("intro_image_max").classList.remove("hidden");
    })

    uppy_intro.on('file-added', (file) => {
        document.getElementById("intro_image_max").classList.add("hidden");
    })


})

let uppy_create_offer = null;
up.compiler('.uppy-create-offer', function(element) {

    uppy_create_offer = new Uppy.Core({
        id: 'uppy-create-offer',
        autoProceed: true,
        debug: true,
        restrictions: {
            maxFileSize: 5242880,
            //maxNumberOfFiles: 1,
            allowedFileTypes: ['.png','.jpg', '.jpeg'],
        },
        locale: {
            strings: {
                exceedsSize: '%{file}, prekročená maximálna povolená veľkosť súboru %{size}',
            }
        }
    })
    // uppy.use(Uppy.DragDrop, { target: '.uppy' })
    uppy_create_offer.use(Uppy.FileInput, { 
        target: '.uppy-create-offer',
        locale: { 
            strings: {
                // The same key is used for the same purpose by @uppy/robodog's `form()` API, but our
                // locale pack scripts can't access it in Robodog. If it is updated here, it should
                // also be updated there!
                chooseFiles: 'Nahrať foto',
            },
        }, 
    })
    uppy_create_offer.use(Uppy.XHRUpload, { 
        formData: true,
        bundle: true,
        fieldName: 'profile_photo',
        endpoint: rootUrl + '/ucet/upload/multiple'
    })

    // uppy.use(Uppy.ProgressBar, {
    //     target: '.UppyProgressBar',
    //     hideAfterFinish: true,
    // })

    uppy_create_offer.on('upload-success', (file, response) => {
        up.render({ target: '#uploadedPhotos, #addOfferDeleteAllPhotosWrapper', url: rootUrl + '/ucet/pridat-ponuku', params: {
            reload: true
        } });
        uppy_create_offer.reset();
    })

    uppy_create_offer.on('upload-error', (file, error, response) => {
        console.log('error with file:', file.id)
        console.log('error message:', error)
        uppy_create_offer.reset();
    })
  
    uppy_create_offer.on('error', (file, error, response) => {
        console.log('error with file:', file.id)
        console.log('error message:', error)
        uppy_create_offer.reset();
    })

    uppy_create_offer.on('upload-retry', (fileID) => {
        console.log('upload retried:', fileID)
    })

    uppy_create_offer.on('restriction-failed', (file, error) => {
        // do some customized logic like showing system notice to users
        console.log('restriction failed:', file, error);

        document.getElementById("image_max").innerHTML = error;
        document.getElementById("image_max").classList.remove("hidden");
    })

    uppy_create_offer.on('file-added', (file) => {
        document.getElementById("image_max").classList.add("hidden");
    })

})

up.compiler('.select-categories select', function(element) {
    profileChangeCategory(element);
})

function profileChangeCategory(element){

    element.addEventListener("change", function(event){
        
        event.preventDefault();
        
        // remove DOM
        let selectSubcategoryEl = element.parentNode.nextElementSibling;
        element.parentNode.parentNode.removeChild(selectSubcategoryEl);

        let currentValue = element.value;
        let newElement2 = "";

        categories.forEach(function(category, index) {
            if(category.id == currentValue){

                let subcategoriesHtml = "";
                categories[index].subcategories.forEach(function(subcategory) {
                    subcategoriesHtml += '<div class="px-5 py-2.5 bg-white border border-gray-600 rounded-md">';
                        subcategoriesHtml += '<input class="mr-2" type="checkbox" name="subcategory" value="' + subcategory.id + '" />' + subcategory.name;
                    subcategoriesHtml += '</div>';
                });
        
                newElement2 = '<div class="col-span-5 select-subcategory">';
                newElement2 += '<div class="mb-2 text-sm text-gray-600">Podkategória</div>';
                    newElement2 += '<div class="grid grid-cols-2 gap-4 text-sm text-gray-600">';
                        newElement2 += subcategoriesHtml;
                    newElement2 += '</div>';
                newElement2 += '</div>';
            }
        });

        const newDiv2 = htmlToElement(newElement2);
        element.parentNode.parentNode.insertBefore(newDiv2, element.parentNode.nextElementSibling);

        // add option to category
        regenerateCategoriesOptions();

        changeAddCategoryButton();

    }, false);

}

up.compiler('.profile-remove-category', function(element) {
    profileRemoveCategory(element);
})

function regenerateCategoriesOptions(){

    let actualCategories = document.querySelectorAll("select[name='category']");
    let usedValues = [];

    actualCategories.forEach(function(category, index) {
        usedValues.push(category.value);
    });

    actualCategories.forEach(function(category, index) {

        let currentValue = category.value;
        // delete all options
        let categoryOptions = category.querySelectorAll("option");
        categoryOptions.forEach(function(option, index) {
            category.removeChild(option);
        });

        // create new options
        categories.forEach(function(cat, index) {
            let optionHtml = "";
            if(currentValue == cat.id){
                optionHtml = '<option selected value="' + cat.id + '">' + cat.name  + '</option>';
            }
            else{
                if(!usedValues.includes(cat.id)){
                    optionHtml = '<option value="' + cat.id + '">' + cat.name  + '</option>';
                }
            }

            if(optionHtml !== ""){
                const newOption = htmlToElement(optionHtml);
                category.appendChild(newOption);
            }
        });
        
    });

}

function profileRemoveCategory(element){

    element.addEventListener("click", function(event){
        event.preventDefault();
        
        // remove DOM
        let selectCategoryEl = element.parentNode.parentNode.previousElementSibling.previousElementSibling;
        let selectSubcategoryEl = element.parentNode.parentNode.previousElementSibling;
        let removeCategoryEl = element.parentNode.parentNode;
        element.parentNode.parentNode.parentNode.removeChild(selectCategoryEl);
        element.parentNode.parentNode.parentNode.removeChild(selectSubcategoryEl);
        element.parentNode.parentNode.parentNode.removeChild(removeCategoryEl);

        // add option to category
        regenerateCategoriesOptions();
        
        changeAddCategoryButton();
        
    }, false);

}

function changeAddCategoryButton(){
    let currentCategories = document.querySelectorAll("select[name='category']");
    let profileAddCategoryBtn = document.querySelector("#profile-add-category");

    let currentCategoriesLength = currentCategories.length;
    let categoriesLength = categories.length

    if(currentCategoriesLength == categoriesLength){
        profileAddCategoryBtn.classList.add('disabled', 'cursor-not-allowed', 'opacity-30', 'pointer-events-none');
    }
    else{
        profileAddCategoryBtn.classList.remove('disabled', 'cursor-not-allowed', 'opacity-30', 'pointer-events-none');
    }
}

function htmlToElement(html) {
    var template = document.createElement('template');
    html = html.trim(); // Never return a text node of whitespace as the result
    template.innerHTML = html;
    return template.content.firstChild;
}

up.compiler('#profile-add-category', function(element) {

    element.addEventListener("click", function(event){
        event.preventDefault();

        // get all actual categories
        let currentCategories = document.querySelectorAll("select[name='category']");
        let currentSubcategories = document.querySelectorAll("input[name='subcategory']:checked");
        let categoriesWrapper = document.querySelector(".select-categories");

        let selectedCategories = [];
        let selectedSubcategories = [];

        currentCategories.forEach(function(select) {
            selectedCategories.push(select.value);
        });

        currentSubcategories.forEach(function(input) {
            selectedSubcategories.push(input.value);
        });

        let categoriesHtml = "";
        let subcategoriesHtml = "";
        let actualId = false;
        let actualIndex = false;
        categories.forEach(function(category, index) {
            if(!selectedCategories.includes(category.id)){
                if(!actualId){
                    actualId = category.id;
                    actualIndex = index;
                }
                categoriesHtml += '<option value="' + category.id + '">' + category.name  + '</option>';
            }
        });

        categories[actualIndex].subcategories.forEach(function(subcategory) {
            subcategoriesHtml += '<div class="px-5 py-2.5 bg-white border border-gray-600 rounded-md">';
                subcategoriesHtml += '<input class="mr-2" type="checkbox" name="subcategory" value="' + subcategory.id + '" />' + subcategory.name;
            subcategoriesHtml += '</div>';
        });

        let newElement = '<div class="col-span-5 select-category">';
            newElement += '<div class="mb-2 text-sm text-gray-600">Kategória</div>';
                newElement += '<select name="category" class="w-full rounded-md">';
                    newElement += categoriesHtml;
                newElement += '</select>';
            newElement += '</div>';
        newElement += '</div>';

        let newElement2 = '<div class="col-span-5 select-subcategory">';
        newElement2 += '<div class="mb-2 text-sm text-gray-600">Podkategória</div>';
            newElement2 += '<div class="grid grid-cols-2 gap-4 text-sm text-gray-600">';
                newElement2 += subcategoriesHtml;
            newElement2 += '</div>';
        newElement2 += '</div>';

        let newElement3 = '<div class="col-span-2 remove-category"><div class="pt-10"><a class="cursor-pointer profile-remove-category">Delete</a></div></div>';

        const newDiv = htmlToElement(newElement);
        const newDiv2 = htmlToElement(newElement2);
        const newDiv3 = htmlToElement(newElement3);
        categoriesWrapper.appendChild(newDiv);
        categoriesWrapper.appendChild(newDiv2);
        categoriesWrapper.appendChild(newDiv3);

        profileRemoveCategory(newDiv3.querySelector(".profile-remove-category"));
        profileChangeCategory(newDiv.querySelector("select"));
        regenerateCategoriesOptions();
        changeAddCategoryButton();

    })

})

up.compiler('#demand-modal-btn', function(element) {

    element.addEventListener("click", function(event){
        event.preventDefault();
        document.getElementById("demand-modal").classList.add("modal-open");
    })

})

up.compiler('#demand-modal-close-btn', function(element) {

    element.addEventListener("click", function(event){
        event.preventDefault();
        document.getElementById("demand-modal").classList.remove("modal-open");
    })

})

up.compiler('#demand-modal-submit-btn', function(element) {

    element.addEventListener("click", async function(event){
        event.preventDefault();

        let id = document.getElementById("cart-id-value-modal").value;
        let qty = document.getElementById("cart-qty-value-modal").value;
        let message = document.getElementById("cart-message-value-modal").value;

        console.log("#demand-modal-submit-btn", "click");

        try {
            let response = await up.request(rootUrl + '/form/product/demand', { 
                params: { 
                    id: id,
                    qty: qty,
                    message: message
                },
                method: 'POST',
                solo: true,
                cache: false,
            })

            const responseValue = JSON.parse(response.text);
            if(responseValue.status === 'ok'){
                up.render({ 
                    target: '.product-qty-selector', 
                    url: rootUrl + '/form/product/demand/success/qty',
                    params: {
                        id: id, 
                        qty: qty,
                        message: message
                    },
                    method: 'POST'
                });
                document.getElementById("demand-modal").classList.remove("modal-open");
            }

            up.render({
                target: '.product-demand-form', 
                url: rootUrl + '/form/product/demand/success/modal',
                params: {
                    id: id, 
                    qty: qty,
                    message: message
                },
                method: 'POST'
            });

        } catch (e) {
            if (e instanceof up.Response) {
                console.log('Server responded with HTTP status %s and text %s', e.status, e.text)
            } else {
                console.log('Fatal error during request:', e.message)
            }
        }

    })

})

up.compiler('#favorite-btn', function(element) {

    element.addEventListener("click", async function(event){
        event.preventDefault();

        let id = document.getElementById("product-id-value").value;

        try {
            let response = await up.request(rootUrl + '/form/product/favorite/' + id, { 
                method: 'GET',
                solo: true,
                cache: false,
            })

            const responseValue = JSON.parse(response.text);
            if(responseValue.status === 'ok'){
                up.reload('.product-favorite');
            }
            if(responseValue.status === 'logout'){
                up.navigate('body', { url: rootUrl + '/prihlasenie'});
            }

            up.cache.clear('/produkty');
            up.cache.clear('/produkt/*');
            up.cache.clear('/');
            up.cache.clear('/ucet*');

        } catch (e) {
            if (e instanceof up.Response) {
                console.log('Server responded with HTTP status %s and text %s', e.status, e.text)
            } else {
                console.log('Fatal error during request:', e.message)
            }
        }

    })

})

up.compiler('#farmer-favorite-btn', function(element) {

    element.addEventListener("click", async function(event){
        event.preventDefault();

        let id = document.getElementById("farmer-id-value").value;

        try {
            let response = await up.request(rootUrl + '/form/farmer/favorite/' + id, { 
                method: 'GET',
                solo: true,
                cache: false,
            })

            const responseValue = JSON.parse(response.text);
            if(responseValue.status === 'ok'){
                up.reload('.farmer-favorite, .favorite-farmers');
            }
            if(responseValue.status === 'logout'){
                up.navigate('body', { url: rootUrl + '/prihlasenie'});
            }

            up.cache.clear('/farmari');
            up.cache.clear('/farma/*');
            up.cache.clear('/');
            up.cache.clear('/ucet*');

        } catch (e) {
            if (e instanceof up.Response) {
                console.log('Server responded with HTTP status %s and text %s', e.status, e.text)
            } else {
                console.log('Fatal error during request:', e.message)
            }
        }

    })

})

up.compiler('.favorite-remove-btn', function(element) {

    element.addEventListener("click", async function(event){
        event.preventDefault();

        let id = element.getAttribute("data-id");
        up.render({ target: 'body', url: rootUrl + '/ucet/oblubene/remove/' +  id, solo: true });
        up.cache.clear('/produkty');
        up.cache.clear('/produkt/*');
        up.cache.clear('/');
        up.cache.clear('/ucet*');

    })

})

up.compiler('#newsletter-form', function(element) {
    element.addEventListener("submit", async function(event){

        event.preventDefault();

        let emailEl = document.getElementById("newsletter-email");
        let email = emailEl.value;

        try {
            let response = await up.request(rootUrl + '/form/newsletter', { 
                params: { 
                    email: email,
                },
                method: 'POST',
                solo: true,
                cache: false,
            })

            const responseValue = JSON.parse(response.text);
            if(responseValue.status === 'ok'){
                document.getElementById("newsletter-modal").classList.add("modal-open");
            }

            if(responseValue.status === 'exists'){
                document.getElementById("newsletter-exists-modal").classList.add("modal-open");
            }

            if(responseValue.status === 'exists' || responseValue.status === 'ok'){
                up.render({ 
                    target: '#newsletter-form-wrapper', 
                    url: rootUrl + '/fragment/newsletter',
                    method: 'POST',
                    params: { 
                        clear: true,
                    },
                });
            }

            if(responseValue.status === 'validationError'){
                up.render({ 
                    target: '#newsletter-form-wrapper', 
                    url: rootUrl + '/fragment/newsletter',
                    method: 'POST',
                    params: { 
                        email: email,
                    },
                });
            }

            } catch (e) {
            if (e instanceof up.Response) {
                console.log('Server responded with HTTP status %s and text %s', e.status, e.text)
            } else {
                console.log('Fatal error during request:', e.message)
            }
        }

    })
})

up.compiler('#newsletter-exists-modal-close-btn', function(element) {

    element.addEventListener("click", function(event){
        event.preventDefault();
        document.getElementById("newsletter-exists-modal").classList.remove("modal-open");
    })

})

up.compiler('#newsletter-modal-close-btn', function(element) {

    element.addEventListener("click", function(event){
        event.preventDefault();
        document.getElementById("newsletter-modal").classList.remove("modal-open");
    })

})

up.compiler('#category-select', function(element) {

    element.addEventListener("change", function(event){
        event.preventDefault();
        console.log("change event");

        let category = element.value;
        console.log('category', category);

        up.render({
            target: '#subcategory-select-wrapper', 
            url: rootUrl + '/fragment/profile/subcategory',
            params: {
                category: category
            },
            method: 'POST'
        });
    })

})

up.compiler('#uploadedPhotos', async function(element) {
    var sortable = Sortable.create(element, {
        animation: 150,
        draggable: ".item",
        onSort: async function (evt) {
            let uploadedPhotosValue = document.getElementById('uploadedPhotosValue');
            let uploadedPhotos = document.querySelectorAll('#uploadedPhotos .item');

            let values = [];
            for(let x=0;x<uploadedPhotos.length;x++){
                let id = uploadedPhotos[x].getAttribute('data-id');
                values.push(id);
            }
            uploadedPhotosValue.value = values.toString();

            let response = await up.request(rootUrl + '/ucet/produkt/zoradit-fotky', {
                params: { 
                    uploadedPhotosValue: uploadedPhotosValue.value,
                },
                method: 'POST',
                solo: true,
                cache: false,
            })
        },
    });
})

up.compiler('#addOfferDeleteAllPhotos', function(element) {

    element.addEventListener("click", function(event){
        event.preventDefault();

        uppy_create_offer.reset();
        up.render({ target: '#uploadedPhotos, #addOfferDeleteAllPhotosWrapper', url: rootUrl + '/ucet/produkt/odstranit-fotky', method: 'POST'});
    })
    
})

up.compiler('.profile-add-offer-remove-photo', function(element) {

    element.addEventListener("click", async function(event){
        event.preventDefault();

        let id = element.getAttribute("data-id");

        let response = await up.request(rootUrl + '/ucet/produkt/odstranit-fotku', {
            params: { 
                id: id,
            },
            method: 'POST',
            solo: true,
            cache: false,
        })

        up.render({ target: '#uploadedPhotos, #addOfferDeleteAllPhotosWrapper', url: rootUrl + '/ucet/pridat-ponuku', params: {
            reload: true
        } });

        uppy_create_offer.reset();
        
    })
    
})

up.compiler('.profile-delete-product', function(element) {

    element.addEventListener("click", async function(event){
        event.preventDefault();

        const id = element.getAttribute("data-id");
        document.querySelector(".delete-product-modal-" + id).classList.add("modal-open");

    })
    
})


up.compiler('#delete-product-modal-close-btn', function(element) {

    element.addEventListener("click", function(event){
        event.preventDefault();

        const id = element.getAttribute("data-id");
        document.querySelector(".delete-product-modal-" + id).classList.remove("modal-open");
    })

})

up.compiler('.profile-product-delete-confirm-btn', async function(element) {

    element.addEventListener("click", async function(event){
        event.preventDefault();

        const id = element.getAttribute("data-id");

        try {
            let response = await up.request(rootUrl + '/ucet/produkt/odstranit', { 
                params: { 
                    id: id,
                },
                method: 'POST',
                solo: true,
                cache: false,
            })

            const responseValue = JSON.parse(response.text);

            if(responseValue.status === 'ok'){

                up.render({ 
                    target: 'body',
                    url: rootUrl + '/ucet/moja-ponuka',
                });

                document.querySelector(".delete-product-modal-" + id).classList.remove("modal-open");
            }

        } catch (e) {
            if (e instanceof up.Response) {
                console.log('Server responded with HTTP status %s and text %s', e.status, e.text)
            } else {
                console.log('Fatal error during request:', e.message)
            }
        }

    })

})

up.compiler('.sharer', async function(element) {
    window.Sharer.init();
})

up.compiler('.date', async function(element) {
    const datepicker = new Datepicker(element, {
        language: 'sk',
        format: 'dd.mm.yyyy',
    }); 
})

up.compiler('#profile-generate-gps', async function(element) {

    element.addEventListener("click", async function(event){

        event.preventDefault();

        let street = document.getElementById('street').value;
        let houseno = document.getElementById('houseno').value;
        let city = document.getElementById('city').value;
        let zip = document.getElementById('zip').value;
    
        up.render({ 
            target: '#gps', 
            url: rootUrl + '/ucet/gps', 
            params: {
                street: street,
                houseno: houseno,
                city: city,
                zip: zip,
            }, 
            method: 'POST',
        });

    });

})

up.compiler('#unlimited', async function(element) {

    element.addEventListener("change", async function(event){

        event.preventDefault();

        let checked = this.checked;
        const start_date = document.querySelector('.start-date-wrapper');
        const end_date = document.querySelector('.end-date-wrapper');

        if(checked){
            start_date.classList.add('hidden');
            end_date.classList.add('hidden');
        }
        else{
            start_date.classList.remove('hidden');
            end_date.classList.remove('hidden');
        }

    });

})

up.compiler('#rating', async function(element) {

    console.log("#rating", element);

    let average = parseFloat(element.getAttribute("data-average"));
    let readonly = parseInt(element.getAttribute("data-readonly"));
    let id = parseInt(element.getAttribute("data-id"));
    let readonlyValue = false;

    if(readonly){
        readonlyValue = true;
    }

    let rating = raterJs({
		starSize: 20, 
        rating: average,
		element: element,
        readOnly: readonlyValue,
		rateCallback: async function (rating, done) {
            console.log('rating', rating);
			this.setRating(rating);
            this.disable();

            let response = await up.request(rootUrl + '/farma/hodnotit', {
                params: { 
                    rating: rating,
                    id: id,
                },
                method: 'POST',
                solo: true,
                cache: false,
            })
    
            up.render({ target: '#farmer_wrapper', url: rootUrl + '/farma/' + id, method: 'GET' });

			done(); 
		}
	});

})

up.compiler('.profile-delete-contact', async function(element) {

    element.addEventListener("click", async function(event){

        let id = parseInt(element.getAttribute("data-id"));
        console.log('id', id);

        try {
            let response = await up.request(rootUrl + '/ucet/kontakt/odstranit', { 
                params: { 
                    id: id,
                },
                method: 'POST',
                solo: true,
                cache: false,
            })

            const responseValue = JSON.parse(response.text);

            if(responseValue.status === 'ok'){

                up.render({ 
                    target: 'body',
                    url: rootUrl + '/ucet/kontakty',
                });

                // document.querySelector(".delete-product-modal-" + id).classList.remove("modal-open");
            }

        } catch (e) {
            if (e instanceof up.Response) {
                console.log('Server responded with HTTP status %s and text %s', e.status, e.text)
            } else {
                console.log('Fatal error during request:', e.message)
            }
        }

        

    });

})

up.compiler('.profile-add-contact', async function(element) {

    element.addEventListener("click", async function(event){

        let id = parseInt(element.getAttribute("data-id"));
        console.log('id', id);

        try {
            let response = await up.request(rootUrl + '/ucet/kontakt/pridat', { 
                params: { 
                    id: id,
                },
                method: 'POST',
                solo: true,
                cache: false,
            })

            const responseValue = JSON.parse(response.text);

            if(responseValue.status === 'ok'){

                up.render({ 
                    target: 'body',
                    url: rootUrl + '/ucet/kontakty/sledujuci-pouzivatelia',
                });

                // document.querySelector(".delete-product-modal-" + id).classList.remove("modal-open");
            }

        } catch (e) {
            if (e instanceof up.Response) {
                console.log('Server responded with HTTP status %s and text %s', e.status, e.text)
            } else {
                console.log('Fatal error during request:', e.message)
            }
        }

        

    });

})

up.compiler('#reg-accept-conditions', async function(element) {

    console.log(element);

    let regBtn = document.getElementById("reg-btn");

    element.addEventListener('change', (event) => {

        console.log('change');

        if (event.currentTarget.checked) {
            regBtn.removeAttribute('disabled');
        } else {
            regBtn.setAttribute('disabled', 'disabled');
        }
    })

})

up.compiler('#cookies-consent', async function(element) {

    let cookiesConsentAccepted = Cookies.get('cookiesConsentAccept');
    if(!cookiesConsentAccepted){
        element.classList.remove('hidden');
    }

})

up.compiler('#cookies-consent-accept', async function(element) {

    let cookiesConsent = document.getElementById("cookies-consent");
    element.addEventListener('click', function(){
        if(cookiesConsent){
            cookiesConsent.classList.add('hidden');
            Cookies.set('cookiesConsentAccept', 1);
        }
    });

})



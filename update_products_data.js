const fastifyEnv = require('fastify-env')
const app = require('fastify')({
  logger: true
})
const path = require('path')
const fs = require('fs')
const { v4: uuidv4 } = require('uuid')
const slugify = require('slugify')
const { Op, QueryTypes, Sequelize } = require("sequelize");
const mysql = require('mysql2');
const argon2 = require('argon2')
const v = require('voca')

// db 
const fsequelize = require('./src/plugins/cockroachdb')
const sequelizeConfig = {
  instance: 'db', // name of instance will be mapped to fastify
  // autoConnect: true, // auto authentication and test connection on first run
  dialect: 'postgres',
  host: 'localhost',
  port: '26257',
  username: 'root',
  password: '',
  database: 'odfarmara_imported',
  timezone: 'Europe/Bratislava',
  dialectOptions: {
    timezone: "local",
    // ssl: {
    //   rejectUnauthorized: false,
    //   // For secure connection:
    //   /*ca: fs.readFileSync('certs/ca.crt').toString()*/
    // },
  },
  // timout for long running queries
  pool: {
    max: 10,
    min: 0,
    idle: 100000
  },
  logging: false,
}

app.register(fsequelize, sequelizeConfig).ready()

app.register(require('./src/models/farmer_rating'))
app.register(require('./src/models/region'))
app.register(require('./src/models/favorite'))
app.register(require('./src/models/category'))
app.register(require('./src/models/user'))
app.register(require('./src/models/image'))
app.register(require('./src/models/tag'))
app.register(require('./src/models/product'))
app.register(require('./src/models/product_view'))
app.register(require('./src/models/article'))
app.register(require('./src/models/farmer_image'))
app.register(require('./src/models/newsletter'))
app.register(require('./src/models/opening_hours'))
app.register(require('./src/models/demand'))
app.register(require('./src/models/inbox'))
app.register(require('./src/models/recipe'))
app.register(require('./src/models/allergen'))
app.register(require('./src/models/nutrition_value'))
app.register(require('./src/models/cities'))
app.register(require('./src/models/seasonal_offer'))
app.register(require('./src/models/flags'))

async function start() {

  //const odfarmara_old = new Sequelize('mysql://root:@localhost:3306/odfarmara_old')
  //const [results, metadata] = await Sequelize.query("SELECT * FROM users");

  // create the connection to database
  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    database: 'odfarmara_old'
  });

  connection.query(
    'SELECT * FROM offers',
    async function (err, results, fields) {

      for (var x = 0; x < results.length; x++) {

        let item = results[x];

        let id = item.id;
        let user_id = item.user_id;
        let category_id = item.category_id;
        let subcategory_id = item.subcategory_id;
        let title = item.title;
        let active = item.enabled;
        let created = item.created;
        let modified = item.modified;
        let description = item.description;
        if (active) {
          active = "1";
        }
        else {
          active = "0";
        }
        let region_id = item.region_id;
        let price = item.price;
        let price_for = item.price_for;
        let start_date = item.start_date;
        let end_date = item.end_date;
        let unit = item.unit;
        let quantity = item.quantity;

        let data = {
          old_id: id,
          user_id: user_id,
          category_id: category_id,
          subcategory_id: subcategory_id,
          title: title,
          description: description,
          active: active,
          created: created,
          modified: modified,
          region_id: region_id,
          price: price,
          price_for: price_for,
          start_date: start_date,
          end_date: end_date,
          unit: unit,
          quantity: quantity,
          min_quantity: 1
        }

        let newProduct = await app.db.models.product.create(data);

        console.log(id);

      }

    }

  );



  //console.log('results', results);

  // let items = await app.db.models.product.findAll();

  // for(x in items){

  //     let id = items[x].old_id;
  //     let uuid = uuidv4();
  //     let title = items[x].title;
  //     let slug = slugify(title, {
  //       lower: true,      // convert to lower case, defaults to `false`
  //       locale: 'sk'       // language code of the locale to use
  //     });

  //     let user = await app.db.models.product.update({ 
  //       uuid: uuid,
  //       slug: slug,
  //     }, {
  //         where: {
  //           old_id: id
  //         }
  //     });

  // }
}

app.ready((err) => {
  start();
})


